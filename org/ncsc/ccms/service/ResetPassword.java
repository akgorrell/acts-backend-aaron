package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class ResetPassword extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public ResetPassword()
	{
		super();
		ResetPassword.logger = LogManager.getLogger(ResetPassword.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		ServletContext context = getServletContext();
		String resetToken = null;

		BufferedReader reader = request.getReader();
		String clientJSON = CCMSDomainUtilities.getInstance().fetchJSONString(
				reader);
		logger.debug("Input JSON: " + clientJSON);

		try
		{
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			String userName = (String) jsonObject.get("userName");
			String password = (String) jsonObject.get("password");
			String token = (String) jsonObject.get("token");
			Connection conn = CCMSDomainUtilities.getInstance().connect();

			CCMSParty tokenParty = CCMSDomainUtilities.getInstance()
					.parseResetToken(token);
			if (tokenParty.getUserName().equals(userName))
			{
				// Set the user password to the temporary password
				PreparedStatement updatePwdStmt = tokenParty.genPasswordReset(
						conn, password);
				updatePwdStmt.execute();
				conn.commit();

				ResultSet rs = tokenParty.genSelectSQL(conn).get(0)
						.executeQuery();
				while (rs.next())
				{
					tokenParty = new CCMSParty(rs, conn, true);
				}
			}
			else
			{
				logger.error("Not authorized reset");
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"User is not authorized");
			}

			Gson myGson = new Gson();
			String json = myGson.toJson(tokenParty);
			logger.debug("Returned JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

			// TODO: Ask if we should redirect on successful reset
			//RequestDispatcher dispatcher = context
			//		.getRequestDispatcher(RESET_PWD_URL);
			// dispatcher.forward(request, response);
		}
		catch (Exception ex)
		{
			logger.error(ex);
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
