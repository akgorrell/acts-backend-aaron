package org.ncsc.ccms.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSStaffRole;

import com.google.gson.Gson;

public class FetchJudicialOfficer extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchJudicialOfficer()
	{
		super();
		FetchJudicialOfficer.logger = LogManager
				.getLogger(FetchJudicialOfficer.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public static void main(String[] args)
	{

		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			List<CCMSParty> judicialOfficers = FetchJudicialOfficer.retrieveJudicialOfficers(conn, court);
			System.out.println("judicialOfficers");

			

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);
			
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			List<CCMSParty> judicialOfficers = new ArrayList<CCMSParty>();

			Gson myGson = new Gson();
			judicialOfficers = this.retrieveJudicialOfficers(conn, court);

			String json = myGson.toJson(judicialOfficers);

			logger.debug("SERVLET RESPONSE:" + json);

			response.setContentType("application/json");

			response.setCharacterEncoding("UTF-8");

			response.getWriter().write(json);
		}
		catch (Exception e)
		{
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}

	}
	
	public static List<CCMSParty> retrieveJudicialOfficers(Connection conn, CCMSCourt court) throws Exception
	{
		List<CCMSParty> judicialOfficers = new ArrayList<CCMSParty>();
		CCMSParty judParty = new CCMSParty();
		judParty.setCourtUser(true);
		
		// Fetch all parties
		PreparedStatement pStmt = judParty.genSelectSQL(conn).get(0);
		ResultSet rs = pStmt.executeQuery();
		while (rs.next())
		{
			// Add the party to the returned list only if they have a role
			// that is that of a judicial officer
			CCMSParty dbParty = new CCMSParty(rs, conn, true);
			for (CCMSStaffRole role : dbParty.getRoles())
			{
				if (role.isJudicialOfficer() && role.getCourtOID() == court.getCourtOID() )
				{
					judicialOfficers.add(dbParty);
					break;
				}
			}
		}
		return judicialOfficers;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
