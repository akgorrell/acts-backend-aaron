package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSPartyAddress;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseCharge;
import org.ncsc.ccms.domain.CCMSCaseEvent;
import org.ncsc.ccms.domain.CCMSCaseParty;
import org.ncsc.ccms.domain.CCMSCasePartyRole;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCaseStatus;
import org.ncsc.ccms.domain.CCMSCaseType;
import org.ncsc.ccms.domain.CCMSChargeFactor;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSException;
import org.ncsc.ccms.domain.CCMSICCSLocalCharge;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPermission;

import com.google.gson.Gson;

public class SaveCourtCase extends HttpServlet
{
	private static Logger logger = null;
	private static String UPDATE_CASE_EVENT_TYPE_NAME = "UPTD";
	private static String NEW_CASE_EVENT_TYPE_NAME = "CRTD";

	/**
	 * Constructor of the object.
	 */
	public SaveCourtCase()
	{
		super();
		SaveCourtCase.logger = LogManager.getLogger(SaveCourtCase.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Gson myGson = new Gson();
		String responseMessage = "";
		CCMSCase newCase = null;
		Connection conn = null;
		String token = request.getHeader("token");

		CCMSParty actionParty = (CCMSParty) request.getSession().getAttribute(
				"user");

		if (actionParty == null)
		{
			response.sendError(HttpServletResponse.SC_FORBIDDEN,
					"Invalid User");
		}

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			newCase = new CCMSCase(0, court);

			translateJSONToCaseObject(clientJSON, newCase, court);

			// Update court case
			if (newCase.getCaseOID() != 0L)
			{
				if (!actionParty.isAuthorized(CCMSPermission.UPDATE_CASE, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to update case.");
				}
				else
				{
					// Only check for changes if this is an update
					CCMSCaseEvent updatedCaseEvent = createUpdatedCaseFilingEvents(
							newCase, conn, actionParty);

					List<PreparedStatement> updateStmts = newCase
							.genUpdateSQL(conn);
					for (PreparedStatement updateStmt : updateStmts)
					{
						updateStmt.execute();
						updateStmt.close();
					}

					if (updatedCaseEvent != null)
					{
						// Insert the case event
						updatedCaseEvent.genInsertSQL(conn).get(0).execute();

						// Trigger any workflows
						CCMSDomainUtilities.getInstance().triggerWorkflow(
								updatedCaseEvent, court, conn);
					}

					conn.commit();
				}
			}
			else
			{
				if (!actionParty.isAuthorized(CCMSPermission.CREATE_CASE, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to create new case.");
				}
				else
				{
					// NEW CASE, set the case oid and insert
					newCase.setCaseOID(CCMSDomainUtilities.getInstance()
							.genOID());
					String caseNumber = newCase.calculateCaseNumber(conn);
					newCase.setCaseNumber(caseNumber);

					// Save the Case
					List<PreparedStatement> pStmts = newCase.genInsertSQL(conn);
					for (PreparedStatement pStmt : pStmts)
					{
						pStmt.execute();
						pStmt.close();
					}

					// Create the case event registry
					CCMSCaseEvent newCaseEvent = createNewCaseFilingEvents(
							newCase, conn, actionParty);
					if (newCaseEvent != null)
					{
						// Insert the case event
						newCaseEvent.genInsertSQL(conn).get(0).execute();

						// Trigger any workflows
						CCMSDomainUtilities.getInstance().triggerWorkflow(
								newCaseEvent, court, conn);
					}

					conn.commit();
				}

			}

			List<CCMSCase> cases = new ArrayList<CCMSCase>();

			// Grab the updated case
			List<PreparedStatement> pStmts = newCase.genSelectSQL(conn);
			logger.debug("Query Statements: " + pStmts.size());
			for (PreparedStatement pStmt : pStmts)
			{
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					CCMSCase dbCase = new CCMSCase(rs, conn, court, false);
					cases.add(dbCase);
					logger.debug("Matching Case: " + dbCase);
				}
				pStmt.close();
				rs.close();
			}

			// cases.add(newCase);

			responseMessage = myGson.toJson(cases);

			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseMessage);
		}
		catch (Exception e)
		{
			logger.error(e);
			;
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}

	}

	private CCMSCaseEvent createNewCaseFilingEvents(CCMSCase newCase,
			Connection conn, CCMSParty actionParty) throws Exception
	{
		CCMSCaseEvent caseEvent = null;
		// Grab the event type for update UPTD
		CCMSEventType newEventType = new CCMSEventType(
				SaveCourtCase.NEW_CASE_EVENT_TYPE_NAME);
		newEventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(newEventType, newCase.getCourt());

		// Notify logger of error if cannot retrieve appropriate event type
		if (newEventType == null)
		{
			logger.error("Unable to retrieve NEW event type ["
					+ SaveCourtCase.NEW_CASE_EVENT_TYPE_NAME
					+ "] from database. Likely cause is that the NEW event type name has changed");
		}
		else
		{
			String description = "Filing Date: " + newCase.getCreateDate()
					+ " Case Type: " + newCase.getCaseType().getName()
					+ " Case Phase: " + newCase.getCasePhase().getName()
					+ " Case Status: " + newCase.getCaseStatus().getName();
			caseEvent = new CCMSCaseEvent(CCMSDomainUtilities.getInstance()
					.genOID(), newCase.getCourt().getCourtOID());
			caseEvent.setEventType(newEventType);
			caseEvent.setCaseOID(newCase.getCaseOID());
			caseEvent.setDescription(description);
			caseEvent.setEnteringParty(actionParty);
			caseEvent
					.setEventDateTime(new Timestamp(System.currentTimeMillis()));
		}

		return caseEvent;
	}

	private CCMSCaseEvent createUpdatedCaseFilingEvents(CCMSCase updatedCase,
			Connection conn, CCMSParty updateParty) throws Exception
	{
		CCMSCaseEvent event = null;
		// Retrieve the current database
		CCMSCase originalCase = new CCMSCase(updatedCase.getCaseOID(),
				updatedCase.getCourt());
		PreparedStatement stmt = originalCase.genSelectSQL(conn).get(0);
		ResultSet rs = stmt.executeQuery();
		while (rs.next())
		{
			originalCase = new CCMSCase(rs, conn, originalCase.getCourt(),
					false);
		}
		stmt.close();
		rs.close();

		// Grab the event type for update UPTD
		CCMSEventType updateEventType = new CCMSEventType(
				SaveCourtCase.UPDATE_CASE_EVENT_TYPE_NAME);
		updateEventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(updateEventType, updatedCase.getCourt());

		// Notify logger of error if cannot retrieve appropriate event type
		if (updateEventType == null)
		{
			logger.error("Unable to retrieve update event type ["
					+ SaveCourtCase.UPDATE_CASE_EVENT_TYPE_NAME
					+ "] from database. Likely cause is that the update event type name has changed");
		}
		else
		{
			String changeDescription = updatedCase.getCaseChangeDescription(
					originalCase, conn);

			event = new CCMSCaseEvent(CCMSDomainUtilities.getInstance()
					.genOID(), updatedCase.getCourt().getCourtOID());
			event.setEventType(updateEventType);
			event.setCaseOID(updatedCase.getCaseOID());
			event.setEventDateTime(new Timestamp(System.currentTimeMillis()));
			event.setDescription(changeDescription);
			event.setEnteringParty(updateParty);
		}

		return event;

	}

	public static void main(String[] args)
	{
		SaveCourtCase saveCase = new SaveCourtCase();
		// String json =
		// "{\"caseFilingDate\":\"2017-3-18\",\"caseType\":\"2\",\"caseStatus\":\"1\",\"casePhase\":\"85\",\"caseWeight\":null,\"caseParties\":[{\"partyRoleOID\":\"6\",\"partyOID\":\"2\",\"startDate\":\"2016-01-01\"}],\"caseCharges\":[{\"iccsCodeOID\":\"1\",\"lea\":\"asdf\"}]}";

		String json = "{\"caseFilingDate\":\"2017-3-25\",\"caseType\":\"15\",\"caseStatus\":\"2\",\"casePhase\":\"92\",\"caseWeight\":\"0\",\"caseParties\":[{\"partyRoleOID\":\"6\",\"partyOID\":\"8063544002074348\",\"startDate\":\"2017-3-24\",\"endDate\":null}],\"caseCharges\":[{\"iccsCodeOID\":\"1\",\"lea\":\"asdf\",\"factors\":[\"4\"]}]}";
		System.out.println(json);
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			court.setLocationCode("N");
			;
			CCMSCase newCase = new CCMSCase(CCMSDomainUtilities.getInstance()
					.genOID(), court);
			saveCase.translateJSONToCaseObject(json, newCase, court);
			newCase.setCaseNumber(newCase.calculateCaseNumber(conn));

			List<PreparedStatement> pStmts = newCase.genInsertSQL(conn);
			for (PreparedStatement pStmt : pStmts)
			{
				pStmt.execute();
				pStmt.close();
			}

			conn.commit();

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CCMSCase translateJSONToCaseObject(String caseJSON,
			CCMSCase newCase, CCMSCourt court) throws Exception

	{
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(caseJSON);

		if (jsonCaseObject.containsKey("caseOID"))
		{
			String caseOIDString = (String) jsonCaseObject.get("caseOID");
			newCase.setCaseOID(Long.parseLong(caseOIDString));
			logger.debug("Updating Case: " + newCase);
		}

		String filingDateString = (String) jsonCaseObject.get("caseFilingDate");
		if (filingDateString != null)
		{
			Date filingDate = CCMSDomainUtilities.getInstance()
					.convertStringToDate(filingDateString);
			newCase.setCreateDate(filingDate);
		}
		
		String caseCaptionString = (String) jsonCaseObject.get("caseCaption");
		if (caseCaptionString != null)
		{
			newCase.setCaseCaption(caseCaptionString);
		}

		String caseTypeString = (String) jsonCaseObject.get("caseType");
		if (caseTypeString != null)
		{
			CCMSCaseType caseType = new CCMSCaseType(
					Long.parseLong(caseTypeString), newCase.getCourt()
							.getCourtOID());
			// Retrieve the full object from memory
			caseType = (CCMSCaseType) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(caseType, newCase.getCourt());
			logger.debug("Full Case Type: " + caseType);
			newCase.setCaseType(caseType);
		}

		String caseStatusString = (String) jsonCaseObject.get("caseStatus");
		if (caseStatusString != null)
		{
			CCMSCaseStatus caseStatus = new CCMSCaseStatus(
					Long.parseLong(caseStatusString), newCase.getCourt()
							.getCourtOID());
			caseStatus = (CCMSCaseStatus) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(caseStatus, court);
			newCase.setCaseStatus(caseStatus);
		}

		String casePhaseString = (String) jsonCaseObject.get("casePhase");
		if (casePhaseString != null)
		{
			CCMSCasePhase casePhase = new CCMSCasePhase(
					Long.parseLong(casePhaseString), newCase.getCourt()
							.getCourtOID());
			casePhase = (CCMSCasePhase) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(casePhase, court);
			newCase.setCasePhase(casePhase);
		}

		String caseWeight = (String) jsonCaseObject.get("caseWeight");
		if (caseWeight != null)
		{
			newCase.setCaseWeight(Integer.parseInt(caseWeight));
		}

		JSONArray casePartiesArray = (JSONArray) jsonCaseObject
				.get("caseParties");
		Iterator<JSONObject> casePartiesIterator = casePartiesArray.iterator();
		while (casePartiesIterator.hasNext())
		{
			CCMSCaseParty caseParty = new CCMSCaseParty();
			CCMSParty party = new CCMSParty();
			JSONObject jsonPartyObject = (JSONObject) casePartiesIterator
					.next();
			String partyOIDString = (String) jsonPartyObject.get("partyOID");
			party.setPartyOID(Long.parseLong(partyOIDString));
			caseParty.setParty(party);

			String partyRoleOIDString = (String) jsonPartyObject
					.get("partyRoleOID");
			if (partyRoleOIDString != null)
			{
				CCMSCasePartyRole casePartyRole = new CCMSCasePartyRole(
						Long.parseLong(partyRoleOIDString), newCase.getCourt()
								.getCourtOID());
				caseParty.setRole(casePartyRole);
			}

			String startDate = (String) jsonPartyObject.get("startDate");
			if (startDate != null && !startDate.isEmpty())
			{
				caseParty.setStartDate(CCMSDomainUtilities.getInstance()
						.convertStringToDate(startDate));
			}

			String endDate = (String) jsonPartyObject.get("endDate");
			if (endDate != null && !endDate.isEmpty())
			{
				caseParty.setEndDate(CCMSDomainUtilities.getInstance()
						.convertStringToDate(endDate));
			}

			newCase.getCaseParties().add(caseParty);
		}

		JSONArray caseChargesArray = (JSONArray) jsonCaseObject
				.get("caseCharges");
		Iterator<JSONObject> caseChargesIterator = caseChargesArray.iterator();
		while (caseChargesIterator.hasNext())
		{
			CCMSCaseCharge newCharge = new CCMSCaseCharge(CCMSDomainUtilities
					.getInstance().genOID(), newCase.getCourt().getCourtOID());
			JSONObject jsonChargeObject = (JSONObject) caseChargesIterator
					.next();

			String iccsCodeOID = (String) jsonChargeObject.get("iccsCodeOID");
			newCharge.setIccsChargeCategoryOID(Long.parseLong(iccsCodeOID));

			String leaChargingDetails = (String) jsonChargeObject.get("lea");
			newCharge.setLeaChargingDetails(leaChargingDetails);
			newCase.getCaseCharges().add(newCharge);

			String localChargeOIDString = (String) jsonChargeObject
					.get("localChargeOID");
			if (localChargeOIDString != null && !localChargeOIDString.isEmpty())
			{
				CCMSICCSLocalCharge localCharge = new CCMSICCSLocalCharge(
						Long.parseLong(localChargeOIDString), newCase
								.getCourt().getCourtOID());
				newCharge.setLocalCharge(localCharge);
			}

			String startDateString = (String) jsonChargeObject.get("startDate");
			if (startDateString != null)
			{
				Date startDateTime = CCMSDomainUtilities.getInstance()
						.convertStringToDate(startDateString);
				newCharge.setStartDate(startDateTime);
			}

			String endDateString = (String) jsonChargeObject.get("endDate");
			if (endDateString != null)
			{
				Date endDateTime = CCMSDomainUtilities.getInstance()
						.convertStringToDate(endDateString);
				newCharge.setEndDate(endDateTime);
			}

			// Charge Factors
			JSONArray chargeFactorArray = (JSONArray) jsonChargeObject
					.get("factors");
			for (Object jsonObj : chargeFactorArray)
			{
				String chargeFactorOID = (String) jsonObj;

				CCMSChargeFactor newFactor = new CCMSChargeFactor(0L,
						newCharge.getCourtOID());
				newFactor.setChargeFactorOID(Long.parseLong(chargeFactorOID));
				newCharge.getChargeFactors().add(newFactor);
			}
		}
		return newCase;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}
}