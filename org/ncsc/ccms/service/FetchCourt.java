package org.ncsc.ccms.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;

import com.google.gson.Gson;

public class FetchCourt extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchCourt()
	{
		super();
		FetchCourt.logger = LogManager.getLogger(FetchCourt.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		List<CCMSCourt> dbCourts = new ArrayList<CCMSCourt>();
		CCMSCourt court = (CCMSCourt) request.getSession()
				.getAttribute("court");
		
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt qCourt = new CCMSCourt();
			PreparedStatement stmt = qCourt.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while ( rs.next())
			{
				dbCourts.add(new CCMSCourt(rs, conn));
			}
			rs.close();
			stmt.close();
		}
		catch (Exception e)
		{
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
		

		Gson myGson = new Gson();

		String json = myGson.toJson(dbCourts);
		
		logger.debug("SERVLET RESPONSE:" + json);

		response.setContentType("application/json");

		response.setCharacterEncoding("UTF-8");

		response.getWriter().write(json);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}
	
	public static void main(String[] args)
	{

		List<CCMSCourt> dbCourts = new ArrayList<CCMSCourt>();
		
		
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt qCourt = new CCMSCourt();
			PreparedStatement stmt = qCourt.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while ( rs.next())
			{
				dbCourts.add(new CCMSCourt(rs, conn));
			}
		}
		catch (Exception e)
		{
			logger.error(e);
		}
		
		System.out.println("courts: " + dbCourts);
	}

}
