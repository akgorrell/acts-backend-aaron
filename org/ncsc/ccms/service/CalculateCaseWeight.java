package org.ncsc.ccms.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseCharge;
import org.ncsc.ccms.domain.CCMSCaseParty;
import org.ncsc.ccms.domain.CCMSCasePartyRole;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSICCSCode;
import org.ncsc.ccms.domain.CCMSParty;

public class CalculateCaseWeight
{
	private static int maxCodefendant = 15;
	private static int maxChargeCount = 20;
	private static int maxChargeSeverity = 40;
	private static int maxInterpreter = 15;

	private static int pointsPerCodefendant = 5;
	private static int pointsIfInterpreter = 15;
	private static float reducedPointsPerSection = 1.67F;
	private static int pointsPerCaseCharge = 20;

	public static void main(String[] args)
	{

		try
		{
			long startTime = System.currentTimeMillis();
			// Retrieve case 30
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			CCMSCase qCase = new CCMSCase(0L, court);
			qCase.setCaseNumber("C-N-CR-99-2017-1");
			PreparedStatement pStmt = qCase.genSelectSQL(conn).get(0);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSCase newCase = new CCMSCase(rs, conn, court, false);
				// System.out.println("["+newCase.getCaseNumberBase()+"]");

				CalculateCaseWeight calcWeight = new CalculateCaseWeight();
				System.out.println("Weight: "
						+ calcWeight.calculateCaseWeight(newCase, conn));

			}
			System.out.println("Elapsed Time: "
					+ ((System.currentTimeMillis() - startTime) / 1000));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public int calculateCaseWeight(CCMSCase courtCase, Connection conn)
			throws Exception
	{
		int caseWeight = 0;

		// Calculate codefendant score

		// Identify associated cases
		List<CCMSCase> associatedCases = courtCase.getAssociatedCases(conn);
		int coDefendantScore = CalculateCaseWeight.pointsPerCodefendant
				* associatedCases.size();
		if (coDefendantScore > CalculateCaseWeight.maxCodefendant)
		{
			coDefendantScore = maxCodefendant;
		}

		// Calculate charge count score
		int chargeCountScore = courtCase.getCaseCharges().size()
				* CalculateCaseWeight.pointsPerCaseCharge;
		if (chargeCountScore > CalculateCaseWeight.maxChargeCount)
		{
			chargeCountScore = CalculateCaseWeight.maxChargeCount;
		}

		// Calculate charge type
		// Grab the first charge which is the highest charge
		float chargeSeverityScore = 0F;
		if (!courtCase.getCaseCharges().isEmpty())
		{
			CCMSCaseCharge highestCharge = courtCase.getCaseCharges().get(0);
			CCMSICCSCode iccsCode = new CCMSICCSCode(
					highestCharge.getIccsChargeCategoryOID(), courtCase
							.getCourt().getCourtOID());
			// Retrieve the ICCS Code
			iccsCode = (CCMSICCSCode) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(iccsCode, courtCase.getCourt());

			String sectionIDString = iccsCode.getCategoryIdentifier()
					.substring(0, 2);
			int sectionIDInt = Integer.parseInt(sectionIDString);
			chargeSeverityScore = maxChargeSeverity
					- (reducedPointsPerSection * sectionIDInt);
		}

		// Case Party Interpreter Score
		int interpreterScore = 0;
		for (CCMSCaseParty caseParty : courtCase.getCaseParties())
		{
			CCMSCasePartyRole role = caseParty.getRole();
			role = (CCMSCasePartyRole) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(role, courtCase.getCourt());

			// Get full party

			PreparedStatement stmt = caseParty.getCaseParty()
					.genSelectSQL(conn).get(0);

			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				caseParty.setCaseParty(new CCMSParty(rs, conn, false));
			}

			if (role.isCodefendantIndicator()
					&& caseParty.getParty().isInterpreterRequiredIndicator())
			{
				interpreterScore = CalculateCaseWeight.maxInterpreter;
			}
		}

		caseWeight = coDefendantScore + chargeCountScore
				+ new Float(chargeSeverityScore).intValue() + interpreterScore;

		// Reduce score to fit on a scale from 1-10
		caseWeight = caseWeight / 10;

		return caseWeight;
	}
}
