package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.text.RandomStringGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;
import com.sendgrid.SendGrid;

public class RequestPasswordReset extends HttpServlet
{
	private static Logger logger = null;
	private String RESET_PWD_URL = "/#/reset-password";

	/**
	 * Constructor of the object.
	 */
	public RequestPasswordReset()
	{
		super();
		RequestPasswordReset.logger = LogManager
				.getLogger(RequestPasswordReset.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		doPost(request, response);

	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		ServletContext context = getServletContext();
		String resetToken = null;
		String serverName = request.getServerName();

		BufferedReader reader = request.getReader();
		String clientJSON = CCMSDomainUtilities.getInstance().fetchJSONString(
				reader);
		logger.debug("Input JSON: " + clientJSON);

		try
		{
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			String userName = (String) jsonObject.get("userName");
			Connection conn = CCMSDomainUtilities.getInstance().connect();

			// Get the Party associated with the user name
			CCMSParty qParty = new CCMSParty();
			CCMSParty dbParty = null;
			qParty.setUserName(userName);
			ResultSet rs = qParty.genSelectSQL(conn).get(0).executeQuery();
			while (rs.next())
			{
				dbParty = new CCMSParty(rs, conn, false);
			}

			// If cannot retrieve a party with that user id, then return
			// unauthorized error
			if (dbParty == null)
			{
				logger.error("Not authorized reset");
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"User is not authorized");
			}
			else
			{
				
				
				// Must have an email on file to reset the password
				if (dbParty.getEmails().size() > 0)
				{

					resetToken = CCMSDomainUtilities.getInstance()
							.createResetToken(userName);
					
					// Format for email
					// https://[ccms-domain]/#/reset-password?token=[token-here]
					String resetURL = "https://" +  serverName + "/#/reset-password?token=" + resetToken;
					
					// Per Jeff on 4/19, do not return token because would allow anyone to reset a password of a user
					//qParty.setToken(resetToken);

					RandomStringGenerator generator = new RandomStringGenerator.Builder()
							.withinRange('a', 'z').build();
					String tempPassword = generator.generate(8);

					// Set the user password to the temporary password
					PreparedStatement updatePwdStmt = dbParty.genPasswordReset(
							conn, tempPassword);
					updatePwdStmt.execute();
					conn.commit();

					// send Email to the user
					this.sendPasswordResetEmail(dbParty.getEmails().get(0)
							.getEmailAddress(), tempPassword, resetURL);

					Gson myGson = new Gson();
					String json = myGson.toJson(qParty);
					logger.debug("Returned JSON: " + json);
					response.setContentType("application/json");
					response.setCharacterEncoding("UTF-8");
					response.getWriter().write(json);

					//RequestDispatcher dispatcher = context
					//		.getRequestDispatcher(RESET_PWD_URL);
					//dispatcher.forward(request, response);
				}
				else
				{
					logger.error("Not authorized reset");
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
							"No user email on record");
				}
			}
		}

		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					"Error on setting or sending temp password");
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	private void sendPasswordResetEmail(String userEmail, String password, String resetURL)
			throws Exception
	{
		SendGrid sendGridFactory = CCMSDomainUtilities.getInstance()
				.getSendGridFactory();
		SendGrid.Email email = new SendGrid.Email();

		email.addTo(userEmail);
		email.setFrom("do-not-reply@youremail.com");
		email.setSubject("CCMS Password Reset");
		email.setHtml("Someone has requested a password reset for your CCMS Account. Please use the code "
				+ password + " at the <a href='" + resetURL + "'>Password Reset Page</a>");

		SendGrid.Response response = sendGridFactory.send(email);
	}

}
