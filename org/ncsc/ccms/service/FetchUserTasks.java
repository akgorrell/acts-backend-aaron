package org.ncsc.ccms.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchUserTasks extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchUserTasks()
	{
		super();
		FetchUserTasks.logger = LogManager.getLogger(FetchUserTasks.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		String json = "";
		String token = request.getHeader("token");
		List<CCMSCaseTask> tasks = null;

		try
		{
			Gson myGson = new Gson();
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			String auth = request.getHeader("Authorization");

			if (auth != null && court != null)
			{
				CCMSParty authParty = CCMSDomainUtilities.getInstance()
						.parseLoginToken(auth);

				if (authParty != null)
				{
					tasks = this.retrieveIndividualAssignedTasks(authParty,
							court);
					
					// Retrieve all tasks including pool assigned tasks in the SQL so this likely doesn't need to be called
					//tasks.addAll(retrievePoolAssignedTasks(authParty, court));
					json = myGson.toJson(tasks);

				}

				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
				
			}
			else
			{
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
						"Token Expired");
			}
		}
		catch (Exception e)
		{
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
		
	}

	private List<CCMSCaseTask> retrieveIndividualAssignedTasks(
			CCMSParty authParty, CCMSCourt court) throws Exception
	{
		Connection conn = CCMSDomainUtilities.getInstance().connect();
		List<CCMSCaseTask> tasks = new ArrayList<CCMSCaseTask>();

		// Retrieve Tasks associated with the case party
		PreparedStatement pStmt = conn
				.prepareStatement(CCMSCaseTask.FETCH_USER_TASK_SQL);
		pStmt.setString(1, authParty.getUserName());
		pStmt.setLong(2, court.getCourtOID());

		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			tasks.add(new CCMSCaseTask(rs1, conn, court, false));
		}
		rs1.close();
		pStmt.close();

		return tasks;
	}

	private List<CCMSCaseTask> retrievePoolAssignedTasks(CCMSParty authParty,
			CCMSCourt court) throws Exception
	{
		Connection conn = CCMSDomainUtilities.getInstance().connect();
		List<CCMSCaseTask> tasks = new ArrayList<CCMSCaseTask>();

		// Retrieve Tasks associated with the case party
		String sqlString = " SELECT CASE_TASK.* FROM NJCCCMS.CASE_TASK, NJCCCMS.PARTY, NJCCCMS.STAFF_POOL_REL "
				+ " WHERE PARTY.ID = STAFF_POOL_REL.PARTY_ID "
				+ "   AND STAFF_POOL_REL.POOL_ID = CASE_TASK.ASSIGNED_POOL_ID "
				+ "   AND PARTY.USER_NAME = ? ";
		PreparedStatement pStmt = conn
				.prepareStatement(CCMSCaseTask.FETCH_POOL_TASK_SQL);
		pStmt.setString(1, authParty.getUserName());
		pStmt.setLong(2, court.getCourtOID());

		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			tasks.add(new CCMSCaseTask(rs1, conn, court, false));
		}
		rs1.close();
		pStmt.close();

		return tasks;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			FetchUserTasks fetch = new FetchUserTasks();
			CCMSParty authParty = new CCMSParty();
			authParty.setUserName("akgorrell");
			List<CCMSCaseTask> tasks = fetch.retrieveIndividualAssignedTasks(
					authParty, court);
			tasks.addAll(fetch.retrievePoolAssignedTasks(authParty, court));

			Gson myGson = new Gson();
			System.out.println(myGson.toJson(tasks));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
