package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSEventWorkflow;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchEventWorkflow extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchEventWorkflow()
	{
		super();
		FetchEventWorkflow.logger = LogManager
				.getLogger(FetchEventWorkflow.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;
		CCMSEventWorkflow workflow = null;
		JSONParser parser = new JSONParser();
		String token = request.getHeader("token");
		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance().getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			logger.debug("Input JSON: " + clientJSON);

			String eventTypeOIDString = (String) jsonObject.get("eventTypeOID");

			workflow = new CCMSEventWorkflow(0L, court);
			workflow.setTriggeringEvent(new CCMSEventType(Long
					.parseLong(eventTypeOIDString), court.getCourtOID()));
			PreparedStatement stmt = workflow.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				workflow = new CCMSEventWorkflow(rs, conn, court);
			}
			
			Gson myGson = new Gson();
			String json = myGson.toJson(workflow);
			logger.debug("SaveCaseType Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSEventWorkflow workflow = new CCMSEventWorkflow(2, court);
			/*
			 * PreparedStatement stmt = workflow.genSelectSQL(conn).get(0);
			 * ResultSet rs = stmt.executeQuery(); while ( rs.next() ) {
			 * CCMSEventWorkflow wk = new CCMSEventWorkflow(rs, conn, court);
			 * System.out.println(wk); }
			 */

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
