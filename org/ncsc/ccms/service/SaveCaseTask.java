package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseCharge;
import org.ncsc.ccms.domain.CCMSCasePartyRole;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCaseStatus;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCaseType;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDocumentTemplate;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSException;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPermission;
import org.ncsc.ccms.domain.CCMSStaffPool;
import org.ncsc.ccms.domain.CCMSTaskType;

import com.google.gson.Gson;

public class SaveCaseTask extends HttpServlet
{

	private static Logger scLogger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveCaseTask()
	{
		super();
		SaveCaseTask.scLogger = LogManager.getLogger(SaveCaseTask.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			// Case Add
			CCMSCaseTask task = new CCMSCaseTask(0L, 5);

			String clientJSON = "{\"taskOID\":\"8883754532723139\"\"caseOID\":\"811419662578044\",\"taskDetails\":\"Task Details\",\"taskDueDate\":\"2017-5-1\",\"taskParty\":\"2\",\"taskStaffPool\":\"0\",\"taskType\":\"2\"}";

			SaveCaseTask saveTask = new SaveCaseTask();

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			scLogger.error(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;

		String auth = request.getHeader("Authorization");
		String token = request.getHeader("token");

		CCMSParty actionParty = (CCMSParty) request.getSession().getAttribute(
				"user");

		if (actionParty == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Invalid User");
		}

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			CCMSParty authParty = CCMSDomainUtilities.getInstance()
					.parseLoginToken(auth);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			scLogger.debug("SaveCaseTask Input JSON: " + clientJSON);

			CCMSCaseTask task = translateJSONToTaskObject(clientJSON, court);

			if (task.getTaskOID() != 0L)
			{
				if (!actionParty.isAuthorized(CCMSPermission.UPDATE_TASK, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to update a task.");
				}
				else
				{
					// Retrieve update SQL
					PreparedStatement updateStmt = task.genUpdateSQL(conn).get(
							0);
					updateStmt.execute();

					// If the task was completed, create the entry into the case
					// registry
					if (task.getDoneDate() != null)
					{
						// Retrieve the full task type
						CCMSTaskType taskType = (CCMSTaskType) CCMSDomainUtilities
								.getInstance().retrieveFullObject(
										task.getTaskType(), court);

						scLogger.trace("Completed CCMSTask taskType: "
								+ taskType);

						// Retrieve the details for the authParty since the
						// token only include the user id
						authParty = CCMSDomainUtilities.getInstance().getParty(
								authParty, conn);

						String description = "Completed: " + taskType.getName();
						if (task.getDetails() != null
								&& !task.getDetails().trim().isEmpty())
						{
							description += " Description: " + task.getDetails();
						}

						// Retrieve the event type object
						CCMSEventType eventType = new CCMSEventType(0L,
								court.getCourtOID());
						eventType
								.setEventTypeName(CCMSDomainUtilities.TASK_COMPLETED);
						eventType = (CCMSEventType) CCMSDomainUtilities
								.getInstance().retrieveFullObject(eventType,
										court);

						scLogger.trace("Create Registry Event -> Description: "
								+ description + " Completed Task Type Name: "
								+ taskType.getName());

						CCMSDomainUtilities.getInstance().triggerEvent(null,
								task.getAssociatedCase(), eventType, authParty,
								court, conn, description);
					}
					conn.commit();

					updateStmt.close();
				}
			}
			else
			{
				if (!actionParty.isAuthorized(CCMSPermission.CREATE_TASK, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to create a task.");
				}
				else
				{
					// new insert
					task.setTaskOID(CCMSDomainUtilities.getInstance().genOID());
					PreparedStatement insertStmt = task.genInsertSQL(conn).get(
							0);
					insertStmt.execute();
					conn.commit();
					insertStmt.close();
				}
			}

			// Return all matching values to the AJAX calling screen
			List<CCMSCaseTask> tasks = new ArrayList<CCMSCaseTask>();
			tasks.add(task);
			Gson myGson = new Gson();
			String json = myGson.toJson(tasks);
			scLogger.debug("SaveCaseType Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			scLogger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}

	public CCMSCaseTask translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSCaseTask task = new CCMSCaseTask(0L, court.getCourtOID());

		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		// taskOID will only be populated if this is an update
		String taskOIDString = (String) jsonCaseObject.get("taskOID");
		if (taskOIDString != null && !taskOIDString.isEmpty())
		{
			// Updated task, will need to delete old task first
			task.setTaskOID(Long.parseLong(taskOIDString));
		}

		String caseOIDString = (String) jsonCaseObject.get("caseOID");
		if (caseOIDString != null && !caseOIDString.isEmpty())
		{
			task.setAssociatedCase(new CCMSCase(Long.parseLong(caseOIDString),
					new CCMSCourt(task.getCourtOID())));
		}

		String details = (String) jsonCaseObject.get("taskDetails");
		task.setDetails(details);

		String taskDueDateString = (String) jsonCaseObject.get("taskDueDate");
		if (taskDueDateString != null && !taskDueDateString.isEmpty())
		{
			Date dueDate = CCMSDomainUtilities.getInstance()
					.convertStringToDate(taskDueDateString);
			task.setDueDate(dueDate);
		}

		String taskPartyOIDString = (String) jsonCaseObject.get("taskParty");
		if (taskPartyOIDString != null && !taskPartyOIDString.isEmpty())
		{
			task.setAssignedParty(new CCMSParty(Long
					.parseLong(taskPartyOIDString), court.getCourtOID()));
		}

		String taskStaffPoolOIDString = (String) jsonCaseObject
				.get("taskStaffPool");
		if (taskStaffPoolOIDString != null && !taskStaffPoolOIDString.isEmpty())
		{
			task.setAssignedPool(new CCMSStaffPool(Long
					.parseLong(taskStaffPoolOIDString), task.getCourtOID()));
		}

		String taskTypeOIDString = (String) jsonCaseObject.get("taskType");
		if (taskTypeOIDString != null && !taskTypeOIDString.isEmpty())
		{
			task.setTaskType(new CCMSTaskType(
					Long.parseLong(taskTypeOIDString), task.getCourtOID()));
		}

		String doneDateString = (String) jsonCaseObject.get("doneDate");
		if (doneDateString != null && !doneDateString.isEmpty())
		{
			Date doneDate = CCMSDomainUtilities.getInstance()
					.convertStringToDate(doneDateString);
			task.setDoneDate(doneDate);
		}

		String notDoneReason = (String) jsonCaseObject.get("notDoneReason");
		if (notDoneReason != null && !notDoneReason.isEmpty())
		{
			task.setNotDoneReason(notDoneReason);
		}

		String taskPriorityCode = (String) jsonCaseObject
				.get("taskPriorityCode");
		if (taskPriorityCode != null && !taskPriorityCode.isEmpty())
		{
			task.setTaskPriorityCode(Integer.parseInt(taskPriorityCode));
		}

		String documentTemplateOIDString = (String) jsonCaseObject
				.get("documentTemplateOID");
		if (documentTemplateOIDString != null
				&& !documentTemplateOIDString.isEmpty())
		{
			CCMSDocumentTemplate template = new CCMSDocumentTemplate(
					Long.parseLong(documentTemplateOIDString),
					court.getCourtOID());
			task.setDocTemplate(template);
		}

		Date now = new Date(System.currentTimeMillis());
		task.setAssignedDate(now);

		return task;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
