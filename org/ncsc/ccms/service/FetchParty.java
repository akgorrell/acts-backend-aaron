package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchParty extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchParty()
	{
		super();
		FetchParty.logger = LogManager.getLogger(FetchParty.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String json;
		String dobString = null, partyName = null, courtUserFlag = null, firstName = null, lastName = null, partyOID = null;

		// Get token object from attributes
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			JSONParser parser = new JSONParser();
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			if (jsonObject.containsKey("partyName"))
			{
				partyName = (String) jsonObject.get("partyName");
			}
			if (jsonObject.containsKey("courtUser"))
			{
				courtUserFlag = (String) jsonObject.get("courtUser");
			}
			if (jsonObject.containsKey("firstName"))
			{
				firstName = (String) jsonObject.get("firstName");
			}
			if (jsonObject.containsKey("lastName"))
			{
				lastName = (String) jsonObject.get("lastName");
			}
			if (jsonObject.containsKey("partyOID"))
			{
				partyOID = (String) jsonObject.get("partyOID");
			}
			if (jsonObject.containsKey("dob"))
			{
				dobString = (String) jsonObject.get("dob");
			}

			Map<Long, CCMSParty> casePartyMap = new LinkedHashMap<Long, CCMSParty>();

			CCMSParty qParty = new CCMSParty(0L, court.getCourtOID());

			if (partyName != null && !partyName.isEmpty())
			{
				qParty.setQueryName(partyName);
			}

			if (partyOID != null && !partyOID.isEmpty())
			{
				qParty.setPartyOID(Long.parseLong(partyOID));
			}

			if (firstName != null && !firstName.isEmpty())
			{
				qParty.setFirstName(firstName);
			}

			if (lastName != null && !lastName.isEmpty())
			{
				qParty.setLastName(lastName);
			}

			if (dobString != null && !dobString.isEmpty())
			{
				Date dob = CCMSDomainUtilities.getInstance()
						.convertStringToDate(dobString);
				qParty.setDob(dob);
			}

			Connection conn = CCMSDomainUtilities.getInstance().connect();
			ArrayList<CCMSParty> partyList = new ArrayList<CCMSParty>();

			List<PreparedStatement> pStmts = qParty.genSelectSQL(conn);
			for (PreparedStatement pStmt : pStmts)
			{
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					if (courtUserFlag != null && courtUserFlag.equals("true"))
					{
						// Retrieve full case party info since the UI will use the returned
						// Info to populate the fields in the user management screens
						CCMSParty dbCaseParty = new CCMSParty(rs, conn, false);
						if (dbCaseParty.isCourtUser())
						{
							// Populate the authorized courts and roles within
							// the court

							dbCaseParty.getAuthorizedCourts(conn);
							// If the query stipulates court users only, then
							// only add if the retrieved user is from the court
							partyList.add(dbCaseParty);
						}
					}
					else
					{
						CCMSParty dbCaseParty = new CCMSParty(rs, conn, false);
						// If the query does not stipulate all court users, then
						// return non court user parties to the response. This
						// is done to ensure we do not overwrite
						// fields specific to a court user (i.e., user role and
						// user court relationship tables)
						if (!dbCaseParty.isCourtUser())
						{
							partyList.add(dbCaseParty);
						}
					}
				}
				pStmt.close();
				rs.close();
			}

			Gson myGson = new Gson();

			json = myGson.toJson(partyList);

			// Return all matching values to the AJAX calling screen

			logger.debug("Servlet Returning: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);

			CCMSParty qParty = new CCMSParty(0L, court.getCourtOID());
			qParty.setFirstName("Tester");
			qParty.setLastName("Testee");

			Date dob = CCMSDomainUtilities.getInstance().convertStringToDate(
					"2017-09-25");
			qParty.setDob(dob);

			Connection conn = CCMSDomainUtilities.getInstance().connect();
			List<PreparedStatement> pStmts = qParty.genSelectSQL(conn);
			for (PreparedStatement pStmt : pStmts)
			{
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					CCMSParty dbCaseParty = new CCMSParty(rs, conn, false);

					System.out.println(dbCaseParty);

				}
				pStmt.close();
				rs.close();
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
