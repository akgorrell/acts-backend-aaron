package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class UpdateSelectedCourt extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public UpdateSelectedCourt()
	{
		super();
		UpdateSelectedCourt.logger = LogManager
				.getLogger(UpdateSelectedCourt.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String token = request.getHeader("token");

		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSDomainUtilities.getInstance().setCORSHeader(request, response,
					this, this.logger);
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input: " + clientJSON);
			
			CCMSParty tokenParty = CCMSDomainUtilities.getInstance().parseLoginToken(token);

			JSONParser parser = new JSONParser();
			JSONObject jsonObject = (JSONObject) parser.parse(clientJSON);
			String courtOIDString = (String) jsonObject.get("courtOID");

			if (CCMSDomainUtilities.isNumber(courtOIDString))
			{
				CCMSCourt court = new CCMSCourt(Long.parseLong(courtOIDString));
				PreparedStatement stmt = court.genSelectSQL(conn).get(0);
				ResultSet rs1 = stmt.executeQuery();
				while (rs1.next())
				{
					court = new CCMSCourt(rs1, conn);
				}
				stmt.close();
				rs1.close();
							
				request.getSession().setAttribute("court", court);
				logger.info("Switched court for user " + tokenParty.getUserName() + " to: " + court.getCourtName());
				
				response.sendError(HttpServletResponse.SC_OK);
			}
			else
			{
				response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
			}

		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
