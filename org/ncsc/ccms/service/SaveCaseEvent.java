package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.bindings.spi.LinkAccess;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSAlfrescoUtilities;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCaseEvent;
import org.ncsc.ccms.domain.CCMSCaseHearing;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSCourtLocation;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSEventWorkflow;
import org.ncsc.ccms.domain.CCMSHearingType;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPersonIdentificationType;
import org.ncsc.ccms.domain.CCMSWorkflowStep;

import com.google.gson.Gson;

public class SaveCaseEvent extends HttpServlet
{
	private String SAVE_FILE_LOC = System.getProperty("java.io.tmpdir");
	private static Logger logger = null;
	String documentName = null;
	FileItem uploadedDocument = null;
	String caseNumber = "C-M-21-2018"; // Default value for testing - remove
										// when passed caseNumber

	/**
	 * Constructor of the object.
	 */
	public SaveCaseEvent()
	{
		super();
		SaveCaseEvent.logger = LogManager.getLogger(SaveCaseEvent.class
				.getName());
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		this.doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Gson myGson = new Gson();
		String responseMessage = "";
		Connection conn = null;


		String auth = request.getHeader("Authorization");
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			
			CCMSParty authParty = CCMSDomainUtilities.getInstance().parseLoginToken(auth);
			// Need to retrieve details about auth party since only their user id is populated
			PreparedStatement stmt3 = authParty.genSelectSQL(conn).get(0);
			ResultSet rs3 = stmt3.executeQuery();
			while ( rs3.next())
			{
				authParty = new CCMSParty(rs3, conn, false);
			}
			stmt3.close();
			rs3.close();
			
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			CCMSCaseEvent caseEvent = this.translateJSONToObject(authParty, clientJSON,
					court);

			List<PreparedStatement> insStmts = caseEvent.genInsertSQL(conn);
			for (PreparedStatement stmt : insStmts)
			{
				stmt.execute();
				stmt.close();
			}
			conn.commit();

			CCMSCaseEvent qCaseEvent = new CCMSCaseEvent(
					caseEvent.getCaseEventOID(), court.getCourtOID());
			
			PreparedStatement selStmt = qCaseEvent.genSelectSQL(conn).get(0);
			ResultSet rs = selStmt.executeQuery();
			while (rs.next())
			{
				qCaseEvent = new CCMSCaseEvent(rs, conn, court);
			}
			rs.close();
			selStmt.close();
			

			List<CCMSCaseEvent> caseEvents = new ArrayList<CCMSCaseEvent>();
			caseEvents.add(caseEvent);

			responseMessage = myGson.toJson(caseEvents);
			logger.debug("Servlet Returning: " + responseMessage);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseMessage);

		}
		catch (Exception e)
		{
			logger.error(e);
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	public void init() throws ServletException
	{
		// Put your code here
	}

	public CCMSCaseEvent translateJSONToObject(CCMSParty authParty, String json, CCMSCourt court)
			throws Exception
	{
		CCMSCaseEvent event = new CCMSCaseEvent(CCMSDomainUtilities
				.getInstance().genOID(), court.getCourtOID());
		event.setEventDateTime(new Timestamp(System.currentTimeMillis()));

		// Set the creating user
		event.setEnteringParty(authParty);

		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(json);

		// Check to see if there is a hearing OID
		String eventTypeOIDString = (String) jsonCaseObject.get("eventTypeOID");
		if (eventTypeOIDString != null && !eventTypeOIDString.isEmpty())
		{
			long eventTypeOID = Long.parseLong(eventTypeOIDString);
			event.setEventType(new CCMSEventType(eventTypeOID, court
					.getCourtOID()));
		}
		

		String caseOIDString = (String) jsonCaseObject.get("caseOID");
		if (caseOIDString != null && !caseOIDString.isEmpty())
		{
			event.setCaseOID(Long.parseLong(caseOIDString));
		}
		
		String initiatedByOIDString = (String) jsonCaseObject.get("initiatedByPartyOID");
		if (initiatedByOIDString != null && !initiatedByOIDString.isEmpty())
		{
			long initiatedByOID = Long.parseLong(initiatedByOIDString);
			event.setInitiatedByParty(new CCMSParty(initiatedByOID, court
					.getCourtOID()));
		}

		String durationTimeMinString = (String) jsonCaseObject.get("durationTimeMin");
		if (durationTimeMinString != null && !durationTimeMinString.isEmpty())
		{
			event.setDurationTimeMin(Integer.parseInt(durationTimeMinString));
		}
		

		String documentTemplateOIDString = (String) jsonCaseObject.get("documentTemplateOID");
		if (documentTemplateOIDString != null && !documentTemplateOIDString.isEmpty())
		{
			long documentOID = Long.parseLong(documentTemplateOIDString);
			event.setDocument(new CCMSCaseDocument(documentOID, court.getCourtOID()));
		}
		

		String descriptionString = (String) jsonCaseObject.get("description");
		if (descriptionString != null && !descriptionString.isEmpty())
		{
			event.setDescription(descriptionString);
		}



		return event;
	}

}
