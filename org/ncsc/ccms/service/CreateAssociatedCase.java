package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseCharge;
import org.ncsc.ccms.domain.CCMSCaseParty;
import org.ncsc.ccms.domain.CCMSChargeFactor;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class CreateAssociatedCase extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public CreateAssociatedCase()
	{
		super();
		CreateAssociatedCase.logger = LogManager
				.getLogger(CreateAssociatedCase.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Connection conn = null;

		String token = request.getHeader("token");

		try
		{
			conn = CCMSDomainUtilities.getInstance().connect();
			
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);
			CCMSParty eventParty = CCMSDomainUtilities.getInstance()
					.parseLoginToken(token);
			eventParty = CCMSDomainUtilities.getInstance().getParty(eventParty,
					conn);

			JSONParser parser = new JSONParser();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			JSONObject jsonObject = (JSONObject) parser.parse(clientJSON);
			CCMSCase qCase = new CCMSCase(0L, court);

			// Retrieve the case to be duplicated

			if (jsonObject.containsKey("caseOID"))
			{
				String caseOIDString = (String) jsonObject.get("caseOID");
				if (CCMSDomainUtilities.isNumber(caseOIDString))
				{
					qCase.setCaseOID(Long.parseLong(caseOIDString));
				}
			}

			// Retrieve the case for this OID
			CCMSCase assocCase = new CCMSCase(0L, court);
			conn = CCMSDomainUtilities.getInstance().connect();
			List<PreparedStatement> pStmts = qCase.genSelectSQL(conn);
			logger.debug("Query Statements: " + pStmts.size());
			for (PreparedStatement pStmt : pStmts)
			{
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					assocCase = new CCMSCase(rs, conn, court, false);
				}
				pStmt.close();
				rs.close();
			}

			/**
			 * A case weight has to be > 0 for the creation of a case The party
			 * must be of type child in order for the case to be created.
			 * Associated cases need to be created from a case where there is a
			 * party and a child attached to it.
			 **/
			
			// Indicate that the case is associated - note that this is not stored in the database, only needed for
			// appropriate SQL generation
			assocCase.setAssociatedCase(true);

			// Provide the assoc case a new OID
			assocCase.setCaseOID(CCMSDomainUtilities.getInstance().genOID());

			// Set an updated case number for the associated case
			assocCase.setCaseNumber(assocCase.calculateCaseNumber(conn));

			// Remove information that is not copied
			assocCase.getCaseParties().clear();
			assocCase.getCaseDocs().clear();
			assocCase.getCaseHearings().clear();
			assocCase.getCaseTasks().clear();
			assocCase.getJudicialAssignments().clear();
			assocCase.getCaseEvents().clear();
			assocCase.setCaseFilingDate(new Date(System.currentTimeMillis()));
			assocCase.getCaseCharges().clear();

			List<PreparedStatement> insertStmts = assocCase.genInsertSQL(conn);
			for (PreparedStatement pStmt : insertStmts)
			{
				pStmt.execute();
				pStmt.close();
			}

			CCMSEventType eventType = new CCMSEventType(0L, court.getCourtOID());
			eventType.setEventTypeName(CCMSDomainUtilities.CASE_CREATED_EVENT);
			eventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(eventType, court);
			CCMSDomainUtilities.getInstance().triggerEvent(null, assocCase,
					eventType, eventParty, court, conn, "");

			conn.commit();

			// Grab the updated case
			PreparedStatement qStmt = assocCase.genSelectSQL(conn).get(0);
			logger.debug("Query Statements: " + qStmt);

			ResultSet rs1 = qStmt.executeQuery();
			while (rs1.next())
			{
				assocCase = new CCMSCase(rs1, conn, court, false);
			}
			qStmt.close();
			rs1.close();
			
			Gson myGson = new Gson();
			String json = myGson.toJson(assocCase);
			logger.debug("CreateAssociatedCase Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception e)
		{
			logger.error(e);
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
				logger.error(e1);
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	public static void main(String[] args)
	{
		String json = "{\"caseOID\":\"7130152139420373\"}";
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
