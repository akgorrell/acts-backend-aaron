package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPartyAddress;
import org.ncsc.ccms.domain.CCMSPartyEmail;
import org.ncsc.ccms.domain.CCMSPartyIdentifier;
import org.ncsc.ccms.domain.CCMSPartyPhone;
import org.ncsc.ccms.domain.CCMSSpokenLanguage;
import org.ncsc.ccms.domain.CCMSStaffRole;

import com.google.gson.Gson;

public class SaveStaffParty extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveStaffParty()
	{
		super();
		SaveStaffParty.logger = LogManager.getLogger(SaveStaffParty.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;

		CCMSParty party = new CCMSParty(0L);

		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Received: " + clientJSON);
			
			// If the JSON from the client contains a partyOID (i.e., if update) , this random OID will be overwritten by that OID
			// This is to ensure that all of the associated objects have a Party OID (i.e., email, etc) 
			translateJSONToStaffPartyObject(clientJSON, party);

			if (party.getPartyOID() == 0L)
			{
				// Set the party OID
				party.setPartyOID( CCMSDomainUtilities.getInstance().genOID());

				// Now that we have a party OID, set the party OID on the emails.
				for ( CCMSPartyEmail email : party.getEmails() )
				{
					email.setPartyOID(party.getPartyOID());
				}
				// insert new party

				List<PreparedStatement> stmts = party.genInsertSQL(conn);
				for (PreparedStatement stmt : stmts)
				{
					stmt.execute();
					stmt.close();
				}
			}
			else
			{
				// Update the exiting party
				List<PreparedStatement> stmts = party.genUpdateSQL(conn);
				for (PreparedStatement stmt : stmts)
				{
					stmt.execute();
					stmt.close();
				}
			}

			conn.commit();

			// Retrieve the full party and return to UI
			List<CCMSParty> parties = new ArrayList<CCMSParty>();
			CCMSParty qParty = new CCMSParty(party.getPartyOID());
			qParty.setCourtUser(true);

			PreparedStatement pStmt = qParty.genSelectSQL(conn).get(0);

			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSParty dbParty = new CCMSParty(rs, conn, false);
				dbParty.getAuthorizedCourts(conn);
				parties.add(dbParty);
			}
			pStmt.close();
			rs.close();

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(parties);
			logger.debug("SaveParty Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			try
			{
				conn.rollback();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				logger.error(e);
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}

	public CCMSParty translateJSONToStaffPartyObject(String taskJSON,
			CCMSParty party) throws Exception
	{
		JSONParser parser = new JSONParser();
		JSONObject jsonPartyObject = (JSONObject) parser.parse(taskJSON);

		// Can hard code this value since this service will only be called when
		// adding/updating a system user
		party.setCourtUser(true);

		// partyOID will only be populated if this is an update
		String partyOIDString = (String) jsonPartyObject.get("partyOID");
		if (partyOIDString != null && !partyOIDString.isEmpty() && !partyOIDString.equals("0"))
		{
			// Updated task, will need to delete old task first
			party.setPartyOID(Long.parseLong(partyOIDString));
		}

		String firstName = (String) jsonPartyObject.get("firstName");
		if (firstName != null && !firstName.isEmpty())
		{
			party.setFirstName(firstName);
		}

		String lastName = (String) jsonPartyObject.get("lastName");
		if (lastName != null && !lastName.isEmpty())
		{
			party.setLastName(lastName);
		}

		String userName = (String) jsonPartyObject.get("userName");
		if (userName != null && !userName.isEmpty())
		{
			party.setUserName(userName);
		}

		String password = (String) jsonPartyObject.get("password");
		if (password != null && !password.isEmpty())
		{
			party.setPassword(password);
		}

		JSONArray authorizedCourtsArray = (JSONArray) jsonPartyObject
				.get("authorizedCourts");
		if (authorizedCourtsArray != null && !authorizedCourtsArray.isEmpty())
		{
			for (Object jsonObj : authorizedCourtsArray)
			{
				JSONObject authorizedCourt = (JSONObject) jsonObj;

				String authorizedCourtOIDString = (String) authorizedCourt
						.get("courtOID");
				CCMSCourt authCourt = new CCMSCourt();
				authCourt.setCourtOID(Long.parseLong(authorizedCourtOIDString));
				// Add the unique authorized courts here
				if (!party.getAuthorizedCourts().contains(authCourt))
				{
					party.getAuthorizedCourts().add(authCourt);
				}

				JSONArray authorizedRolesArray = (JSONArray) authorizedCourt
						.get("roles");
				if (authorizedRolesArray != null
						&& !authorizedRolesArray.isEmpty())
				{
					for (Object authorizedRoleObject : authorizedRolesArray)
					{
						JSONObject authorizedRole = (JSONObject) authorizedRoleObject;
						String staffRoleOIDString = (String) authorizedRole
								.get("staffRoleOID");
						CCMSStaffRole role = createStaffRole(
								authorizedCourtOIDString, staffRoleOIDString);
						if (role != null)
						{
							party.getRoles().add(role);
						}
					}
				}

			}
		}
		
		// Get emails associated with party
		JSONArray emailArray = (JSONArray) jsonPartyObject.get("emails");
		if (emailArray != null && !emailArray.isEmpty())
		{
			Iterator emailIterator = emailArray.iterator();
			while (emailIterator.hasNext())
			{
				JSONObject jsonEmailObject = (JSONObject) emailIterator.next();

				CCMSPartyEmail newEmail = new CCMSPartyEmail(
						CCMSDomainUtilities.getInstance().genOID());
				newEmail.setPartyOID(party.getPartyOID());

				String emailAddressType = (String) jsonEmailObject
						.get("emailAddressType");
				if (emailAddressType != null && !emailAddressType.isEmpty())
				{
					newEmail.setEmailAddressType(emailAddressType);
				}

				String emailAddress = (String) jsonEmailObject
						.get("emailAddress");
				if (emailAddress != null && !emailAddress.isEmpty())
				{
					newEmail.setEmailAddress(emailAddress);
				}

				String startDate = (String) jsonEmailObject.get("startDate");
				if (startDate != null && !startDate.isEmpty())
				{
					Date startDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(startDate);
					newEmail.setStartDate(startDateTime);
				}

				String endDate = (String) jsonEmailObject.get("endDate");
				if (endDate != null && !endDate.isEmpty())
				{
					Date endDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(endDate);
					newEmail.setEndDate(endDateTime);
				}
				party.getEmails().add(newEmail);
			}
		}

		return party;
	}

	private CCMSStaffRole createStaffRole(String authorizedCourtOIDString,
			String staffRoleOIDString)
	{
		CCMSStaffRole role = null;

		if (authorizedCourtOIDString != null
				&& CCMSDomainUtilities.isNumber(authorizedCourtOIDString)
				&& staffRoleOIDString != null
				&& CCMSDomainUtilities.isNumber(staffRoleOIDString))
		{
			role = new CCMSStaffRole(Long.parseLong(staffRoleOIDString),
					Long.parseLong(authorizedCourtOIDString));
		}

		return role;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
