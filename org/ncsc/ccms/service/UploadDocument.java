package org.ncsc.ccms.service;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;

import com.google.gson.Gson;

public class UploadDocument extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public UploadDocument()
	{
		super();
		UploadDocument.logger = LogManager.getLogger(UploadDocument.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		HttpSession session = request.getSession();

		ServletContext context = getServletContext();

		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		// Create a factory for disk-based file items
		FileItemFactory factory = new DiskFileItemFactory();

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		String auth = request.getHeader("Authorization");
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			String fileName = null, caseOIDString = null;
			List items = upload.parseRequest(request);
			// Process the uploaded items
			Iterator iter = items.iterator();
			File uploadedFile = null;
			while (iter.hasNext())
			{
				// Process uploaded items
				FileItem item = (FileItem) iter.next();

				if (item.isFormField())
				{
					if (item.getFieldName().equals("caseOID"))
					{
						caseOIDString = item.getString();
					}
				}
				else if (item.getSize() > 0)
				{
					uploadedFile = processUploadedFile(item, context);
				}
			}

			if (uploadedFile != null)
			{
				fileName = uploadedFile.getName();
			}
			

			Gson myGson = new Gson();

			// TODO: Replace object below with document
			String responseMessage = myGson.toJson(null);
			logger.debug("Servlet Returning: " + responseMessage);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseMessage);

		}

		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	private File processUploadedFile(FileItem item, ServletContext context)
			throws Exception
	{
		String fieldName = item.getFieldName();

		String fileName = item.getName();
		fileName = fileName.replaceAll(" ", "_");

		fileName = CCMSDomainUtilities.getInstance().genOID() + "_" + fileName;
		String contentType = item.getContentType();
		boolean isInMemory = item.isInMemory();
		long sizeInBytes = item.getSize();

		String fileDir = context.getRealPath("/");
		File uploadedFile = new File(System.getProperty("java.io.tmpdir"), fileName);
		item.write(uploadedFile);
		return uploadedFile;
	}

}
