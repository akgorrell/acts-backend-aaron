package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSChargeFactor;
import org.ncsc.ccms.domain.CCMSPartyAddress;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPartyEmail;
import org.ncsc.ccms.domain.CCMSPartyIdentifier;
import org.ncsc.ccms.domain.CCMSPartyPhone;
import org.ncsc.ccms.domain.CCMSPermission;
import org.ncsc.ccms.domain.CCMSSpokenLanguage;
import org.ncsc.ccms.domain.CCMSStaffPool;
import org.ncsc.ccms.domain.CCMSTaskType;

import com.google.gson.Gson;

public class SaveParty extends HttpServlet
{

	private static Logger logger = null;

	public SaveParty()
	{
		super();
		SaveParty.logger = LogManager.getLogger(SaveParty.class.getName());
	}

	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	public static void main(String[] args)
	{
		Connection conn = null;
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			conn = CCMSDomainUtilities.getInstance().connect();
			// Case Add
			CCMSParty party = new CCMSParty(0L);

			String clientJSON = "{\"partyOID\":\"0\",\"firstName\": \"newfirstName\",\"lastName\": \"lastName\",\"sex\":\"M\",\"dob\": \"1988-3-16\",\"email\": \"test@example.com\",\"phone\": \"1234567890\",\"addresses\":[{\"address1\": \"Address1\" ,\"address2\": \"Address2\" ,\"address3\": \"Address3\",\"communityCode\": \"Comm Code\",\"administrativeArea\": \"AA Area\",\"municipalityName\": \"CelbrationCity\",\"postalCode\": \"123456\",\"countryName\": \"Trinidad & Tobago\",\"addressDescriptionText\": \"23.43\",\"startDate\": \"2017-01-01\",\"endDate\": \"2017-12-31\",\"addressType\": \"23\"}]}";

			SaveParty saveParty = new SaveParty();

			saveParty.translateJSONToTaskObject(clientJSON, party);

			if (party.getPartyOID() == 0L)
			{
				// insert new party
				party.setPartyOID(CCMSDomainUtilities.getInstance().genOID());
				List<PreparedStatement> stmts = party.genInsertSQL(conn);
				for (PreparedStatement stmt : stmts)
				{
					stmt.execute();
				}
			}
			else
			{
				// Update
				PreparedStatement stmt = party.genUpdateSQL(conn).get(0);
				stmt.execute();
				stmt.close();
			}

			conn.commit();

			// Retrieve the full task and return to UI
			List<CCMSParty> parties = new ArrayList<CCMSParty>();
			CCMSParty qParty = new CCMSParty(party.getPartyOID());

			PreparedStatement pStmt = qParty.genSelectSQL(conn).get(0);

			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSParty dbParty = new CCMSParty(rs, conn, false);
				parties.add(dbParty);
			}
			pStmt.close();
			rs.close();

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(parties);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;

		CCMSParty party = new CCMSParty(0L);

		String token = request.getHeader("token");

		CCMSParty actionParty = (CCMSParty) request.getSession().getAttribute(
				"user");

		if (actionParty == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Invalid User");
		}

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Received: " + clientJSON);

			translateJSONToTaskObject(clientJSON, party);

			if (party.getPartyOID() == 0L)
			{
				if (!actionParty.isAuthorized(CCMSPermission.CREATE_PARTY, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to create a party.");
				}
				else
				{
					// insert new party
					party.setPartyOID(CCMSDomainUtilities.getInstance()
							.genOID());
					List<PreparedStatement> stmts = party.genInsertSQL(conn);
					for (PreparedStatement stmt : stmts)
					{
						stmt.execute();
						stmt.close();
					}
				}
			}
			else
			{

				if (!actionParty.isAuthorized(CCMSPermission.UPDATE_PARTY, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to update a party.");
				}
				else
				{
					// Update the exiting party
					List<PreparedStatement> stmts = party.genUpdateSQL(conn);
					for (PreparedStatement stmt : stmts)
					{
						stmt.execute();
						stmt.close();
					}
				}
			}

			conn.commit();

			// Retrieve the full task and return to UI
			List<CCMSParty> parties = new ArrayList<CCMSParty>();
			CCMSParty qParty = new CCMSParty(party.getPartyOID());

			PreparedStatement pStmt = qParty.genSelectSQL(conn).get(0);

			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSParty dbParty = new CCMSParty(rs, conn, false);
				parties.add(dbParty);
			}
			pStmt.close();
			rs.close();

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(parties);
			logger.debug("SaveParty Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			try
			{
				conn.rollback();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				logger.error(e);
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}

	public boolean isTaskUpdate(String clientJSON) throws Exception
	{
		/**
		 * Determine if this is a add or an update
		 */
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(clientJSON);
		String taskOIDString = (String) jsonCaseObject.get("taskOID");
		if (taskOIDString != null && !taskOIDString.isEmpty()
				&& !taskOIDString.equals("0"))
		{
			// If the taskOID is provided AND the taskOID is not zero (0), this
			// is an update
			return true;
		}

		return false;
	}

	public CCMSParty translateJSONToTaskObject(String taskJSON, CCMSParty party)
			throws Exception
	{
		JSONParser parser = new JSONParser();
		JSONObject jsonPartyObject = (JSONObject) parser.parse(taskJSON);

		// partyOID will only be populated if this is an update
		String partyOIDString = (String) jsonPartyObject.get("partyOID");
		if (partyOIDString != null && !partyOIDString.isEmpty())
		{
			// Updated task, will need to delete old task first
			party.setPartyOID(Long.parseLong(partyOIDString));
		}

		String firstName = (String) jsonPartyObject.get("firstName");
		if (firstName != null && !firstName.isEmpty())
		{
			party.setFirstName(firstName);
		}

		String lastName = (String) jsonPartyObject.get("lastName");
		if (lastName != null && !lastName.isEmpty())
		{
			party.setLastName(lastName);
		}

		String dobString = (String) jsonPartyObject.get("dob");
		if (dobString != null && !dobString.isEmpty())
		{
			Date dob = CCMSDomainUtilities.getInstance().convertStringToDate(
					dobString);
			party.setDob(dob);
		}

		String sex = (String) jsonPartyObject.get("sex");
		if (sex != null && !sex.isEmpty())
		{
			party.setSex(sex);
		}

		String isCourtUser = (String) jsonPartyObject.get("isCourtUser");
		if (isCourtUser != null && isCourtUser.equals("yes"))
		{
			party.setCourtUser(true);
		}

		String userName = (String) jsonPartyObject.get("userName");
		if (userName != null && !userName.isEmpty())
		{
			party.setUserName(userName);
		}

		String notes = (String) jsonPartyObject.get("notes");
		if (notes != null && !notes.isEmpty())
		{
			party.setNotes(notes);
		}

		String alternativeName = (String) jsonPartyObject
				.get("alternativeName");
		if (alternativeName != null && !alternativeName.isEmpty())
		{
			party.setAlternativeName(alternativeName);
		}

		String interpreterRequiredIndicator = (String) jsonPartyObject
				.get("interpreterRequiredIndicator");
		if (interpreterRequiredIndicator != null
				&& !interpreterRequiredIndicator.isEmpty())
		{
			int interpreterRequiredIndicatorInt = Integer
					.parseInt(interpreterRequiredIndicator);
			party.setInterpreterRequiredIndicator(CCMSDomainUtilities
					.intToBool(interpreterRequiredIndicatorInt));
		}

		// Get address information associated with a case party

		JSONArray addressArray = (JSONArray) jsonPartyObject.get("addresses");
		if (addressArray != null && !addressArray.isEmpty())
		{
			Iterator addressIterator = addressArray.iterator();

			while (addressIterator.hasNext())
			{
				JSONObject jsonAddressObject = (JSONObject) addressIterator
						.next();

				CCMSPartyAddress newAddress = new CCMSPartyAddress(
						CCMSDomainUtilities.getInstance().genOID());
				newAddress.setPartyOID(party.getPartyOID());

				String address1 = (String) jsonAddressObject.get("address1");
				if (address1 != null && !address1.isEmpty())
				{
					newAddress.setAddress1(address1);
				}

				String address2 = (String) jsonAddressObject.get("address2");
				if (address2 != null && !address2.isEmpty())
				{
					newAddress.setAddress2(address2);
				}

				String address3 = (String) jsonAddressObject.get("address3");
				if (address3 != null && !address3.isEmpty())
				{
					newAddress.setAddress3(address3);
				}

				String communityCode = (String) jsonAddressObject
						.get("communityCode");
				if (communityCode != null && !communityCode.isEmpty())
				{
					newAddress.setCommunityCode(communityCode);
				}

				String administrativeArea = (String) jsonAddressObject
						.get("administrativeArea");
				if (administrativeArea != null && !administrativeArea.isEmpty())
				{
					newAddress.setAdministrativeArea(administrativeArea);
				}

				String municipalityName = (String) jsonAddressObject
						.get("municipalityName");
				if (municipalityName != null && !municipalityName.isEmpty())
				{
					newAddress.setMunicipalityName(municipalityName);
				}

				String postalCode = (String) jsonAddressObject
						.get("postalCode");
				if (postalCode != null && !postalCode.isEmpty())
				{
					newAddress.setPostalCode(postalCode);
				}

				String countryName = (String) jsonAddressObject
						.get("countryName");
				if (countryName != null && !countryName.isEmpty())
				{
					newAddress.setCountryName(countryName);
				}

				String addressDescriptionText = (String) jsonAddressObject
						.get("addressDescriptionText");
				if (addressDescriptionText != null
						&& !addressDescriptionText.isEmpty())
				{
					newAddress
							.setAddressDescriptionText(addressDescriptionText);
				}

				String addressType = (String) jsonAddressObject
						.get("addressType");
				if (addressType != null && !addressType.isEmpty())
				{
					newAddress.setAddressType(addressType);
				}

				String startDate = (String) jsonAddressObject.get("startDate");
				if (startDate != null && !startDate.isEmpty())
				{
					Date startDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(startDate);
					newAddress.setStartDate(startDateTime);
				}

				String endDate = (String) jsonAddressObject.get("endDate");
				if (endDate != null && !endDate.isEmpty())
				{
					Date endDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(endDate);
					newAddress.setEndDate(endDateTime);
				}

				party.getAddresses().add(newAddress);
			}
		}

		// Get emails associated with party
		JSONArray emailArray = (JSONArray) jsonPartyObject.get("emails");
		if (emailArray != null && !emailArray.isEmpty())
		{
			Iterator emailIterator = emailArray.iterator();
			while (emailIterator.hasNext())
			{
				JSONObject jsonEmailObject = (JSONObject) emailIterator.next();

				CCMSPartyEmail newEmail = new CCMSPartyEmail(
						CCMSDomainUtilities.getInstance().genOID());
				newEmail.setPartyOID(party.getPartyOID());

				String emailAddressType = (String) jsonEmailObject
						.get("emailAddressType");
				if (emailAddressType != null && !emailAddressType.isEmpty())
				{
					newEmail.setEmailAddressType(emailAddressType);
				}

				String emailAddress = (String) jsonEmailObject
						.get("emailAddress");
				if (emailAddress != null && !emailAddress.isEmpty())
				{
					newEmail.setEmailAddress(emailAddress);
				}

				party.getEmails().add(newEmail);

				String startDate = (String) jsonEmailObject.get("startDate");
				if (startDate != null && !startDate.isEmpty())
				{
					Date startDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(startDate);
					newEmail.setStartDate(startDateTime);
				}

				String endDate = (String) jsonEmailObject.get("endDate");
				if (endDate != null && !endDate.isEmpty())
				{
					Date endDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(endDate);
					newEmail.setEndDate(endDateTime);
				}
			}
		}

		// Get phone numbers associated with party
		JSONArray phoneNumberArray = (JSONArray) jsonPartyObject
				.get("phoneNumbers");
		if (phoneNumberArray != null && !phoneNumberArray.isEmpty())
		{
			Iterator phoneNumberIterator = phoneNumberArray.iterator();
			while (phoneNumberIterator.hasNext())
			{
				JSONObject jsonPhoneObject = (JSONObject) phoneNumberIterator
						.next();

				CCMSPartyPhone newPhone = new CCMSPartyPhone(
						CCMSDomainUtilities.getInstance().genOID());
				newPhone.setPartyOID(party.getPartyOID());

				String phoneType = (String) jsonPhoneObject.get("phoneType");
				if (phoneType != null && !phoneType.isEmpty())
				{
					newPhone.setPhoneType(phoneType);
				}

				String phoneNumber = (String) jsonPhoneObject
						.get("phoneNumber");
				if (phoneNumber != null && !phoneNumber.isEmpty())
				{
					newPhone.setPhoneNumber(phoneNumber);
				}

				String textMessagesIndicator = (String) jsonPhoneObject
						.get("textMessagesIndicator");
				if (textMessagesIndicator != null
						&& !textMessagesIndicator.isEmpty())
				{
					int textMessageIndicatorInt = Integer
							.parseInt(textMessagesIndicator);
					newPhone.setTextMessagesIndicator(CCMSDomainUtilities
							.intToBool(textMessageIndicatorInt));
				}

				String startDate = (String) jsonPhoneObject.get("startDate");
				if (startDate != null && !startDate.isEmpty())
				{
					Date startDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(startDate);
					newPhone.setStartDate(startDateTime);
				}

				String endDate = (String) jsonPhoneObject.get("endDate");
				if (endDate != null && !endDate.isEmpty())
				{
					Date endDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(endDate);
					newPhone.setEndDate(endDateTime);
				}

				party.getPhoneNumbers().add(newPhone);
			}
		}

		JSONArray spokenLanguagesArray = (JSONArray) jsonPartyObject
				.get("spokenLanguages");
		if (spokenLanguagesArray != null && !spokenLanguagesArray.isEmpty())
		{
			for (Object jsonObj : spokenLanguagesArray)
			{
				CCMSSpokenLanguage newLang = new CCMSSpokenLanguage(
						CCMSDomainUtilities.getInstance().genOID());
				newLang.setPartyOID(party.getPartyOID());
				String languageName = (String) jsonObj;
				newLang.setLanguageName(languageName);
				party.getSpokenLanguages().add(newLang);
			}
		}

		// Get identifiers associated with party
		JSONArray identifierArray = (JSONArray) jsonPartyObject
				.get("identifiers");
		if (identifierArray != null && !identifierArray.isEmpty())
		{
			Iterator identifierIterator = identifierArray.iterator();
			while (identifierIterator.hasNext())
			{
				JSONObject jsonIdentifierObject = (JSONObject) identifierIterator
						.next();

				CCMSPartyIdentifier newIdentifier = new CCMSPartyIdentifier(
						CCMSDomainUtilities.getInstance().genOID());
				newIdentifier.setPartyOID(party.getPartyOID());

				String identifierType = (String) jsonIdentifierObject
						.get("identifierType");
				if (identifierType != null && !identifierType.isEmpty())
				{
					newIdentifier.setIdentifierType(identifierType);
				}

				String identifierValue = (String) jsonIdentifierObject
						.get("identifierValue");
				if (identifierValue != null && !identifierValue.isEmpty())
				{
					newIdentifier.setIdentifierValue(identifierValue);
				}

				String startDate = (String) jsonIdentifierObject
						.get("startDate");
				if (startDate != null && !startDate.isEmpty())
				{
					Date startDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(startDate);
					newIdentifier.setStartDate(startDateTime);
				}

				String endDate = (String) jsonIdentifierObject.get("endDate");
				if (endDate != null && !endDate.isEmpty())
				{
					Date endDateTime = CCMSDomainUtilities.getInstance()
							.convertStringToDate(endDate);
					newIdentifier.setEndDate(endDateTime);
				}

				String identifierNotes = (String) jsonIdentifierObject
						.get("notes");
				if (identifierNotes != null && !identifierNotes.isEmpty())
				{
					newIdentifier.setNotes(identifierNotes);
				}

				party.getIdentifiers().add(newIdentifier);
			}
		}

		return party;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
