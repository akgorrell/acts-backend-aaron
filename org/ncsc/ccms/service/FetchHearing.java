package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSCaseHearing;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSCourtLocation;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchHearing extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchHearing()
	{
		super();
		FetchHearing.logger = LogManager
				.getLogger(FetchHearing.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			List<CCMSCaseHearing> hearings = new ArrayList<CCMSCaseHearing>();
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			CCMSCaseHearing qHearing = new CCMSCaseHearing(0L,
					court.getCourtOID());
			
			JSONParser parser = new JSONParser();
			String clientJSON = "{\"hearingQueryDate\":\"2017-03-30\",\"caseOID\":\"539583784447181\",\"caseHearingOID\":\"6\"\"judicialOfficer\":{\"partyOID\":\"2\",\"courtOID\":\"0\"}}";
			System.out.println(clientJSON);
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;

			String hearingQueryDateString = (String) jsonObject
					.get("hearingQueryDate");




			if (hearingQueryDateString != null
					&& !hearingQueryDateString.isEmpty())
			{
				Date queryDate = CCMSDomainUtilities.getInstance()
						.convertStringToDate(hearingQueryDateString);
				//qHearing.setHearingQueryDate(queryDate);
			}

			JSONObject judicialOfficerObj = (JSONObject) jsonObject.get("judicialOfficer");
			if (judicialOfficerObj != null)
			{
				String partyOID = (String) judicialOfficerObj.get("partyOID");
				CCMSParty judOfficer = new CCMSParty(Long.parseLong(partyOID));
				//qHearing.setJudicialOfficer(judOfficer);
			}

			String courtLocOID = (String) judicialOfficerObj.get("courtLoc");
			if (courtLocOID != null)
			{
				CCMSCourtLocation courtLocation = new CCMSCourtLocation(Long.parseLong(courtLocOID), court.getCourtOID());
				//qHearing.setJudicialOfficer(judOfficer);
			}
			

			String caseOIDString = (String) jsonObject
					.get("caseOID");
			
			if (caseOIDString != null && !caseOIDString.isEmpty())
			{
				//qHearing.setCaseOID(Long.parseLong(caseOIDString));
			}

			String caseHearingOIDString = (String) jsonObject
					.get("caseHearingOID");
			
			if (caseHearingOIDString != null && !caseHearingOIDString.isEmpty())
			{
				qHearing.setCaseHearingOID(Long.parseLong(caseHearingOIDString));
			}
			
			

			PreparedStatement hrngQueryStmt = qHearing.genSelectSQL(conn)
					.get(0);
			ResultSet rs = hrngQueryStmt.executeQuery();
			while (rs.next())
			{
				hearings.add(new CCMSCaseHearing(rs, conn, court));
			}

			System.out.println("here");

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		List<CCMSCaseHearing> hearings = new ArrayList<CCMSCaseHearing>();
		JSONParser parser = new JSONParser();
		String json;
		String token = request.getHeader("token");

		Connection conn = null;
		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance().getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCaseHearing qHearing = new CCMSCaseHearing(0L,
					court.getCourtOID());
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			logger.debug("Input JSON: " + clientJSON);

			// Set the query date
			String hearingQueryDateString = (String) jsonObject
					.get("hearingQueryDate");
			if (hearingQueryDateString != null
					&& !hearingQueryDateString.isEmpty())
			{
				Date queryDate = CCMSDomainUtilities.getInstance()
						.convertStringToDate(hearingQueryDateString);
				qHearing.setHearingQueryDate(queryDate);
			}

			// Set the selected judicial officer
			JSONObject judicialOfficerObj = (JSONObject) jsonObject
					.get("judicialOfficer");

			if (judicialOfficerObj != null)
			{
				String partyOID = (String) judicialOfficerObj.get("partyOID");
				CCMSParty judOfficer = new CCMSParty(Long.parseLong(partyOID));
				qHearing.setJudicialOfficer(judOfficer);
			}
			

			String courtLocOID = (String) jsonObject.get("courtLoc");
			if (courtLocOID != null)
			{
				CCMSCourtLocation courtLoc = new CCMSCourtLocation(Long.parseLong(courtLocOID), court.getCourtOID());
				qHearing.setCourtLoc(courtLoc);
			}
			
			

			String caseOIDString = (String) jsonObject
					.get("caseOID");
			
			if (caseOIDString != null && !caseOIDString.isEmpty())
			{
				qHearing.setCaseOID(Long.parseLong(caseOIDString));
			}

			String caseHearingOIDString = (String) jsonObject
					.get("caseHearingOID");
			
			if (caseHearingOIDString != null && !caseHearingOIDString.isEmpty())
			{
				qHearing.setCaseHearingOID(Long.parseLong(caseHearingOIDString));
			}

			PreparedStatement hrngQueryStmt = qHearing.genSelectSQL(conn)
					.get(0);
			ResultSet rs = hrngQueryStmt.executeQuery();
			while (rs.next())
			{
				hearings.add(new CCMSCaseHearing(rs, conn, court));
			}
			Gson myGson = new Gson();
			json = myGson.toJson(hearings);
			logger.debug("Servlet Returning: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}

		// startDateTime
		catch (Exception e)
		{
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
