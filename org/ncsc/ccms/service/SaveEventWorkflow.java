package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseEvent;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSCourtLocation;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSEventWorkflow;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSStaffPool;
import org.ncsc.ccms.domain.CCMSTaskType;
import org.ncsc.ccms.domain.CCMSWorkflowStep;

import com.google.gson.Gson;

public class SaveEventWorkflow extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveEventWorkflow()
	{
		super();
		SaveEventWorkflow.logger = LogManager.getLogger(SaveEventWorkflow.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Connection conn = null;

		CCMSEventWorkflow eventWorkflow = null;
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			eventWorkflow = this.translateJSONToTaskObject(clientJSON, court);

			if (eventWorkflow.getEventWorkflowOID() != 0L)
			{
				// Update
				List<PreparedStatement> updateStmts = eventWorkflow
						.genUpdateSQL(conn);
				for (PreparedStatement stmt : updateStmts)
				{
					stmt.execute();
					stmt.close();
				}

				conn.commit();
			}
			else
			{
				// Insert new workflow
				eventWorkflow.setEventWorkflowOID(CCMSDomainUtilities
						.getInstance().genOID());

				// Set the parent workflow OID of the steps
				for (CCMSWorkflowStep step : eventWorkflow.getWorkflowSteps())
				{
					step.setWorkflowOID(eventWorkflow.getEventWorkflowOID());
				}

				List<PreparedStatement> insertStmts = eventWorkflow
						.genInsertSQL(conn);
				for (PreparedStatement stmt : insertStmts)
				{
					stmt.execute();
					stmt.close();
				}

				conn.commit();
				

				List<CCMSEventWorkflow> eventWorkflows = new ArrayList<CCMSEventWorkflow>();
				eventWorkflows.add(eventWorkflow);

				Gson myGson = new Gson();
				String json = myGson.toJson(eventWorkflows);
				logger.debug("SaveCaseType Response JSON: " + json);
				response.setContentType("application/json");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().write(json);
			}
		}
		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
		

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public CCMSEventWorkflow translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSEventWorkflow eventWorkflow = new CCMSEventWorkflow(0L, court);
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);
		

		// workflowOID will only be populated if this is an update
		String workflowOIDString = (String) jsonCaseObject
				.get("workflowOID");
		if (workflowOIDString != null && !workflowOIDString.isEmpty())
		{
			eventWorkflow.setEventWorkflowOID(Long
					.parseLong(workflowOIDString));
		}


		String eventWorkflowOIDString = (String) jsonCaseObject
				.get("triggeringEventOID");
		if (eventWorkflowOIDString != null && !eventWorkflowOIDString.isEmpty())
		{
			CCMSEventType triggeringEvent = new CCMSEventType(Long.parseLong(eventWorkflowOIDString), court.getCourtOID());
			eventWorkflow.setTriggeringEvent(triggeringEvent);
		}

		String descriptionString = (String) jsonCaseObject.get("description");
		if (descriptionString != null && !descriptionString.isEmpty())
		{
			eventWorkflow.setDescription(descriptionString);
		}

		
		// Steps are always inserted so we can generate a new OID for each add OR update
		JSONArray workflowStepsArray = (JSONArray) jsonCaseObject
				.get("workflowSteps");
		Iterator<JSONObject> workflowStepsIterator = workflowStepsArray
				.iterator();
		while (workflowStepsIterator.hasNext())
		{
			CCMSWorkflowStep step = new CCMSWorkflowStep(CCMSDomainUtilities.getInstance().genOID(),
					court.getCourtOID());
			JSONObject workflowStepObject = (JSONObject) workflowStepsIterator
					.next();

			// Set the parent OID (if update). If this is a new workflow then in
			// the main
			// flow of the code we will need to set this to the newly assigned
			// workflow OID
			step.setWorkflowOID(eventWorkflow.getEventWorkflowOID());

			String delayDaysString = (String) workflowStepObject
					.get("delayDays");
			if (delayDaysString != null
					&& CCMSDomainUtilities.isNumber(delayDaysString))
			{
				step.setDelayDays(Integer.parseInt(delayDaysString));
			}

			String taskTypeOIDString = (String) workflowStepObject
					.get("taskTypeOID");
			if (taskTypeOIDString != null
					&& CCMSDomainUtilities.isNumber(taskTypeOIDString))
			{
				CCMSTaskType taskType = new CCMSTaskType(
						Long.parseLong(taskTypeOIDString), court.getCourtOID());
				step.setTaskType(taskType);
			}

			String assignedPartyOIDString = (String) workflowStepObject
					.get("assignedPartyOID");
			if (assignedPartyOIDString != null
					&& CCMSDomainUtilities.isNumber(assignedPartyOIDString))
			{
				CCMSParty assignedParty = new CCMSParty(
						Long.parseLong(assignedPartyOIDString),
						court.getCourtOID());
				step.setAssignedParty(assignedParty);
			}

			String assignedPoolOIDString = (String) workflowStepObject
					.get("assignedPoolOID");
			if (assignedPoolOIDString != null
					&& CCMSDomainUtilities.isNumber(assignedPoolOIDString))
			{
				CCMSStaffPool assignedPool = new CCMSStaffPool(
						Long.parseLong(assignedPoolOIDString),
						court.getCourtOID());
				step.setAssignedPool(assignedPool);
			}
			

			String documentTemplateOIDString = (String) workflowStepObject
					.get("documentTemplateOID");
			if (documentTemplateOIDString != null
					&& CCMSDomainUtilities.isNumber(documentTemplateOIDString))
			{
				long documentTemplateOID = Long.parseLong(documentTemplateOIDString);
				step.setDocumentTemplateOID(documentTemplateOID);
			}
			
			eventWorkflow.getWorkflowSteps().add(step);
		}

		return eventWorkflow;
	}

	public static void main(String[] args)
	{
		String jsonString = "{\"workflowSteps\":[{\"delayDays\":\"1\",\"taskTypeOID\":\"2\",\"assignedPartyOID\":\"2\"}],\"triggeringEventOID\":\"1\",\"description\":\"This is a description\"}";
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			SaveEventWorkflow saveEvent = new SaveEventWorkflow();
			CCMSEventWorkflow workflow = saveEvent.translateJSONToTaskObject(jsonString, court);


			// Test Insert
			workflow.setEventWorkflowOID(CCMSDomainUtilities.getInstance().genOID());
			for ( CCMSWorkflowStep step : workflow.getWorkflowSteps())
			{
				step.setWorkflowOID(workflow.getEventWorkflowOID());
			}
			List<PreparedStatement> pStmts = workflow.genInsertSQL(conn);
			for (PreparedStatement pStmt : pStmts)
			{
				pStmt.execute();
				pStmt.close();
			}

			conn.commit();

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
