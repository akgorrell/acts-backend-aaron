package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSHearingType;
import org.ncsc.ccms.domain.CourtLookupTable;

import com.google.gson.Gson;

public class SaveHearingType extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveHearingType()
	{
		super();
		SaveHearingType.logger = LogManager
				.getLogger(SaveHearingType.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Connection conn = null;
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Received: " + clientJSON);
			CCMSHearingType hearingType = translateJSONToTaskObject(clientJSON, court);

			if (hearingType.getHearingTypeOID() != 0L)
			{
				// Update
				PreparedStatement updateStmt = hearingType.genUpdateSQL(conn).get(
						0);
				updateStmt.execute();
				conn.commit();
				updateStmt.close();
			}
			else
			{
				// Insert new type
				hearingType.setHearingTypeOID(CCMSDomainUtilities.getInstance()
						.genOID());
				PreparedStatement insertStmt = hearingType.genInsertSQL(conn).get(
						0);
				insertStmt.execute();
				conn.commit();
				insertStmt.close();
			}
			

			CourtLookupTable lookupTable = CCMSDomainUtilities.getInstance()
					.getLookupTableForCourt(court);
			lookupTable.updateHearingTypes(conn);
			
			hearingType = (CCMSHearingType)CCMSDomainUtilities.getInstance().retrieveFullObject(hearingType, court);
			
			List<CCMSHearingType> hearingTypes = new ArrayList<CCMSHearingType>();
			hearingTypes.add(hearingType);
			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(hearingTypes);
			logger.debug("SaveCaseType Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			try
			{
				conn.rollback();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}
	


	public CCMSHearingType translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSHearingType hearingType = new CCMSHearingType(0L, court.getCourtOID());
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		// casePhaseOID will only be populated if this is an update
		String hearingTypeOIDString = (String) jsonCaseObject.get("hearingTypeOID");
		if (hearingTypeOIDString != null && !hearingTypeOIDString.isEmpty())
		{
			hearingType.setHearingTypeOID(Long.parseLong(hearingTypeOIDString));
		}
	

		String hearingName = (String) jsonCaseObject.get("hearingName");
		if (hearingName != null && !hearingName.isEmpty())
		{
			hearingType.setHearingName(hearingName);
		}
		

		String description = (String) jsonCaseObject.get("description");
		if (description != null && !description.isEmpty())
		{
			hearingType.setDescription(description);
		}
		
		String durationInMinutesString = (String) jsonCaseObject.get("durationInMinutes");
		if (durationInMinutesString != null && !durationInMinutesString.isEmpty())
		{
			hearingType.setDurationInMinutes(Integer.parseInt(durationInMinutesString));
		}
		return hearingType;
	}


	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}
	
	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			
			CCMSHearingType hType = new CCMSHearingType(9110827661328076L, court.getCourtOID());
			hType.setHearingName("Test 2");
			
			PreparedStatement stmt = hType.genUpdateSQL(conn).get(0);
			stmt.execute();
			conn.commit();
			stmt.close();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
