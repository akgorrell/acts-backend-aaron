package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCaseJudicialAssignment;
import org.ncsc.ccms.domain.CCMSCaseType;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPermission;

import com.google.gson.Gson;

public class SaveJudicialAssignment extends HttpServlet
{
	private static Logger logger = null;
	private static String UPDATE_JUDICIAL_OFFICER = "UPDJ";
	private static String CHANGE_JUDICIAL_OFFICER = "CHGJ";

	/**
	 * Constructor of the object.
	 */
	public SaveJudicialAssignment()
	{
		super();
		SaveJudicialAssignment.logger = LogManager
				.getLogger(SaveJudicialAssignment.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Connection conn = null;
		String token = request.getHeader("token");

		CCMSParty actionParty = (CCMSParty) request.getSession().getAttribute(
				"user");

		if (actionParty == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Invalid User");
		}

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();

			// Get the user requesting the change
			String auth = request.getHeader("Authorization");
			// CCMSParty eventParty =
			// CCMSDomainUtilities.getInstance().parseLoginToken(
			// auth);
			// eventParty =
			// CCMSDomainUtilities.getInstance().getParty(eventParty, conn);

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Received: " + clientJSON);
			CCMSCaseJudicialAssignment judAssgnmt = translateJSONToTaskObject(
					clientJSON, court);

			if (judAssgnmt.getJudicialAssignmentOID() != 0L)
			{
				if (!actionParty.isAuthorized(CCMSPermission.UPDATE_JUD_ASSGMT, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to update judicial assignment.");
				}
				else
				{
					// Update
					PreparedStatement updateStmt = judAssgnmt
							.genUpdateSQL(conn).get(0);
					updateStmt.execute();
					conn.commit();
					updateStmt.close();

					// Create new event
					CCMSCase myCase = new CCMSCase(judAssgnmt.getCaseOID(),
							court);
					CCMSEventType eventType = new CCMSEventType(0L,
							court.getCourtOID());
					eventType
							.setEventTypeName(SaveJudicialAssignment.UPDATE_JUDICIAL_OFFICER);
					eventType = (CCMSEventType) CCMSDomainUtilities
							.getInstance().retrieveFullObject(eventType, court);
					CCMSDomainUtilities.getInstance().triggerEvent(null,
							myCase, eventType, actionParty, court, conn, "");
				}

			}
			else
			{

				if (!actionParty.isAuthorized(CCMSPermission.CREATE_JUD_ASSGMT, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to create judicial assignment.");
				}
				else
				{
					// Insert new assignment
					judAssgnmt.setJudicialAssignmentOID(CCMSDomainUtilities
							.getInstance().genOID());
					PreparedStatement insertStmt = judAssgnmt
							.genInsertSQL(conn).get(0);
					insertStmt.execute();
					conn.commit();
					insertStmt.close();

					// Create new event
					CCMSCase myCase = new CCMSCase(judAssgnmt.getCaseOID(),
							court);
					CCMSEventType eventType = new CCMSEventType(0L,
							court.getCourtOID());
					eventType
							.setEventTypeName(SaveJudicialAssignment.CHANGE_JUDICIAL_OFFICER);
					eventType = (CCMSEventType) CCMSDomainUtilities
							.getInstance().retrieveFullObject(eventType, court);
					CCMSDomainUtilities.getInstance().triggerEvent(null,
							myCase, eventType, actionParty, court, conn, "");
				}
			}

			// Retrieve the full party within the judicial assignment so the UI
			// has the full name
			CCMSCaseJudicialAssignment qAssgnmt = new CCMSCaseJudicialAssignment(
					judAssgnmt.getJudicialAssignmentOID(),
					judAssgnmt.getCourtOID());
			PreparedStatement stmt2 = qAssgnmt.genSelectSQL(conn).get(0);
			ResultSet rs2 = stmt2.executeQuery();
			while (rs2.next())
			{
				qAssgnmt = new CCMSCaseJudicialAssignment(rs2, conn);
			}
			stmt2.close();
			rs2.close();

			List<CCMSCaseJudicialAssignment> assignments = new ArrayList<CCMSCaseJudicialAssignment>();
			assignments.add(qAssgnmt);

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(assignments);
			logger.debug("SaveJudicialAssignment Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			try
			{
				conn.rollback();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}

	public CCMSCaseJudicialAssignment translateJSONToTaskObject(
			String taskJSON, CCMSCourt court) throws Exception
	{
		CCMSCaseJudicialAssignment judicialAssignment = new CCMSCaseJudicialAssignment(
				0L, court.getCourtOID());
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		String judicialAssignmentOIDString = (String) jsonCaseObject
				.get("judicialAssignmentOID");
		if (judicialAssignmentOIDString != null
				&& !judicialAssignmentOIDString.isEmpty())
		{
			judicialAssignment.setJudicialAssignmentOID(Long
					.parseLong(judicialAssignmentOIDString));
		}

		String caseOIDString = (String) jsonCaseObject.get("caseOID");
		if (caseOIDString != null && !caseOIDString.isEmpty())
		{
			judicialAssignment.setCaseOID(Long.parseLong(caseOIDString));
		}

		String partyOIDString = (String) jsonCaseObject.get("partyOID");
		if (partyOIDString != null && !partyOIDString.isEmpty())
		{
			CCMSParty party = new CCMSParty(Long.parseLong(partyOIDString));
			judicialAssignment.setJudicialOfficial(party);
		}

		String startDateString = (String) jsonCaseObject.get("startDate");
		if (startDateString != null && !startDateString.isEmpty())
		{
			Date startDate = CCMSDomainUtilities.getInstance()
					.convertStringToDate(startDateString);
			judicialAssignment.setStartDate(startDate);
		}

		String endDateString = (String) jsonCaseObject.get("endDate");
		if (endDateString != null && !endDateString.isEmpty())
		{
			Date endDateTime = CCMSDomainUtilities.getInstance()
					.convertStringToDate(endDateString);
			judicialAssignment.setEndDate(endDateTime);
		}

		return judicialAssignment;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
