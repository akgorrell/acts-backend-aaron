package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSICCSCode;

import com.google.gson.Gson;

public class FetchICCSCodeParent extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchICCSCodeParent()
	{
		super();
		FetchICCSCodeParent.logger = LogManager
				.getLogger(FetchICCSCodeParent.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Gson myGson = new Gson();
		String responseMessage = "";
		CCMSCase newCase = null;
		Connection conn = null;

		CCMSCourt court = (CCMSCourt) request.getSession()
				.getAttribute("court");

		try
		{
			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();

			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);

			JSONParser parser = new JSONParser();
			JSONObject jsonCaseObject = (JSONObject) parser.parse(clientJSON);

			// Retrieve the child category type
			String childCategoryTypeString = (String) jsonCaseObject
					.get("categoryType");
			int childCategoryType = Integer.parseInt(childCategoryTypeString);
			List<CCMSICCSCode> iccsParentCodes = this.retrieveParents(conn,
					court, childCategoryType);

			responseMessage = myGson.toJson(iccsParentCodes);
			

			logger.debug("Servlet Returning: " + responseMessage);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseMessage);

		}
		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}

	}

	private List<CCMSICCSCode> retrieveParents(Connection conn,
			CCMSCourt court, int childCategoryType) throws Exception
	{
		List<CCMSICCSCode> iccsParentCodes = new ArrayList<CCMSICCSCode>();

		if (childCategoryType > 1)
		{
			// Do not include highest level of section since it doesn't have
			// parents
			int parentCategoryType = childCategoryType - 1;
			CCMSICCSCode parentType = new CCMSICCSCode(0L, court.getCourtOID());
			parentType.setCategoryType(parentCategoryType);
			PreparedStatement stmt = parentType.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				iccsParentCodes.add(new CCMSICCSCode(rs, conn));
			}
			stmt.close();
		}
		return iccsParentCodes;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);

			FetchICCSCodeParent fetcher = new FetchICCSCodeParent();
			List<CCMSICCSCode> list = fetcher.retrieveParents(conn, court, 1);
			System.out.println("list: " + list);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
