package org.ncsc.ccms.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.openkm.sdk4j.OKMWebservices;
import com.openkm.sdk4j.OKMWebservicesFactory;
import com.openkm.sdk4j.bean.Folder;
import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.exception.PathNotFoundException;

/**
 * Servlet implementation class GetDocument
 */
// @WebServlet("/GetDocument")
public class GetDocument extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public static String DOC_GEN_EVENT_NAME = "DOCG";
	private static Logger logger = null;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public GetDocument()
	{
		super();
		GetDocument.logger = LogManager.getLogger(GetDocument.class.getName());
		// TODO Auto-generated constructor stub
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * /**
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		ServletOutputStream stream = null;
		ServletContext context = getServletContext();
		Connection conn = null;
		BufferedReader reader = request.getReader();
		String clientJSON = CCMSDomainUtilities.getInstance().fetchJSONString(
				reader);
		CCMSCourt court = (CCMSCourt) request.getSession()
				.getAttribute("court");
		// call to GetDocument looks like:
		// http://<serverURL>/GetDocument?<OpenKM file path>
		String docQuery = request.getQueryString();
		if (docQuery != "")
		{
			try
			{
				String auth = request.getHeader("Authorization");

				conn = CCMSDomainUtilities.getInstance().connect();
				// CCMSParty authParty =
				// CCMSDomainUtilities.getInstance().parseJWT(auth);
				// authParty =
				// CCMSDomainUtilities.getInstance().getParty(authParty, conn);
				// if (!clientJSON.isEmpty())
				// {
				String[] fileNameParts = docQuery.split("/");
				String fileName = fileNameParts[fileNameParts.length - 1];
				ServletOutputStream odoc = response.getOutputStream();
				response.setContentType("application/msword");
				response.addHeader("Content-Disposition",
						"attachment; filename=" + fileName);
				OKMWebservices okm = CCMSDomainUtilities.getInstance()
						.connOpenKM();
				InputStream idoc = okm.getContent(docQuery);
				IOUtils.copy(idoc, odoc);
				IOUtils.closeQuietly(idoc);
				IOUtils.closeQuietly(odoc);
				// }
			}

			catch (Exception ioe)
			{
				logger.error(ioe);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException
	{
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
