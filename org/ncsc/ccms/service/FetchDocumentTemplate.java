package org.ncsc.ccms.service;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDocumentTemplate;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventWorkflow;

import com.google.gson.Gson;

public class FetchDocumentTemplate extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchDocumentTemplate()
	{
		super();
		FetchDocumentTemplate.logger = LogManager
				.getLogger(FetchDocumentTemplate.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;

		String token = request.getHeader("token");

		List<CCMSDocumentTemplate> templates = new ArrayList<CCMSDocumentTemplate>();

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			CCMSDocumentTemplate template = new CCMSDocumentTemplate(0L,
					court.getCourtOID());
			PreparedStatement stmt = template.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				templates.add(new CCMSDocumentTemplate(rs, conn));
			}
			stmt.close();
			rs.close();
			

			Gson myGson = new Gson();
			String json = myGson.toJson(templates);
			logger.debug("FetchDocumentTemplate Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);

			List<CCMSDocumentTemplate> templates = new ArrayList<CCMSDocumentTemplate>();
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSDocumentTemplate template = new CCMSDocumentTemplate(0L,
					court.getCourtOID());
			PreparedStatement stmt = template.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				templates.add(new CCMSDocumentTemplate(rs, conn));
			}
			stmt.close();
			rs.close();
		}
		catch (Exception ex)
		{
			logger.error(ex);
		}
	}

}
