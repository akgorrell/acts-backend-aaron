package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSStaffPool;

import com.google.gson.Gson;

public class FetchStaffPool extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchStaffPool()
	{
		super();
		FetchStaffPool.logger = LogManager.getLogger(FetchStaffPool.class
				.getName());

	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String json;
		List<CCMSStaffPool> staffPools = new ArrayList<CCMSStaffPool>();
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			// Retrieve the Staff Pools
			CCMSStaffPool qPool = new CCMSStaffPool(0L, court.getCourtOID());
			PreparedStatement stmt = qPool.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				CCMSStaffPool pool = new CCMSStaffPool(rs, conn);
				staffPools.add(pool);
			}
			

			Gson myGson = new Gson();
			json = myGson.toJson(staffPools);
			logger.debug(json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
