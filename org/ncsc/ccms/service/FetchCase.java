package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCaseStatus;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCaseType;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;


public class FetchCase extends HttpServlet
{

	private static Logger logger = null;

	public FetchCase()
	{
		super();
		FetchCase.logger = LogManager.getLogger(FetchCase.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}
	
	public static void main(String args[])
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			//String clientJSON = "{\"caseNumber\":\"21\"}";
			String clientJSON = "{\"caseOID\":\"9143873094336458\"}";
			FetchCase fCase = new FetchCase();
			List courtCases = fCase.retrieveCase(clientJSON, court);
		}
		catch ( Exception e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		
		String json;

		String token = request.getHeader("token");


		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance().getSessionCourtObject(request.getSession(), token);

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);
			
			List<CCMSCase> courtCases = this.retrieveCase(clientJSON, court);

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			json = myGson.toJson(courtCases);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}
	
	private List<CCMSCase> retrieveCase(String clientJSON, CCMSCourt court) throws Exception
	{
		boolean retrieveCaseSummaryFlag = true;
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(clientJSON);
		JSONObject jsonObject = (JSONObject) obj;

		String caseNumber = null;
		CCMSCase qCase = new CCMSCase(0L, court);
		
		if (jsonObject.containsKey("caseNumber"))
		{
			caseNumber = (String) jsonObject.get("caseNumber");
			qCase.setCaseNumber(caseNumber);
			logger.debug("Query on caseNumber: " + caseNumber);
		}
		if ( jsonObject.containsKey("caseOID"))
		{
			// Case OID is primarily called when the user is retrieving the case details
			// retrieve the full object
			retrieveCaseSummaryFlag = false;
			String caseOIDString = (String)jsonObject.get("caseOID");
			if ( CCMSDomainUtilities.getInstance().isNumber(caseOIDString))
			{
				qCase.setCaseOID(Long.parseLong(caseOIDString));
			}
		}

		String casePartyName = null;
		if (jsonObject.containsKey("casePartyName"))
		{
			casePartyName = (String) jsonObject.get("casePartyName");
			qCase.setSearchCasePartyName(casePartyName);
			logger.debug("Query on casePartyName: " + caseNumber);
		}
		
		if ( jsonObject.containsKey("casePartyRoleOID"))
		{
			String searchCasePartyRoleOID = (String)jsonObject.get("casePartyRoleOID");
			if ( !searchCasePartyRoleOID.isEmpty() )
			{
				qCase.setSearchCasePartyRoleOID(Long.parseLong(searchCasePartyRoleOID));
			}
		}
		
		if ( jsonObject.containsKey("caseType"))
		{
			String caseTypeOIDString = (String)jsonObject.get("caseType");
			CCMSCaseType caseType = new CCMSCaseType(Long.parseLong(caseTypeOIDString), court.getCourtOID());
			qCase.setCaseType(caseType);
		}
		
		if ( jsonObject.containsKey("casePhase"))
		{
			String casePhaseOIDString = (String)jsonObject.get("casePhase");
			CCMSCasePhase casePhase = new CCMSCasePhase(Long.parseLong(casePhaseOIDString), court.getCourtOID());
			qCase.setCasePhase(casePhase);
		}
		
		
		if ( jsonObject.containsKey("caseStatus"))
		{
			String caseStatusOIDString = (String)jsonObject.get("caseStatus");
			CCMSCaseStatus status = new CCMSCaseStatus(Long.parseLong(caseStatusOIDString), court.getCourtOID());
			qCase.setCaseStatus(status);
		}

		List<CCMSCase> courtCases = new ArrayList<CCMSCase>();
		Connection conn = CCMSDomainUtilities.getInstance().connect();
		List<PreparedStatement> pStmts = qCase.genSelectSQL(conn);
		logger.debug("Query Statements: " + pStmts.size());
		for (PreparedStatement pStmt : pStmts)
		{
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSCase dbCase = new CCMSCase(rs, conn, court, retrieveCaseSummaryFlag);
				courtCases.add(dbCase);
				logger.debug("Matching Case: " + dbCase);
			}
			pStmt.close();
			rs.close();
		}
		return courtCases;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
