package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCasePartyRole;
import org.ncsc.ccms.domain.CCMSCasePhase;
import org.ncsc.ccms.domain.CCMSCaseType;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CourtLookupTable;

import com.google.gson.Gson;

public class SaveCasePhase extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveCasePhase()
	{
		super();
		SaveCasePhase.logger = LogManager
				.getLogger(SaveCasePhase.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Connection conn = null;
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Received: " + clientJSON);
			CCMSCasePhase casePhase = translateJSONToTaskObject(clientJSON, court);

			if (casePhase.getCasePhaseOID() != 0L)
			{
				// Update
				PreparedStatement updateStmt = casePhase.genUpdateSQL(conn).get(
						0);
				updateStmt.execute();
				conn.commit();
				updateStmt.close();
			}
			else
			{
				// Insert new type
				casePhase.setCasePhaseOID(CCMSDomainUtilities.getInstance()
						.genOID());
				PreparedStatement insertStmt = casePhase.genInsertSQL(conn).get(
						0);
				insertStmt.execute();
				conn.commit();
				insertStmt.close();
			}
			

			CourtLookupTable lookupTable = CCMSDomainUtilities.getInstance()
					.getLookupTableForCourt(court);
			lookupTable.updateCasePhases(conn);
			

			List<CCMSCasePhase> casePhases = new ArrayList<CCMSCasePhase>();
			casePhases.add(casePhase);

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			String json = myGson.toJson(casePhases);
			logger.debug("SaveCaseType Response JSON: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			try
			{
				conn.rollback();
			}
			catch (SQLException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}
	


	public CCMSCasePhase translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSCasePhase casePhase = new CCMSCasePhase(0L, court.getCourtOID());
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		// casePhaseOID will only be populated if this is an update
		String casePhaseOIDString = (String) jsonCaseObject.get("casePhaseOID");
		if (casePhaseOIDString != null && !casePhaseOIDString.isEmpty())
		{
			casePhase.setCasePhaseOID(Long.parseLong(casePhaseOIDString));
		}
		

		// casePhaseOID will only be populated if this is an update
		String caseTypeOIDString = (String) jsonCaseObject.get("caseTypeOID");
		if (caseTypeOIDString != null && !caseTypeOIDString.isEmpty())
		{
			casePhase.setCaseTypeOID(Long.parseLong(caseTypeOIDString));
		}

		String name = (String) jsonCaseObject.get("name");
		if (name != null && !name.isEmpty())
		{
			casePhase.setName(name);
		}
		

		String description = (String) jsonCaseObject.get("description");
		if (description != null && !description.isEmpty())
		{
			casePhase.setDescription(description);
		}
		return casePhase;
	}


	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}
	


	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			
			CCMSCasePhase casePhase = new CCMSCasePhase(0, court.getCourtOID());
			casePhase.setCaseTypeOID(1);
			casePhase.setCasePhaseOID(2858265791136204L);
			casePhase.setName("PhaseTest2");
			

			if (casePhase.getCasePhaseOID() != 0L)
			{
				// Update
				PreparedStatement updateStmt = casePhase.genUpdateSQL(conn).get(
						0);
				updateStmt.execute();
				conn.commit();
				updateStmt.close();
			}
			else
			{
				// Insert new type
				casePhase.setCasePhaseOID(CCMSDomainUtilities.getInstance()
						.genOID());
				PreparedStatement insertStmt = casePhase.genInsertSQL(conn).get(
						0);
				insertStmt.execute();
				conn.commit();
				insertStmt.close();
			}
			
			
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}
