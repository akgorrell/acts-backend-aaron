package org.ncsc.ccms.service;

import java.io.BufferedInputStream;
import java.io.BufferedReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.io.*;
import java.util.*;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Date;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseCharge;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCaseEvent;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDocumentTemplate;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSEventType;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSCaseParty;
import org.ncsc.ccms.domain.CCMSICCSCode;
import org.ncsc.ccms.domain.CCMSPartyAddress;
import org.ncsc.ccms.domain.CCMSPartyIdentifier;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.openxml4j.opc.OPCPackage;

public class GenerateCourtDocument extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public GenerateCourtDocument()
	{
		super();
		GenerateCourtDocument.logger = LogManager
				.getLogger(GenerateCourtDocument.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		BufferedInputStream buf = null;
		ServletOutputStream stream = null;
		ServletContext context = getServletContext();
		Connection conn = null;
		BufferedReader reader = request.getReader();
		String clientJSON = CCMSDomainUtilities.getInstance().fetchJSONString(
				reader);
		CCMSParty authParty = null;
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			String auth = request.getHeader("Authorization");

			conn = CCMSDomainUtilities.getInstance().connect();

			// Looking for a non null length on the authorization string. By
			// default, even if blank, will come back prepended with "Bearer..."
			if (auth != null && auth.length() > 15)
			{
				authParty = CCMSDomainUtilities.getInstance().parseLoginToken(
						auth);
				authParty = CCMSDomainUtilities.getInstance().getParty(
						authParty, conn);
			}

			if (!clientJSON.isEmpty())
			{
				this.generateCourtDocument(authParty, clientJSON, court, conn,
						context, response, buf, stream);
			}
		}

		catch (Exception ex)
		{
			logger.error(ex);
			ex.printStackTrace();
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
		finally
		{
			if (stream != null)
				stream.close();
			if (buf != null)
				buf.close();
		}

	}

	private static String ip2code(String reqURL)
	{
		List<String> matches = new ArrayList<String>();
		String encURL = "";
		Pattern pat = Pattern
				.compile("http(s?):\\/\\/([A-Za-z0-9\\._-]+)(:8080)?(\\/([a-zA-Z0-9_-]*)\\/?)?");
		Matcher m = pat.matcher(reqURL);
		int last;
		if (m.find())
		{
			if (m.group(1) != null)
			{
				encURL = ((m.group(1).equals("s")) ? "^s" : "^h");
			}
			if (m.group(2) != null && !m.group(2).isEmpty())
			{
				encURL = encURL + m.group(2);
			}
			if (m.group(3) != null && !m.group(3).isEmpty())
			{
				encURL = encURL + ((m.group(3).equals(":8080")) ? "^l" : "");
			}
			if (m.group(4) != null && !m.group(4).isEmpty())
			{
				encURL = encURL + (((m.group(4)).replace("/", "^b")));
			}
		}
		return encURL;
	}

	/**
	 * @param authParty
	 * @param clientJSON
	 * @param court
	 * @param conn
	 * @param context
	 * @param response
	 * @param buf
	 * @param stream
	 * @throws Exception
	 */
	@SuppressWarnings("deprecation")
	protected void generateCourtDocument(CCMSParty authParty,
			String clientJSON, CCMSCourt court, Connection conn,
			ServletContext context, HttpServletResponse response,
			BufferedInputStream buf, ServletOutputStream stream)
			throws Exception
	{
		// where all the substitutions go
		Map<String, String> myMap = new HashMap<String, String>();

		String[] niceDays = new String[]
		{ "0th", "1st", "2d", "3d", "4th", "5th", "6th", "7th", "8th", "9th",
				"10th", "11th", "12th", "13th", "14th", "15th", "16th", "17th",
				"18th", "19th", "20th", "21st", "22d", "23d", "24th", "25th",
				"26th", "27th", "28th", "29th", "30th", "31st" };

		Calendar mCalendar = Calendar.getInstance();
		String month = mCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG,
				Locale.getDefault());
		String day = niceDays[mCalendar.get(Calendar.DAY_OF_MONTH)];
		String year = Integer.toString(mCalendar.get(Calendar.YEAR) - 2000);

		myMap.put("month", month);
		myMap.put("day", day);
		myMap.put("year", year);

		stream = response.getOutputStream();
		conn = CCMSDomainUtilities.getInstance().connect();
		JSONParser parser = new JSONParser();
		Object obj = parser.parse(clientJSON);
		JSONObject jsonObject = (JSONObject) obj;

		// Receive the Case OID and the Requested Document
		String caseOIDString = (String) jsonObject.get("caseOID");
		String documentTemplateOIDString = (String) jsonObject
				.get("documentTemplateOID");

		if (!caseOIDString.isEmpty() && !documentTemplateOIDString.isEmpty())
		{
			long caseOID = Long.parseLong(caseOIDString);
			long documentTemplateOID = Long
					.parseLong(documentTemplateOIDString);
			// Retrieve the associated case
			CCMSCase courtCase = new CCMSCase(caseOID, court);
			PreparedStatement stmt = courtCase.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				courtCase = new CCMSCase(rs, conn, court, false);
			}
			stmt.close();
			rs.close();

			// now that we have a courtCase, we also have a caseNumber for myMap
			// and with a little work, the case Charges. Like dates, these are
			// on all templates.
			myMap.put("case_number", courtCase.getCaseNumber());
			String theCharges = "";
			CCMSICCSCode theICCSCode = null;
			List<CCMSCaseCharge> caseCharges = courtCase.getCaseCharges();
			for (CCMSCaseCharge aCaseCharge : caseCharges)
			{
				theICCSCode = aCaseCharge.getIccsCode();
				theCharges = theCharges + theICCSCode.getCategoryIdentifier()
						+ " " + theICCSCode.getCategoryName() + "\n";
			}
			myMap.put("charge_text", theCharges);
			myMap.put("charge", theCharges);

			// Retrieve the substition data from the courtCase object;
			// NOTE: this is the 'simple' version that only gets a limited
			// number of fields
			// NOTE !!! this only handles real names, I need a way to handle
			// Anames.
			List<CCMSCaseParty> caseParties = courtCase.getCaseParties();
			String defendantName = null;
			String defendantFname = null;
			String defendantLname = null;
			String defendantAddress = null;
			String defendantGender = null;
			String defendantCUID = null;
			java.util.Date defendantDOB = null;
			Date today = Calendar.getInstance().getTime();

			for (CCMSCaseParty aCaseParty : caseParties)
			{
				if (aCaseParty.getRole().getName()
						.equalsIgnoreCase("defendant")
						|| aCaseParty.getRole().getName()
								.equalsIgnoreCase("child"))
				{
					CCMSParty defendantParty = aCaseParty.getParty();
					// get the basic party information: name, DOB
					defendantFname = defendantParty.getFirstName();
					defendantLname = defendantParty.getLastName();
					defendantDOB = defendantParty.getDob();
					defendantName = defendantParty.getFirstName() + " "
							+ defendantParty.getLastName();
					// then get the address of party, if any
					ArrayList<CCMSPartyAddress> defendantAddresses = defendantParty
							.getAddresses();
					// aP is short for aPartyAddress ... a code ormating kluge
					for (CCMSPartyAddress aP : defendantAddresses)
					{
						if ((aP.getEndDate() == null || aP.getEndDate()
								.compareTo(today) > 0)
								&& (aP.getAddressType().equalsIgnoreCase(
										"primary") || aP.getAddressType()
										.equalsIgnoreCase("personal")))
						{ // we have the primary address ... make an address
							// block
							defendantAddress = (aP.getAddress1() != null ? aP
									.getAddress1() : "")
									+ (aP.getAddress2() != null ? "\n"
											+ aP.getAddress2() : "")
									+ (aP.getAddress3() != null ? "\n"
											+ aP.getAddress3() : "")
									+ (aP.getMunicipalityName() != null ? "\n"
											+ aP.getMunicipalityName() : "")
									+ (aP.getPostalCode() != null ? "   "
											+ aP.getPostalCode() : "")
									+ (aP.getCountryName() != null ? "\n"
											+ aP.getCountryName() : "");
							// try to get primary, if possible
							if (aP.getAddressType().equalsIgnoreCase("primary"))
							{
								break;
							}
						}
					}
					// now get he CUID - the child's unique identifer, if any
					ArrayList<CCMSPartyIdentifier> defendantIdentifier = defendantParty
							.getIdentifiers();
					// aI is short for partyIdentifier
					for (CCMSPartyIdentifier aI : defendantIdentifier)
					{
						if ((aI.getEndDate() == null || aI.getEndDate()
								.compareTo(today) > 0)
								&& aI.getIdentifierType().equalsIgnoreCase(
										"Child Unique Identifier"))
						{
							defendantCUID = aI.getIdentifierValue();
							break;
						}
					}
					// before we leave party, get gender
					defendantGender = defendantParty.getSex() != null ? defendantParty
							.getSex() : "";
					// done with the party defendant / child
					break;
				}
			}
			myMap.put("address", defendantAddress);
			myMap.put("child", defendantName);
			myMap.put("childname", defendantName);
			myMap.put("childnameFname", defendantFname);
			myMap.put("childnameLname", defendantLname);

			myMap.put("defendant", defendantName);
			myMap.put("defendantFname", defendantFname);
			myMap.put("defendantLname", defendantLname);
			myMap.put("childId", defendantCUID);
			myMap.put("childID", defendantCUID);
			myMap.put("gender", defendantGender);
			SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd");

			if (defendantDOB != null)
			{
				String sDOB = ft.format(defendantDOB);
				// String sNow = LocalDate.now().toString();
				// LocalDate lDOB = LocalDate.parse(sDOB);
				// LocalDate lNow = LocalDate.parse(sNow);
				// String Age =
				// Integer.toString(Period.between(lDOB,lNow).getYears());
				String DOB = defendantDOB.toString();
				// LocalDate now = LocalDate.now();
				// String Age =
				// Integer.toString(Period.between(ldDOB,now).getYears());
				long ageInMillis = new Date().getTime()
						- defendantDOB.getTime();
				Date age = new Date(ageInMillis);
				

				myMap.put("AGE", Integer.toString(age.getYear()));
				myMap.put("DOB", DOB);
			}
			else
			{

				myMap.put("AGE", "");
				myMap.put("DOB", "");
			}


			// Retrieve the associated template

			CCMSDocumentTemplate template = new CCMSDocumentTemplate(
					documentTemplateOID, court.getCourtOID());
			stmt = template.genSelectSQL(conn).get(0);
			rs = stmt.executeQuery();
			while (rs.next())
			{
				template = new CCMSDocumentTemplate(rs, conn);
			}

			/*
			 * Method method =
			 * CCMSDomainUtilities.getNoArgumentMethod(courtCase,
			 * "getCaseNumber"); if ( method != null ) { String caseNumber =
			 * (String)method.invoke(courtCase); System.out.println(caseNumber);
			 * }
			 */

			// Retrieve the template and save it with a file name that
			// indicates the case number and date
			// String fileDir = context.getRealPath("/");
			String fileDir = "/var/CCMS3";
			logger.debug("GenerateCourtDocument file location: " + fileDir);
			if (!fileDir.endsWith("/"))
			{
				fileDir = fileDir + "/";
			}

			File doc = new File(fileDir + "templates/" + template.getFileName());
			FileInputStream input = new FileInputStream(doc);
			buf = new BufferedInputStream(input);

			// Determine the file name that includes the case number and current
			// date
			// The file name consists of:
			// <doc name>.<case no>;<return URL>;<hex courtOID>.<hex
			// timestamp>.<hex-docCode>.<extension>
			String strURL = CCMSDomainUtilities.getInstance().getProperties()
					.getProperty("SERVER_URL");
			String urlCode = ip2code(strURL);
			// And in case anyone forget to set SERVER_NAME ...
			if (urlCode == "")
			{
				response.sendError(505);
			}
			Timestamp tsNow = new Timestamp(System.currentTimeMillis());
			// clear out the fractional part since mySQL does not use it
			tsNow.setNanos(0);
			String hexNow = Long.toHexString(tsNow.getTime());
			long cOID = court.getCourtOID();
			String hexCourt = Long.toHexString(cOID);
			Long ranCode = ThreadLocalRandom.current().nextLong(0, 0xFFFFFF);
			String ranCodeStr = Long.toHexString(ranCode);
			int dot = template.getFileName().lastIndexOf('.');
			String extension = (dot == -1) ? "" : template.getFileName()
					.substring(dot + 1);
			String documentName = template.getDocumentName();
			String returnedFileName = documentName + ";"
					+ courtCase.getCaseNumber() + ";" + urlCode + ";"
					+ hexCourt + "." + hexNow + "." + ranCodeStr + "."
					+ extension;

			// Generate Case Document for Event
			CCMSCaseDocument caseDoc = new CCMSCaseDocument(CCMSDomainUtilities
					.getInstance().genOID(), court.getCourtOID());
			caseDoc.setCaseOID(courtCase.getCaseOID());
			caseDoc.setDocumentName(returnedFileName);
			caseDoc.setDocCode(ranCode);
			caseDoc.setDocSent(tsNow);
			caseDoc.setDocReceived(tsNow); // we will check to see if the
											// Received time is *after* DocSent
			PreparedStatement stmt2 = caseDoc.genInsertSQL(conn).get(0);
			stmt2.execute();
			stmt2.close();

			// Retrieve the event type object
			CCMSEventType eventType = new CCMSEventType(0L, court.getCourtOID());
			eventType.setEventTypeName(CCMSDomainUtilities.DOC_GEN_EVENT_NAME);
			eventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(eventType, court);

			// Generate Case Event Related to the Document Generation
			CCMSDomainUtilities.getInstance().triggerEvent(caseDoc, courtCase,
					eventType, authParty, court, conn, "");

			conn.commit();

			// Stream the document to the output stream
			stream = response.getOutputStream();
			response.setContentType("application/msword");
			response.addHeader("Content-Disposition", "attachment; filename="
					+ returnedFileName);
			// response.setContentLength((int) doc.length());
			// String nowDay = String nowYear =
			// Integer.toString(nowDateTime.getYear()-2000);
			// String nowMonth = months[nowDateTime.getMonthValue() - 1];
			// String nowYear = Integer.toString(nowDateTime.getYear() - 2000);
			// String nowDay = niceDays[nowDateTime.getDayOfMonth()];
			buf = new BufferedInputStream(input);
			replaceDoc(buf, myMap, stream);
			// replaceDoc(File inFile, Map<String, String> data, OutputStream
			// out)
			// int readBytes = 0;
			// while ((readBytes = buf.read()) != -1)
			// {
			// stream.write(readBytes);
			// }
		}
	}

	/*
	 * replaceDoc eats the input .docx file paragraph by paragraph and hands it
	 * off to RiP which does that actual replacing. We eat the file in units of
	 * paragraphs because 'runs' can not cross paragraphs
	 */
	protected void replaceDoc(BufferedInputStream inFile,
			Map<String, String> data, OutputStream out) throws Exception,
			IOException
	{
		XWPFDocument doc = new XWPFDocument(OPCPackage.open(inFile));
		for (XWPFParagraph p : doc.getParagraphs())
		{
			RiP(p, data);
		}
		doc.write(out);
		doc = null;
	}

	/*
	 * Replace in Paragraph (hence RiP) searches the paragraph looking for the
	 * substitution marker ${key}. Once it finds a substitution marker, it grabs
	 * the substitution key, looks it up its value in an associative array,
	 * replaces the substitution marker with the value and moves on. The
	 * complexity comes because Apache POI reads .docx files in units of 'runs'
	 * - fragments of text with the same style. Since even a single word can
	 * have different styles in parts of the work, of the same style, but
	 * applied at different times, the software must be able to accumulate runs
	 * into units that can be have substitution markers found and replace.
	 */
	protected void RiP(XWPFParagraph p, Map<String, String> data)
	{
		String pText = p.getText(); // complete paragraph as string
		if (pText.contains("${"))
		{ // if paragraph does not include our pattern, ignore
			TreeMap<Integer, XWPFRun> posRuns = getPosToRuns(p);
			Pattern pat = Pattern.compile("\\$\\{(.+?)\\}");
			Matcher m = pat.matcher(pText);
			while (m.find())
			{ // for all patterns in the paragraph
				String g = m.group(1); // extract key start and end pos
				int s = m.start(1);
				int e = m.end(1);
				String key = g;
				String x = data.get(key);
				if (x == null)
					x = "** Enter: " + key + " **";
				SortedMap<Integer, XWPFRun> range = posRuns.subMap(s - 2, true,
						e + 1, true); // get runs which contain the pattern
				boolean foundDoller = false; // found $
				boolean foundLBrace = false; // found {
				boolean foundRBrace = false; // found }
				XWPFRun prevRun = null; // previous run handled in the loop
				XWPFRun foundLBraceRun = null; // run in which { was found
				int foundLBracePos = -1; // pos of { within above run
				for (XWPFRun r : range.values())
				{
					if (r == prevRun)
						continue; // this run has already been handled
					if (foundRBrace)
						break; // done working on current key pattern
					prevRun = r;
					for (int k = 0;; k++)
					{ // iterate over texts of run r
						if (foundRBrace)
							break;
						String txt = null;
						try
						{
							txt = r.getText(k); // note: should return null, but
												// throws exception if the text
												// does not exist
						}
						catch (Exception ex)
						{

						}
						if (txt == null)
							break; // no more texts in the run, exit loop
						if (txt.contains("$") && !foundDoller)
						{ // found $, replace it with value from data map
							txt = txt.replaceFirst("\\$", x);
							foundDoller = true;
						}
						if (txt.contains("{") && !foundLBrace && foundDoller)
						{
							foundLBraceRun = r; // found { replace it with empty
												// string and remember location
							foundLBracePos = txt.indexOf('{');
							txt = txt.replaceFirst("\\{", "");
							foundLBrace = true;
						}
						if (foundDoller && foundLBrace && !foundRBrace)
						{ // find } and set all chars between { and } to blank
							if (txt.contains("}"))
							{
								if (r == foundLBraceRun)
								{ // complete pattern was within a single run
									txt = txt.substring(0, foundLBracePos)
											+ txt.substring(txt.indexOf('}'));
								}
								else
									// pattern spread across multiple runs
									txt = txt.substring(txt.indexOf('}'));
							}
							else if (r == foundLBraceRun) // same run as { but
															// no }, remove all
															// text starting at
															// {
								txt = txt.substring(0, foundLBracePos);
							else
								txt = ""; // run between { and }, set text to
											// blank
						}
						if (txt.contains("}") && !foundRBrace)
						{
							txt = txt.replaceFirst("\\}", "");
							foundRBrace = true;
						}
						r.setText(txt, k);
					}
				}
			}
			System.out.println(p.getText());
		}
	}

	private TreeMap<Integer, XWPFRun> getPosToRuns(final XWPFParagraph paragraph)
	{
		int pos = 0;
		TreeMap<Integer, XWPFRun> map = new TreeMap<Integer, XWPFRun>();
		for (XWPFRun run : paragraph.getRuns())
		{
			String runText = run.text();
			if (runText != null && runText.length() > 0)
			{
				for (int i = 0; i < runText.length(); i++)
				{
					map.put(pos + i, run);
				}
				pos += runText.length();
			}
		}
		return map;
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			GenerateCourtDocument gen = new GenerateCourtDocument();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
