package org.ncsc.ccms.service;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;



import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCaseStatus;import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDocumentTemplate;
import org.ncsc.ccms.domain.CCMSDomainUtilities;

import com.openkm.sdk4j.OKMWebservices;
import com.openkm.sdk4j.OKMWebservicesFactory;
import com.openkm.sdk4j.bean.Folder;
import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.exception.PathNotFoundException;
public class CollectSavedDocument extends HttpServlet
{

	private static Logger logger = null;
	/**
	 * Constructor of the object.
	 */
	public CollectSavedDocument()
	{
		super();
		CollectSavedDocument.logger = LogManager.getLogger(CollectSavedDocument.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 * This is a small change
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		Connection conn = null;
		String[] fnamParts = new String[4];
		String[] hexParts = new String[4];
//		String strURL = CCMSDomainUtilities.getInstance().getProperties().getProperty("SERVER_URL");


		// first parse the returned File-name:
		// documentName;CaseNumber;urlCode;hexCourt.hexNow.ranCodeStr.extension;
		String rawFileName = request.getHeader("File-name");
		fnamParts = rawFileName.split(";");
		if (fnamParts.length != 4) {
			throw new IllegalArgumentException("fileName parts invalid");}
		String fileName = fnamParts[0];
		String caseName = fnamParts[1];
		String valCodes = fnamParts[3];
		hexParts = valCodes.split("\\."); // split takes a regex and the \ needs its own \
		if (hexParts.length!=4) {throw new IllegalArgumentException("fileName hex invalid");}
		Timestamp tsThen=new Timestamp(Long.valueOf(hexParts[1],16).longValue());
		Timestamp tsNow = new Timestamp(System.currentTimeMillis());
		// clear out the fractional part since mySQL does not use it
		tsNow.setNanos(0);
		String thenCode = new SimpleDateFormat("MMddyyyyHHmmss").format(tsThen);
		long ranCode=Long.valueOf(hexParts[2],16).longValue();
		String longFileName = fileName+"."+thenCode+"."+hexParts[3];
		long courtCode=Long.valueOf(hexParts[0],16).longValue(); 
	
		// Now we need to get the associated CaseDocument by court, thenTime and ranCode
		// CCMSCaseDocument(Timestamp docSent, long docCode, long courtOID)
		//		caseDoc = new CCMSCaseDocument(tsThen, ranCode, court.getCourtOID());
		try {
			conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court=new CCMSCourt();
			court.setCourtOID(courtCode);
			//I need the full court record to get court name
			List<PreparedStatement> courtStmts = court.genSelectSQL(conn);
			List<CCMSCourt> courts = new ArrayList<CCMSCourt>();
			for (PreparedStatement stmt : courtStmts)
			{
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					courts.add(new CCMSCourt(rs,conn));
				}
				rs.close();
				stmt.close();
			}
			if (courts == null || courts.size() != 1) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
			court=courts.get(0);
			
			CCMSCaseDocument caseDoc = new CCMSCaseDocument(tsThen, ranCode, courtCode);
			List<PreparedStatement> caseDocStmts = caseDoc.genSelectSQL(conn);
			List<CCMSCaseDocument> caseDocuments = new ArrayList<CCMSCaseDocument>();
			for (PreparedStatement stmt : caseDocStmts)
			{
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					caseDocuments.add(new CCMSCaseDocument(rs,conn));
				}
				rs.close();
				stmt.close();
			}
			// Validity check: record must exist. If a record exists, the the court, time and ranCode
			//   match a record; we (probably) sent this document.  Now make sure it has not been sent
			//   already.
			// BUT FIRST - we may be retuning a file with a different extension (.pdf rather than .docx).
			//   Remove the tailing extensions before comparing.
			if (caseDocuments == null || caseDocuments.size() != 1) {
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
			else {
				caseDoc=caseDocuments.get(0);
				String rawDname=caseDoc.getDocumentName().substring(0, caseDoc.getDocumentName().lastIndexOf("."));
				String rawFname=rawFileName.substring(0, rawFileName.lastIndexOf("."));
				if (caseDoc.getDocReceived().compareTo(caseDoc.getDocSent()) > 0 || rawDname.compareTo(rawFname) != 0) {
					response.setStatus(HttpServletResponse.SC_CONFLICT); //Actually, already there
					}
				}
		 
		// ** we will have to update the CaseDocument record in the database
		// with the new information.
		
		// So far, so good.  Now lets get the document and send it to openKM.
		// We assume that the Court directory is already set up in openKM.  We then
		//   check to see if there is already a directory setup for this case.  If not
		//   we create it.  We then create the document file and sent it up.
		// Easy-peesy.
			InputStream idoc = request.getInputStream();
			OKMWebservices okm = CCMSDomainUtilities.getInstance().connOpenKM();
			//Check to make sure the case folder exists
			String casePath = "/okm:root/" + court.getLocationCode() + " " + court.getCourtName() + "/" + caseName;
			try {
				okm.isValidFolder(casePath);
			}
			catch (PathNotFoundException e) {
				okm.createFolderSimple(casePath);
			}
			catch (Exception e) {
				response.setStatus(HttpServletResponse.SC_CONFLICT);
			}
			Document odoc = new Document();
			try {
				String okmDocPath=casePath + "/" + longFileName;
				odoc.setPath(okmDocPath);
				okm.createDocument(odoc, idoc);
				// code here to make sure there is a valid record to update
				//PreparedStatement stmt = caseDoc.genSelectSQL(conn).get(0);
				//ResultSet rs1=stmt.executeQuery();
				// ** the following line did not work, returned 0 but
				//    the result set was one.
				//int noRows = stmt.getFetchSize();
				//logger.debug("preUpdate records seen", noRows);
				// now update the caseDoc with the latest info ...arg17316682169234203
				caseDoc.setDocumentURL(okmDocPath);
				caseDoc.setDocumentName(longFileName);
				caseDoc.setDocReceived(tsNow);
				PreparedStatement stmt = caseDoc.genUpdateSQL(conn).get(0);
				int noRows=stmt.executeUpdate();
				logger.debug("Update records seen", noRows);
				// the conn.commit is essential.  I spent a week trying to figure out
				//    why a record was not being updated when it went through this
				//    code.
				conn.commit();
				stmt.close();
			} catch (SQLException se) {
				se.printStackTrace();
			} catch (Exception e) { //from the copy and caseDoc update
				response.sendError(501,"Sever Error");
			}
			// if we get to this point without an exception
			//   I am as surprised as you are.
			}
		catch (Exception e) {
			e.printStackTrace();
			response.sendError(501,"Sever Error");		
		}
		// All tasks completed. !
//	    response.sendError(201,"Document Saved");
	}
			

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}
	
	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);

			List<CCMSDocumentTemplate> templates = new ArrayList<CCMSDocumentTemplate>();
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSDocumentTemplate template = new CCMSDocumentTemplate(0L, court.getCourtOID());
			PreparedStatement stmt = template.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				templates.add(new CCMSDocumentTemplate(rs, conn));
			}
			stmt.close();
			rs.close();
		}
		catch ( Exception ex)
		{
			logger.error(ex);
		}
	}

}
