package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseEvent;
import org.ncsc.ccms.domain.CCMSCaseHearing;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSCourtLocation;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSHearingType;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSPermission;

import com.google.gson.Gson;

public class SaveCaseHearing extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public SaveCaseHearing()
	{
		super();
		SaveCaseHearing.logger = LogManager.getLogger(SaveCaseHearing.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			String json = "{\"caseOID\":\"6942110920321203\",\"hearingType\":\"1\",\"judicialOfficer\":\"2\",\"startDateTime\":\"2017-04-01 09:00\",\"endDateTime\":\"2017-04-01 14:00\",\"description\":\"testing add case hearing.\n\ncoool line breaks.\"}";
			SaveCaseHearing hrng = new SaveCaseHearing();
			hrng.translateJSONToTaskObject(json, court);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Gson myGson = new Gson();
		String responseMessage = "";
		Connection conn = null;

		CCMSParty actionParty = (CCMSParty) request.getSession().getAttribute(
				"user");

		if (actionParty == null)
		{
			response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
					"Invalid User");
		}

		String errorMessage = null;

		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			CCMSCaseHearing hearing = this.translateJSONToTaskObject(
					clientJSON, court);

			if (hearing.getCaseHearingOID() != 0L)
			{
				if (!actionParty
						.isAuthorized(CCMSPermission.UPDATE_CASE_HEARING, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to update a case hearing.");
				}
				else
				{
					// Update hearing, Remove the previous version of the
					// hearing
					// and then readd
					List<PreparedStatement> delStmts = hearing
							.genDeleteSQL(conn);
					for (PreparedStatement stmt : delStmts)
					{
						stmt.execute();
						stmt.close();
					}

					List<PreparedStatement> insStmts = hearing
							.genInsertSQL(conn);
					for (PreparedStatement stmt : insStmts)
					{
						stmt.execute();
						stmt.close();
					}
				}

			}
			else
			{
				if (!actionParty
						.isAuthorized(CCMSPermission.CREATE_CASE_HEARING, court))
				{
					response.sendError(HttpServletResponse.SC_FORBIDDEN,
							"User not authorized to add a case hearing.");
				}
				else
				{
					// New hearing

					// If this is a new hearing, check to see if this case
					// already
					// has a hearing scheduled for the same start date/time
					// before
					// adding
					if (!hearing.isDuplicateHearing(conn))
					{
						hearing.setCaseHearingOID(CCMSDomainUtilities
								.getInstance().genOID());

						List<PreparedStatement> insStmts = hearing
								.genInsertSQL(conn);
						for (PreparedStatement stmt : insStmts)
						{
							stmt.execute();
							stmt.close();
						}
					}
					else
					{
						errorMessage = "There is already a hearing scheduled for this date/time";
					}
				}
			}

			conn.commit();

			CCMSCaseHearing qHearing = new CCMSCaseHearing();

			if (errorMessage == null)
			{
				// No error message, retrieve the hearing from teh database
				qHearing = new CCMSCaseHearing(hearing.getCaseHearingOID(),
						hearing.getCourtOID());
				PreparedStatement selStmt = qHearing.genSelectSQL(conn).get(0);
				ResultSet rs = selStmt.executeQuery();
				while (rs.next())
				{
					qHearing = new CCMSCaseHearing(rs, conn, court);
				}
				rs.close();
				selStmt.close();
			}
			else
			{
				qHearing.setErrorMessage(errorMessage);
			}

			List<CCMSCaseHearing> caseHearings = new ArrayList<CCMSCaseHearing>();
			caseHearings.add(qHearing);

			responseMessage = myGson.toJson(caseHearings);
			logger.debug("Servlet Returning: " + responseMessage);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(responseMessage);

		}
		catch (Exception e)
		{
			logger.error(e);
			try
			{
				conn.rollback();
			}
			catch (SQLException e1)
			{
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	public CCMSCaseHearing translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSCaseHearing hearing = new CCMSCaseHearing();
		hearing.setCourtOID(court.getCourtOID());

		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		// Check to see if there is a hearing OID
		String caseHearingOIDString = (String) jsonCaseObject
				.get("caseHearingOID");
		if (caseHearingOIDString != null && !caseHearingOIDString.isEmpty())
		{
			long caseHearingOID = Long.parseLong(caseHearingOIDString);
			hearing.setCaseHearingOID(caseHearingOID);
		}

		String caseOIDString = (String) jsonCaseObject.get("caseOID");
		if (caseOIDString != null && !caseOIDString.isEmpty())
		{
			long caseOID = Long.parseLong(caseOIDString);
			hearing.setCaseOID(caseOID);
		}

		String hearingTypeOIDString = (String) jsonCaseObject
				.get("hearingType");
		if (hearingTypeOIDString != null && !hearingTypeOIDString.isEmpty())
		{
			long hearingTypeOID = Long.parseLong(hearingTypeOIDString);
			CCMSHearingType hearingType = new CCMSHearingType(hearingTypeOID,
					court.getCourtOID());
			hearing.setHearingType(hearingType);
		}

		String judicialOfficerOIDString = (String) jsonCaseObject
				.get("judicialOfficer");
		if (judicialOfficerOIDString != null
				&& !judicialOfficerOIDString.isEmpty())
		{
			long judicialOfficerOID = Long.parseLong(judicialOfficerOIDString);
			CCMSParty judicialOfficer = new CCMSParty(judicialOfficerOID);
			hearing.setJudicialOfficer(judicialOfficer);
		}

		String courtLocOIDString = (String) jsonCaseObject.get("courtLoc");
		if (courtLocOIDString != null && !courtLocOIDString.isEmpty())
		{
			long courtLocOID = Long.parseLong(courtLocOIDString);
			CCMSCourtLocation courtLoc = new CCMSCourtLocation(courtLocOID,
					court.getCourtOID());
			hearing.setCourtLoc(courtLoc);
		}

		String startDateTimeString = (String) jsonCaseObject
				.get("startDateTime");

		if (startDateTimeString != null && !startDateTimeString.isEmpty())
		{
			Timestamp startDateTime = CCMSDomainUtilities
					.convertStringToTimestamp(startDateTimeString);
			hearing.setStartDateTime(startDateTime);
		}

		String endDateTimeString = (String) jsonCaseObject.get("endDateTime");

		if (endDateTimeString != null && !endDateTimeString.isEmpty())
		{
			Timestamp endDateTime = CCMSDomainUtilities
					.convertStringToTimestamp(endDateTimeString);
			hearing.setEndDateTime(endDateTime);
		}

		String descriptionString = (String) jsonCaseObject.get("description");
		hearing.setDescription(descriptionString);

		return hearing;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
