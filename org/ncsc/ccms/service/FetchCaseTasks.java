package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCaseTask;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSException;
import org.ncsc.ccms.domain.CCMSParty;
import org.ncsc.ccms.domain.CCMSStaffPool;
import org.ncsc.ccms.domain.CCMSTaskType;

import com.google.gson.Gson;

public class FetchCaseTasks extends HttpServlet
{

	private static Logger logger = null;
	private String target = "/index.jsp";

	/**
	 * Constructor of the object.
	 */
	public FetchCaseTasks()
	{
		super();
		FetchCaseTasks.logger = LogManager.getLogger(FetchCaseTasks.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		this.doPost(request, response);
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			// Case Add
			CCMSCourt court = new CCMSCourt(5);

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String json;
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			Connection conn = CCMSDomainUtilities.getInstance().connect();
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			CCMSCaseTask qTask = translateJSONToTaskObject(clientJSON, court);

			List<CCMSCaseTask> tasks = new ArrayList<CCMSCaseTask>();

			PreparedStatement pStmt = conn
					.prepareStatement(CCMSCaseTask.FETCH_CASE_TASK_SQL);
			pStmt.setLong(1, court.getCourtOID());
			pStmt.setLong(2, qTask.getAssociatedCase().getCaseOID());

			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSCaseTask dbTask = new CCMSCaseTask(rs, conn, court, false);
				tasks.add(dbTask);
			}
			pStmt.close();
			rs.close();

			// Return all matching values to the AJAX calling screen
			Gson myGson = new Gson();
			json = myGson.toJson(tasks);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);

		}

		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}

	}

	public CCMSCaseTask translateJSONToTaskObject(String taskJSON,
			CCMSCourt court) throws Exception
	{
		CCMSCaseTask task = new CCMSCaseTask(0L, court.getCourtOID());
		JSONParser parser = new JSONParser();
		JSONObject jsonCaseObject = (JSONObject) parser.parse(taskJSON);

		String partyOIDString = (String) jsonCaseObject.get("partyOID");
		String poolOIDString = (String) jsonCaseObject.get("poolOID");
		String taskOIDString = (String) jsonCaseObject.get("taskOID");

		// taskOID will only be populated if this is an update.
		// caseOID is not an option for FetchCaseTasks because tasks are
		// already retrieved with each case
		if (taskOIDString != null && !taskOIDString.isEmpty())
		{
			task.setTaskOID(Long.parseLong(taskOIDString));
		}

		if (partyOIDString != null && !partyOIDString.isEmpty())
		{
			task.setAssignedParty(new CCMSParty(Long.parseLong(partyOIDString),
					court.getCourtOID()));
		}

		if (poolOIDString != null && !poolOIDString.isEmpty())
		{
			task.setAssignedPool(new CCMSStaffPool(Long
					.parseLong(poolOIDString), court.getCourtOID()));
		}

		return task;
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
