package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchToken extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchToken()
	{
		super();
		FetchToken.logger = LogManager.getLogger(FetchToken.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		BufferedReader reader = request.getReader();
		String clientJSON = CCMSDomainUtilities.getInstance().fetchJSONString(reader);

		try
		{
			CCMSDomainUtilities.getInstance().setCORSHeader(request, response, this, this.logger);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			String userName = (String) jsonObject.get("userName");
			String courtOID = (String)jsonObject.get("courtOID");
			

			CCMSParty courtUser = new CCMSParty();
			courtUser.setUserName(userName);
			
			
			// Retrieve the full user object to make sure that UI has key information such as admin flag
			PreparedStatement stmt2 = courtUser.genSelectSQL(conn).get(0);
			ResultSet rs2 = stmt2.executeQuery();
			while ( rs2.next() )
			{
				courtUser = new CCMSParty(rs2, conn, false);
			}
			rs2.close();
			stmt2.close();
			

			if (courtUser.getAuthorizedCourts().size() > 0)
			{
				String token = CCMSDomainUtilities.getInstance().createLoginToken(
						courtUser.getUserName(),
						Long.toString(courtUser.getAuthorizedCourts().get(0)
								.getCourtOID()));

				courtUser.setToken(token);
			}
			
			
			// Set the user attribute
			request.getSession().setAttribute("user", courtUser);
			logger.debug("Setting User Object" + courtUser);
			
			// Send the user object back to the client
			Gson myGson = new Gson();
			String json = myGson.toJson(courtUser);
			
			logger.debug("Servlet Response: " + json);
			
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
