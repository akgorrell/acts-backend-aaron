package org.ncsc.ccms.service;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.text.SimpleDateFormat;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCaseStatus;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCaseDocument;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDocumentTemplate;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSCase;

import com.openkm.sdk4j.OKMWebservices;
import com.openkm.sdk4j.OKMWebservicesFactory;
import com.openkm.sdk4j.bean.Folder;
import com.openkm.sdk4j.bean.Document;
import com.openkm.sdk4j.exception.PathNotFoundException;

import com.google.gson.Gson;

public class GetAttachment extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public GetAttachment()
	{
		super();
		GetAttachment.logger = LogManager.getLogger(GetAttachment.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred This is a small change
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		HttpSession session = request.getSession();
		ServletContext context = getServletContext();
		boolean isMultipart = ServletFileUpload.isMultipartContent(request);

		// Create a factory for disk-based file items
		FileItemFactory factory = new DiskFileItemFactory();
		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// Parse the request
		String auth = request.getHeader("Authorization");
		String token = request.getHeader("token");
		String caseOIDString = request.getHeader("caseOID");
		Connection conn = null;
		String resp = null;

		try
		{ // standard setup: court and connection
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			conn = CCMSDomainUtilities.getInstance().connect();

			// we need the court name, so we need to get the whole court record
			List<PreparedStatement> courtStmts = court.genSelectSQL(conn);
			List<CCMSCourt> courts = new ArrayList<CCMSCourt>();
			for (PreparedStatement stmt : courtStmts)
			{
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					courts.add(new CCMSCourt(rs, conn));
				}
				rs.close();
				stmt.close();
			}
			if (courts == null || courts.size() != 1)
			{
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
			court = courts.get(0);

			List items = upload.parseRequest(request);
			Iterator iter = items.iterator();
			InputStream uploadFileStream = null;
			String uploadFileName = null;
			String uploadContentType = null;
			while (iter.hasNext())
			{
				// Process uploaded items
				FileItem item = (FileItem) iter.next();

				if (item.getSize() > 0)
				{
					uploadFileStream = item.getInputStream();
					uploadFileName = item.getName();
					uploadContentType = item.getContentType();
				}
			}
			// now go get the case since we need the case number
			String caseNumber = null;
			CCMSCase courtCase = new CCMSCase(0L, court);
			CCMSDomainUtilities.getInstance().isNumber(caseOIDString);
			courtCase.setCaseOID(Long.parseLong(caseOIDString));
			List<CCMSCase> courtCases = new ArrayList<CCMSCase>();
			List<PreparedStatement> pStmts = courtCase.genSelectSQL(conn);
			logger.debug("Query Statements: " + pStmts.size());
			for (PreparedStatement pStmt : pStmts)
			{
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					CCMSCase dbCase = new CCMSCase(rs, conn, court, false);
					courtCases.add(dbCase);
					logger.debug("Matching Case: " + dbCase);
				}
				pStmt.close();
				rs.close();
			}
			if (courtCases == null || courtCases.size() != 1)
			{
				response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			}
			courtCase = courtCases.get(0);
			String caseName = courtCase.getCaseNumber();
			// **********************************************************
			// Get the document extension
			String fnExtension = null;
			String fnMainName = null;
			try
			{
				fnExtension = uploadFileName.substring(uploadFileName
						.lastIndexOf(".") + 1);
				fnMainName = uploadFileName.substring(0,
						uploadFileName.lastIndexOf("."));
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
			Timestamp tsNow = new Timestamp(System.currentTimeMillis());
			String nowCode = new SimpleDateFormat("MMddyyyyHHmmss")
					.format(tsNow);
			String longFileName = fnMainName + ".A" + nowCode + "."
					+ fnExtension;
			String casePath = null;
			if (uploadFileStream != null)
			{
				casePath = "/okm:root/" + court.getLocationCode() + " "
						+ court.getCourtName() + "/" + caseName;
				resp = openKMupload(uploadFileStream, casePath, longFileName,
						context);
			}
			CCMSCaseDocument caseDoc = new CCMSCaseDocument(CCMSDomainUtilities
					.getInstance().genOID(), court.getCourtOID());
			caseDoc.setCaseOID(courtCase.getCaseOID());
			caseDoc.setDocumentName(longFileName);
			caseDoc.setDocSent(tsNow);
			caseDoc.setDocReceived(tsNow);
			caseDoc.setDocumentURL(casePath + "/" + longFileName);
			caseDoc.setDocReceived(tsNow); // we will check to see if the
											// Received time is *after* DocSent
			PreparedStatement stmt2 = caseDoc.genInsertSQL(conn).get(0);
			stmt2.execute();
			stmt2.close();
			conn.commit();

		}
		catch (Exception e)
		{
			e.printStackTrace();
			logger.error(e);
		}

		// generate a response for the nice person
		Gson myGson = new Gson();

		// TODO: Replace object below with document
		String responseMessage = myGson.toJson(resp);
		logger.debug("Servlet Returning: " + responseMessage);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(responseMessage);
		// All tasks completed. !
		// response.sendError(201,"Document Saved");
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

	/*
	 * private File processUploadedFile(FileItem item, ServletContext context)
	 * throws Exception { String fieldName = item.getFieldName();
	 * 
	 * String fileName = item.getName(); fileName = fileName.replaceAll(" ",
	 * "_");
	 * 
	 * fileName = CCMSDomainUtilities.getInstance().genOID() + "_" + fileName;
	 * String contentType = item.getContentType(); boolean isInMemory =
	 * item.isInMemory(); long sizeInBytes = item.getSize();
	 * 
	 * String fileDir = context.getRealPath("/"); File uploadedFile = new
	 * File("", fileName); item.write(uploadedFile); return uploadedFile; }
	 */

	private String openKMupload(InputStream idoc, String casePath,
			String fileName, ServletContext context) throws Exception
	{
		OKMWebservices okm = CCMSDomainUtilities.getInstance().connOpenKM();
		// Check to make sure the case folder exists
		try
		{
			okm.isValidFolder(casePath);
		}
		catch (PathNotFoundException e)
		{
			okm.createFolderSimple(casePath);
		}
		catch (Exception e)
		{
			return "file conflict";
		}
		Document odoc = new Document();
		String docPath = casePath + "/" + fileName;
		try
		{
			odoc.setPath(docPath);
			okm.createDocument(odoc, idoc);

		}
		catch (Exception e)
		{ // from the copy and caseDoc update
			return "upload failed";
		}
		// if we get to this point without an exception
		// I am as surprised as you are.
		return "success";
	}
} // end of class
