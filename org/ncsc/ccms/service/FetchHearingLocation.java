package org.ncsc.ccms.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CourtLookupTable;

import com.google.gson.Gson;

public class FetchHearingLocation extends HttpServlet
{
	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchHearingLocation()
	{
		super();
		FetchHearingLocation.logger = LogManager
				.getLogger(FetchHearingLocation.class.getName());
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Gson myGson = new Gson();
			CourtLookupTable lookupTable = CCMSDomainUtilities.getInstance()
					.getLookupTableForCourt(court);
			System.out.println(myGson.toJson(lookupTable.getCourtLocations()));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		String token = request.getHeader("token");

		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);

			Gson myGson = new Gson();

			CourtLookupTable lookupTable = CCMSDomainUtilities.getInstance()
					.getLookupTableForCourt(court);
			String json = myGson.toJson(lookupTable.getCourtLocations());

			logger.debug("SERVLET RESPONSE:" + json);

			response.setContentType("application/json");

			response.setCharacterEncoding("UTF-8");

			response.getWriter().write(json);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
