package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSConstants;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSICCSCode;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchICCSCategory extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchICCSCategory()
	{
		super();
		FetchICCSCategory.logger = LogManager.getLogger(FetchICCSCategory.class
				.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{

		String json;
		String token = request.getHeader("token");

		CCMSICCSCode qCode = new CCMSICCSCode();
		List<CCMSICCSCode> iccsCodes = new ArrayList<CCMSICCSCode>();
		try
		{
			CCMSCourt court = CCMSDomainUtilities.getInstance()
					.getSessionCourtObject(request.getSession(), token);
			
			qCode.setCourtOID(court.getCourtOID());
			Connection conn = CCMSDomainUtilities.getInstance().connect();

			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input JSON: " + clientJSON);

			if (clientJSON != null && !clientJSON.isEmpty())
			{
				JSONParser parser = new JSONParser();
				Object obj = parser.parse(clientJSON);
				JSONObject jsonObject = (JSONObject) obj;

				String iccsCodeOID = (String) jsonObject.get("iccsCodeOID");
				if (iccsCodeOID != null
						&& CCMSDomainUtilities.isNumber(iccsCodeOID))
				{
					logger.debug("iccsCodeOID: " + iccsCodeOID);
					qCode.setIccsCodeOID(Long.parseLong(iccsCodeOID));
				}
				
				// Look for categoryType attribute
			}
			else
			{
				// Assume default, retrieve only the top level section
				qCode.setCategoryType(CCMSConstants.ICCS_CAT_SECTION);
			}

			PreparedStatement pStmt = qCode.genSelectSQL(conn).get(0);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				iccsCodes.add(new CCMSICCSCode(rs, conn));
			}
			pStmt.close();
			rs.close();

			// Return all matching values to the AJAX calling screen

			Gson myGson = new Gson();
			json = myGson.toJson(iccsCodes);
			logger.debug("Servlet Returning: " + json);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(json);
		}

		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					e.toString());
		}
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		// Put your code here
	}

}
