package org.ncsc.ccms.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.ncsc.ccms.domain.CCMSAlfrescoUtilities;
import org.ncsc.ccms.domain.CCMSCase;
import org.ncsc.ccms.domain.CCMSCourt;
import org.ncsc.ccms.domain.CCMSDomainUtilities;
import org.ncsc.ccms.domain.CCMSException;
import org.ncsc.ccms.domain.CCMSParty;

import com.google.gson.Gson;

public class FetchLoginCredentials extends HttpServlet
{

	private static Logger logger = null;

	/**
	 * Constructor of the object.
	 */
	public FetchLoginCredentials()
	{
		super();
		FetchLoginCredentials.logger = LogManager
				.getLogger(FetchLoginCredentials.class.getName());
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy()
	{
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurredhttp://192.168.1.45/login
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		doPost(request, response);
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to
	 * post.
	 * 
	 * @param request
	 *            the request send by the client to the server
	 * @param response
	 *            the response send by the server to the client
	 * @throws ServletException
	 *             if an error occurred
	 * @throws IOException
	 *             if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		Gson myGson = new Gson();
		String responseMessage = "";
		CCMSParty courtUser = null;
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.addHeader("Access-Control-Allow-Origin", "*");

		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSDomainUtilities.getInstance().setCORSHeader(request, response,
					this, this.logger);
			BufferedReader reader = request.getReader();
			String clientJSON = CCMSDomainUtilities.getInstance()
					.fetchJSONString(reader);
			logger.debug("Input: " + clientJSON);
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(clientJSON);
			JSONObject jsonObject = (JSONObject) obj;
			String userName = (String) jsonObject.get("userName");
			String password = (String) jsonObject.get("password");
			logger.debug("UserName: " + userName + " Password: " + password);

			if (userName != null && !userName.isEmpty() && password != null
					&& !password.isEmpty())
			{
				courtUser = authenticateUser(userName, password, conn);
			}

			/**
			 * User must have provided the appropriate userName and password and
			 * be authorized for at least once court to be able to log in
			 */
			
			if (courtUser != null && courtUser.getAuthorizedCourts().size() > 0)
			{
				logger.info("courtUser: " + courtUser + " Auth Courts: " + courtUser.getAuthorizedCourts().size());
				// Set the applicable court, which will be the first court listed as an authorized court
				// Retrieve the court object and set it as a session object
				CCMSCourt court = new CCMSCourt(courtUser.getAuthorizedCourts().get(0).getCourtOID());
				PreparedStatement stmt = court.genSelectSQL(conn).get(0);
				ResultSet rs1 = stmt.executeQuery();
				while (rs1.next())
				{
					court = new CCMSCourt(rs1, conn);
				}
				stmt.close();
				rs1.close();
				
				

				request.getSession().setAttribute("user", courtUser);
				request.getSession().setAttribute("court", court);
				logger.debug("Setting Court Object" + court);
				
				
				responseMessage = myGson.toJson(courtUser);
			}
			else
			{
				if (courtUser == null)
				{
					logger.error("No login found for " + userName);
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
							"Incorrect user name or password provided");
				}
				else if ( courtUser.getAuthorizedCourts().size() == 0 )
				{
					logger.error("No authorized courts for " + userName);
					response.sendError(HttpServletResponse.SC_UNAUTHORIZED,
							"User is not authorized for any court");
				}
			}
			

			// Return all matching values to the AJAX calling screen
			logger.debug("FetchLoginCredentials Response:" + responseMessage);
			response.getWriter().write(responseMessage);
		}
		catch (Exception ex)
		{
			logger.error(ex);
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,
					ex.toString());
		}


	}

	// Sets the token that is passed back to the UI, once implemented can remove
	// service FetchToken
	private void setPartyToken(CCMSParty party)
	{
		if (party.getAuthorizedCourts().size() > 0)
		{
			String token = CCMSDomainUtilities.getInstance().createLoginToken(
					party.getUserName(),
					Long.toString(party.getAuthorizedCourts().get(0)
							.getCourtOID()));

			party.setToken(token);
		}
	}

	private CCMSParty authenticateUser(String userName, String password, Connection conn)
	{
		CCMSParty dbParty = null;
		try
		{
			CCMSParty qParty = new CCMSParty();
			qParty.setUserName(userName);
			qParty.setPassword(password);

			PreparedStatement pStmt = qParty.genSelectSQL(conn).get(0);
			logger.info("Querying for Party: " + pStmt.toString());
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				dbParty = new CCMSParty(rs, conn, false);
				dbParty.getAuthorizedCourts(conn);
				this.setPartyToken(dbParty);
			}
			pStmt.close();
			rs.close();

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dbParty;

	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException
	 *             if an error occurs
	 */
	public void init() throws ServletException
	{
		logger.info("FetchLoginCredentials on startup");
		CCMSDomainUtilities.getInstance();
	}

}
