package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCasePhase extends CCMSDomain
{

	private long casePhaseOID = 0L, courtOID = 0L, caseTypeOID = 0L;

	private String name = "", shortName = "", description = "";
	private static Logger logger = null;

	public CCMSCasePhase()
	{

	}

	public CCMSCasePhase(long casePhaseOID, long courtOID)
	{
		CCMSCasePhase.logger = LogManager.getLogger(CCMSCasePhase.class
				.getName());
		this.casePhaseOID = casePhaseOID;
		this.courtOID = courtOID;
	}

	public CCMSCasePhase(ResultSet rs) throws Exception
	{
		CCMSCasePhase.logger = LogManager.getLogger(CCMSCasePhase.class
				.getName());
		this.casePhaseOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.shortName = rs.getString("SHORT_NAME");
		this.courtOID = rs.getLong("COURT_ID");
		this.caseTypeOID = rs.getLong("CASE_TYPE_ID");
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.CASE_PH_TYPES " + 
				" ( ID, COURT_ID, NAME, SHORT_NAME, DESCRIPTION, CASE_TYPE_ID ) " + 
				" values ( ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.casePhaseOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.name);
		stmt.setString(4, this.shortName);
		stmt.setString(5, this.description);
		stmt.setLong(6, this.caseTypeOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement(" UPDATE NJCCCMS.CASE_PH_TYPES " + 
				"   SET name = ?, description = ? " + 
				" WHERE ID = ? AND CASE_TYPE_ID = ? AND COURT_ID = ?" );
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setLong(3, this.casePhaseOID);
		stmt.setLong(4, this.caseTypeOID);
		stmt.setLong(5, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CASE_PH_TYPES "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}


	public long getStatusOID()
	{
		return casePhaseOID;
	}

	public void setStatusOID(long statusOID)
	{
		this.casePhaseOID = statusOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getShortName()
	{
		return shortName;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public long getCaseTypeOID()
	{
		return this.caseTypeOID;
	}

	public void setCaseTypeOID(long caseTypeOID)
	{
		this.caseTypeOID = caseTypeOID;
	}

	public long getCasePhaseOID()
	{
		return casePhaseOID;
	}

	public void setCasePhaseOID(long casePhaseOID)
	{
		this.casePhaseOID = casePhaseOID;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCasePhase)
		{
			CCMSCasePhase qPhase = (CCMSCasePhase) obj;
			if (qPhase.getCasePhaseOID() == this.getCasePhaseOID())
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String objString = " Case Phase OID: " + this.casePhaseOID
				+ " Phase Case Type: " + this.caseTypeOID + " Court OID: "
				+ this.courtOID + " Desc: " + this.description + " Name: "
				+ this.name + " Short Name: " + this.shortName;

		return objString;
	}
	

}
