package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseStatus extends CCMSDomain
{

	private long statusOID, courtOID;
	private String name = "", description = "";
	private static Logger logger = null;

	public CCMSCaseStatus()
	{
		CCMSCaseStatus.logger = LogManager.getLogger(CCMSCaseStatus.class
				.getName());

	}

	public CCMSCaseStatus(long statusOID, long courtOID)
	{
		CCMSCaseStatus.logger = LogManager.getLogger(CCMSCaseStatus.class
				.getName());
		this.statusOID = statusOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseStatus(long statusOID, long courtOID, String name)
	{
		CCMSCaseStatus.logger = LogManager.getLogger(CCMSCaseStatus.class
				.getName());
		this.statusOID = statusOID;
		this.courtOID = courtOID;
		this.name = name;
	}

	public CCMSCaseStatus(ResultSet rs) throws Exception
	{
		CCMSCaseStatus.logger = LogManager.getLogger(CCMSCaseStatus.class
				.getName());
		this.statusOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.courtOID = rs.getLong("COURT_ID");
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.CASE_STATUS " + 
				" ( ID, COURT_ID, NAME, DESCRIPTION ) " + 	
				" values ( ?, ?, ?, ? ) " );
		stmt.setLong(1, this.statusOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.name);
		stmt.setString(4, this.description);
		
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.CASE_STATUS " + 
				"   SET NAME = ?, DESCRIPTION = ? " + 
				" WHERE ID = ? AND COURT_ID = ?" );
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setLong(3, this.statusOID);
		stmt.setLong(4, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CASE_STATUS "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}


	public long getStatusOID()
	{
		return statusOID;
	}

	public void setStatusOID(long statusOID)
	{
		this.statusOID = statusOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCaseStatus)
		{
			CCMSCaseStatus qStatus = (CCMSCaseStatus) obj;
			if (qStatus.getStatusOID() == this.getStatusOID())
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String objString = " StatusOID: " + this.statusOID + " Name: "
				+ this.name + " Desc: " + this.description;

		return objString;
	}

}
