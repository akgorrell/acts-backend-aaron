package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CCMSEventWorkflow extends CCMSDomain
{
	private long eventWorkflowOID;
	private CCMSCourt court;

	private CCMSEventType triggeringEvent = null;
	private String description = "";
	
	private List<CCMSWorkflowStep> workflowSteps = new ArrayList<CCMSWorkflowStep>();

	public CCMSEventWorkflow(long eventWorkflowOID, CCMSCourt court)
	{
		this.eventWorkflowOID = eventWorkflowOID;
		this.court = court;
	}

	public CCMSEventWorkflow(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		this.eventWorkflowOID = rs.getLong("ID");
		this.court = court;

		long triggeringEventOID = rs.getLong("EVENT_TYPE_ID");
		this.triggeringEvent = new CCMSEventType(triggeringEventOID, court.getCourtOID());
		this.triggeringEvent = (CCMSEventType) CCMSDomainUtilities
				.getInstance().retrieveFullObject(triggeringEvent, court);
		
		this.description = rs.getString("DESCRIPTION");
		
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM NJCCCMS.WORKFLOW_STEP " + 
				" WHERE WORKFLOW_ID = ? " );
		stmt.setLong(1, this.eventWorkflowOID);
		ResultSet rs1 = stmt.executeQuery();
		while (rs1.next())
		{
			this.workflowSteps.add(new CCMSWorkflowStep(rs1, conn, court));
			
		}
		stmt.close();
		rs1.close();
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.EVENT_WORKFLOW " + 
				" (ID, COURT_ID, EVENT_TYPE_ID, DESCRIPTION ) " +
				" VALUES ( ?, ?, ?, ? ) " );
		stmt.setLong(1, this.eventWorkflowOID);
		stmt.setLong(2, this.court.getCourtOID());
		stmt.setLong(3, this.triggeringEvent.getEventTypeOID());
		stmt.setString(4, this.description);
		
		stmts.add(stmt);
		
		for ( CCMSWorkflowStep step : this.workflowSteps )
		{
			stmts.addAll(step.genInsertSQL(conn));
		}
		
		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		return stmts;
	}

	/**
	 * For an update, first remove the associated workflow step
	 * then we can update the workflow itself (all except the event which cannot be modified)
	 */
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		// Remove all workflow steps
		PreparedStatement stmt = conn.prepareStatement("DELETE FROM NJCCCMS.WORKFLOW_STEP WHERE WORKFLOW_ID = ?" );
		stmt.setLong(1, this.eventWorkflowOID);
		stmts.add(stmt);
		
		PreparedStatement stmt1 = conn.prepareStatement("UPDATE NJCCCMS.EVENT_WORKFLOW " + 
				"   SET  DESCRIPTION = ?  " +
				" WHERE ID = ? ");
		stmt1.setString(1, this.description);
		stmt1.setLong(2, this.eventWorkflowOID);
		stmts.add(stmt1);
		
		// now add back the workflow steps
		for ( CCMSWorkflowStep step : this.workflowSteps )
		{
			stmts.addAll(step.genInsertSQL(conn));
		}
		
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		String sqlString = "SELECT * FROM NJCCCMS.EVENT_WORKFLOW " +
				" WHERE COURT_ID = " +  this.court.getCourtOID();
		
		if ( this.triggeringEvent != null )
		{
			sqlString += " AND EVENT_TYPE_ID = " + this.triggeringEvent.getEventTypeOID();
		}
		
		
		PreparedStatement stmt = conn.prepareStatement(sqlString);
		
		stmts.add(stmt);
		
		return stmts;
	}

	public long getEventWorkflowOID()
	{
		return eventWorkflowOID;
	}

	public void setEventWorkflowOID(long eventWorkflowOID)
	{
		this.eventWorkflowOID = eventWorkflowOID;
	}

	public CCMSCourt getCourt()
	{
		return court;
	}

	public void setCourt(CCMSCourt court)
	{
		this.court = court;
	}

	public CCMSEventType getTriggeringEvent()
	{
		return triggeringEvent;
	}

	public void setTriggeringEvent(CCMSEventType triggeringEvent)
	{
		this.triggeringEvent = triggeringEvent;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<CCMSWorkflowStep> getWorkflowSteps()
	{
		return workflowSteps;
	}

	public void setWorkflowSteps(List<CCMSWorkflowStep> workflowSteps)
	{
		this.workflowSteps = workflowSteps;
	}

}
