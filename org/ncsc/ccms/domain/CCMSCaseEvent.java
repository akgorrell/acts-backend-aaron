package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseEvent extends CCMSDomain
{
	private long caseEventOID, courtOID, caseOID;
	private CCMSEventType eventType;

	// The case party that entered/created the event
	private CCMSParty enteringParty;

	// This is the case party that triggered the event (not the party id that
	// entered the event)
	private CCMSParty initiatedByParty;
	private String eventShortcutCode, description;
	private Timestamp eventDate;
	private int durationTimeMin;
	private static Logger logger = null;
	private CCMSCaseDocument document = null;
	

	public CCMSCaseEvent()
	{
		CCMSCaseEvent.logger = LogManager.getLogger(CCMSCaseEvent.class
				.getName());
	}

	public CCMSCaseEvent(long caseEventOID, long courtOID)
	{
		CCMSCaseEvent.logger = LogManager.getLogger(CCMSCaseEvent.class
				.getName());
		CCMSCaseEvent.logger = LogManager.getLogger(CCMSCaseEvent.class
				.getName());
		this.caseEventOID = caseEventOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseEvent(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		CCMSCaseEvent.logger = LogManager.getLogger(CCMSCaseEvent.class
				.getName());
		this.caseEventOID = rs.getLong("CASE_EVENT.ID");
		this.courtOID = rs.getLong("CASE_EVENT.COURT_ID");
		this.description = rs.getString("CASE_EVENT.DESCRIPTION");
		this.eventDate = rs.getTimestamp("CASE_EVENT.EVENT_DATE");
		this.eventShortcutCode = rs.getString("CASE_EVENT.EVENT_SHORTCUT_CODE");
		this.caseOID = rs.getLong("CASE_EVENT.CASE_ID");
		this.durationTimeMin = rs.getInt("CASE_EVENT.DURATION_TIME_MIN");

		long eventTypeOID = rs.getLong("CASE_EVENT.EVENT_TYPE_ID");
		if (eventTypeOID != 0L)
		{
			this.eventType = new CCMSEventType(eventTypeOID, this.courtOID);
			this.eventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(this.eventType, court);
		}

		// Entering Party
		CCMSParty enteringParty = new CCMSParty();
		enteringParty.setLastName(rs.getString("PARTY.LAST_NAME"));
		enteringParty.setFirstName(rs.getString("PARTY.FIRST_NAME"));
		this.setEnteringParty(enteringParty);

		// Associated document(s)
		long documentOID = rs.getLong("CASE_EVENT.DOCUMENT_ID");
		if (documentOID != 0L )
		{
			CCMSCaseDocument doc = new CCMSCaseDocument(documentOID, court.getCourtOID());
			doc.setDocumentName(rs.getString("CASE_DOCUMENT.DOC_NAME"));
			this.setDocument(doc);
		}

		long initiatedByOID = rs.getLong("CASE_EVENT.INITIATED_BY_PARTY_OID");
		if (initiatedByOID != 0L)
		{
			logger.info("Retrieving Initiated By Party: " + initiatedByOID);
			this.initiatedByParty = new CCMSParty(initiatedByOID, this.courtOID);
			PreparedStatement pStmt2 = initiatedByParty.genSelectSQL(conn).get(
					0);
			ResultSet rs2 = pStmt2.executeQuery();
			while (rs2.next())
			{
				initiatedByParty = new CCMSParty(rs2, conn, true);
			}
			pStmt2.close();
			rs2.close();
		}
	}

	/*
	 * public CCMSCaseEvent(ResultSet rs, Connection conn, CCMSCourt court)
	 * throws Exception { CCMSCaseEvent.logger =
	 * LogManager.getLogger(CCMSCaseEvent.class .getName()); this.caseEventOID =
	 * rs.getLong("ID"); this.courtOID = rs.getLong("COURT_ID");
	 * this.description = rs.getString("DESCRIPTION"); this.eventDate =
	 * rs.getTimestamp("EVENT_DATE"); this.eventShortcutCode =
	 * rs.getString("EVENT_SHORTCUT_CODE"); this.caseOID =
	 * rs.getLong("CASE_ID"); this.durationTimeMin =
	 * rs.getInt("DURATION_TIME_MIN");
	 * 
	 * long eventTypeOID = rs.getLong("EVENT_TYPE_ID"); if (eventTypeOID != 0L)
	 * { this.eventType = new CCMSEventType(eventTypeOID, this.courtOID);
	 * this.eventType = (CCMSEventType) CCMSDomainUtilities.getInstance()
	 * .retrieveFullObject(this.eventType, court); }
	 * 
	 * long enteringPartyOID = rs.getLong("PARTY_ID"); if (enteringPartyOID !=
	 * 0L) { logger.info("Retrieving Entering Party: " + enteringPartyOID);
	 * this.enteringParty = new CCMSParty(enteringPartyOID, this.courtOID);
	 * PreparedStatement pStmt1 = enteringParty.genSelectSQL(conn).get(0);
	 * ResultSet rs1 = pStmt1.executeQuery(); while (rs1.next()) { enteringParty
	 * = new CCMSParty(rs1, conn); } pStmt1.close(); rs1.close(); }
	 * 
	 * long initiatedByOID = rs.getLong("INITIATED_BY_PARTY_OID"); if
	 * (initiatedByOID != 0L) { logger.info("Retrieving Initiated By Party: " +
	 * initiatedByOID); this.initiatedByParty = new CCMSParty(initiatedByOID,
	 * this.courtOID); PreparedStatement pStmt2 =
	 * initiatedByParty.genSelectSQL(conn).get( 0); ResultSet rs2 =
	 * pStmt2.executeQuery(); while (rs2.next()) { initiatedByParty = new
	 * CCMSParty(rs2, conn); } pStmt2.close(); rs2.close(); }
	 * 
	 * long documentOID = rs.getLong("DOCUMENT_ID"); if (documentOID != 0L) {
	 * logger.info("Retrieving Document: " + documentOID); this.document = new
	 * CCMSCaseDocument(documentOID, court.getCourtOID()); PreparedStatement
	 * pStmt3 = document.genSelectSQL(conn).get(0); ResultSet rs3 =
	 * pStmt3.executeQuery(); while (rs3.next()) { this.document = new
	 * CCMSCaseDocument(rs3, conn); } pStmt3.close(); rs3.close(); } }
	 */

	public long getCaseEventOID()
	{
		return caseEventOID;
	}
	
	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			PreparedStatement stmt = conn.prepareStatement("select * from NJCCCMS.CASE_EVENT where ID = 7297369771154");
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				CCMSCaseEvent event = new CCMSCaseEvent(rs, conn, court);
			}
		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}

	public void setCaseEventOID(long caseEventOID)
	{
		this.caseEventOID = caseEventOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getEventShortcutCode()
	{
		return eventShortcutCode;
	}

	public void setEventShortcutCode(String eventShortcutCode)
	{
		this.eventShortcutCode = eventShortcutCode;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Date getEventDateTime()
	{
		return eventDate;
	}

	public void setEventDateTime(Timestamp eventDateTime)
	{
		this.eventDate = eventDateTime;
	}

	public long getCaseOID()
	{
		return caseOID;
	}

	public void setCaseOID(long caseOID)
	{
		this.caseOID = caseOID;
	}

	public CCMSEventType getEventType()
	{
		return eventType;
	}

	public void setEventType(CCMSEventType eventType)
	{
		this.eventType = eventType;
	}

	public CCMSParty getEnteringParty()
	{
		return enteringParty;
	}

	public void setEnteringParty(CCMSParty enteringParty)
	{
		this.enteringParty = enteringParty;
	}

	public CCMSCaseDocument getDocument()
	{
		return document;
	}

	public CCMSParty getInitiatedByParty()
	{
		return initiatedByParty;
	}

	public void setInitiatedByParty(CCMSParty initiatedByParty)
	{
		this.initiatedByParty = initiatedByParty;
	}

	public void setDocument(CCMSCaseDocument document)
	{
		this.document = document;
	}

	public int getDurationTimeMin()
	{
		return durationTimeMin;
	}

	public void setDurationTimeMin(int durationTimeMin)
	{
		this.durationTimeMin = durationTimeMin;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.CASE_EVENT "
						+ " ( ID, COURT_ID, EVENT_DATE, EVENT_TYPE_ID, EVENT_SHORTCUT_CODE, PARTY_ID, "
						+ "   DESCRIPTION, CASE_ID, DURATION_TIME_MIN, INITIATED_BY_PARTY_OID, DOCUMENT_ID ) "
						+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		pStmt.setLong(1, this.caseEventOID);
		pStmt.setLong(2, this.courtOID);
		pStmt.setTimestamp(3, this.eventDate);
		pStmt.setLong(4, this.eventType.getEventTypeOID());
		pStmt.setString(5, this.eventShortcutCode);
		if (enteringParty != null)
		{
			pStmt.setLong(6, this.enteringParty.getPartyOID());
		}
		else
		{
			pStmt.setLong(6, 0L);
		}
		pStmt.setString(7, this.description);
		pStmt.setLong(8, this.caseOID);
		pStmt.setLong(9, this.durationTimeMin);

		if (this.initiatedByParty != null)
		{
			pStmt.setLong(10, initiatedByParty.getPartyOID());
		}
		else
		{
			pStmt.setLong(10, 0L);
		}

		if (this.document != null)
		{
			pStmt.setLong(11, document.getDocumentOID());
		}
		else
		{
			pStmt.setLong(11, 0L);
		}

		pStmts.add(pStmt);

		return pStmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_EVENT "
						+ " WHERE ID = ? AND COURT_ID = ?");
		pStmt.setLong(1, this.caseEventOID);
		pStmt.setLong(2, this.courtOID);

		pStmts.add(pStmt);

		return pStmts;
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genUpdateSQL(conn);
	}

	@Override
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
	
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		String sqlString = "SELECT CASE_EVENT.*, PARTY.FIRST_NAME, PARTY.LAST_NAME, CASE_DOCUMENT.DOC_NAME " 
						+ " FROM NJCCCMS.PARTY, NJCCCMS.CASE_EVENT LEFT OUTER JOIN NJCCCMS.CASE_DOCUMENT ON CASE_EVENT.DOCUMENT_ID = CASE_DOCUMENT.ID  "
						+ " WHERE CASE_EVENT.PARTY_ID = PARTY.ID " 
						+ " AND CASE_EVENT.COURT_ID = " + this.courtOID;

		if (this.caseEventOID != 0L)
		{
			sqlString += " AND CASE_EVENT.ID = " + this.caseEventOID;
		}

		if (this.caseOID != 0L)
		{
			sqlString += " AND CASE_EVENT.CASE_ID = " + this.caseOID;
		}

		PreparedStatement stmt = conn.prepareStatement(sqlString);
		pStmts.add(stmt);

		return pStmts;
	}

	public boolean equals(Object obj)
	{
		return false;
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return super.toString();
	}
}
