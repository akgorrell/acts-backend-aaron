package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Timestamp;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseDocument extends CCMSDomain
{
	private static Timestamp then = new java.sql.Timestamp(0L);// January 1, 1970 00:00:00
	private long documentOID, courtOID, caseOID;
	private String documentName = "", documentURL = "  ";
	private Date lastUpdateDate;
	private Timestamp docSent = then;
	private Timestamp docReceived = then;
	private long docCode;
	private static Logger logger = null;
	

	public CCMSCaseDocument()
	{
		CCMSCaseDocument.logger = LogManager
				.getLogger(CCMSCaseDocument.class.getName());

		
	}

	public CCMSCaseDocument(long documentOID, long courtOID)
	{
		this.documentOID = documentOID;
		this.courtOID = courtOID;
		CCMSCaseDocument.logger = LogManager
				.getLogger(CCMSCaseDocument.class.getName());

	}

	public CCMSCaseDocument(Timestamp docSent, long docCode, long courtOID)  throws Exception
		{
			this.docSent = docSent;
			this.courtOID = courtOID;
			this.docCode = docCode;
			CCMSCaseDocument.logger = LogManager
					.getLogger(CCMSCaseDocument.class.getName());
		}
	
	public CCMSCaseDocument(ResultSet rs, Connection conn) throws Exception
	{
		CCMSCaseDocument.logger = LogManager
				.getLogger(CCMSCaseDocument.class.getName());
		this.documentOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.caseOID = rs.getLong("CASE_ID");
		this.documentName = rs.getString("DOC_NAME");
		this.documentURL = rs.getString("DOC_URL");
		this.lastUpdateDate = new Date(rs.getDate("LAST_UPDATE_DATE").getTime());
		this.docSent = rs.getTimestamp("DOC_SENT");
		this.docReceived = rs.getTimestamp("DOC_RECEIVED");
		this.docCode = rs.getLong("DOC_CODE");
	}


	public long getDocumentOID()
	{
		return documentOID;
	}

	public void setDocumentOID(long documentOID)
	{
		this.documentOID = documentOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public long getCaseOID()
	{
		return caseOID;
	}

	public void setCaseOID(long caseOID)
	{
		this.caseOID = caseOID;
	}

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	public String getDocumentURL()
	{
		return documentURL;
	}

	public void setDocumentURL(String documentURL)
	{
		this.documentURL = documentURL;
	}
	
	public long getDocCode()
	{
		return docCode;
	}

	public void setDocCode(long docCode)
	{
		this.docCode = docCode;
	}

	public Timestamp getDocSent()
	{
		return docSent;
	}

	public void setDocSent(Timestamp docSent)
	{
		this.docSent = docSent;
	}

	public Timestamp getDocReceived()
	{
		return docReceived;
	}

	public void setDocReceived(Timestamp docReceived)
	{
		this.docReceived = docReceived;
	}
	
	public Date getLastUpdateDate()
	{
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate)
	{
		this.lastUpdateDate = lastUpdateDate;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		java.sql.Date now = new java.sql.Date(System.currentTimeMillis());
		java.sql.Timestamp then = new java.sql.Timestamp(0L);
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.CASE_DOCUMENT " + 
				" ( ID, CASE_ID, COURT_ID, DOC_NAME, DOC_URL, LAST_UPDATE_DATE, DOC_SENT, DOC_RECEIVED, DOC_CODE ) " + 
				" VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? ) " );
		stmt.setLong(1, this.documentOID);
		stmt.setLong(2, this.caseOID);
		stmt.setLong(3, this.courtOID);
		stmt.setString(4, this.documentName);
		stmt.setString(5, this.documentURL);
		stmt.setDate(6, now);
		stmt.setTimestamp(7,this.docSent);
		stmt.setTimestamp(8,this.docReceived);
		stmt.setLong(9,this.docCode);
		pStmts.add(stmt);
		return pStmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		
		
		return pStmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		String sqlString = " SELECT * FROM NJCCCMS.CASE_DOCUMENT WHERE COURT_ID = " + this.courtOID;
		
		if ( this.documentOID != 0L )
		{
			sqlString += " AND ID = " + this.documentOID;
		}
		else
		{
			sqlString += " AND DOC_SENT = '" +this.docSent + "' AND DOC_CODE = " + this.docCode;
		}
		
		
		sqlString += " ORDER BY LAST_UPDATE_DATE DESC ";
		
		PreparedStatement stmt = conn.prepareStatement(sqlString);
		pStmts.add(stmt);
		
		return pStmts;
	}
	
    @Override
    public List<PreparedStatement> genUpdateSQL(Connection conn)
                    throws Exception
    {
            List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
            PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.CASE_DOCUMENT " +
                            "   SET DOC_RECEIVED = \"" + this.docReceived + "\", DOC_NAME = \"" + this.documentName + "\", DOC_URL = \"" + this.documentURL + "\"" +
                            " WHERE COURT_ID = " + this.courtOID + " AND ID = " + this.documentOID);
            stmts.add(stmt);
            
            return stmts;
    }


	@Override
	public boolean equals(Object obj)
	{
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return super.toString();
	}
}
