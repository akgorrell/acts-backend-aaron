package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSChargeFactor extends CCMSDomain
{
	private long chargeFactorOID, courtOID;
	private String name, description;
	private static Logger logger = null;

	public CCMSChargeFactor()
	{
		CCMSChargeFactor.logger = LogManager.getLogger(CCMSChargeFactor.class
				.getName());

	}

	public CCMSChargeFactor(long chargeFactorOID, long courtOID)
	{
		CCMSChargeFactor.logger = LogManager.getLogger(CCMSChargeFactor.class
				.getName());
		this.chargeFactorOID = chargeFactorOID;
		this.courtOID = courtOID;
	}

	public CCMSChargeFactor(ResultSet rs, Connection conn) throws Exception
	{
		CCMSChargeFactor.logger = LogManager.getLogger(CCMSChargeFactor.class
				.getName());
		this.chargeFactorOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSChargeFactor)
		{
			CCMSChargeFactor chgFact = (CCMSChargeFactor) obj;
			if (chgFact.chargeFactorOID == this.chargeFactorOID)
			{
				return true;
			}
		}
		return false;
	}

	public long getChargeFactorOID()
	{
		return chargeFactorOID;
	}

	public void setChargeFactorOID(long chargeFactorOID)
	{
		this.chargeFactorOID = chargeFactorOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CHARGE_FACTOR "
						+ " WHERE COURT_ID = ? ");
		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);

		return stmts;
	}
}
