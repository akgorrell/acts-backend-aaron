package org.ncsc.ccms.domain;

import java.util.Date;

public class CCMSCaseParty extends CCMSDomain
{
	private CCMSParty caseParty;
	private CCMSCasePartyRole role;
	private Date startDate, endDate = null;
	
	public CCMSCaseParty()
	{
		
	}
	
	public CCMSCaseParty(CCMSParty party, CCMSCasePartyRole role)
	{
		this.caseParty = party;
		this.role = role;
	}
	

	public CCMSParty getParty()
	{
		return caseParty;
	}

	public void setParty(CCMSParty caseParty)
	{
		this.caseParty = caseParty;
	}

	public CCMSCasePartyRole getRole()
	{
		return role;
	}

	public void setRole(CCMSCasePartyRole role)
	{
		this.role = role;
	}

	public CCMSParty getCaseParty()
	{
		return caseParty;
	}

	public void setCaseParty(CCMSParty caseParty)
	{
		this.caseParty = caseParty;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public boolean equals(Object obj)
	{
		if ( obj instanceof CCMSCaseParty )
		{
			CCMSCaseParty objCaseParty = (CCMSCaseParty)obj;
			if ( this.caseParty.getPartyOID() == objCaseParty.getParty().getPartyOID())
			{
				return true;
			}
		}
		return false;
	}
	
	
}
