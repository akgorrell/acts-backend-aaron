package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSPermission extends CCMSDomain
{
	private static Logger logger = null;
	private long permissionOID = 0L, courtOID = 0L;
	private int permissionID = 0;
	private String permissionName = null, permissionDescription = null;
	
	// Permission Levels
	public static int CREATE_CASE = 2000;
	public static int UPDATE_CASE = 2001;

	public static int CREATE_PARTY = 3000;
	public static int UPDATE_PARTY = 3001;
	
	public static int CREATE_JUD_ASSGMT = 4000;
	public static int UPDATE_JUD_ASSGMT = 4001;
	
	public static int CREATE_CASE_HEARING = 4050;
	public static int UPDATE_CASE_HEARING = 4051;
	public static int DELETE_CASE_HEARING = 4052;
	
	public static int CREATE_TASK = 4100;
	public static int UPDATE_TASK = 4101;

	public CCMSPermission()
	{
		CCMSPermission.logger = LogManager.getLogger(CCMSPermission.class.getName());

	}

	public CCMSPermission(long permissionOID, long courtOID)
	{
		CCMSPermission.logger = LogManager.getLogger(CCMSPermission.class.getName());
		this.permissionOID = permissionOID;
		this.courtOID = courtOID;
	}

	public CCMSPermission(ResultSet rs, Connection conn)
			throws Exception
	{
		CCMSPermission.logger = LogManager.getLogger(CCMSCaseHearing.class
				.getName());
		this.permissionOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.permissionID = rs.getInt("PERMISSION_ID");
		this.permissionName = rs.getString("NAME");
		this.permissionDescription = rs.getString("DESCRIPTION");
	}

	public long getPermissionOID()
	{
		return permissionOID;
	}

	public void setPermissionOID(long permissionOID)
	{
		this.permissionOID = permissionOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public int getPermissionID()
	{
		return permissionID;
	}

	public void setPermissionID(int permissionID)
	{
		this.permissionID = permissionID;
	}

	public String getPermissionName()
	{
		return permissionName;
	}

	public void setPermissionName(String permissionName)
	{
		this.permissionName = permissionName;
	}

	public String getPermissionDescription()
	{
		return permissionDescription;
	}

	public void setPermissionDescription(String permissionDescription)
	{
		this.permissionDescription = permissionDescription;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.PERMISSION "
						+ " ( ID, COURT_ID, PERMISSION_ID, NAME, DESCRIPTION ) "
						+ " VALUES ( ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.permissionOID);
		stmt.setLong(2, this.courtOID);
		stmt.setInt(3, this.permissionID);
		stmt.setString(4, this.permissionName);
		stmt.setString(5, this.permissionDescription);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("UPDATE NJCCCMS.PERMISSION "
						+ "   SET NAME = ?, DESCRIPTION = ?"
						+ " WHERE ID = ? AND COURT_ID = ?");
		stmt.setString(1, this.permissionName);
		stmt.setString(2, this.permissionDescription);
		stmt.setLong(3, this.permissionOID);
		stmt.setLong(4, this.courtOID);

		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.PERMISSION"
						+ " WHERE COURT_ID = ? ORDER BY PERMISSION_ID");
		pStmt.setLong(1, this.courtOID);
		pStmts.add(pStmt);

		return pStmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSPermission)
		{
			CCMSPermission permObj = (CCMSPermission) obj;
			if (permObj.getPermissionOID() == this.getPermissionOID()) 
			{ 
				return true; 
				}
			if (permObj.getPermissionID() == this.getPermissionID()) 
			{ 
				return true; 
			}
		}
		return false;
	}

}
