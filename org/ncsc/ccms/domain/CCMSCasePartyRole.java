package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CCMSCasePartyRole extends CCMSDomain
{

	private long casePartyRoleOID, courtOID;

	// True indicates this is a role that can have codefendants
	private boolean codefendantIndicator = false;
	
	private String name, shortName, description;
	
	public CCMSCasePartyRole()
	{
		
	}

	public CCMSCasePartyRole(long casePartyRoleOID, long courtOID)
	{
		this.casePartyRoleOID = casePartyRoleOID;
		this.courtOID = courtOID;
	}
	

	public CCMSCasePartyRole(String roleText, long courtOID)
	{
		this.name = roleText;
		this.courtOID = courtOID;
	}

	public CCMSCasePartyRole(long casePartyRoleOID, long courtOID, String name)
	{
		this.casePartyRoleOID = casePartyRoleOID;
		this.courtOID = courtOID;
		this.name = name;
	}

	public CCMSCasePartyRole(ResultSet rs) throws Exception
	{
		this.casePartyRoleOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.courtOID = rs.getLong("COURT_ID");
		
		int codefendantInt = rs.getInt("CODEFENDANT_INDICATOR");
		this.codefendantIndicator = CCMSDomainUtilities.intToBool(codefendantInt);
	}


	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.PARTY_ROLES " +
				" ( ID, NAME, DESCRIPTION, COURT_ID, CODEFENDANT_INDICATOR ) " +
				" VALUES (?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.casePartyRoleOID);
		stmt.setString(2, this.name);
		stmt.setString(3, this.description);
		stmt.setLong(4, this.courtOID);
		stmt.setInt(5, CCMSDomainUtilities.boolToInt(codefendantIndicator));
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.PARTY_ROLES " + 
				"   SET NAME = ?, DESCRIPTION = ?, CODEFENDANT_INDICATOR = ? " + 
				" WHERE ID = ? AND COURT_ID = ?");
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setInt(3, CCMSDomainUtilities.boolToInt(this.codefendantIndicator));
		stmt.setLong(4, this.casePartyRoleOID);
		stmt.setLong(5, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.PARTY_ROLES "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}


	public long getStatusOID()
	{
		return casePartyRoleOID;
	}

	public void setStatusOID(long statusOID)
	{
		this.casePartyRoleOID = statusOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getShortName()
	{
		return shortName;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public long getCaseTypeOID()
	{
		return casePartyRoleOID;
	}

	public void setCaseTypeOID(long caseTypeOID)
	{
		this.casePartyRoleOID = caseTypeOID;
	}

	public long getCasePhaseOID()
	{
		return casePartyRoleOID;
	}

	public void setCasePhaseOID(long casePhaseOID)
	{
		this.casePartyRoleOID = casePhaseOID;
	}

	public long getCasePartyRoleOID()
	{
		return casePartyRoleOID;
	}

	public void setCasePartyRoleOID(long casePartyRoleOID)
	{
		this.casePartyRoleOID = casePartyRoleOID;
	}
	

	public boolean isCodefendantIndicator()
	{
		return codefendantIndicator;
	}

	public void setCodefendantIndicator(boolean codefendantIndicator)
	{
		this.codefendantIndicator = codefendantIndicator;
	}


	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCasePartyRole)
		{
			CCMSCasePartyRole qRole = (CCMSCasePartyRole) obj;
			if (qRole.casePartyRoleOID == this.casePartyRoleOID)
			{
				return true;
			}
			else if ( qRole.name.equals(this.name))
			{
				return true;
			}
		}
		return false;
	}
}
