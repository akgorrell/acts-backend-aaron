package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
// import org.ncsc.ccms.domain.CCMSDomain;

public class CCMSPersonIdentificationType extends CCMSDomain
{

	private long personIdentificationTypeOID, courtOID;
	private String name = "", description = "";
	private static Logger logger = null;

	public CCMSPersonIdentificationType()
	{
		CCMSPersonIdentificationType.logger = LogManager
				.getLogger(CCMSPersonIdentificationType.class.getName());
	}

	public CCMSPersonIdentificationType(long personIdentificationTypeOID,
			long courtOID)
	{
		CCMSPersonIdentificationType.logger = LogManager
				.getLogger(CCMSPersonIdentificationType.class.getName());
		this.personIdentificationTypeOID = personIdentificationTypeOID;
		this.courtOID = courtOID;
	}

	public CCMSPersonIdentificationType(ResultSet rs) throws Exception
	{
		CCMSPersonIdentificationType.logger = LogManager
				.getLogger(CCMSPersonIdentificationType.class.getName());
		this.personIdentificationTypeOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.courtOID = rs.getLong("COURT_ID");
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.PERSON_IDENTIFICATION_TYPE "
						+ " 	( ID, COURT_ID, NAME, DESCRIPTION )"
						+ " VALUES ( ?, ?, ?, ? ) ");
		stmt.setLong(1, this.personIdentificationTypeOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.name);
		stmt.setString(4, this.description);
		stmts.add(stmt);

		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("UPDATE NJCCCMS.PERSON_IDENTIFICATION_TYPE "
						+ "   SET NAME = ?, DESCRIPTION = ? "
						+ " WHERE ID = ? AND COURT_ID = ?");
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setLong(3, this.personIdentificationTypeOID);
		stmt.setLong(4, this.courtOID);

		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.PERSON_IDENTIFICATION_TYPE "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public long getPersonIdentificationTypeOID()
	{
		return personIdentificationTypeOID;
	}

	public void setPersonIdentificationTypeOID(long personIdentificationTypeOID)
	{
		this.personIdentificationTypeOID = personIdentificationTypeOID;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSPersonIdentificationType)
		{
			CCMSPersonIdentificationType qIDType = (CCMSPersonIdentificationType) obj;
			if (qIDType.getPersonIdentificationTypeOID() == this
					.getPersonIdentificationTypeOID())
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String stringObj = " PersonIdentificationTypeOID: "
				+ this.personIdentificationTypeOID + " Name: " + this.name
				+ " Desc: " + this.description;

		return stringObj;
	}

	public static void main(String args[])
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			long idOID = CCMSDomainUtilities.getInstance().genOID();

			// Insert
			CCMSPersonIdentificationType qID = new CCMSPersonIdentificationType();
			qID.setPersonIdentificationTypeOID(idOID);
			qID.setCourtOID(court.getCourtOID());
			qID.setName("IDTYPE_NAME");
			qID.setDescription("DESC");

			qID.genInsertSQL(conn).get(0).execute();
			conn.commit();

			// Query
			qID = new CCMSPersonIdentificationType();
			qID.setCourtOID(court.getCourtOID());
			ResultSet rs = qID.genSelectSQL(conn).get(0).executeQuery();
			while (rs.next())
			{
				CCMSPersonIdentificationType dbID = new CCMSPersonIdentificationType(
						rs);
				System.out.println(dbID.toString());
			}

			// Update
			qID.setPersonIdentificationTypeOID(idOID);
			qID.setCourtOID(court.getCourtOID());
			qID.setName("Updated NAME");
			qID.setDescription("Updated DESC");
			qID.genUpdateSQL(conn).get(0).execute();

			conn.commit();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

}
