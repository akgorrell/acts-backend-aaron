package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CCMSDocumentTemplate extends CCMSDomain
{
	private long documentTemplateOID;
	private long courtOID;
	private String documentName = "";
	private String fileName = "";
	

	public CCMSDocumentTemplate()
	{
	}

	public CCMSDocumentTemplate(long documentTemplateOID, long courtOID)
	{
		this.documentTemplateOID = documentTemplateOID;
		this.courtOID = courtOID;
	}

	public CCMSDocumentTemplate(ResultSet rs, Connection conn) throws Exception
	{
		this.documentTemplateOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.documentName = rs.getString("DOCUMENT_NAME");
		this.fileName = rs.getString("FILE_NAME");
	}

	public long getDocumentTemplateOID()
	{
		return documentTemplateOID;
	}

	public void setDocumentTemplateOID(long documentTemplateOID)
	{
		this.documentTemplateOID = documentTemplateOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getDocumentName()
	{
		return documentName;
	}

	public void setDocumentName(String documentName)
	{
		this.documentName = documentName;
	}

	public String getFileName()
	{
		return fileName;
	}

	public void setFileName(String fileName)
	{
		this.fileName = fileName;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		String sqlString = "SELECT * FROM NJCCCMS.DOCUMENT_TEMPLATE " + 
				" WHERE COURT_ID = " + this.courtOID;
		
		if ( this.documentTemplateOID != 0L )
		{
			sqlString += " AND ID = " + this.documentTemplateOID;
		}
		
		stmts.add( conn.prepareStatement(sqlString));
		
		return stmts;
	}

}
