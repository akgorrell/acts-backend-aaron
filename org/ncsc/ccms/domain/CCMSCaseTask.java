package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseTask extends CCMSDomain
{
	private long taskOID = 0L, courtOID = 0L;
	private CCMSDocumentTemplate docTemplate = null;
	private int taskPriorityCode = 0;
	private CCMSTaskType taskType = null;

	// either an individual staff member will be assigned to a task or an entire
	// pool of staff members
	private CCMSParty assignedParty = new CCMSParty();
	private CCMSStaffPool assignedPool = new CCMSStaffPool();

	private Date assignedDate, dueDate, doneDate;
	private String notDoneReason = "";
	private String details = "";
	private CCMSCase associatedCase = null;
	private static Logger logger = null;

	// Requires setting of court oid and case OID
	public static String FETCH_CASE_TASK_SQL = "SELECT CASE_TASK.*, CASE.CASE_NUMBER, PARTY.FIRST_NAME, PARTY.LAST_NAME, STAFF_POOL.NAME, DOCUMENT_TEMPLATE.* "
			+ " FROM NJCCCMS.CASE, NJCCCMS.CASE_TASK "
			+ "		LEFT OUTER JOIN NJCCCMS.PARTY ON CASE_TASK.ASSIGNED_PARTY_ID = PARTY.ID "
			+ "		LEFT OUTER JOIN NJCCCMS.DOCUMENT_TEMPLATE ON CASE_TASK.DOCUMENT_TEMPLATE_ID = DOCUMENT_TEMPLATE.ID "
			+ "		LEFT OUTER JOIN NJCCCMS.STAFF_POOL ON CASE_TASK.ASSIGNED_POOL_ID = STAFF_POOL.ID "
			+ " WHERE CASE.CASE_ID = NJCCCMS.CASE_TASK.CASE_ID "
			+ " AND CASE_TASK.COURT_ID = ? AND CASE_TASK.CASE_ID = ? ";
	
	// This SQL is used when fetching all tasks associated with a particular user ID
	public static String FETCH_USER_TASK_SQL = "SELECT CASE_TASK.*, CASE.CASE_NUMBER, PARTY.FIRST_NAME, PARTY.LAST_NAME, STAFF_POOL.NAME, DOCUMENT_TEMPLATE.* "
			+ " FROM NJCCCMS.CASE, NJCCCMS.CASE_TASK "
			+ "		LEFT OUTER JOIN NJCCCMS.PARTY ON CASE_TASK.ASSIGNED_PARTY_ID = PARTY.ID "
			+ "		LEFT OUTER JOIN NJCCCMS.DOCUMENT_TEMPLATE ON CASE_TASK.DOCUMENT_TEMPLATE_ID = DOCUMENT_TEMPLATE.ID "
			+ "		LEFT OUTER JOIN NJCCCMS.STAFF_POOL ON CASE_TASK.ASSIGNED_POOL_ID = STAFF_POOL.ID "
			+ " WHERE CASE.CASE_ID = NJCCCMS.CASE_TASK.CASE_ID "
			+ "   AND PARTY.USER_NAME = ? "
			+ "   AND CASE_TASK.COURT_ID = ? ";
	
	// This SQL is used when fetching all tasks associated with a particular pool
	public static String FETCH_POOL_TASK_SQL = "SELECT CASE_TASK.*, CASE.CASE_NUMBER, PARTY.FIRST_NAME, PARTY.LAST_NAME, DOCUMENT_TEMPLATE.* "
			+ " FROM NJCCCMS.STAFF_POOL_REL, NJCCCMS.CASE, NJCCCMS.CASE_TASK "
			+ "		LEFT OUTER JOIN NJCCCMS.PARTY ON CASE_TASK.ASSIGNED_PARTY_ID = PARTY.ID "
			+ "		LEFT OUTER JOIN NJCCCMS.DOCUMENT_TEMPLATE ON CASE_TASK.DOCUMENT_TEMPLATE_ID = DOCUMENT_TEMPLATE.ID "
			+ " WHERE CASE.CASE_ID = NJCCCMS.CASE_TASK.CASE_ID " 
			+ "   AND PARTY.ID = STAFF_POOL_REL.PARTY_ID "
			+ "   AND STAFF_POOL_REL.POOL_ID = CASE_TASK.ASSIGNED_POOL_ID "
			+ "   AND PARTY.USER_NAME = ? "
			+ "   AND CASE_TASK.COURT_ID = ? ";

	public CCMSCaseTask()
	{
		CCMSCaseTask.logger = LogManager
				.getLogger(CCMSCaseTask.class.getName());
	}

	public CCMSCaseTask(long taskOID, long courtOID)
	{
		CCMSCaseTask.logger = LogManager
				.getLogger(CCMSCaseTask.class.getName());
		this.taskOID = taskOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseTask(ResultSet rs, Connection conn, CCMSCourt court,
			boolean flag) throws Exception
	{
		CCMSCaseTask.logger = LogManager
				.getLogger(CCMSCaseTask.class.getName());
		this.taskOID = rs.getLong("CASE_TASK.ID");
		this.courtOID = rs.getLong("CASE_TASK.COURT_ID");
		this.assignedDate = rs.getDate("CASE_TASK.ASSIGNED_DT");
		this.dueDate = rs.getDate("CASE_TASK.DUE_DT");
		this.doneDate = rs.getDate("CASE_TASK.DONE_DT");
		this.details = rs.getString("CASE_TASK.DETAILS");
		this.taskPriorityCode = rs.getInt("CASE_TASK.TASK_PRIORITY_CODE");

		// Set the task type
		long taskTypeOID = rs.getLong("CASE_TASK.TASK_TYPE_ID");
		this.taskType = new CCMSTaskType(taskTypeOID, court.getCourtOID());
		taskType = (CCMSTaskType)CCMSDomainUtilities.getInstance().retrieveFullObject(taskType, court);
		this.setTaskType(taskType);

		this.notDoneReason = rs.getString("CASE_TASK.NOT_DONE_REASON");

		// Set the associated case
		long associatedCaseOID = rs.getLong("CASE_TASK.CASE_ID");
		String caseNumber = rs.getString("CASE.CASE_NUMBER");
		CCMSCase associatedCase = new CCMSCase(associatedCaseOID, court);
		associatedCase.setCaseNumber(caseNumber);
		this.setAssociatedCase(associatedCase);

		// Set Assigned Party
		long assignedPartyOID = rs.getLong("CASE_TASK.ASSIGNED_PARTY_ID");
		if (assignedPartyOID != 0L)
		{
			String assignedPartyFirst = rs.getString("PARTY.FIRST_NAME");
			String assignedPartyLast = rs.getString("PARTY.LAST_NAME");
			CCMSParty assignedParty = new CCMSParty(assignedPartyOID,
					this.courtOID);
			assignedParty.setFirstName(assignedPartyFirst);
			assignedParty.setLastName(assignedPartyLast);
			this.setAssignedParty(assignedParty);
		}
		
		long assignedPoolOID = rs.getLong("CASE_TASK.ASSIGNED_POOL_ID");
		if (assignedPoolOID != 0L)
		{
			String poolName = rs.getString("STAFF_POOL.NAME");
			CCMSStaffPool assignedPool = new CCMSStaffPool(assignedPoolOID,
					this.courtOID);
			assignedPool.setPoolName(poolName);
			this.setAssignedPool(assignedPool);
		}

		// Any assigned documents with this task. Grabbing
		long documentTemplateOID = rs.getLong("DOCUMENT_TEMPLATE_ID");
		if (documentTemplateOID != 0L)
		{
			CCMSDocumentTemplate docTemplate = new CCMSDocumentTemplate(documentTemplateOID,
					this.courtOID);
			docTemplate.setDocumentName(rs.getString("DOCUMENT_TEMPLATE.DOCUMENT_NAME"));
			docTemplate.setFileName(rs.getString("DOCUMENT_TEMPLATE.FILE_NAME"));

			this.docTemplate = docTemplate;
		}
	}

	/**
	 * Used when retrieving all tasks assigned to a user or a pool
	 * @param rs
	 * @param conn
	 * @param court
	 * @throws Exception
	 */
	public CCMSCaseTask(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		CCMSCaseTask.logger = LogManager
				.getLogger(CCMSCaseTask.class.getName());
		this.taskOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.assignedDate = rs.getDate("ASSIGNED_DT");
		this.dueDate = rs.getDate("DUE_DT");
		this.doneDate = rs.getDate("DONE_DT");
		this.details = rs.getString("DETAILS");
		this.taskPriorityCode = rs.getInt("TASK_PRIORITY_CODE");

		long taskTypeOID = rs.getLong("TASK_TYPE_ID");
		this.taskType = new CCMSTaskType(taskTypeOID, court.getCourtOID());
		this.setTaskType(taskType);

		this.notDoneReason = rs.getString("NOT_DONE_REASON");
		long associatedCaseOID = rs.getLong("CASE_ID");
		this.setAssociatedCase(new CCMSCase(associatedCaseOID, court));

		// Create the party object from the party OID
		long assignedPartyOID = rs.getLong("ASSIGNED_PARTY_ID");
		if (assignedPartyOID != 0L)
		{
			this.assignedParty = new CCMSParty(assignedPartyOID, this.courtOID);
			PreparedStatement pStmt1 = assignedParty.genSelectSQL(conn).get(0);
			ResultSet rs1 = pStmt1.executeQuery();
			while (rs1.next())
			{
				assignedParty = new CCMSParty(rs1, conn, false);
			}
			pStmt1.close();
			rs1.close();
		}

		long assignedPoolOID = rs.getLong("ASSIGNED_POOL_ID");
		if (assignedPoolOID != 0L)
		{
			this.assignedPool = new CCMSStaffPool(assignedPoolOID,
					this.courtOID);

			PreparedStatement pStmt2 = assignedPool.genSelectSQL(conn).get(0);
			ResultSet rs2 = pStmt2.executeQuery();
			while (rs2.next())
			{
				assignedPool = new CCMSStaffPool(rs2, conn);
			}
			pStmt2.close();
			rs2.close();
		}

		// Set the Task Type based on the retrieve taskTypeOID
		this.taskType = new CCMSTaskType(taskTypeOID, court.getCourtOID());
		this.taskType = (CCMSTaskType)CCMSDomainUtilities.getInstance().retrieveFullObject(taskType, new CCMSCourt(this.courtOID));

		// Retrieve the case number associated with this task. Do not retrieve
		// entire case because
		// Will result in a recursive query (Task queries case which in turn
		// queries task)
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT CASE_NUMBER FROM NJCCCMS.CASE "
						+ " WHERE CASE.CASE_ID = ? "
						+ " AND CASE.COURT_ID = ? ");
		pStmt.setLong(1, this.associatedCase.getCaseOID());
		pStmt.setLong(2, this.courtOID);

		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			CCMSCase rsCase = new CCMSCase(this.associatedCase.getCaseOID(),
					court);
			rsCase.setCaseNumber(rs1.getString("CASE_NUMBER"));
			this.associatedCase = rsCase;
		}
		pStmt.close();
		rs1.close();

		long documentTemplateOID = rs.getLong("DOCUMENT_TEMPLATE_ID");
		if (documentTemplateOID != 0L)
		{
			this.docTemplate = new CCMSDocumentTemplate(documentTemplateOID,
					this.courtOID);
			PreparedStatement pStmt5 = docTemplate.genSelectSQL(conn).get(0);
			ResultSet rs5 = pStmt5.executeQuery();
			while (rs5.next())
			{
				this.docTemplate = new CCMSDocumentTemplate(rs5, conn);
			}
			pStmt5.close();
			rs5.close();
		}

	}

	public static void main(String[] args)
	{


	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.CASE_TASK ( "
						+ " ID, COURT_ID, ASSIGNED_DT, DUE_DT, DONE_DT, DETAILS, ASSIGNED_PARTY_ID, ASSIGNED_POOL_ID, TASK_TYPE_ID, "
						+ " NOT_DONE_REASON, CASE_ID, TASK_PRIORITY_CODE, DOCUMENT_TEMPLATE_ID ) "
						+ " values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");

		pStmt.setLong(1, this.taskOID);

		pStmt.setLong(2, this.courtOID);

		if (this.assignedDate != null)
		{
			pStmt.setDate(3, new java.sql.Date(this.assignedDate.getTime()));
		}
		else
		{
			pStmt.setNull(3, java.sql.Types.DATE);
		}

		if (this.dueDate != null)
		{
			pStmt.setDate(4, new java.sql.Date(this.dueDate.getTime()));
		}
		else
		{
			pStmt.setNull(4, java.sql.Types.DATE);
		}

		if (this.doneDate != null)
		{
			pStmt.setDate(5, new java.sql.Date(this.doneDate.getTime()));
		}
		else
		{
			pStmt.setNull(5, java.sql.Types.DATE);
		}

		if (this.details != null)
		{
			pStmt.setString(6, this.details);
		}
		else
		{
			pStmt.setNull(6, java.sql.Types.VARCHAR);
		}

		if (this.assignedParty != null)
		{
			pStmt.setLong(7, this.assignedParty.getPartyOID());
		}

		if (this.assignedPool != null)
		{
			pStmt.setLong(8, this.assignedPool.getPoolOID());
		}

		if (this.taskType != null)
		{
			pStmt.setLong(9, taskType.getTaskTypeOID());
		}
		else
		{
			pStmt.setNull(9, java.sql.Types.NUMERIC);
		}

		if (this.notDoneReason != null)
		{
			pStmt.setString(10, this.notDoneReason);
		}
		else
		{
			pStmt.setNull(10, java.sql.Types.VARCHAR);
		}

		pStmt.setLong(11, this.associatedCase.getCaseOID());

		pStmt.setInt(12, this.taskPriorityCode);

		if (this.docTemplate != null)
		{
			pStmt.setLong(13, this.docTemplate.getDocumentTemplateOID());
		}
		else
		{
			pStmt.setNull(13, java.sql.Types.NUMERIC);
		}

		pStmts.add(pStmt);

		return pStmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_TASK "
						+ " WHERE ID = ? AND COURT_ID = ?");
		stmt.setLong(1, this.taskOID);
		stmt.setLong(2, this.courtOID);
		stmts.add(stmt);

		return stmts;
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("UPDATE NJCCCMS.CASE_TASK "
						+ " SET ASSIGNED_DT = ?, ASSIGNED_PARTY_ID = ?, ASSIGNED_POOL_ID = ?, "
						+ " DETAILS = ?, DONE_DT = ?, DUE_DT = ?, TASK_TYPE_ID = ?, TASK_PRIORITY_CODE = ?, DOCUMENT_TEMPLATE_ID = ? "
						+ " WHERE ID = ? AND COURT_ID = ?");

		stmt.setDate(1, new java.sql.Date(this.assignedDate.getTime()));

		if (this.assignedParty != null)
		{
			stmt.setLong(2, this.assignedParty.getPartyOID());
		}
		else
		{
			stmt.setLong(2, 0L);
		}

		if (this.assignedPool != null)
		{
			stmt.setLong(3, this.assignedPool.getPoolOID());
		}
		else
		{
			stmt.setLong(3, 0L);
		}

		stmt.setString(4, this.details);

		if (this.doneDate != null)
		{
			stmt.setDate(5, new java.sql.Date(this.doneDate.getTime()));
		}
		else
		{
			stmt.setNull(5, java.sql.Types.DATE);
		}

		if (this.dueDate != null)
		{
			stmt.setDate(6, new java.sql.Date(this.dueDate.getTime()));
		}
		else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}

		stmt.setLong(7, this.taskType.getTaskTypeOID());

		stmt.setInt(8, this.getTaskPriorityCode());

		if (this.docTemplate != null)
		{
			stmt.setLong(9, this.docTemplate.getDocumentTemplateOID());
		}
		else
		{
			stmt.setNull(9, java.sql.Types.NUMERIC);
		}

		stmt.setLong(10, this.taskOID);
		stmt.setLong(11, this.courtOID);

		pStmts.add(stmt);

		return pStmts;
	}

	@Override
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		String sqlString = "SELECT * FROM NJCCCMS.CASE_TASK WHERE COURT_ID = "
				+ this.courtOID;

		if (this.taskOID != 0L)
		{
			sqlString += " AND ID = " + this.taskOID;
		}

		if (this.assignedParty != null && assignedParty.getPartyOID() != 0L)
		{
			sqlString += " AND ASSIGNED_PARTY_ID = "
					+ this.assignedParty.getPartyOID();
		}

		if (this.assignedPool != null && assignedPool.getPoolOID() != 0L)
		{
			sqlString += " AND ASSIGNED_POOL_ID = "
					+ this.assignedPool.getPoolOID();
		}

		PreparedStatement stmt = conn.prepareStatement(sqlString);
		pStmts.add(stmt);

		return pStmts;

	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}

	public CCMSTaskType getTaskType()
	{
		return taskType;
	}

	public void setTaskType(CCMSTaskType taskType)
	{
		this.taskType = taskType;
	}

	public long getTaskOID()
	{
		return taskOID;
	}

	public void setTaskOID(long taskOID)
	{
		this.taskOID = taskOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public Date getAssignedDate()
	{
		return assignedDate;
	}

	public void setAssignedDate(Date assignedDate)
	{
		this.assignedDate = assignedDate;
	}

	public Date getDueDate()
	{
		return dueDate;
	}

	public void setDueDate(Date dueDate)
	{
		this.dueDate = dueDate;
	}

	public Date getDoneDate()
	{
		return doneDate;
	}

	public void setDoneDate(Date doneDate)
	{
		this.doneDate = doneDate;
	}

	public String getNotDoneReason()
	{
		return notDoneReason;
	}

	public void setNotDoneReason(String notDoneReason)
	{
		this.notDoneReason = notDoneReason;
	}

	public String getDetails()
	{
		return details;
	}

	public void setDetails(String details)
	{
		this.details = details;
	}

	public CCMSParty getAssignedParty()
	{
		return assignedParty;
	}

	public void setAssignedParty(CCMSParty assignedParty)
	{
		this.assignedParty = assignedParty;
	}

	public CCMSCase getAssociatedCase()
	{
		return associatedCase;
	}

	public void setAssociatedCase(CCMSCase associatedCase)
	{
		this.associatedCase = associatedCase;
	}

	public String getCaseCaption()
	{
		return "CAPTION";
	}

	public CCMSStaffPool getAssignedPool()
	{
		return assignedPool;
	}

	public void setAssignedPool(CCMSStaffPool assignedPool)
	{
		this.assignedPool = assignedPool;
	}

	public int getTaskPriorityCode()
	{
		return taskPriorityCode;
	}

	public void setTaskPriorityCode(int taskPriorityCode)
	{
		this.taskPriorityCode = taskPriorityCode;
	}

	public CCMSDocumentTemplate getDocTemplate()
	{
		return docTemplate;
	}

	public void setDocTemplate(CCMSDocumentTemplate docTemplate)
	{
		this.docTemplate = docTemplate;
	}

	public String toString()
	{
		String objString = " TaskOID: " + this.taskOID + " Task Type: "
				+ this.taskType + " Details: " + this.details + " Assigned: "
				+ this.assignedDate + " Due: " + this.dueDate;

		return objString;
	}

}
