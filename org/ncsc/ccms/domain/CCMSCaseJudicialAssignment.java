package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CCMSCaseJudicialAssignment extends CCMSDomain
{


	private long judicialAssignmentOID = 0L, courtOID = 0L;
	private CCMSParty judicialOfficial = null;
	private Date startDate = null;
	private Date endDate = null;
	private long caseOID = 0L;

	public long getCaseOID()
	{
		return caseOID;
	}

	public void setCaseOID(long caseOID)
	{
		this.caseOID = caseOID;
	}

	public CCMSCaseJudicialAssignment(long judicialAssignmentOID, long courtOID)
	{
		this.judicialAssignmentOID = judicialAssignmentOID;
		this.courtOID = courtOID;
	}
	
	public CCMSCaseJudicialAssignment( ResultSet rs, Connection conn ) throws Exception
	{
		this.judicialAssignmentOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.caseOID = rs.getLong("CASE_ID");
		
		long judicialPartyOID = rs.getLong("JUDICIAL_PARTY_ID");
		judicialOfficial = new CCMSParty(judicialPartyOID);
		PreparedStatement pStmt1 = judicialOfficial.genSelectSQL(conn).get(0);
		ResultSet rs1 = pStmt1.executeQuery();
		while (rs1.next())
		{
			judicialOfficial = new CCMSParty(rs1, conn, true);
		}
		pStmt1.close();
		rs1.close();
		
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");
	}

	public long getJudicialAssignmentOID()
	{
		return judicialAssignmentOID;
	}

	public void setJudicialAssignmentOID(long judicialAssignmentOID)
	{
		this.judicialAssignmentOID = judicialAssignmentOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public CCMSParty getJudicialOfficial()
	{
		return judicialOfficial;
	}

	public void setJudicialOfficial(CCMSParty judicialOfficial)
	{
		this.judicialOfficial = judicialOfficial;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}
	
	
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement(" INSERT INTO NJCCCMS.CASE_JUDICIAL_ASSIGNMENT " +
				" ( ID, COURT_ID, CASE_ID, JUDICIAL_PARTY_ID, START_DATE, END_DATE ) " + 
				" VALUES (?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.judicialAssignmentOID);
		stmt.setLong(2, this.courtOID);
		stmt.setLong(3, this.caseOID);
		stmt.setLong(4, this.judicialOfficial.getPartyOID());
		
		if (this.startDate != null)
		{
			stmt.setDate(5, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			stmt.setNull(5, java.sql.Types.DATE);
		}

		if (this.endDate != null)
		{
			stmt.setDate(6, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}
		
		pStmts.add(stmt);
		
		return pStmts;
	}

	
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.CASE_JUDICIAL_ASSIGNMENT " + 
				"   SET START_DATE = ?, END_DATE = ? " + 
				" WHERE ID = ? ");
		
		if (this.startDate != null)
		{
			stmt.setDate(1, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			stmt.setNull(1, java.sql.Types.DATE);
		}

		if (this.endDate != null)
		{
			stmt.setDate(2, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			stmt.setNull(2, java.sql.Types.DATE);
		}
		
		stmt.setLong(3, this.judicialAssignmentOID);
		
		pStmts.add(stmt);
		
		
		return pStmts;
	}

	
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		
		String sqlString = "SELECT * FROM NJCCCMS.CASE_JUDICIAL_ASSIGNMENT " +
				" WHERE COURT_ID = " + this.courtOID;
		
		if ( this.judicialOfficial != null )
		{
			sqlString += " AND JUDICIAL_PARTY_ID = " + this.judicialOfficial.getPartyOID();
		}
		
		if ( this.judicialAssignmentOID != 0L )
		{
			sqlString += " AND ID = " + this.judicialAssignmentOID;
		}
		
		PreparedStatement stmt = conn.prepareStatement(sqlString);
		
		pStmts.add(stmt);
		return pStmts;
	}
	
	public static void main(String[] args)
	{
		try
		{
			CCMSCourt court = new CCMSCourt(5);
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			
			CCMSCaseJudicialAssignment asmt = new CCMSCaseJudicialAssignment(3, court.getCourtOID());
			asmt.setCaseOID(7215135718696990L);
			asmt.setJudicialAssignmentOID(7940369023351274L);
			asmt.setJudicialOfficial(new CCMSParty(16, 5));
			asmt.setStartDate(new Date(System.currentTimeMillis()-10000000));
			asmt.setEndDate(CCMSDomainUtilities.getInstance()
					.convertStringToDate("2017-06-01"));
			
			asmt.genUpdateSQL(conn).get(0).execute();
			conn.commit();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
