package org.ncsc.ccms.domain;

public class CCMSConstants
{
	public static int ICCS_CAT_SECTION = 1;
	public static int ICCS_CAT_DIVISION = 2;
	public static int ICCS_CAT_GROUP = 3;
	public static int ICCS_CAT_CLASS = 4;
}
