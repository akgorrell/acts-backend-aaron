package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSEventType extends CCMSDomain
{
	private long eventTypeOID, courtOID;
	
	// eventTypeName is a four digit code that uniquely identifies an event
	private String eventTypeName, description, eventCategoryText;
	private static Logger logger = null;

	public CCMSEventType()
	{
		CCMSEventType.logger = LogManager.getLogger(CCMSEventType.class
				.getName());
	}

	public CCMSEventType(long eventTypeOID, long courtOID)
	{
		CCMSEventType.logger = LogManager.getLogger(CCMSEventType.class
				.getName());
		this.eventTypeOID = eventTypeOID;
		this.courtOID = courtOID;
	}
	
	public CCMSEventType(String eventTypeName)
	{
		this.eventTypeName = eventTypeName;
	}

	public CCMSEventType(ResultSet rs, Connection conn) throws Exception
	{
		CCMSEventType.logger = LogManager.getLogger(CCMSEventType.class
				.getName());
		this.eventTypeOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.eventTypeName = rs.getString("EVENT_TYPE_NAME");
		this.description = rs.getString("DESCRIPTION");
		this.eventCategoryText = rs.getString("EVENT_CAT_TEXT");
	}

	public long getEventTypeOID()
	{
		return eventTypeOID;
	}

	public void setEventTypeOID(long eventTypeOID)
	{
		this.eventTypeOID = eventTypeOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getEventTypeName()
	{
		return eventTypeName;
	}

	public void setEventTypeName(String eventTypeName)
	{
		this.eventTypeName = eventTypeName;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getEventCategoryText()
	{
		return eventCategoryText;
	}

	public void setEventCategoryText(String eventCategoryText)
	{
		this.eventCategoryText = eventCategoryText;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.EVENT_TYPE " +
				" ( ID, COURT_ID, EVENT_TYPE_NAME, DESCRIPTION, EVENT_CAT_TEXT ) " +
				" VALUES ( ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.eventTypeOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.eventTypeName);
		stmt.setString(4, description);
		stmt.setString(5, this.eventCategoryText);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}


	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.EVENT_TYPE " + 
				"   SET EVENT_TYPE_NAME = ?, DESCRIPTION = ?, EVENT_CAT_TEXT = ? " + 
				" WHERE ID = ? AND COURT_ID = ? " );
		stmt.setString(1, this.eventTypeName);
		stmt.setString(2, this.description);
		stmt.setString(3, this.eventCategoryText);
		stmt.setLong(4, this.eventTypeOID);
		stmt.setLong(5, this.courtOID);
		
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.EVENT_TYPE"
						+ " WHERE COURT_ID = ? ORDER BY EVENT_TYPE_NAME");
		pStmt.setLong(1, this.courtOID);
		pStmts.add(pStmt);

		return pStmts;
	}

	@Override
	public boolean equals(Object obj)
	{
		if ( obj instanceof CCMSEventType)
		{
			CCMSEventType eventObj = (CCMSEventType) obj;
			if ( eventObj.getEventTypeOID() == this.getEventTypeOID())
			{
				return true;
			}
			if ( eventObj.getEventTypeName().equals(this.getEventTypeName()))
			{
				return true;
			}
			
		}
		return false;
	}

	@Override
	public String toString()
	{
		String objString = "EVENTOID: " + this.eventTypeOID + " COURTOID: "
				+ this.courtOID + " EVENTTYPENAME: " + this.eventTypeName
				+ " DESC: " + this.description;
		return objString;
	}
}
