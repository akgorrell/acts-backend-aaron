package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CCMSPartyEmail extends CCMSDomain
{

	private long partyEmailOID;
	private long partyOID;
	private String emailAddressType;
	private String emailAddress;
	private Date startDate = null;
	private Date endDate = null;
	
	
	public CCMSPartyEmail(long partyEmailOID)
	{
		this.partyEmailOID = partyEmailOID;
	}
	
	public CCMSPartyEmail( ResultSet rs, Connection conn ) throws Exception
	{
		this.partyEmailOID = rs.getLong("ID");
		this.partyOID = rs.getLong("PARTY_ID");
		this.emailAddressType = rs.getString("EMAIL_ADDRESS_TYPE");
		this.emailAddress = rs.getString("EMAIL_ADDRESS");
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");
	}

	public long getPartyEmailOID()
	{
		return partyEmailOID;
	}

	public void setPartyEmailOID(long partyEmailOID)
	{
		this.partyEmailOID = partyEmailOID;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getEmailAddressType()
	{
		return emailAddressType;
	}

	public void setEmailAddressType(String emailAddressType)
	{
		this.emailAddressType = emailAddressType;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.PARTY_EMAIL " +
				" ( ID, PARTY_ID, EMAIL_ADDRESS_TYPE, EMAIL_ADDRESS, START_DATE, END_DATE ) " + 
				" VALUES ( ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.partyEmailOID);
		stmt.setLong(2, this.partyOID);
		stmt.setString(3, this.emailAddressType);
		stmt.setString(4, this.emailAddress);
		

		if (this.startDate != null)
		{
			stmt.setDate(5, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			stmt.setNull(5, java.sql.Types.DATE);
		}

		
		if (this.endDate != null)
		{
			stmt.setDate(6, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}
		
		
		stmts.add(stmt);
		
		return stmts;
	}


}
