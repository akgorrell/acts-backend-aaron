package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSTaskType extends CCMSDomain
{

	private long taskTypeOID, courtOID;
	private String name = "", description = "";
	private static Logger logger = null;

	public CCMSTaskType()
	{
		CCMSTaskType.logger = LogManager
				.getLogger(CCMSTaskType.class.getName());
	}

	public CCMSTaskType(long taskTypeOID, long courtOID)
	{
		CCMSTaskType.logger = LogManager
				.getLogger(CCMSTaskType.class.getName());
		this.taskTypeOID = taskTypeOID;
		this.courtOID = courtOID;
	}

	public CCMSTaskType(long taskTypeOID, long courtOID, String name)
	{
		CCMSTaskType.logger = LogManager
				.getLogger(CCMSTaskType.class.getName());
		this.taskTypeOID = taskTypeOID;
		this.courtOID = courtOID;
		this.name = name;
	}

	public CCMSTaskType(ResultSet rs) throws Exception
	{
		CCMSTaskType.logger = LogManager
				.getLogger(CCMSTaskType.class.getName());
		this.taskTypeOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.courtOID = rs.getLong("COURT_ID");
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.TASK_TYPES " +
				" 	( ID, COURT_ID, NAME, DESCRIPTION )" +
				" VALUES ( ?, ?, ?, ? ) ");
		stmt.setLong(1, this.taskTypeOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.name);
		stmt.setString(4, this.description);
		stmts.add(stmt);

		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("UPDATE NJCCCMS.TASK_TYPES "
						+ "   SET NAME = ?, DESCRIPTION = ? "
						+ " WHERE ID = ? AND COURT_ID = ? ");
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setLong(3, this.taskTypeOID);
		stmt.setLong(4, this.courtOID);

		stmts.add(stmt);
		
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.TASK_TYPES "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}


	public long getStatusOID()
	{
		return taskTypeOID;
	}

	public void setStatusOID(long statusOID)
	{
		this.taskTypeOID = statusOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public long getTaskTypeOID()
	{
		return taskTypeOID;
	}

	public void setTaskTypeOID(long taskTypeOID)
	{
		this.taskTypeOID = taskTypeOID;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSTaskType)
		{
			CCMSTaskType qTask = (CCMSTaskType) obj;
			if (qTask.getTaskTypeOID() == this.getTaskTypeOID())
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String stringObj = " TaskTypeOID: " + this.taskTypeOID + " Name: "
				+ this.name + " Desc: " + this.description;

		return stringObj;
	}

}
