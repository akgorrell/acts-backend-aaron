package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCourtLocation extends CCMSDomain
{
	private long locationOID, courtOID;
	private String locationID, locationName;
	public static Logger logger = null;

	public CCMSCourtLocation()
	{

		CCMSCourtLocation.logger = LogManager.getLogger(CCMSCourtLocation.class
				.getName());
	}

	public CCMSCourtLocation(long locationOID, long courtOID)
	{
		CCMSCourtLocation.logger = LogManager.getLogger(CCMSCourtLocation.class
				.getName());
		this.locationOID = locationOID;
		this.courtOID = courtOID;
	}

	public CCMSCourtLocation(ResultSet rs, Connection conn) throws Exception
	{
		CCMSCourtLocation.logger = LogManager.getLogger(CCMSCourtLocation.class
				.getName());
		this.locationOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.locationID = rs.getString("LOCATION_ID");
		this.locationName = rs.getString("LOCATION_NAME");
	}

	public long getLocationOID()
	{
		return locationOID;
	}

	public void setLocationOID(long locationOID)
	{
		this.locationOID = locationOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getLocationID()
	{
		return locationID;
	}

	public void setLocationID(String locationID)
	{
		this.locationID = locationID;
	}

	public String getLocationName()
	{
		return locationName;
	}

	public void setLocationName(String locationName)
	{
		this.locationName = locationName;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO COURT_LOCATION "
						+ " ( ID, COURT_ID, LOCATION_ID, LOCATION_NAME ) "
						+ " VALUES (?, ?, ?, ? ) ");
		stmt.setLong(1, this.locationOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.locationID);
		stmt.setString(4, this.locationName);
		stmts.add(stmt);

		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		return super.genDeleteSQL(conn);
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn.prepareStatement("UPDATE COURT_LOCATION "
				+ "   SET LOCATION_ID = ?, LOCATION_NAME = ? "
				+ " WHERE ID = ? AND COURT_ID = ? ");
		stmt.setString(1, this.locationID);
		stmt.setString(2, this.locationName);
		stmt.setLong(3, this.locationOID);
		stmt.setLong(4, this.courtOID);
		stmts.add(stmt);

		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.COURT_LOCATION WHERE COURT_ID=?");
		stmt.setLong(1, this.courtOID);
		pStmts.add(stmt);
		return pStmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCourtLocation)
		{
			CCMSCourtLocation locObj = (CCMSCourtLocation) obj;
			if (locObj.getLocationOID() == this.getLocationOID())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return super.toString();
	}
}
