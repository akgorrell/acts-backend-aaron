package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CourtLookupTable implements Runnable
{
	long courtOID = 0;
	Connection conn = null;
	private List<CCMSCasePhase> casePhases = new ArrayList<CCMSCasePhase>();
	private List<CCMSCaseStatus> caseStatuses = new ArrayList<CCMSCaseStatus>();
	private List<CCMSCaseType> caseTypes = new ArrayList<CCMSCaseType>();
	private List<CCMSCasePartyRole> casePartyRoles = new ArrayList<CCMSCasePartyRole>();
	private List<CCMSHearingType> hearingTypes = new ArrayList<CCMSHearingType>();
	private List<CCMSTaskType> caseTaskTypes = new ArrayList<CCMSTaskType>();
	private List<CCMSICCSCode> iccsCodes = new ArrayList<CCMSICCSCode>();
	private List<CCMSEventType> eventTypes = new ArrayList<CCMSEventType>();
	private List<CCMSChargeFactor> chargeFactors = new ArrayList<CCMSChargeFactor>();
	private List<CCMSCourtLocation> courtLocations = new ArrayList<CCMSCourtLocation>();
	private List<CCMSStaffRole> staffRoles = new ArrayList<CCMSStaffRole>();
	private List<CCMSPermission> permissions = new ArrayList<CCMSPermission>();
	private List<CCMSPersonIdentificationType> personIDTypes = new ArrayList<CCMSPersonIdentificationType>();
	private List<CCMSICCSLocalCharge> localCharges = new ArrayList<CCMSICCSLocalCharge>();
	public static Logger logger = null;

	public CourtLookupTable(long courtOID, Connection conn) throws Exception
	{
		CourtLookupTable.logger = LogManager.getLogger(CourtLookupTable.class
				.getName());

		this.courtOID = courtOID;
		this.conn = conn;
	}

	public CourtLookupTable(long courtOID) throws Exception
	{
		CourtLookupTable.logger = LogManager.getLogger(CourtLookupTable.class
				.getName());
		this.courtOID = courtOID;
	}

	public void refreshAllLookupTables(Connection conn)
	{
		try
		{
			long now = System.currentTimeMillis();
			logger.info("Starting refresh of lookup tables for courtOID: "
					+ courtOID);
			// Must do permissions before roles because of dependency
			updatePermissions(conn);
			updateCaseStatus(conn);
			updateCaseTypes(conn);
			updateCasePartyRoles(conn);
			updateCaseTaskTypes(conn);
			updateCasePhases(conn);
			updateEventTypes(conn);
			updateHearingTypes(conn);
			updateChargeFactors(conn);
			updateCourtLocations(conn);
			updateStaffRoles(conn);
			updateICCSCodes(conn);
			updatePersonIDTypes(conn);
			
			// update local charges after iccs codes because of dependency
			this.updateLocalCharges(conn);

			logger.info("Completed refresh of lookup tables for courtOID: "
					+ courtOID + " Elapsed Time (MS): "
					+ (System.currentTimeMillis() - now));
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void run()
	{
		try
		{
			long now = System.currentTimeMillis();
			logger.info("Starting retrieval of lookup tables for courtOID: "
					+ courtOID);
			// Must do permissions before roles because of dependency
			updatePermissions(conn);
			updateCaseStatus(this.conn);
			updateCaseTypes(conn);
			updateCasePartyRoles(conn);
			updateCaseTaskTypes(conn);
			updateCasePhases(conn);
			updateEventTypes(conn);
			updateHearingTypes(conn);
			updateChargeFactors(conn);
			updateCourtLocations(conn);
			updateStaffRoles(conn);
			updateICCSCodes(conn);
			updatePersonIDTypes(conn);
			
			// update local charges after iccs codes because of dependency
			this.updateLocalCharges(conn);

			logger.info("Completed retrieval of lookup tables for courtOID: "
					+ courtOID + " Elapsed Time (MS): "
					+ (System.currentTimeMillis() - now));

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateCaseStatus(Connection conn) throws Exception
	{
		this.caseStatuses.clear();
		// Retrieve All Case Status Types for the Indicated Court
		CCMSCaseStatus status = new CCMSCaseStatus(0L, this.courtOID);
		List<PreparedStatement> statusStmts = status.genSelectSQL(conn);
		for (PreparedStatement stmt : statusStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.caseStatuses.add(new CCMSCaseStatus(rs));
			}
			rs.close();
			stmt.close();
		}

	}

	public void updateLocalCharges(Connection conn) throws Exception
	{
		this.localCharges.clear();
		// Retrieve All Case Status Types for the Indicated Court
		CCMSICCSLocalCharge localCharge = new CCMSICCSLocalCharge(0L,
				this.courtOID);
		List<PreparedStatement> statusStmts = localCharge.genSelectSQL(conn);
		for (PreparedStatement stmt : statusStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.localCharges.add(new CCMSICCSLocalCharge(rs, conn, true));
			}
			rs.close();
			stmt.close();
		}

	}

	public void updateCaseTypes(Connection conn) throws Exception
	{
		this.caseTypes.clear();
		// Retrieve All Case Types for the Indicated Court
		CCMSCaseType typeQuery = new CCMSCaseType(0L, this.courtOID);
		List<PreparedStatement> typeStmts = typeQuery.genSelectSQL(conn);
		for (PreparedStatement stmt : typeStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.caseTypes.add(new CCMSCaseType(rs, conn));
			}
			rs.close();
			stmt.close();
		}
		typeStmts.clear();
	}

	public void updateCasePartyRoles(Connection conn) throws Exception
	{
		this.casePartyRoles.clear();
		// Retrieve All Case Party Roles for the Indicated Court
		CCMSCasePartyRole partyRoleQuery = new CCMSCasePartyRole(0L,
				this.courtOID);
		List<PreparedStatement> partyRoleStmts = partyRoleQuery
				.genSelectSQL(conn);
		for (PreparedStatement stmt : partyRoleStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.casePartyRoles.add(new CCMSCasePartyRole(rs));
			}
			rs.close();
			stmt.close();
		}
		partyRoleStmts.clear();
	}

	public void updateCaseTaskTypes(Connection conn) throws Exception
	{
		this.caseTaskTypes.clear();
		// Retrieve All Case TaskTypes for the Indicated Court
		CCMSTaskType taskTypeQuery = new CCMSTaskType(0L, this.courtOID);
		List<PreparedStatement> taskTypeStmts = taskTypeQuery
				.genSelectSQL(conn);
		for (PreparedStatement stmt : taskTypeStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.caseTaskTypes.add(new CCMSTaskType(rs));
			}
			rs.close();
			stmt.close();
		}
		taskTypeStmts.clear();
	}

	public void updateCasePhases(Connection conn) throws Exception
	{
		this.casePhases.clear();
		// Retrieve All Case Phases
		CCMSCasePhase casePhaseQuery = new CCMSCasePhase(0L, this.courtOID);
		List<PreparedStatement> casePhaseStmts = casePhaseQuery
				.genSelectSQL(conn);
		for (PreparedStatement stmt : casePhaseStmts)
		{
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				this.casePhases.add(new CCMSCasePhase(rs));
			}
			rs.close();
			stmt.close();
		}
		casePhaseStmts.clear();

	}

	public void updateICCSCodes(Connection conn) throws Exception
	{
		this.iccsCodes.clear();
		// ICCS Codes
		CCMSICCSCode qCode = new CCMSICCSCode(0L, this.courtOID);
		PreparedStatement iccsStmt = qCode.genSelectSQL(conn).get(0);
		ResultSet rs3 = iccsStmt.executeQuery();
		while (rs3.next())
		{
			CCMSICCSCode code = new CCMSICCSCode(rs3, conn);
			this.iccsCodes.add(code);
		}
		rs3.close();
		iccsStmt.close();

	}

	public void updatePermissions(Connection conn) throws Exception
	{
		this.permissions.clear();
		// Event Types
		CCMSPermission qPerm = new CCMSPermission(0L, this.courtOID);
		PreparedStatement permStmt = qPerm.genSelectSQL(conn).get(0);
		ResultSet rs4 = permStmt.executeQuery();
		while (rs4.next())
		{
			CCMSPermission perm = new CCMSPermission(rs4, conn);
			this.permissions.add(perm);
		}
		rs4.close();
		permStmt.close();
	}

	public void updateEventTypes(Connection conn) throws Exception
	{
		this.eventTypes.clear();
		// Event Types
		CCMSEventType qEventType = new CCMSEventType(0L, this.courtOID);
		PreparedStatement eventTypeStmt = qEventType.genSelectSQL(conn).get(0);
		ResultSet rs4 = eventTypeStmt.executeQuery();
		while (rs4.next())
		{
			CCMSEventType eventType = new CCMSEventType(rs4, conn);
			this.eventTypes.add(eventType);
		}
		rs4.close();
		eventTypeStmt.close();
	}

	public void updateHearingTypes(Connection conn) throws Exception
	{
		this.hearingTypes.clear();
		// Hearing Types
		CCMSHearingType qHearingType = new CCMSHearingType(0L, this.courtOID);
		PreparedStatement hearingTypeStmt = qHearingType.genSelectSQL(conn)
				.get(0);
		ResultSet rs5 = hearingTypeStmt.executeQuery();
		while (rs5.next())
		{
			CCMSHearingType hearingType = new CCMSHearingType(rs5, conn);
			this.hearingTypes.add(hearingType);
		}
		rs5.close();
		hearingTypeStmt.close();
	}

	public void updateChargeFactors(Connection conn) throws Exception
	{
		this.chargeFactors.clear();
		// Charge Factors
		CCMSChargeFactor qChargeFactor = new CCMSChargeFactor(0L, this.courtOID);
		PreparedStatement chargeFactorStmt = qChargeFactor.genSelectSQL(conn)
				.get(0);
		ResultSet rs6 = chargeFactorStmt.executeQuery();
		while (rs6.next())
		{
			CCMSChargeFactor dbChargeFactor = new CCMSChargeFactor(rs6, conn);
			this.chargeFactors.add(dbChargeFactor);
		}
		rs6.close();
		chargeFactorStmt.close();
	}

	public void updateCourtLocations(Connection conn) throws Exception
	{
		this.courtLocations.clear();
		// Court Location
		CCMSCourtLocation qCourtLoc = new CCMSCourtLocation(0L, this.courtOID);
		PreparedStatement courtLocStmt = qCourtLoc.genSelectSQL(conn).get(0);
		ResultSet rs7 = courtLocStmt.executeQuery();
		while (rs7.next())
		{
			CCMSCourtLocation dbCourtLoc = new CCMSCourtLocation(rs7, conn);
			this.courtLocations.add(dbCourtLoc);
		}
		rs7.close();
		courtLocStmt.close();

	}

	public void updatePersonIDTypes(Connection conn) throws Exception
	{
		this.personIDTypes.clear();
		// Identification Types
		CCMSPersonIdentificationType qPersonIDType = new CCMSPersonIdentificationType(
				0L, this.courtOID);
		PreparedStatement personIDTypeStmt = qPersonIDType.genSelectSQL(conn)
				.get(0);
		ResultSet rs8 = personIDTypeStmt.executeQuery();
		while (rs8.next())
		{
			CCMSPersonIdentificationType dbPersonIDType = new CCMSPersonIdentificationType(
					rs8);
			this.personIDTypes.add(dbPersonIDType);
		}
		rs8.close();
	}

	public void updateStaffRoles(Connection conn) throws Exception
	{
		this.staffRoles.clear();
		// Staff Roles
		CCMSStaffRole qStaffRoles = new CCMSStaffRole(0L, this.courtOID);
		PreparedStatement staffRoleStmt = qStaffRoles.genSelectSQL(conn).get(0);
		ResultSet rs8 = staffRoleStmt.executeQuery();
		while (rs8.next())
		{
			CCMSStaffRole dbStaffRole = new CCMSStaffRole(rs8, conn);
			this.staffRoles.add(dbStaffRole);
		}
		rs8.close();
		staffRoleStmt.close();
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public List<CCMSCasePhase> getCasePhases()
	{
		return casePhases;
	}

	public void setCasePhases(List<CCMSCasePhase> casePhases)
	{
		this.casePhases = casePhases;
	}

	public List<CCMSCaseStatus> getCaseStatuses()
	{
		return caseStatuses;
	}

	public void setCaseStatuses(List<CCMSCaseStatus> caseStatuses)
	{
		this.caseStatuses = caseStatuses;
	}

	public List<CCMSCaseType> getCaseTypes()
	{
		return caseTypes;
	}

	public void setCaseTypes(List<CCMSCaseType> caseTypes)
	{
		this.caseTypes = caseTypes;
	}

	public List<CCMSCasePartyRole> getCasePartyRoles()
	{
		return casePartyRoles;
	}

	public void setCasePartyRoles(List<CCMSCasePartyRole> casePartyRoles)
	{
		this.casePartyRoles = casePartyRoles;
	}

	public List<CCMSHearingType> getHearingTypes()
	{
		return hearingTypes;
	}

	public void setHearingTypes(List<CCMSHearingType> hearingTypes)
	{
		this.hearingTypes = hearingTypes;
	}

	public List<CCMSTaskType> getCaseTaskTypes()
	{
		return caseTaskTypes;
	}

	public void setCaseTaskTypes(List<CCMSTaskType> caseTaskTypes)
	{
		this.caseTaskTypes = caseTaskTypes;
	}

	public List<CCMSICCSCode> getIccsCodes()
	{
		return iccsCodes;
	}

	public void setIccsCodes(List<CCMSICCSCode> iccsCodes)
	{
		this.iccsCodes = iccsCodes;
	}

	public List<CCMSEventType> getEventTypes()
	{
		return eventTypes;
	}

	public void setEventTypes(List<CCMSEventType> eventTypes)
	{
		this.eventTypes = eventTypes;
	}

	public List<CCMSChargeFactor> getChargeFactors()
	{
		return chargeFactors;
	}

	public void setChargeFactors(List<CCMSChargeFactor> chargeFactors)
	{
		this.chargeFactors = chargeFactors;
	}

	public List<CCMSCourtLocation> getCourtLocations()
	{
		return courtLocations;
	}

	public void setCourtLocations(List<CCMSCourtLocation> courtLocations)
	{
		this.courtLocations = courtLocations;
	}

	public List<CCMSStaffRole> getStaffRoles()
	{
		return staffRoles;
	}

	public void setStaffRoles(List<CCMSStaffRole> staffRoles)
	{
		this.staffRoles = staffRoles;
	}

	public List<CCMSPersonIdentificationType> getPersonIDTypes()
	{
		return personIDTypes;
	}

	public void setPersonIDTypes(
			List<CCMSPersonIdentificationType> personIDTypes)
	{
		this.personIDTypes = personIDTypes;
	}

	public List<CCMSPermission> getPermissions()
	{
		return this.permissions;
	}

	public void setPermissions(List<CCMSPermission> permissions)
	{
		this.permissions = permissions;
	}

	public List<CCMSICCSLocalCharge> getLocalCharges()
	{
		return localCharges;
	}

	public void setLocalCharges(List<CCMSICCSLocalCharge> localCharges)
	{
		this.localCharges = localCharges;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CourtLookupTable)
		{
			CourtLookupTable parm1 = (CourtLookupTable) obj;
			if (parm1.getCourtOID() == this.getCourtOID()) { return true; }
		}
		return false;
	}
}