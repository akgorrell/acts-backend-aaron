package org.ncsc.ccms.domain;

import java.util.Date;

public class CCMSException extends Exception
{
	public static final long serialVersionUID = 0;
	private Date dateTime;
	private int error = 1;
	private String message;
	
	public CCMSException(String message)
	{
		this.dateTime = new Date(System.currentTimeMillis());
		this.message = message;
	}
	
	public Date getDateTime()
	{
		return dateTime;
	}
	public void setDateTime(Date dateTime)
	{
		this.dateTime = dateTime;
	}
	public int getError()
	{
		return error;
	}

	public void setError(int error)
	{
		this.error = error;
	}

	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}
	

	public void printStackTrace()
	{
		// TODO Auto-generated method stub
		super.printStackTrace();
	}
}
