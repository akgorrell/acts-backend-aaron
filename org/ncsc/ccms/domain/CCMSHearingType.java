package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

public class CCMSHearingType extends CCMSDomain
{
	private static Logger logger = null;
	private long hearingTypeOID, courtOID;
	private String hearingName, description;
	private int durationInMinutes;

	public CCMSHearingType()
	{

	}

	public CCMSHearingType(long hearingTypeOID, long courtOID)
	{
		this.hearingTypeOID = hearingTypeOID;
		this.courtOID = courtOID;
	}

	public CCMSHearingType(ResultSet rs, Connection conn) throws Exception
	{
		this.hearingTypeOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.hearingName = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.durationInMinutes = rs.getInt("DURATION_MIN");
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public long getHearingTypeOID()
	{
		return hearingTypeOID;
	}

	public void setHearingTypeOID(long hearingTypeOID)
	{
		this.hearingTypeOID = hearingTypeOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getHearingName()
	{
		return hearingName;
	}

	public void setHearingName(String hearingName)
	{
		this.hearingName = hearingName;
	}

	public int getDurationInMinutes()
	{
		return durationInMinutes;
	}

	public void setDurationInMinutes(int durationInMinutes)
	{
		this.durationInMinutes = durationInMinutes;
	}

	@Override
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM NJCCCMS.HEARING_TYPE " + 
				" WHERE COURT_ID = ? ORDER BY NAME " );
		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);

		return stmts;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO HEARING_TYPE " + 
				" ( ID, COURT_ID, NAME, DESCRIPTION, DURATION_MIN ) " + 
				" VALUES (?, ?, ?, ?, ? )");
	
		stmt.setLong(1, this.hearingTypeOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.hearingName);
		stmt.setString(4, this.description);
		stmt.setInt(5, this.durationInMinutes);
		
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		return stmts;
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("UPDATE HEARING_TYPE " +
				"   SET NAME = ?, DURATION_MIN = ?, DESCRIPTION = ? " + 
				" WHERE ID = ? AND COURT_ID = ?");
		stmt.setString(1, this.hearingName);
		stmt.setInt(2, this.durationInMinutes);
		stmt.setString(3, this.description);
		stmt.setLong(4, this.hearingTypeOID);
		stmt.setLong(5, this.courtOID);
		
		stmts.add(stmt);
		
		return stmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSHearingType)
		{
			CCMSHearingType hrngType = (CCMSHearingType)obj;
			if ( hrngType.getHearingTypeOID() == this.getHearingTypeOID())
			{
				return true;
			}
		}
		return false;
		
	}

	@Override
	public String toString()
	{
		// TODO Auto-generated method stub
		return super.toString();
	}

}
