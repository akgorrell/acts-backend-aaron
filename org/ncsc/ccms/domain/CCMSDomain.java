package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class CCMSDomain
{
	
	private String errorMessage = null;
	
	public CCMSDomain()
	{
	}
	
	public List<PreparedStatement> genInsertSQL(Connection conn) throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn) throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn) throws Exception
	{
		return null;
	}

	public boolean hasChanged(CCMSDomain sub)
	{
		return false;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage)
	{
		this.errorMessage = errorMessage;
	}


}
