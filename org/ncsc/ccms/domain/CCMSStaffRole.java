package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSStaffRole extends CCMSDomain
{
	private long staffRoleOID, courtOID;
	private String staffRoleName;
	private boolean judicialOfficer = false, ccmsAdmin = false;
	private static Logger logger = null;
	private ArrayList<CCMSPermission> permissions = new ArrayList<CCMSPermission>();

	public CCMSStaffRole()
	{
		CCMSStaffRole.logger = LogManager.getLogger(CCMSStaffRole.class
				.getName());
	}

	public CCMSStaffRole(long staffRoleOID, long courtOID)
	{
		CCMSStaffRole.logger = LogManager.getLogger(CCMSStaffRole.class
				.getName());
		this.staffRoleOID = staffRoleOID;
		this.courtOID = courtOID;
	}

	public CCMSStaffRole(ResultSet rs, Connection conn) throws Exception
	{
		CCMSStaffRole.logger = LogManager.getLogger(CCMSStaffRole.class
				.getName());
		this.staffRoleOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.staffRoleName = rs.getString("STAFF_ROLE_NAME");
		int judicialOffInd = rs.getInt("JUDICIAL_OFFICER_IND");
		this.judicialOfficer = CCMSDomainUtilities.intToBool(judicialOffInd);
		int ccmsAdminInd = rs.getInt("SYSTEM_ADMIN_IND");
		this.ccmsAdmin = CCMSDomainUtilities.intToBool(ccmsAdminInd);

		// Retrieve the Permissions associated with this role
		PreparedStatement stmt = conn.prepareStatement("SELECT PERMISSION_ID "
				+ " FROM NJCCCMS.ROLE_PERMISSION_REL "
				+ " WHERE COURT_ID = ? AND ROLE_ID = ? ");
		stmt.setLong(1, this.courtOID);
		stmt.setLong(2, this.staffRoleOID);

		ResultSet rs1 = stmt.executeQuery();
		while (rs1.next())
		{
			long permissionOID = rs1.getLong("PERMISSION_ID");
			CCMSPermission perm = new CCMSPermission(permissionOID,
					this.courtOID);
			perm = (CCMSPermission) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(perm, new CCMSCourt(this.courtOID));
			if (perm != null)
			{
				this.permissions.add(perm);
			}
		}
		stmt.close();
	}

	public long getStaffRoleOID()
	{
		return staffRoleOID;
	}

	public void setStaffRoleOID(long staffRoleOID)
	{
		this.staffRoleOID = staffRoleOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getStaffRoleName()
	{
		return staffRoleName;
	}

	public void setStaffRoleName(String staffRoleName)
	{
		this.staffRoleName = staffRoleName;
	}

	public boolean isJudicialOfficer()
	{
		return judicialOfficer;
	}

	public void setJudicialOfficer(boolean judicialOfficer)
	{
		this.judicialOfficer = judicialOfficer;
	}

	public boolean isCcmsAdmin()
	{
		return ccmsAdmin;
	}

	public void setCcmsAdmin(boolean ccmsAdmin)
	{
		this.ccmsAdmin = ccmsAdmin;
	}

	public ArrayList<CCMSPermission> getPermissions()
	{
		return permissions;
	}

	public void setPermissions(ArrayList<CCMSPermission> permissions)
	{
		this.permissions = permissions;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.STAFF_ROLE WHERE COURT_ID = ?");
		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);

		return stmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSStaffRole)
		{
			CCMSStaffRole staffRole = (CCMSStaffRole) obj;
			if (staffRole.getStaffRoleOID() == this.getStaffRoleOID()) { return true; }
		}
		return true;
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSDomainUtilities.getInstance().connect();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}
