package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseCharge extends CCMSDomain
{
	private long caseChargeOID, courtOID, iccsChargeCategoryOID, caseOID;
	private CCMSICCSCode iccsCode = null;
	private String leaChargingDetails;
	private List<CCMSChargeFactor> chargeFactors = new ArrayList<CCMSChargeFactor>();
	private CCMSICCSLocalCharge localCharge = null;
	private Date startDate = null, endDate = null;
	private static Logger logger = null;

	public CCMSCaseCharge()
	{
		CCMSCaseCharge.logger = LogManager.getLogger(CCMSCaseCharge.class
				.getName());

	}

	public CCMSCaseCharge(long caseChargeOID, long courtOID)
	{
		CCMSCaseCharge.logger = LogManager.getLogger(CCMSCaseCharge.class
				.getName());
		this.caseChargeOID = caseChargeOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseCharge(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		CCMSCaseCharge.logger = LogManager.getLogger(CCMSCaseCharge.class
				.getName());
		this.caseChargeOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.caseOID = rs.getLong("CASE_ID");
		this.iccsChargeCategoryOID = rs.getLong("ICCS_CHARGE_ID");
		this.leaChargingDetails = rs.getString("LEA_CHARGE_TEXT");
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");

		long localChargeOID = rs.getLong("LOCAL_CHARGE_ID");

		long startTime = System.currentTimeMillis();

		// Replace the ICCS Code with the full code from the lookup tables
		CCMSICCSCode iccsCode = new CCMSICCSCode(iccsChargeCategoryOID,
				this.getCourtOID());
		iccsCode = (CCMSICCSCode) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(iccsCode, court);
		logger.debug("Get ICCS Code: "
				+ (System.currentTimeMillis() - startTime) + " ms");
		startTime = System.currentTimeMillis();
		this.iccsCode = iccsCode;

		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CHARGE_FACTOR, NJCCCMS.CHARGE_FACTOR_REL "
						+ " WHERE CHARGE_FACTOR_REL.CHARGE_ID = ? AND CHARGE_FACTOR_REL.COURT_ID = ?"
						+ "   AND CHARGE_FACTOR.ID = CHARGE_FACTOR_REL.FACTOR_ID " + 
						"     AND CHARGE_FACTOR.COURT_ID = CHARGE_FACTOR_REL.COURT_ID ");
		pStmt.setLong(1, this.caseChargeOID);
		pStmt.setLong(2, this.courtOID);

		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			this.chargeFactors.add(new CCMSChargeFactor(rs1, conn));
		}
		rs1.close();
		pStmt.close();

		logger.debug("Charge Factor Elapsed: "
				+ (System.currentTimeMillis() - startTime) + " ms");
		startTime = System.currentTimeMillis();

		PreparedStatement pStmt2 = conn
				.prepareStatement("SELECT * FROM NJCCCMS.ICCS_LOCAL_CHARGE "
						+ " WHERE COURT_ID = ? AND ID = ? ");
		pStmt2.setLong(1, this.courtOID);
		pStmt2.setLong(2, localChargeOID);
		ResultSet rs2 = pStmt2.executeQuery();
		while (rs2.next())
		{
			this.localCharge = new CCMSICCSLocalCharge(rs2, conn, false);
		}
		logger.debug("Local Charge Elapsed: "
				+ (System.currentTimeMillis() - startTime) + " ms");
		startTime = System.currentTimeMillis();
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCaseCharge charge = new CCMSCaseCharge(CCMSDomainUtilities
					.getInstance().genOID(), 5);
			PreparedStatement delStmt = charge.genDeleteSQL(conn).get(0);
			delStmt.execute();
			delStmt.close();

			PreparedStatement insStmt = charge.genInsertSQL(conn).get(0);
			insStmt.execute();
			insStmt.close();
			conn.commit();
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public long getCaseChargeOID()
	{
		return caseChargeOID;
	}

	public void setCaseChargeOID(long caseChargeOID)
	{
		this.caseChargeOID = caseChargeOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public long getIccsChargeCategoryOID()
	{
		return iccsChargeCategoryOID;
	}

	public void setIccsChargeCategoryOID(long iccsChargeCategoryOID)
	{
		this.iccsChargeCategoryOID = iccsChargeCategoryOID;
	}

	public String getLeaChargingDetails()
	{
		return leaChargingDetails;
	}

	public void setLeaChargingDetails(String leaChargingDetails)
	{
		this.leaChargingDetails = leaChargingDetails;
	}

	public CCMSICCSCode getIccsCode()
	{
		return iccsCode;
	}

	public CCMSICCSLocalCharge getLocalCharge()
	{
		return localCharge;
	}

	public void setLocalCharge(CCMSICCSLocalCharge localCharge)
	{
		this.localCharge = localCharge;
	}

	public void setIccsCode(CCMSICCSCode iccsCode)
	{
		this.iccsCode = iccsCode;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.CASE_CHARGE (ID, COURT_ID, ICCS_CHARGE_ID, LEA_CHARGE_TEXT, LOCAL_CHARGE_ID, START_DATE, END_DATE ) "
						+ " values (?, ?, ?, ?, ?, ?, ?) ");
		pStmt.setLong(1, this.caseChargeOID);
		pStmt.setLong(2, this.courtOID);
		pStmt.setLong(3, iccsChargeCategoryOID);
		pStmt.setString(4, this.leaChargingDetails);

		if (localCharge != null)
		{
			pStmt.setLong(5, this.localCharge.getLocalChargeOID());
		}
		else
		{
			pStmt.setLong(5, 0L);
		}

		if (this.startDate != null)
		{
			pStmt.setDate(6, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			pStmt.setNull(6, java.sql.Types.DATE);
		}

		if (this.endDate != null)
		{
			pStmt.setDate(7, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			pStmt.setNull(7, java.sql.Types.DATE);
		}

		stmts.add(pStmt);
		// Add Charge Factors
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_CHARGE WHERE COURT_ID = ? "
						+ " AND ID = ? ");
		pStmt.setLong(1, this.courtOID);
		pStmt.setLong(2, this.caseChargeOID);
		stmts.add(pStmt);

		// Remove case charges
		PreparedStatement factorStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CHARGE_FACTOR_REL WHERE COURT_ID = ? "
						+ " AND CASE_ID = ? ");
		factorStmt.setLong(1, this.courtOID);
		factorStmt.setLong(2, this.caseOID);
		stmts.add(factorStmt);

		// Remove case parties
		PreparedStatement partiesStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_PARTY_REL WHERE COURT_ID = ? "
						+ " AND CASE_ID = ? ");
		partiesStmt.setLong(1, this.courtOID);
		partiesStmt.setLong(2, this.caseOID);
		stmts.add(partiesStmt);

		// Remove case ?
		PreparedStatement caseStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE WHERE COURT_ID = ? "
						+ " AND CASE_ID = ? ");
		caseStmt.setLong(1, this.courtOID);
		caseStmt.setLong(2, this.caseOID);
		stmts.add(caseStmt);

		return stmts;
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genUpdateSQL(conn);
	}

	@Override
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CASE_CHARGE WHERE COURT_ID = ? "
						+ " AND ID = ? ");
		pStmt.setLong(1, this.courtOID);
		pStmt.setLong(2, this.caseChargeOID);

		stmts.add(pStmt);
		return stmts;
	}

	public List<CCMSChargeFactor> getChargeFactors()
	{
		return chargeFactors;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCaseCharge)
		{
			CCMSCaseCharge charge = (CCMSCaseCharge) obj;
			if (charge.getCaseChargeOID() == this.getCaseChargeOID()) 
			{ 
				return true; 
			}
			else if ( charge.getIccsChargeCategoryOID() == this.getIccsChargeCategoryOID() )
			{
				return true;
			}
		}
		return false;

	}

	public String toString()
	{
		String desc = "";
		try
		{
			CCMSICCSCode chargeCode = new CCMSICCSCode(this.iccsChargeCategoryOID, this.getCourtOID());
			chargeCode = (CCMSICCSCode)CCMSDomainUtilities.getInstance().retrieveFullObject(chargeCode, new CCMSCourt(this.getCourtOID()));
			desc = " Charge Code: " + chargeCode.getCategoryIdentifier() + ": " + chargeCode.getCategoryName();
		}
		catch (Exception e)
		{
			logger.error(e);
		}
		// TODO Auto-generated method stub
		return desc;
	}
	
	

}
