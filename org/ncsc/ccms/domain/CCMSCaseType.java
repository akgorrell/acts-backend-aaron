package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCaseType extends CCMSDomain
{
	public static int CHINS_CASE_TYPE = 2;
	public static int APPLICATION_CASE_TYPE = 1;
	public static int CRIMINAL = 2;

	private long caseTypeOID, courtOID;

	private String name, shortName, description;
	private List<CCMSCasePhase> casePhases = new ArrayList<CCMSCasePhase>();
	private static Logger logger = null;

	public CCMSCaseType()
	{
		CCMSCaseType.logger = LogManager
				.getLogger(CCMSCaseType.class.getName());
	}

	public CCMSCaseType(long caseTypeOID, long courtOID)
	{
		CCMSCaseType.logger = LogManager
				.getLogger(CCMSCaseType.class.getName());
		this.caseTypeOID = caseTypeOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseType(long caseTypeOID, long courtOID, String name)
	{
		CCMSCaseType.logger = LogManager
				.getLogger(CCMSCaseType.class.getName());
		this.caseTypeOID = caseTypeOID;
		this.courtOID = courtOID;
		this.name = name;
	}

	public CCMSCaseType(ResultSet rs, Connection conn) throws Exception
	{
		CCMSCaseType.logger = LogManager
				.getLogger(CCMSCaseType.class.getName());
		this.caseTypeOID = rs.getLong("ID");
		this.name = rs.getString("NAME");
		this.description = rs.getString("DESCRIPTION");
		this.shortName = rs.getString("SHORT_NAME");
		this.courtOID = rs.getLong("COURT_ID");

	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.CASE_TYPES " + 
				" ( ID, COURT_ID, NAME, DESCRIPTION, SHORT_NAME ) " + 
				" VALUES ( ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.caseTypeOID);
		stmt.setLong(2, this.courtOID);
		stmt.setString(3, this.name);
		stmt.setString(4, this.description);
		stmt.setString(5, this.shortName);
		stmts.add(stmt);
		
		return stmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}


	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.CASE_TYPES " + 
				"   SET NAME = ?, DESCRIPTION = ? " + 
				" WHERE ID = ? ");
		stmt.setString(1, this.name);
		stmt.setString(2, this.description);
		stmt.setLong(3, this.caseTypeOID);
		stmts.add(stmt);
		
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CASE_TYPES "
						+ " WHERE COURT_ID = ? " + " ORDER BY NAME ");

		stmt.setLong(1, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}


	public long getStatusOID()
	{
		return caseTypeOID;
	}

	public void setStatusOID(long statusOID)
	{
		this.caseTypeOID = statusOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public String getShortName()
	{
		return shortName;
	}

	public void setShortName(String shortName)
	{
		this.shortName = shortName;
	}

	public long getCaseTypeOID()
	{
		return caseTypeOID;
	}

	public void setCaseTypeOID(long caseTypeOID)
	{
		this.caseTypeOID = caseTypeOID;
	}

	public List<CCMSCasePhase> getCasePhases()
	{
		return casePhases;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCaseType)
		{
			CCMSCaseType qObj = (CCMSCaseType) obj;
			if (qObj.getCaseTypeOID() == this.getCaseTypeOID())
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String objString = this.name;

		return objString;
	}

}
