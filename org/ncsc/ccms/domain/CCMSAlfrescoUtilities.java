package org.ncsc.ccms.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.chemistry.opencmis.client.api.*;
import org.apache.chemistry.opencmis.client.bindings.spi.LinkAccess;
import org.apache.chemistry.opencmis.client.runtime.OperationContextImpl;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.PropertyData;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSAlfrescoUtilities
{
	private static CCMSAlfrescoUtilities _instance;
	private Properties properties = new Properties();
	public static Logger logger = null;
	private Session _session = null;
	

	private CCMSAlfrescoUtilities()
	{
		CCMSAlfrescoUtilities.logger = LogManager
				.getLogger(CCMSAlfrescoUtilities.class.getName());
		try
		{
			// Load properties
			properties.load(this.getClass().getClassLoader()
					.getResourceAsStream("config.properties"));

		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

	}

	public static CCMSAlfrescoUtilities getInstance()
	{
		if (_instance == null)
		{
			_instance = new CCMSAlfrescoUtilities();
		}
		return _instance;
	}

	public static void main(String[] args)
	{
		CCMSAlfrescoUtilities conn2 = new CCMSAlfrescoUtilities();
		try
		{
			Session cmisSession = conn2.getCmisSession();
			Folder parentFolder = conn2.getParentFolder(cmisSession);
			String fileName = "Indictment_1page.pdf";

			Folder childFolder = conn2.createFolder(parentFolder.getId(),
					"AG123", cmisSession);
			String fileType = conn2.getMimeType(fileName);

			Document doc = conn2.createDocument(childFolder, new File(
					"c:\\temp\\Indictment_1page.pdf"), fileType, null,
					cmisSession);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Session getCmisSession() throws Exception
	{
		return _session;
	}

	public Session connectToCmisServer()
	{
		Map<String, String> parameter = new HashMap<String, String>();

		String alfrescoUser = properties.getProperty("ALFRESCO_USER");

		String alfrescoPwd = properties.getProperty("ALFRESCO_PWD");
		String alfrescoURL = properties.getProperty("ALFRESCO_URL");
		String repositoryID = properties.getProperty("ALFRESCO_REPOSITORY_ID");

		// Set the user credentials
		parameter.put(SessionParameter.USER, alfrescoUser);
		parameter.put(SessionParameter.PASSWORD, alfrescoPwd);

		// Specify the connection settings
		parameter.put(SessionParameter.ATOMPUB_URL, alfrescoURL);
		parameter.put(SessionParameter.BINDING_TYPE,
				BindingType.ATOMPUB.value());

		// Add repository Id
		parameter.put(SessionParameter.REPOSITORY_ID, repositoryID);
		// Create a session
		SessionFactory factory = SessionFactoryImpl.newInstance();
		Session session = factory.getRepositories(parameter).get(0)
				.createSession();
		logger.info("Connected to alfresco repository:"
				+ session.getRepositoryInfo().getName());
		_session = session;

		return _session;

	}

	public Folder getParentFolder(Session cmisSession)
	{
		Folder folder = (Folder) cmisSession.getObjectByPath("/");
		System.out.println(folder.getId());
		return folder;
	}

	public void getObjByPath(Session cmisSession, String path)
	{
		Document document = (Document) cmisSession.getObjectByPath(path);
		List<Document> documents = document.getAllVersions();
		Iterator<Document> itr = documents.iterator();
		while (itr.hasNext())
		{
			Document version = (Document) itr.next();
			System.out.println(((LinkAccess) cmisSession.getBinding()
					.getObjectService()).loadContentLink(cmisSession
					.getRepositoryInfo().getId(), version.getId()));
		}
	}

	public void doQuery(String cql, int maxItems, Session cmisSession)
	{

		OperationContext oc = new OperationContextImpl();
		oc.setMaxItemsPerPage(maxItems);

		ItemIterable<QueryResult> results = cmisSession.query(cql, false, oc);

		for (QueryResult result : results)
		{
			for (PropertyData<?> prop : result.getProperties())
			{
				System.out.println(prop.getQueryName() + ": "
						+ prop.getFirstValue());
			}
			System.out.println("--------------------------------------");
		}

		System.out.println("--------------------------------------");
		System.out.println("Total number: " + results.getTotalNumItems());
		System.out.println("Has more: " + results.getHasMoreItems());
		System.out.println("--------------------------------------");
	}

	/**
	 * Will either return the folder corresponding the the indicated case number
	 * or will create and return that folder
	 * 
	 * @param parentFolderId
	 * @param caseOID
	 *            : caseOID
	 * @param cmisSession
	 * @return
	 */
	public Folder createFolder(String parentFolderId, String caseOID,
			Session cmisSession)
	{
		Folder rootFolder = (Folder) cmisSession.getObject(parentFolderId);

		Folder subFolder = null;
		try
		{
			// Making an assumption here that you probably wouldn't normally do
			subFolder = (Folder) cmisSession.getObjectByPath(rootFolder
					.getPath() + caseOID);

			logger.info("Folder already existed!");

		}
		catch (CmisObjectNotFoundException onfe)
		{
			Map<String, Object> props = new HashMap<String, Object>();
			props.put("cmis:objectTypeId", "cmis:folder");
			props.put("cmis:name", caseOID);
			subFolder = rootFolder.createFolder(props);
			String subFolderId = subFolder.getId();
			logger.info("Created new folder: " + subFolderId);
		}

		return rootFolder;
	}

	public Document createDocument(Folder parentFolder, File file,
			String fileType, Map<String, Object> props, Session cmisSession)
			throws FileNotFoundException
	{

		String fileName = file.getName();

		// create a map of properties if one wasn't passed in
		if (props == null)
		{
			props = new HashMap<String, Object>();
		}

		// Add the object type ID if it wasn't already
		if (props.get("cmis:objectTypeId") == null)
		{
			props.put("cmis:objectTypeId", "cmis:document");
		}

		// Add the name if it wasn't already
		if (props.get("cmis:name") == null)
		{
			props.put("cmis:name", fileName);
		}

		ContentStream contentStream = cmisSession.getObjectFactory()
				.createContentStream(fileName, file.length(), fileType,
						new FileInputStream(file));

		Document document = null;
		try
		{
			document = parentFolder.createDocument(props, contentStream, null);

			logger.info(((LinkAccess) cmisSession.getBinding()
					.getObjectService()).loadContentLink(cmisSession
					.getRepositoryInfo().getId(), document.getId()));
		}
		catch (CmisContentAlreadyExistsException ccaee)
		{
			document = (Document) cmisSession.getObjectByPath(parentFolder
					.getPath() + "/" + fileName);
			logger.error("Document already exists: " + fileName);
		}

		return document;
	}

	public String getMimeType(String fileName)
	{
		String fileType;
		// open text document file
		if (fileName.contains(".odt"))
		{
			fileType = "application/vnd.oasis.opendocument.text";
		}
		// open office spreadsheet file
		else if (fileName.contains(".ods"))
		{
			fileType = "application/vnd.oasis.opendocument.spreadsheet";
		}
		// pdf file
		else if (fileName.contains(".pdf"))
		{
			fileType = "application/pdf";
			// excel file
		}
		else if (fileName.contains(".xls"))
		{
			fileType = "application/excel";
		}
		// msword 2003 old file
		else if (fileName.contains(".doc"))
		{
			fileType = "application/msword";
		}
		// msword new file
		else if (fileName.contains(".docx"))
		{
			fileType = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
		}
		// text file
		else
		{
			fileType = "text/plain";
		}
		return fileType;
	}

}
