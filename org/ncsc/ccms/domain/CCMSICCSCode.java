package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSICCSCode extends CCMSDomain
{
	private long iccsCodeOID = 0L, courtOID = 0L;

	// If this is a division, group or class, the category that contains this
	// crime type
	private long parentOID = 0L;
	private CCMSICCSCode parentICCSCode = null;

	// As defined in CCMSConstants Section, Division, Group, Class
	private int categoryType = 0;

	// Describes the crime that the category encompasses
	private String categoryName = null;

	// two to six digit text that uniquely identifies this category
	private String categoryIdentifier = null;
	
	// Not stored in database, used only for loading the ICCS Table
	private String parentCategoryIdentifier = null;

	private static Logger logger = null;
	
	List<CCMSICCSLocalCharge> localCharges = new ArrayList<CCMSICCSLocalCharge>();
	
	public void loadICCSCodes(Connection conn) throws Exception
	{
		
		List<CCMSICCSCode> codeList = new ArrayList<CCMSICCSCode>();
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM ICCS_LOADER");
		ResultSet rs = stmt.executeQuery();
		while (rs.next())
		{
			String categoryID = rs.getString("CAT_ID").trim();
			String categoryName = rs.getString("CAT_NAME").trim();
			
			
			CCMSICCSCode code = new CCMSICCSCode(CCMSDomainUtilities.getInstance().genOID(), 5);
			code.setCategoryIdentifier(categoryID);
			code.setCategoryName(categoryName);
						
			// Set the category ID
			if ( code.getCategoryIdentifier().length() == 2 )
			{
				code.setCategoryType(CCMSConstants.ICCS_CAT_SECTION);
			}
			else if ( code.getCategoryIdentifier().length() == 4 )
			{
				code.setCategoryType(CCMSConstants.ICCS_CAT_DIVISION);
				code.parentCategoryIdentifier = code.getCategoryIdentifier().substring(0, 2);
			}
			else if ( code.getCategoryIdentifier().length() == 5 )
			{
				code.setCategoryType(CCMSConstants.ICCS_CAT_GROUP);
				code.parentCategoryIdentifier = code.getCategoryIdentifier().substring(0, 4);
			}
			else if ( code.getCategoryIdentifier().length() == 6 )
			{
				code.setCategoryType(CCMSConstants.ICCS_CAT_CLASS);
				code.parentCategoryIdentifier = code.getCategoryIdentifier().substring(0, 5);
			}
			else
			{
				System.out.println("ERROR length: " + categoryID.length() + " On " + code.getCategoryIdentifier());
			}
			

			codeList.add(code);
		}
		
		for ( CCMSICCSCode code : codeList )
		{
			// Set Parent OID but only for division, group and class
			if ( code.getCategoryType() > CCMSConstants.ICCS_CAT_SECTION && code.parentCategoryIdentifier != null )
			{
				CCMSICCSCode parentCode = new CCMSICCSCode(0L, 5);
				parentCode.setCategoryIdentifier(code.parentCategoryIdentifier);
				int index = codeList.indexOf(parentCode);
				if ( index >= 0 )
				{
				parentCode = codeList.get(index);
				code.setParentOID(parentCode.getIccsCodeOID());
				
				}
				else
				{
					System.out.println("ERROR: " + code);
				}
			}
			else
			{
				//System.out.println(code);
			}
			
			PreparedStatement pStmt = code.genInsertSQL(conn).get(0);
			pStmt.execute();
		}
		conn.commit();
	}

	public static void main(String[] args)
	{
		// TODO Auto-generated method stub
		try
		{
			CCMSICCSCode qCode = new CCMSICCSCode( 0L, 5 );
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			qCode.loadICCSCodes(conn);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param iccsCourtOID
	 *            : Include in query object to generate sql that will retrieve
	 *            the children of this category
	 * @param courtOID
	 *            : Include for all queries including the first to retrieve the
	 *            ICCS sections
	 */
	public CCMSICCSCode(long iccsCodeOID, long courtOID)
	{
		CCMSICCSCode.logger = LogManager
				.getLogger(CCMSICCSCode.class.getName());
		this.iccsCodeOID = iccsCodeOID;
		this.courtOID = courtOID;
	}

	public CCMSICCSCode()
	{
		CCMSICCSCode.logger = LogManager
				.getLogger(CCMSICCSCode.class.getName());

	}

	public CCMSICCSCode(ResultSet rs, Connection conn) throws Exception
	{
		CCMSICCSCode.logger = LogManager
				.getLogger(CCMSICCSCode.class.getName());
		this.iccsCodeOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.parentOID = rs.getLong("PARENT_ID");
		this.categoryType = rs.getInt("CATEGORY_TYPE");
		this.categoryName = rs.getString("CATEGORY_NAME");
		this.categoryIdentifier = rs.getString("CATEGORY_IDENTIFIER");
		
		PreparedStatement stmt = conn.prepareStatement("SELECT * FROM NJCCCMS.ICCS_LOCAL_CHARGE " + 
				" WHERE COURT_ID = ? AND CATEGORY_IDENTIFIER = ? ");
		stmt.setLong(1, this.courtOID);
		stmt.setString(2, this.categoryIdentifier);
		
		ResultSet rs1 = stmt.executeQuery();
		while ( rs1.next())
		{
			this.localCharges.add(new CCMSICCSLocalCharge(rs1, conn, false));
		}
		rs1.close();
		stmt.close();
	}

	public long getIccsCodeOID()
	{
		return iccsCodeOID;
	}

	public void setIccsCodeOID(long iccsCodeOID)
	{
		this.iccsCodeOID = iccsCodeOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public long getParentOID()
	{
		return parentOID;
	}

	public void setParentOID(long parentOID)
	{
		this.parentOID = parentOID;
	}

	public String getCategoryName()
	{
		return categoryName;
	}

	public void setCategoryName(String categoryName)
	{
		this.categoryName = categoryName;
	}

	public int getCategoryType()
	{
		return categoryType;
	}

	public void setCategoryType(int categoryType)
	{
		this.categoryType = categoryType;
	}

	public String getCategoryIdentifier()
	{
		return categoryIdentifier;
	}

	public void setCategoryIdentifier(String categoryIdentifier)
	{
		this.categoryIdentifier = categoryIdentifier;
	}

	public List<CCMSICCSLocalCharge> getLocalCharges()
	{
		return localCharges;
	}

	public void setLocalCharges(List<CCMSICCSLocalCharge> localCharges)
	{
		this.localCharges = localCharges;
	}

	public CCMSICCSCode getParentICCSCode()
	{
		return parentICCSCode;
	}

	public void setParentICCSCode(CCMSICCSCode parentICCSCode)
	{
		this.parentICCSCode = parentICCSCode;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.ICCS_CODE " + 
				" ( ID, CATEGORY_NAME, CATEGORY_TYPE, COURT_ID, PARENT_ID, CATEGORY_IDENTIFIER ) " + 
				" values ( ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.iccsCodeOID);
		stmt.setString(2, this.categoryName);
		stmt.setInt(3, this.categoryType);
		stmt.setLong(4, this.courtOID);
		stmt.setLong(5, this.parentOID);
		stmt.setString(6, this.categoryIdentifier);
		pStmts.add(stmt);
		return pStmts;
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}


	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.ICCS_CODE " + 
				"   SET CATEGORY_NAME = ?, CATEGORY_TYPE = ?, PARENT_ID = ?, CATEGORY_IDENTIFIER = ? " +
				" WHERE ID = ? ");
		stmt.setString(1, this.categoryName);
		stmt.setInt(2, this.categoryType);
		stmt.setLong(3, this.parentOID);
		stmt.setString(4, this.categoryIdentifier);
		stmt.setLong(5, this.iccsCodeOID);
		pStmts.add(stmt);
		
		return pStmts;
	}

	@Override
	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		String sqlString = " SELECT * FROM NJCCCMS.ICCS_CODE WHERE COURT_ID = "
				+ this.courtOID;

		if (this.iccsCodeOID != 0L)
		{
			// retrieve the children of this category
			sqlString += " AND PARENT_ID = " + this.iccsCodeOID;
		}
		else if ( this.categoryType != 0L )
		{
			sqlString += " AND CATEGORY_TYPE = " + this.categoryType;
		}
		

		sqlString += " ORDER BY CATEGORY_IDENTIFIER ";

		PreparedStatement stmt = conn.prepareStatement(sqlString);
		pStmts.add(stmt);

		return pStmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSICCSCode)
		{
			CCMSICCSCode iccsCode = (CCMSICCSCode) obj;
			if (iccsCode.getIccsCodeOID() == this.getIccsCodeOID()
					&& iccsCode.getCourtOID() == this.getCourtOID())
			{
				return true;
			}
			else if ( iccsCode.getCategoryIdentifier().equals(this.getCategoryIdentifier()))
			{
				return true;
			}
		}
		return false;
	}

	public String toString()
	{
		String stringObj = " ICCS OID: " + this.iccsCodeOID + " Identifier: "
				+ this.categoryIdentifier + " Cat Name: " + this.categoryName
				+ " Cat Type: " + this.categoryType + " ParentOID: "
				+ this.parentOID;
		return stringObj;
	}

}
