package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCourt extends CCMSDomain
{
	private long courtOID;
	private String locationCode, courtName;
	private static Logger logger = null;

	// List of the roles that are available to a specific user for a court
	private ArrayList<CCMSStaffRole> roles = new ArrayList<CCMSStaffRole>();

	public CCMSCourt() throws Exception
	{
		CCMSCourt.logger = LogManager.getLogger(CCMSCourt.class.getName());
	}

	
	/**
	 * The parameter retrieveLookupFlag will cause all lookup values to be retrieved and populated -
	 * This is most often required when running tests.
	 * @param courtOID
	 * @param retrieveLookupFlag
	 * @throws Exception
	 */
	public CCMSCourt(long courtOID)
			throws Exception
	{
		CCMSCourt.logger = LogManager.getLogger(CCMSCourt.class.getName());
		this.courtOID = courtOID;
	}

	public CCMSCourt(ResultSet rs, Connection conn) throws Exception
	{
		CCMSCourt.logger = LogManager.getLogger(CCMSCourt.class.getName());
		this.courtOID = rs.getLong("COURT_ID");
		this.locationCode = rs.getString("LOCATION_CODE");
		this.courtName = rs.getString("COURT_NAME");
	}

	public static void main(String[] args)
	{
		try
		{
			CCMSCourt myTestCourt = new CCMSCourt(5);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getLocationCode()
	{
		return locationCode;
	}

	public void setLocationCode(String locationCode)
	{
		this.locationCode = locationCode;
	}

	public String getCourtName()
	{
		return courtName;
	}

	public void setCourtName(String courtName)
	{
		this.courtName = courtName;
	}


	public ArrayList<CCMSStaffRole> getRoles()
	{
		return roles;
	}

	public void setRoles(ArrayList<CCMSStaffRole> roles)
	{
		this.roles = roles;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		String sqlString = "SELECT * FROM NJCCCMS.COURTS "
				+ " WHERE COURT_ID > 0 ";

		if (this.courtOID != 0L)
		{
			sqlString += " AND COURT_ID = " + this.courtOID;
		}
		PreparedStatement stmt = conn.prepareStatement(sqlString);
		stmts.add(stmt);

		return stmts;
	}

	public String toString()
	{
		String value = "Court Name:" + this.courtName + " Court OID:"
				+ this.courtOID + " + Location Code:" + this.locationCode;
		return value;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSCourt)
		{
			CCMSCourt court = (CCMSCourt) obj;
			if (court.getCourtOID() == this.getCourtOID()) { return true; }
		}
		return false;
	}
}
