package org.ncsc.ccms.domain;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.Elements;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

public class CCMSPartyAddress extends CCMSDomain
{
	// Fields that are in the object AND the database as specific columns
	private long addressOID;
	private long partyOID;
	private String addressType = null;
	private Date startDate = null;
	private Date endDate = null;

	// Object fields that are stored within the XML String in the database
	private String address1 = null;
	private String address2 = null;
	private String address3 = null;
	private String communityCode = null;
	private String administrativeArea = null;
	private String municipalityName = null;
	private String postalCode = null;
	private String countryName = null;
	private String addressDescriptionText = null;

	public CCMSPartyAddress(long addressOID)
	{
		this.addressOID = addressOID;
	}

	public CCMSPartyAddress(ResultSet rs, Connection conn) throws Exception
	{
		this.addressOID = rs.getLong("ID");
		this.partyOID = rs.getLong("PARTY_ID");
		this.addressType = rs.getString("ADDRESS_TYPE");
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");

		String addressXML = rs.getString("ADDRESS_XML");
		parseAddressXML(addressXML);
		
	}

	public long getAddressOID()
	{
		return addressOID;
	}

	public void setAddressOID(long addressOID)
	{
		this.addressOID = addressOID;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getAddressType()
	{
		return addressType;
	}

	public void setAddressType(String addressType)
	{
		this.addressType = addressType;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public String getAddress1()
	{
		return address1;
	}

	public void setAddress1(String address1)
	{
		this.address1 = address1;
	}

	public String getAddress2()
	{
		return address2;
	}

	public void setAddress2(String address2)
	{
		this.address2 = address2;
	}

	public String getAddress3()
	{
		return address3;
	}

	public void setAddress3(String address3)
	{
		this.address3 = address3;
	}

	public String getCommunityCode()
	{
		return communityCode;
	}

	public void setCommunityCode(String communityCode)
	{
		this.communityCode = communityCode;
	}

	public String getAdministrativeArea()
	{
		return administrativeArea;
	}

	public void setAdministrativeArea(String administrativeArea)
	{
		this.administrativeArea = administrativeArea;
	}

	public String getMunicipalityName()
	{
		return municipalityName;
	}

	public void setMunicipalityName(String municipalityName)
	{
		this.municipalityName = municipalityName;
	}

	public String getPostalCode()
	{
		return postalCode;
	}

	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	public String getCountryName()
	{
		return countryName;
	}

	public void setCountryName(String countryName)
	{
		this.countryName = countryName;
	}

	public String getAddressDescriptionText()
	{
		return addressDescriptionText;
	}

	public void setAddressDescriptionText(String addressDescriptionText)
	{
		this.addressDescriptionText = addressDescriptionText;
	}

	private String generateAddressXML()
	{
		String addressXML = null;

		Element addressRoot = new Element("address");

		if (this.address1 != null)
		{
			Element street1Element = new Element("address1");
			street1Element.appendChild(this.address1);
			addressRoot.appendChild(street1Element);
		}

		if (this.address2 != null)
		{
			Element street2Element = new Element("address2");
			street2Element.appendChild(this.address2);
			addressRoot.appendChild(street2Element);
		}

		if (this.address3 != null)
		{
			Element street3Element = new Element("address3");
			street3Element.appendChild(this.address3);
			addressRoot.appendChild(street3Element);
		}

		if (this.communityCode != null)
		{
			Element commCodeElement = new Element("communityCode");
			commCodeElement.appendChild(this.communityCode);
			addressRoot.appendChild(commCodeElement);
		}

		if (this.administrativeArea != null)
		{
			Element adminAreaElement = new Element("administrativeArea");
			adminAreaElement.appendChild(this.administrativeArea);
			addressRoot.appendChild(adminAreaElement);
		}

		if (this.municipalityName != null)
		{
			Element cityElement = new Element("municipalityName");
			cityElement.appendChild(this.municipalityName);
			addressRoot.appendChild(cityElement);
		}

		if (this.postalCode != null)
		{
			Element postalCodeElement = new Element("postalCode");
			postalCodeElement.appendChild(this.postalCode);
			addressRoot.appendChild(postalCodeElement);
		}

		if (this.countryName != null)
		{
			Element countryElement = new Element("countryName");
			countryElement.appendChild(this.countryName);
			addressRoot.appendChild(countryElement);
		}

		if (this.addressDescriptionText != null)
		{
			Element otherElement = new Element("addressDescriptionText");
			otherElement.appendChild(this.addressDescriptionText);
			addressRoot.appendChild(otherElement);
		}
		
		Document doc = new Document(addressRoot);
		addressXML = doc.toXML();

		return addressXML;
	}

	private void parseAddressXML(String addressXML)
	{
		Builder builder = new Builder();

		try
		{
			Document doc = builder.build(addressXML, null);

			Element addressRootElement = doc.getRootElement();

			Element address1Element = addressRootElement
					.getFirstChildElement("address1");
			if (address1Element != null && address1Element.getChild(0) != null)
			{
				this.address1 = address1Element.getChild(0).getValue();
			}

			Element address2Element = addressRootElement
					.getFirstChildElement("address2");
			if (address2Element != null && address2Element.getChild(0) != null)
			{
				this.address2 = address2Element.getChild(0).getValue();
			}

			Element address3Element = addressRootElement
					.getFirstChildElement("address3");
			if (address3Element != null && address3Element.getChild(0) != null)
			{
				this.address3 = address3Element.getChild(0).getValue();
			}

			Element communityCodeElement = addressRootElement
					.getFirstChildElement("communityCode");
			if (communityCodeElement != null
					&& communityCodeElement.getChild(0) != null)
			{
				this.communityCode = communityCodeElement.getChild(0)
						.getValue();
			}

			Element administrativeAreaElement = addressRootElement
					.getFirstChildElement("administrativeArea");
			if (administrativeAreaElement != null
					&& administrativeAreaElement.getChild(0) != null)
			{
				this.administrativeArea = administrativeAreaElement.getChild(0)
						.getValue();
			}

			Element municipalityElement = addressRootElement
					.getFirstChildElement("municipalityName");
			if (municipalityElement != null
					&& municipalityElement.getChild(0) != null)
			{
				this.municipalityName = municipalityElement.getChild(0)
						.getValue();
			}

			Element postalCodeElement = addressRootElement
					.getFirstChildElement("postalCode");
			if (postalCodeElement != null
					&& postalCodeElement.getChild(0) != null)
			{
				this.postalCode = postalCodeElement.getChild(0).getValue();
			}

			Element countryElement = addressRootElement
					.getFirstChildElement("countryName");
			if (countryElement != null && countryElement.getChild(0) != null)
			{
				this.countryName = countryElement.getChild(0).getValue();
			}

			Element addressDescriptionTextElement = addressRootElement
					.getFirstChildElement("addressDescriptionText");
			if (addressDescriptionTextElement != null
					&& addressDescriptionTextElement.getChild(0) != null)
			{
				this.addressDescriptionText = addressDescriptionTextElement
						.getChild(0).getValue();
			}
		}

		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.PARTY_ADDRESS "
						+ " ( ID, PARTY_ID, ADDRESS_TYPE, ADDRESS_XML, START_DATE, END_DATE ) "
						+ " VALUES ( ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.addressOID);
		stmt.setLong(2, this.partyOID);
		stmt.setString(3, this.addressType);
		stmt.setString(4, this.generateAddressXML());

		if (this.startDate != null)
		{
			stmt.setDate(5, new java.sql.Date(startDate.getTime()));
		} else
		{
			stmt.setNull(5, java.sql.Types.DATE);
		}

		if (this.endDate != null)
		{
			stmt.setDate(6, new java.sql.Date(endDate.getTime()));
		} else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}

		stmts.add(stmt);

		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		return null;
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			ResultSet rs = conn.prepareStatement("SELECT * FROM PARTY_ADDRESS")
					.executeQuery();
			while (rs.next())
			{
				CCMSPartyAddress add = new CCMSPartyAddress(rs, conn);
				System.out.println("here");
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		System.exit(1);
		// TODO Auto-generated method stub

		Element addressRoot = new Element("address");

		Element street1Element = new Element("address1");
		street1Element.appendChild("2201 Southern Main Road");
		addressRoot.appendChild(street1Element);

		Element street2Element = new Element("address2");
		street2Element.appendChild("Apartment 1B");
		addressRoot.appendChild(street2Element);

		Element street3Element = new Element("address3");
		street3Element.appendChild("Cap-de-Ville");
		addressRoot.appendChild(street3Element);

		Element commCodeElement = new Element("communityCode");
		commCodeElement.appendChild("9806");
		addressRoot.appendChild(commCodeElement);

		Element adminAreaElement = new Element("administrativeArea");
		adminAreaElement.appendChild("50");
		addressRoot.appendChild(adminAreaElement);

		Element cityElement = new Element("municipalityName");
		cityElement.appendChild("Point Fortin");
		addressRoot.appendChild(cityElement);

		Element postalCodeElement = new Element("postalCode");
		postalCodeElement.appendChild("N/A");
		addressRoot.appendChild(postalCodeElement);

		Element countryElement = new Element("countryName");
		countryElement.appendChild("Trinidad & Tobago");
		addressRoot.appendChild(countryElement);

		Element otherElement = new Element("addressDescriptionText");
		otherElement.appendChild("N/A");
		addressRoot.appendChild(otherElement);

		Document doc = new Document(addressRoot);

		String result = doc.toXML();
		System.out.println(result);

		Elements elements = doc.getRootElement().getChildElements("countryName");
		Element elmt = elements.get(0);
	}

}
