package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSWorkflowStep extends CCMSDomain
{
	private static Logger logger = null;
	private long workflowStepOID, courtOID, workflowOID, documentTemplateOID;
	private int delayDays;

	private CCMSTaskType taskType = null;
	private CCMSParty assignedParty = null; // pass as
														// assignedPartyOID
	private CCMSStaffPool assignedPool = null; // pass as
																// assignedPoolOID
	
	public CCMSWorkflowStep(long workflowStepOID, long courtOID )
	{
		this.workflowStepOID = workflowStepOID;
		this.courtOID = courtOID;
	}

	public CCMSWorkflowStep(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		CCMSWorkflowStep.logger = LogManager.getLogger(CCMSWorkflowStep.class
				.getName());

		this.workflowStepOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.workflowOID = rs.getLong("WORKFLOW_ID");
		this.delayDays = rs.getInt("DELAY_DAYS");

		long taskTypeOID = rs.getLong("TASK_TYPE_ID");
		this.taskType = new CCMSTaskType(taskTypeOID, this.courtOID);
		taskType = (CCMSTaskType) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(taskType, court);
		
		this.documentTemplateOID = rs.getLong("DOCUMENT_TEMPLATE_ID");

		long assignedPartyOID = rs.getLong("ASSIGNED_PARTY_ID");

		if (assignedPartyOID != 0L)
		{
			logger.info("Retrieving Assigned Party: " + assignedPartyOID);
			this.assignedParty = new CCMSParty(assignedPartyOID, this.courtOID);
			PreparedStatement pStmt1 = assignedParty.genSelectSQL(conn).get(0);
			ResultSet rs1 = pStmt1.executeQuery();
			while (rs1.next())
			{
				assignedParty = new CCMSParty(rs1, conn, false);
			}
			pStmt1.close();
			rs1.close();
		}

		long assignedPoolOID = rs.getLong("ASSIGNED_POOL_ID");
		if (assignedPoolOID != 0L)
		{
			logger.info("Retrieving Assigned Pool: " + assignedPoolOID);
			this.assignedPool = new CCMSStaffPool(assignedPoolOID,
					this.courtOID);

			PreparedStatement pStmt2 = assignedPool.genSelectSQL(conn).get(0);
			ResultSet rs2 = pStmt2.executeQuery();
			while (rs2.next())
			{
				assignedPool = new CCMSStaffPool(rs2, conn);
			}
			pStmt2.close();
			rs2.close();
		}

	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.WORKFLOW_STEP "
						+ " ( ID, COURT_ID, WORKFLOW_ID, DELAY_DAYS, TASK_TYPE_ID, ASSIGNED_PARTY_ID, ASSIGNED_POOL_ID, DOCUMENT_TEMPLATE_ID ) "
						+ " VALUES (?, ?, ?, ?, ?, ?, ?, ? ) ");

		stmt.setLong(1, this.workflowStepOID);
		stmt.setLong(2, this.courtOID);
		stmt.setLong(3, this.workflowOID);
		stmt.setInt(4, this.delayDays);
		stmt.setLong(5, this.taskType.getTaskTypeOID());
		
		if ( this.assignedParty != null )
		{
			stmt.setLong(6, this.assignedParty.getPartyOID());
		}
		else
		{
			stmt.setLong(6, 0L);
		}
		
		if ( this.assignedPool != null )
		{
			stmt.setLong(7, this.assignedPool.getPoolOID());
		}
		else
		{
			stmt.setLong(7, 0L);
		}
		
		stmt.setLong(8, this.documentTemplateOID);
		
		stmts.add(stmt);

		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		// No ability to delete an individual workflow step
		return stmts;
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		// NO ability to update an workflow step
		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		return stmts;
	}

	public long getWorkflowStepOID()
	{
		return workflowStepOID;
	}

	public void setWorkflowStepOID(long workflowStepOID)
	{
		this.workflowStepOID = workflowStepOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public int getDelayDays()
	{
		return delayDays;
	}

	public void setDelayDays(int delayDays)
	{
		this.delayDays = delayDays;
	}

	public CCMSParty getAssignedParty()
	{
		return assignedParty;
	}

	public void setAssignedParty(CCMSParty assignedParty)
	{
		this.assignedParty = assignedParty;
	}

	public CCMSStaffPool getAssignedPool()
	{
		return assignedPool;
	}

	public void setAssignedPool(CCMSStaffPool assignedPool)
	{
		this.assignedPool = assignedPool;
	}

	public long getWorkflowOID()
	{
		return workflowOID;
	}

	public void setWorkflowOID(long workflowOID)
	{
		this.workflowOID = workflowOID;
	}

	public CCMSTaskType getTaskType()
	{
		return taskType;
	}

	public long getDocumentTemplateOID()
	{
		return documentTemplateOID;
	}

	public void setDocumentTemplateOID(long documentTemplateOID)
	{
		this.documentTemplateOID = documentTemplateOID;
	}

	public void setTaskType(CCMSTaskType taskType)
	{
		this.taskType = taskType;
	}
}
