package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;

public class CCMSICCSLocalCharge extends CCMSDomain
{
	private long localChargeOID;

	private long courtOID;

	private String categoryIdentifier = null;

	private String localCharge = null;

	private String localLaw = null;

	private CCMSICCSCode parentICCSCode = null;

	public CCMSICCSLocalCharge(long localChargeOID, long courtOID)
	{
		this.localChargeOID = localChargeOID;
		this.courtOID = courtOID;
	}

	public CCMSICCSLocalCharge(ResultSet rs, Connection conn,
			boolean retrieveParentICCSCode) throws Exception
	{
		this.localChargeOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.categoryIdentifier = rs.getString("CATEGORY_IDENTIFIER");
		this.localCharge = rs.getString("LOCAL_CHARGE_TEXT");
		this.localLaw = rs.getString("LOCAL_LAW_TEXT");

		// The parent ICCS Code only needs to be retrieved for the Local Charge lookup table population (Bottom -> Up approach to charge selection)
		if (retrieveParentICCSCode)
		{
			parentICCSCode = new CCMSICCSCode(0, this.courtOID);
			parentICCSCode.setCategoryIdentifier(this.categoryIdentifier);
			parentICCSCode = (CCMSICCSCode) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(parentICCSCode,
							new CCMSCourt(this.courtOID));
			
			// Work up the ICCS Charge Code Hierarchy to grab parents
			//long parentOID = parentICCSCode.getParentOID();
			CCMSICCSCode currentICCSCode = parentICCSCode;
			
			while ( currentICCSCode != null && currentICCSCode.getParentOID() != 0L )
			{
				CCMSICCSCode parentCode = new CCMSICCSCode(currentICCSCode.getParentOID(), this.courtOID);
				parentCode = (CCMSICCSCode)CCMSDomainUtilities.getInstance().retrieveFullObject(parentCode, new CCMSCourt(this.courtOID));
				if ( parentCode == null )
				{
					System.out.println("error in parent");
					
				}
				currentICCSCode.setParentICCSCode(parentCode);
				currentICCSCode = parentCode;
			}
		}
	}

	public long getLocalChargeOID()
	{
		return localChargeOID;
	}

	public void setLocalChargeOID(long localChargeOID)
	{
		this.localChargeOID = localChargeOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getCategoryIdentifier()
	{
		return categoryIdentifier;
	}

	public void setCategoryIdentifier(String categoryIdentifier)
	{
		this.categoryIdentifier = categoryIdentifier;
	}

	public String getLocalCharge()
	{
		return localCharge;
	}

	public void setLocalCharge(String localCharge)
	{
		this.localCharge = localCharge;
	}

	public String getLocalLaw()
	{
		return localLaw;
	}

	public void setLocalLaw(String localLaw)
	{
		this.localLaw = localLaw;
	}

	public CCMSICCSCode getParentICCSCode()
	{
		return parentICCSCode;
	}

	public void setParentICCSCode(CCMSICCSCode parentICCSCode)
	{
		this.parentICCSCode = parentICCSCode;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		String sqlString = " SELECT * FROM NJCCCMS.ICCS_LOCAL_CHARGE WHERE COURT_ID = "
				+ this.courtOID;

		sqlString += " ORDER BY LOCAL_CHARGE_TEXT ";

		PreparedStatement stmt = conn.prepareStatement(sqlString);
		pStmts.add(stmt);

		return pStmts;
	}
}
