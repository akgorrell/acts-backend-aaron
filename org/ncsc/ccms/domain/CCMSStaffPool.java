package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSStaffPool extends CCMSDomain
{
	private long poolOID, courtOID;
	private String poolName;
	private static Logger logger = null;
	private List<CCMSParty> poolStaffParties = new ArrayList<CCMSParty>();

	public CCMSStaffPool()
	{
		CCMSStaffPool.logger = LogManager.getLogger(CCMSStaffPool.class
				.getName());
	}

	public CCMSStaffPool(long poolOID, long courtOID)
	{
		CCMSStaffPool.logger = LogManager.getLogger(CCMSStaffPool.class
				.getName());
		this.poolOID = poolOID;
		this.courtOID = courtOID;
	}

	public CCMSStaffPool(ResultSet rs, Connection conn) throws Exception
	{
		CCMSStaffPool.logger = LogManager.getLogger(CCMSStaffPool.class
				.getName());
		this.poolOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.poolName = rs.getString("NAME");

		PreparedStatement pStmt = conn
				.prepareStatement("SELECT NJCCCMS.PARTY.* FROM NJCCCMS.STAFF_POOL, NJCCCMS.STAFF_POOL_REL, NJCCCMS.PARTY "
						+ " WHERE STAFF_POOL.COURT_ID = ? "
						+ "   AND STAFF_POOL.ID = ? "
						+ "   AND STAFF_POOL.ID = STAFF_POOL_REL.POOL_ID "
						+ "   AND STAFF_POOL_REL.PARTY_ID = PARTY.ID ");
		pStmt.setLong(1, this.courtOID);
		pStmt.setLong(2, this.poolOID);

		ResultSet rs2 = pStmt.executeQuery();
		while (rs2.next())
		{
			this.poolStaffParties.add(new CCMSParty(rs2, conn, false));
		}
		rs2.close();
		pStmt.close();
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSStaffPool qPool = new CCMSStaffPool(0L, 5);
			PreparedStatement stmt = qPool.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				CCMSStaffPool pool = new CCMSStaffPool(rs, conn);
				System.out.println(pool);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public long getPoolOID()
	{
		return poolOID;
	}

	public void setPoolOID(long poolOID)
	{
		this.poolOID = poolOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public String getPoolName()
	{
		return poolName;
	}

	public void setPoolName(String poolName)
	{
		this.poolName = poolName;
	}

	public List<CCMSParty> getPoolStaffParties()
	{
		return poolStaffParties;
	}

	public void setPoolStaffParties(List<CCMSParty> poolStaffParties)
	{
		this.poolStaffParties = poolStaffParties;
	}

	@Override
	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genInsertSQL(conn);
	}

	@Override
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genDeleteSQL(conn);
	}

	@Override
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		// TODO Auto-generated method stub
		return super.genUpdateSQL(conn);
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();
		String sqlString = "SELECT * FROM NJCCCMS.STAFF_POOL "
				+ " WHERE STAFF_POOL.COURT_ID = " + this.courtOID;
		if ( this.poolOID != 0L )
		{
			sqlString += " AND ID = " + this.poolOID;
		}
		PreparedStatement pStmt = conn
				.prepareStatement(sqlString);
		pStmts.add(pStmt);
		return pStmts;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSStaffPool)
		{
			CCMSStaffPool pool = (CCMSStaffPool) obj;
			if (pool.getPoolOID() == this.getPoolOID())
			{
				return true;
			}
		}
		return false;
	}

	@Override
	public String toString()
	{
		String objString = " PoolOID: " + this.poolOID + " PoolName: "
				+ this.poolName + "PoolStaffMembers: "
				+ this.poolStaffParties.size();
		return objString;
	}
}
