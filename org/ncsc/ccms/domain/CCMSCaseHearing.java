package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

public class CCMSCaseHearing extends CCMSDomain
{
	private long courtOID, caseHearingOID, caseOID;
	private String description;
	private Timestamp startDateTime, endDateTime;
	private CCMSHearingType hearingType;
	private CCMSParty judicialOfficer;
	private CCMSCourtLocation courtLoc;
	private static Logger logger = null;
	private Date hearingQueryDate;
	private CCMSCase associatedCase = null;

	
	public CCMSCaseHearing()
	{
		CCMSCaseHearing.logger = LogManager.getLogger(CCMSCaseHearing.class
				.getName());

	}

	public CCMSCaseHearing(long caseHearingOID, long courtOID)
	{
		CCMSCaseHearing.logger = LogManager.getLogger(CCMSCaseHearing.class
				.getName());
		this.caseHearingOID = caseHearingOID;
		this.courtOID = courtOID;
	}

	public CCMSCaseHearing(ResultSet rs, Connection conn, CCMSCourt court)
			throws Exception
	{
		CCMSCaseHearing.logger = LogManager.getLogger(CCMSCaseHearing.class
				.getName());
		this.caseOID = rs.getLong("CASE_ID");
		this.caseHearingOID = rs.getLong("ID");
		this.courtOID = rs.getLong("COURT_ID");
		this.description = rs.getString("DESCRIPTION");
		long hearingTypeOID = rs.getLong("HEARING_TYPE_ID");
		this.hearingType = new CCMSHearingType(hearingTypeOID, this.courtOID);
		this.hearingType = (CCMSHearingType) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(hearingType, court);
		long courtLocationOID = rs.getLong("COURT_LOCATION_ID");
		this.courtLoc = new CCMSCourtLocation(courtLocationOID,
				this.getCourtOID());
		this.courtLoc = (CCMSCourtLocation) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(this.courtLoc, court);

		if (CCMSDomainUtilities.hasColumn(rs, "CASE_ID"))
		{
			String caseNumber = rs.getString("CASE_ID");
			CCMSCase assocCase = new CCMSCase(caseOID, court);
			assocCase.setCaseNumber(caseNumber);
			this.associatedCase = assocCase;
		}

		long judicialOfficerOID = rs.getLong("JUDICIAL_OFFICER_ID");
		this.judicialOfficer = new CCMSParty(judicialOfficerOID);
		PreparedStatement joStmt = judicialOfficer.genSelectSQL(conn).get(0);
		ResultSet rs1 = joStmt.executeQuery();
		while (rs1.next())
		{
			this.judicialOfficer = new CCMSParty(rs1, conn, false);
		}
		rs1.close();
		joStmt.close();

		this.startDateTime = rs.getTimestamp("START_DATETIME");
		this.endDateTime = rs.getTimestamp("END_DATETIME");
	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			CCMSCaseHearing hrng = new CCMSCaseHearing();
			hrng.setCourtOID(5);
			hrng.setStartDateTime(new Timestamp(System.currentTimeMillis()));
			PreparedStatement pStmt = hrng.genSelectSQL(conn).get(0);
			ResultSet rs = pStmt.executeQuery();
			Gson myGson = new Gson();

			while (rs.next())
			{
				CCMSCaseHearing dbHrng = new CCMSCaseHearing(rs, conn, court);
				System.out.println(dbHrng);
				String json = myGson.toJson(dbHrng);
				System.out.println(json);
			}
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public long getCaseHearingOID()
	{
		return caseHearingOID;
	}

	public void setCaseHearingOID(long caseHearingOID)
	{
		this.caseHearingOID = caseHearingOID;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public long getCaseOID()
	{
		return caseOID;
	}

	public void setCaseOID(long caseOID)
	{
		this.caseOID = caseOID;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public Timestamp getStartDateTime()
	{
		return startDateTime;
	}

	public void setStartDateTime(Timestamp startDateTime)
	{
		this.startDateTime = startDateTime;
	}

	public Timestamp getEndDateTime()
	{
		return endDateTime;
	}

	public void setEndDateTime(Timestamp endDateTime)
	{
		this.endDateTime = endDateTime;
	}

	public CCMSHearingType getHearingType()
	{
		return hearingType;
	}

	public void setHearingType(CCMSHearingType hearingType)
	{
		this.hearingType = hearingType;
	}

	public CCMSParty getJudicialOfficer()
	{
		return judicialOfficer;
	}

	public void setJudicialOfficer(CCMSParty judicialOfficer)
	{
		this.judicialOfficer = judicialOfficer;
	}

	public CCMSCourtLocation getCourtLoc()
	{
		return courtLoc;
	}

	public void setCourtLoc(CCMSCourtLocation courtLoc)
	{
		this.courtLoc = courtLoc;
	}

	public Date getHearingQueryDate()
	{
		return hearingQueryDate;
	}

	public void setHearingQueryDate(Date hearingQueryDate)
	{
		this.hearingQueryDate = hearingQueryDate;
	}

	public CCMSCase getAssociatedCase()
	{
		return associatedCase;
	}

	public void setAssociatedCase(CCMSCase associatedCase)
	{
		this.associatedCase = associatedCase;
	}


	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.CASE_HEARING "
						+ "        ( ID, COURT_ID, CASE_ID, HEARING_TYPE_ID, DESCRIPTION, "
						+ "          START_DATETIME, END_DATETIME, COURT_LOCATION_ID, JUDICIAL_OFFICER_ID ) "
						+ "   VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.caseHearingOID);
		stmt.setLong(2, this.courtOID);
		stmt.setLong(3, this.caseOID);
		stmt.setLong(4, this.hearingType.getHearingTypeOID());
		stmt.setString(5, this.description);
		stmt.setTimestamp(6, this.startDateTime);
		stmt.setTimestamp(7, this.endDateTime);
		stmt.setLong(8, this.courtLoc.getLocationOID());
		stmt.setLong(9, this.judicialOfficer.getPartyOID());
		stmts.add(stmt);
		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		PreparedStatement stmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_HEARING WHERE ID = ? AND COURT_ID = ?");
		stmt.setLong(1, this.caseHearingOID);
		stmt.setLong(2, this.courtOID);
		stmts.add(stmt);
		return stmts;
	}
	
	/**
	 * Returns a true if a case already has a hearing scheduled for the exact same date/time 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public boolean isDuplicateHearing(Connection conn) throws Exception
	{
		boolean isDuplicate = false;
		
		PreparedStatement stmt = conn.prepareStatement("SELECT COUNT(*) FROM NJCCCMS.CASE_HEARING "
				+ " WHERE CASE_HEARING.CASE_ID = ? " 
				+ "   AND CASE_HEARING.COURT_ID = ? "
				+ "   AND CASE_HEARING.START_DATETIME = ? "
				+ "   AND CASE_HEARING.HEARING_TYPE_ID = ? ");
		
		stmt.setLong(1, this.caseOID);
		stmt.setLong(2, this.courtOID);
		stmt.setTimestamp(3, this.startDateTime);
		stmt.setLong(4, this.hearingType.getHearingTypeOID());
		
		ResultSet rs = stmt.executeQuery();
		while (rs.next())
		{
			int count = rs.getInt(1);
			if (count > 0 )
			{
				isDuplicate = true;
			}
		}
		rs.close();
		
		
		return isDuplicate;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		String sqlString = " SELECT * FROM NJCCCMS.CASE_HEARING, NJCCCMS.CASE "
				+ " WHERE CASE_HEARING.CASE_ID = CASE.CASE_ID "
				+ "AND CASE_HEARING.COURT_ID = " + this.courtOID;

		if (this.hearingQueryDate != null)
		{
			String searchDate = CCMSDomainUtilities.getInstance()
					.convertDateToString(this.hearingQueryDate);
			sqlString += " AND "
					+ CCMSDomainUtilities.getInstance().genSQLTimeQuery(
							"START_DATETIME", searchDate);
		}
		if (this.judicialOfficer != null)
		{
			sqlString += " AND JUDICIAL_OFFICER_ID = "
					+ judicialOfficer.getPartyOID();
		}
		if (this.caseOID != 0L)
		{
			sqlString += " AND CASE_ID = " + this.caseOID;
		}
		if (this.caseHearingOID != 0L)
		{
			sqlString += " AND ID = " + this.caseHearingOID;
		}
		if ( this.courtLoc != null )
		{
			sqlString += " AND COURT_LOCATION_ID = " + this.courtLoc.getLocationOID();
		}

		sqlString += " ORDER BY START_DATETIME ";

		logger.debug(sqlString);

		PreparedStatement stmt = conn.prepareStatement(sqlString);
		stmts.add(stmt);

		return stmts;
	}

}
