package org.ncsc.ccms.domain;

import java.util.Comparator;

public class CCMSComparator implements Comparator
{

	public CCMSComparator()
	{
	}

	public int compare(Object o1, Object o2)
	{
		int result = 0;
		try
		{
			if (o1 instanceof CCMSCaseCharge && o2 instanceof CCMSCaseCharge)
			{
				CCMSCaseCharge charge1 = (CCMSCaseCharge) o1;
				CCMSCaseCharge charge2 = (CCMSCaseCharge) o2;

				int iccsCode1 = Integer.parseInt(charge1.getIccsCode()
						.getCategoryIdentifier());
				int iccsCode2 = Integer.parseInt(charge2.getIccsCode()
						.getCategoryIdentifier());

				result = new Integer(iccsCode1)
						.compareTo(new Integer(iccsCode2));
			}
		}
		catch (Exception e)
		{
			// Swallow it, dont do anything
		}
		return result;
	}

	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSComparator)
		{
			CCMSComparator compObj = (CCMSComparator) obj;
			return true;
		}
		else
		{
			return false;
		}
	}
}
