package org.ncsc.ccms.domain;

import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CCMSCase extends CCMSDomain
{
	private long caseOID = 0L;
	private CCMSCourt court = null;
	private String caseNumber = "";
	private List<CCMSCaseParty> caseParties = new ArrayList<CCMSCaseParty>();
	private List<CCMSCaseCharge> caseCharges = new ArrayList<CCMSCaseCharge>();
	private List<CCMSCaseTask> caseTasks = new ArrayList<CCMSCaseTask>();
	private List<CCMSCaseEvent> caseEvents = new ArrayList<CCMSCaseEvent>();

	private List<CCMSCaseDocument> caseDocs = new ArrayList<CCMSCaseDocument>();
	private List<CCMSCaseHearing> caseHearings = new ArrayList<CCMSCaseHearing>();
	private List<CCMSCaseJudicialAssignment> judicialAssignments = new ArrayList<CCMSCaseJudicialAssignment>();
	private CCMSCasePhase casePhase = null;
	private CCMSCaseStatus caseStatus = null;
	private CCMSCaseType caseType = null;
	private Date caseFilingDate = null;
	private String caseCaption = null;
	private int caseWeight = 0;
	private boolean associatedCase = false;

	// fields for searching for a case
	private String searchCasePartyName = null;
	private long searchCasePartyRoleOID = 0L;

	private static Logger logger = null;

	public CCMSCase()
	{
		CCMSCase.logger = LogManager.getLogger(CCMSCase.class.getName());
	}

	public CCMSCase(long caseOID, CCMSCourt court)
	{
		CCMSCase.logger = LogManager.getLogger(CCMSCase.class.getName());
		this.caseOID = caseOID;
		this.court = court;
	}

	/**
	 * 
	 * @param rs
	 * @param conn
	 * @param court
	 *            : Needs to be a court object with the lookup tables populated
	 *            summaryResultFlag: Yes if only should return summary data and
	 *            not all lookup values
	 * @throws Exception
	 */
	public CCMSCase(ResultSet rs, Connection conn, CCMSCourt court,
			boolean summaryResultFlag) throws Exception
	{
		CCMSCase.logger = LogManager.getLogger(CCMSCase.class.getName());
		this.caseOID = rs.getLong("CASE_ID");
		this.court = court;
		this.caseNumber = rs.getString("CASE_NUMBER");
		this.caseFilingDate = rs.getDate("CASE_CREATION_DATE");
		this.caseWeight = rs.getInt("CASE_WEIGHT");

		// Convert case phase to an object by finding the object in the Court
		// object
		long casePhaseOID = rs.getLong("CASE_PHASE_ID");
		this.casePhase = new CCMSCasePhase(casePhaseOID, court.getCourtOID());
		this.casePhase = (CCMSCasePhase) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(this.casePhase, court);

		// Retrieve the case types from the court lookup
		long caseTypeOID = rs.getLong("CASE_TYPE_ID");
		this.caseType = new CCMSCaseType(caseTypeOID, court.getCourtOID());
		this.caseType = (CCMSCaseType) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(this.caseType, court);

		// Convert case status to an object by finding the object in the Court
		// object
		long caseStatusOID = rs.getLong("CASE_STATUS_ID");
		this.caseStatus = new CCMSCaseStatus(caseStatusOID, court.getCourtOID());
		this.caseStatus = (CCMSCaseStatus) CCMSDomainUtilities.getInstance()
				.retrieveFullObject(this.caseStatus, court);

		this.caseCaption = rs.getString("CASE_CAPTION");

		logger.debug("Retrieved Case: " + this.caseNumber);

		// Retrieve all case parties
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.CASE, NJCCCMS.CASE_PARTY_REL, NJCCCMS.PARTY "
						+ " WHERE CASE.CASE_ID = ? "
						+ " AND CASE.COURT_ID = ? "
						+ " AND CASE.CASE_ID = CASE_PARTY_REL.CASE_ID "
						+ " AND CASE_PARTY_REL.PARTY_ID = PARTY.ID "
						+ " AND CASE.COURT_ID = CASE_PARTY_REL.COURT_ID");
		pStmt.setLong(1, this.caseOID);
		pStmt.setLong(2, this.court.getCourtOID());

		// Add case parties
		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			CCMSParty party = new CCMSParty(rs1, conn, false);
			int anonymizationFlag = rs1.getInt("ANONYMIZATION_FLAG");
			party.setAnonymizationFlag(CCMSDomainUtilities
					.intToBool(anonymizationFlag));

			long partyRoleOID = rs1.getLong("ROLE_ID");
			CCMSCasePartyRole casePartyRole = new CCMSCasePartyRole(
					partyRoleOID, this.court.getCourtOID());
			casePartyRole = (CCMSCasePartyRole) CCMSDomainUtilities
					.getInstance()
					.retrieveFullObject(casePartyRole, this.court);

			CCMSCaseParty caseParty = new CCMSCaseParty(party, casePartyRole);
			caseParty.setStartDate(rs1.getDate("START_DATE"));
			caseParty.setEndDate(rs1.getDate("END_DATE"));

			this.caseParties.add(caseParty);
		}
		pStmt.close();
		rs1.close();

		// Set the case caption
		// String calculatedCaption = this.calculateCaseCaption();

		/**
		 * 20180321: Initial version of CCMS had the case caption calculated
		 * with each retrieval, user requirements changed where they want the
		 * caption stored in the database so the user can update from the case
		 * update screen. This block of code will populate the case caption
		 * based on the calculated value ONLY if the database stored caption is
		 * null or blank.
		 */
		// if (this.caseCaption == null || this.caseCaption.isEmpty())
		// {
		// this.caseCaption = calculatedCaption;
		// }

		if (!summaryResultFlag)
		{
			long startTime = System.currentTimeMillis();

			// Retrieve judicial officers
			PreparedStatement judOffStmt = conn
					.prepareStatement(" SELECT * FROM NJCCCMS.CASE_JUDICIAL_ASSIGNMENT "
							+ " WHERE COURT_ID = ? AND CASE_ID = ? "
							+ " ORDER BY START_DATE DESC ");
			judOffStmt.setLong(1, this.court.getCourtOID());
			judOffStmt.setLong(2, this.caseOID);
			ResultSet rs7 = judOffStmt.executeQuery();
			while (rs7.next())
			{
				this.judicialAssignments.add(new CCMSCaseJudicialAssignment(
						rs7, conn));
			}
			judOffStmt.close();
			rs7.close();

			logger.debug("Judicial Officials Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();

			// Retrieve the case charges
			PreparedStatement chargeStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.CASE_CHARGE "
							+ " WHERE COURT_ID = ? AND CASE_ID = ? ");
			chargeStmt.setLong(1, this.court.getCourtOID());
			chargeStmt.setLong(2, this.caseOID);
			ResultSet rs2 = chargeStmt.executeQuery();

			while (rs2.next())
			{
				CCMSCaseCharge charge = new CCMSCaseCharge(rs2, conn, court);
				this.getCaseCharges().add(charge);
			}
			chargeStmt.close();
			rs2.close();

			// Sort the case charge in order of severity
			Collections.sort(this.getCaseCharges(), new CCMSComparator());
			logger.debug("Case Charges Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();

			// Retrieve the case tasks - will need to copy
			PreparedStatement taskStmt = conn
					.prepareStatement(CCMSCaseTask.FETCH_CASE_TASK_SQL);
			taskStmt.setLong(1, this.court.getCourtOID());
			taskStmt.setLong(2, this.caseOID);
			ResultSet rs3 = taskStmt.executeQuery();

			while (rs3.next())
			{
				CCMSCaseTask task = new CCMSCaseTask(rs3, conn, court, true);
				this.caseTasks.add(task);
			}
			taskStmt.close();
			rs3.close();
			logger.debug("Tasks Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();

			// Retrieve the case events
			PreparedStatement eventStmt = conn
					.prepareStatement("SELECT CASE_EVENT.*, PARTY.FIRST_NAME, PARTY.LAST_NAME, CASE_DOCUMENT.DOC_NAME "
							+ " FROM NJCCCMS.CASE_EVENT LEFT OUTER JOIN NJCCCMS.CASE_DOCUMENT ON CASE_EVENT.DOCUMENT_ID = CASE_DOCUMENT.ID  "
							+ " LEFT OUTER JOIN NJCCCMS.PARTY ON CASE_EVENT.PARTY_ID = PARTY.ID  "
							+ " WHERE CASE_EVENT.COURT_ID = ? AND CASE_EVENT.CASE_ID = ? ORDER BY EVENT_DATE DESC");

			eventStmt.setLong(1, this.court.getCourtOID());
			eventStmt.setLong(2, this.caseOID);
			ResultSet rs4 = eventStmt.executeQuery();

			while (rs4.next())
			{
				CCMSCaseEvent caseEvent = new CCMSCaseEvent(rs4, conn, court);
				this.caseEvents.add(caseEvent);
			}
			eventStmt.close();
			rs4.close();
			logger.debug("Case Events Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();

			// Retrieve the case documents
			PreparedStatement docsStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.CASE_DOCUMENT WHERE COURT_ID = ? "
							+ " AND CASE_ID = ? ORDER BY LAST_UPDATE_DATE DESC");
			docsStmt.setLong(1, this.court.getCourtOID());
			docsStmt.setLong(2, this.caseOID);
			ResultSet rs5 = docsStmt.executeQuery();

			while (rs5.next())
			{
				CCMSCaseDocument caseDoc = new CCMSCaseDocument(rs5, conn);
				this.caseDocs.add(caseDoc);
			}
			docsStmt.close();
			rs5.close();
			logger.debug("Case Documents Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();

			// Retrieve the case hearings
			PreparedStatement hrngStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.CASE_HEARING WHERE COURT_ID = ? "
							+ " AND CASE_ID = ? ORDER BY START_DATETIME DESC");
			hrngStmt.setLong(1, this.court.getCourtOID());
			hrngStmt.setLong(2, this.caseOID);
			ResultSet rs6 = hrngStmt.executeQuery();

			while (rs6.next())
			{
				CCMSCaseHearing caseHrng = new CCMSCaseHearing(rs6, conn, court);
				this.caseHearings.add(caseHrng);
			}
			hrngStmt.close();
			rs6.close();
			logger.debug("Case Hearings Elapsed: "
					+ (System.currentTimeMillis() - startTime) + " ms");
			startTime = System.currentTimeMillis();
		}

	}

	public static void main(String[] args)
	{
		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			CCMSCourt court = new CCMSCourt(5);
			CCMSCase qCase = new CCMSCase(0L, court);
			CCMSDomainUtilities.getInstance();
			Thread.sleep(2000);

			PreparedStatement stmt = qCase.genSelectSQL(conn).get(0);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				qCase = new CCMSCase(rs, conn, court, true);
				String caption = qCase.calculateCaseCaption(conn);
				System.out.println(qCase.getCaseNumber() + ": " + caption);
				PreparedStatement updateStmt = conn.prepareStatement(  "UPDATE NJCCCMS.CASE SET CASE_CAPTION = ? " + 
						" WHERE COURT_ID = ? AND CASE_ID = ? ");
				updateStmt.setString(1, caption);
				updateStmt.setLong(2, qCase.getCourt().getCourtOID());
				updateStmt.setLong(3, qCase.getCaseOID());
				int rowsUpdated = updateStmt.executeUpdate();
				conn.commit();
				System.out.println(rowsUpdated + " rows updated");
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public long getCaseOID()
	{
		return caseOID;
	}

	public void setCaseOID(long caseOID)
	{
		this.caseOID = caseOID;
	}

	public List<CCMSCaseParty> getCaseParties()
	{
		return caseParties;
	}

	public CCMSCasePhase getCasePhase()
	{
		return casePhase;
	}

	public void setCasePhase(CCMSCasePhase casePhase)
	{
		this.casePhase = casePhase;
	}

	public CCMSCaseStatus getCaseStatus()
	{
		return caseStatus;
	}

	public void setCaseStatus(CCMSCaseStatus caseStatus)
	{
		this.caseStatus = caseStatus;
	}

	public Date getCreateDate()
	{
		return caseFilingDate;
	}

	public void setCreateDate(Date createDate)
	{
		this.caseFilingDate = createDate;
	}

	public String getCaseNumber()
	{
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber)
	{
		this.caseNumber = caseNumber;
	}

	public CCMSCaseType getCaseType()
	{
		return caseType;
	}

	public void setCaseType(CCMSCaseType caseType)
	{
		this.caseType = caseType;
	}

	public String getCaseCaption()
	{
		return caseCaption;
	}

	public void setCaseCaption(String caseCaption)
	{
		this.caseCaption = caseCaption;
	}

	private String calculateCaseCaption(Connection conn) throws Exception
	{
		String caseCaption = "";
		List<CCMSCaseParty> caseParties = this.getCaseParties();

		if (this.caseType != null
				&& this.caseType.getName().startsWith("Criminal"))
		{
			caseCaption += "The State vs. ";
			String partyName = this.getFirstPartyName("Child", conn);
			caseCaption += partyName;
		}
		else if (this.caseType != null
				&& this.caseType.getName().startsWith("Application-Private"))
		{
			caseCaption += "In the matter of an Application in respect of (";
			caseCaption += this.getFirstPartyName("Child", conn) + ")";
		}
		else if (this.caseType != null
				&& this.caseType.getName().equals(
						"Application-Children Authority"))
		{
			caseCaption += "Children Authority vs. (";
			caseCaption += this.getFirstPartyName("Child", conn) + ")";
		}
		else if (this.caseType != null
				&& this.caseType.getName().equals("CHINS"))
		{
			caseCaption += "Parent/Guardian/Home vs ";
			String partyName = this.getFirstPartyName("Child", conn);
			caseCaption += partyName;
		}

		// Removed 20180323 because impossible to predict case party at point
		// when case caption is being called
		// Reinstated on 20180402 per client request but incorporated into
		// separate methos getFirstPartyName

		return caseCaption;
	}

	/**
	 * Grabs the first party where the case role matches the passed in string
	 * 
	 * @param partyType
	 * @return
	 */
	private String getFirstPartyName(String caseRole, Connection conn)
			throws Exception
	{
		// Need to retrieve the role object and full name for all matching
		// parties
		String partyName = "";
		for (CCMSCaseParty caseParty : caseParties)
		{
			CCMSCasePartyRole role = caseParty.getRole();
			role = (CCMSCasePartyRole) CCMSDomainUtilities.getInstance()
					.retrieveFullObject(role, this.court);
			if (role != null && role.getName().equals(caseRole))
			{
				// Grab full party
				CCMSParty dbParty = new CCMSParty(caseParty.getCaseParty()
						.getPartyOID(), this.court.getCourtOID());
				PreparedStatement pStmt = dbParty.genSelectSQL(conn).get(0);
				ResultSet rs = pStmt.executeQuery();
				while (rs.next())
				{
					dbParty = new CCMSParty(rs, conn, true);
				}
				rs.close();
				pStmt.close();

				partyName = dbParty.getFullName();
				break;
			}
		}
		return partyName;
	}

	public String calculateCaseNumber(Connection conn)
	{
		String caseNumber = "";
		int caseSequenceId = 0;
		int partySequenceId = 1;
		try
		{
			if (this.caseNumber.isEmpty())
			{
				// New case that is not being created as an associated case
				// Retrieve the next case sequence number from an ORIGINAL CASE
				PreparedStatement stmt = conn
						.prepareStatement("SELECT MAX(SEQ) FROM NJCCCMS.PRIMARY_CASE_NUMBER ");
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					caseSequenceId = rs.getInt(1);
					// Increment the case id by one for a new case
					caseSequenceId++;
				}
				stmt.close();
				rs.close();

				// COURT TYPE All ARE 'C' for Children
				caseNumber += "C" + "-";

				// Court Location
				caseNumber += this.getCourt().getLocationCode() + "-";

				// Case Type (i.e., CHINS, etc.)
				caseNumber += this.getCaseType().getShortName() + "-";

				// Case Sequence
				caseNumber += caseSequenceId + "-";

				// Year
				Calendar now = Calendar.getInstance(); // Gets the current date
														// and
														// time
				int year = now.get(Calendar.YEAR);
				caseNumber += year;

				// Case Suffix Identifier
				caseNumber += "-" + partySequenceId;
			}
			else
			{
				// Created as an associated case

				PreparedStatement pStmt = conn
						.prepareStatement("SELECT count(*) "
								+ "  FROM NJCCCMS.CASE "
								+ " WHERE CASE.COURT_ID = "
								+ this.court.getCourtOID()
								+ "   AND CASE.CASE_NUMBER like '"
								+ this.getCaseNumberBase() + "%' ");

				ResultSet rs2 = pStmt.executeQuery();
				while (rs2.next())
				{
					partySequenceId = rs2.getInt(1);
					partySequenceId++;
				}
				rs2.close();
				pStmt.close();

				caseNumber = this.getCaseNumberBase() + "-" + partySequenceId;
			}

		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return caseNumber;
	}

	public int getCaseWeight()
	{
		return caseWeight;
	}

	public void setCaseWeight(int caseWeight)
	{
		this.caseWeight = caseWeight;
	}

	public List<CCMSCase> getAssociatedCases(Connection conn) throws Exception
	{
		List<CCMSCase> associatedCases = new ArrayList<CCMSCase>();

		if (this.caseNumber.isEmpty())
		{
			// Retrieve the full case number if the case number is not available

			PreparedStatement qStatement = conn
					.prepareStatement("SELECT CASE_NUMBER FROM NJCCCMS.CASE "
							+ " WHERE CASE_ID = ? ");
			qStatement.setLong(1, this.caseOID);
			ResultSet rs2 = qStatement.executeQuery();
			while (rs2.next())
			{
				this.caseNumber = rs2.getString("CASE_NUMBER");
			}
			rs2.close();
			qStatement.close();
		}

		// find the last hyphen - the digits before this are the base case
		// number
		if (!this.caseCaption.isEmpty())
		{
			String baseCaseNumber = this.caseNumber.substring(0,
					this.caseNumber.lastIndexOf("-"));

			String sqlString = "SELECT * FROM NJCCCMS.CASE WHERE CASE_NUMBER like '"
					+ baseCaseNumber
					+ "%' "
					+ " AND COURT_ID = "
					+ this.court.getCourtOID();
			PreparedStatement stmt = conn.prepareStatement(sqlString);
			ResultSet rs = stmt.executeQuery();
			while (rs.next())
			{
				associatedCases.add(new CCMSCase(rs, conn, this.getCourt(),
						false));
			}
			stmt.close();
			rs.close();
		}

		return associatedCases;
	}

	public long getSearchCasePartyRoleOID()
	{
		return searchCasePartyRoleOID;
	}

	public void setSearchCasePartyRoleOID(long searchCasePartyRoleOID)
	{
		this.searchCasePartyRoleOID = searchCasePartyRoleOID;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		// Need to calculate the case caption - will be generated on initial
		// case creation but user can modify through UI.
		this.caseCaption = this.calculateCaseCaption(conn);

		if (!this.isAssociatedCase())
		{
			// Update the case numbering table, this table is used for tracking
			// the issuance of all unique court case numbers on the primary case
			PreparedStatement caseNbrStmt = conn
					.prepareStatement(" INSERT INTO NJCCCMS.PRIMARY_CASE_NUMBER (CREATE_DATETIME, ASSIGNED_CASE_NBR, COURT_ID ) "
							+ " values ( ?, ?, ? ) ");

			caseNbrStmt.setTimestamp(1,
					new Timestamp(System.currentTimeMillis()));
			caseNbrStmt.setString(2, this.caseNumber);
			caseNbrStmt.setLong(3, this.court.getCourtOID());
			stmts.add(caseNbrStmt);
		}

		// Insert into case
		PreparedStatement stmt = conn
				.prepareStatement(" INSERT INTO NJCCCMS.CASE (CASE_ID, COURT_ID, CASE_TYPE_ID, CASE_PHASE_ID, "
						+ " CASE_STATUS_ID, CASE_WEIGHT, CASE_CREATION_DATE, CASE_NUMBER, CASE_CAPTION) "
						+ " values ( ?, ?, ?, ?, ?, ?, ?, ?, ? ) ");

		stmt.setLong(1, this.caseOID);
		stmt.setLong(2, this.court.getCourtOID());
		stmt.setLong(3, this.caseType.getCaseTypeOID());
		stmt.setLong(4, this.casePhase.getCasePhaseOID());
		stmt.setLong(5, this.caseStatus.getStatusOID());
		stmt.setInt(6, this.caseWeight);
		stmt.setDate(7, new java.sql.Date(System.currentTimeMillis()));
		stmt.setString(8, this.caseNumber);
		stmt.setString(9, this.caseCaption);
		stmts.add(stmt);

		for (CCMSCaseParty caseParty : this.caseParties)
		{
			PreparedStatement partyStmt = conn
					.prepareStatement(" INSERT INTO NJCCCMS.CASE_PARTY_REL ( CASE_ID, PARTY_ID, ROLE_ID, "
							+ " COURT_ID, ANONYMIZATION_FLAG, START_DATE, END_DATE) values ( ?, ?, ?, ?, ?, ?, ?) ");
			partyStmt.setLong(1, this.getCaseOID());
			partyStmt.setLong(2, caseParty.getParty().getPartyOID());
			// Case Role
			partyStmt.setLong(3, caseParty.getRole().getCasePartyRoleOID());
			partyStmt.setLong(4, this.court.getCourtOID());
			// Anonymization Flag
			partyStmt.setInt(5, 0);
			if (caseParty.getStartDate() != null)
			{
				partyStmt.setDate(6, new java.sql.Date(caseParty.getStartDate()
						.getTime()));
			}
			else
			{
				partyStmt.setNull(6, java.sql.Types.DATE);
			}

			if (caseParty.getEndDate() != null)
			{
				partyStmt.setDate(7, new java.sql.Date(caseParty.getEndDate()
						.getTime()));
			}
			else
			{
				partyStmt.setNull(7, java.sql.Types.DATE);
			}
			stmts.add(partyStmt);
		}

		// Generate insert statements for case charges
		for (CCMSCaseCharge charge : this.caseCharges)
		{
			PreparedStatement pStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.CASE_CHARGE (ID, COURT_ID, CASE_ID, ICCS_CHARGE_ID, LEA_CHARGE_TEXT, LOCAL_CHARGE_ID ) "
							+ " values (?, ?, ?, ?, ?, ? ) ");
			pStmt.setLong(1, charge.getCaseChargeOID());
			pStmt.setLong(2, this.court.getCourtOID());
			pStmt.setLong(3, this.caseOID);
			pStmt.setLong(4, charge.getIccsChargeCategoryOID());
			pStmt.setString(5, charge.getLeaChargingDetails());

			if (charge.getLocalCharge() != null)
			{
				pStmt.setLong(6, charge.getLocalCharge().getLocalChargeOID());
			}
			else
			{
				pStmt.setLong(6, 0L);
			}
			stmts.add(pStmt);

			// Insert charge factors
			for (CCMSChargeFactor factor : charge.getChargeFactors())
			{
				PreparedStatement chgFactorStmt = conn
						.prepareStatement("INSERT INTO NJCCCMS.CHARGE_FACTOR_REL (CHARGE_ID, COURT_ID, CASE_ID, FACTOR_ID ) "
								+ " values (?, ?, ?, ? ) ");
				chgFactorStmt.setLong(1, charge.getCaseChargeOID());
				chgFactorStmt.setLong(2, charge.getCourtOID());
				chgFactorStmt.setLong(3, this.caseOID);
				chgFactorStmt.setLong(4, factor.getChargeFactorOID());

				stmts.add(chgFactorStmt);
			}
		}

		return stmts;

	}

	// Generally CMS shoudln't have the ability to erase a case
	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		return null;
	}

	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{

		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		// Remove Relationships which are automatically created with a case
		// update. Other elements associated
		// with a case such as documents, tasks and events are created
		// independently
		PreparedStatement stmtParties = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_PARTY_REL WHERE CASE_ID = ? AND COURT_ID = ?");
		stmtParties.setLong(1, this.caseOID);
		stmtParties.setLong(2, this.court.getCourtOID());
		stmts.add(stmtParties);

		// Remove case charges
		PreparedStatement stmtCharges = conn
				.prepareStatement("DELETE FROM NJCCCMS.CASE_CHARGE WHERE COURT_ID = ? "
						+ " AND CASE_ID = ? ");
		stmtCharges.setLong(1, this.court.getCourtOID());
		stmtCharges.setLong(2, this.caseOID);
		stmts.add(stmtCharges);

		// Remove case charge factors
		PreparedStatement stmtFactors = conn
				.prepareStatement("DELETE FROM NJCCCMS.CHARGE_FACTOR_REL WHERE COURT_ID = ? "
						+ " AND CASE_ID = ? ");
		stmtFactors.setLong(1, this.court.getCourtOID());
		stmtFactors.setLong(2, this.caseOID);
		stmts.add(stmtFactors);

		// Restore the relationships
		for (CCMSCaseParty caseParty : this.caseParties)
		{
			PreparedStatement partyStmt = conn
					.prepareStatement(" INSERT INTO NJCCCMS.CASE_PARTY_REL ( CASE_ID, PARTY_ID, ROLE_ID, "
							+ " COURT_ID, ANONYMIZATION_FLAG, START_DATE, END_DATE) values ( ?, ?, ?, ?, ?, ?, ?) ");
			partyStmt.setLong(1, this.getCaseOID());
			partyStmt.setLong(2, caseParty.getParty().getPartyOID());
			// Case Role
			if (caseParty.getRole() != null)
			{
				partyStmt.setLong(3, caseParty.getRole().getCasePartyRoleOID());
			}
			else
			{
				partyStmt.setNull(3, Types.INTEGER);
			}
			partyStmt.setLong(4, this.court.getCourtOID());
			// Anonymization Flag
			partyStmt.setInt(5, 0);
			partyStmt.setDate(6, new java.sql.Date(caseParty.getStartDate()
					.getTime()));

			if (caseParty.getEndDate() != null)
			{
				partyStmt.setDate(7, new java.sql.Date(caseParty.getEndDate()
						.getTime()));
			}
			else
			{
				partyStmt.setNull(7, java.sql.Types.DATE);
			}
			stmts.add(partyStmt);
		}

		for (CCMSCaseCharge charge : this.caseCharges)
		{
			PreparedStatement pStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.CASE_CHARGE (ID, COURT_ID, CASE_ID, ICCS_CHARGE_ID, LEA_CHARGE_TEXT, LOCAL_CHARGE_ID ) "
							+ " values (?, ?, ?, ?, ?, ? ) ");
			pStmt.setLong(1, charge.getCaseChargeOID());
			pStmt.setLong(2, this.court.getCourtOID());
			pStmt.setLong(3, this.caseOID);
			pStmt.setLong(4, charge.getIccsChargeCategoryOID());
			pStmt.setString(5, charge.getLeaChargingDetails());

			if (charge.getLocalCharge() != null)
			{
				pStmt.setLong(6, charge.getLocalCharge().getLocalChargeOID());
			}
			else
			{
				pStmt.setLong(6, 0L);
			}

			stmts.add(pStmt);

			// Insert charge factors
			for (CCMSChargeFactor factor : charge.getChargeFactors())
			{
				PreparedStatement chgFactorStmt = conn
						.prepareStatement("INSERT INTO NJCCCMS.CHARGE_FACTOR_REL (CHARGE_ID, COURT_ID, CASE_ID, FACTOR_ID ) "
								+ " values (?, ?, ?, ? ) ");
				chgFactorStmt.setLong(1, charge.getCaseChargeOID());
				chgFactorStmt.setLong(2, charge.getCourtOID());
				chgFactorStmt.setLong(3, this.caseOID);
				chgFactorStmt.setLong(4, factor.getChargeFactorOID());

				stmts.add(chgFactorStmt);
			}
		}

		// Generate Case Update Statement for those elements that can be updated
		// in a case

		PreparedStatement stmt = conn
				.prepareStatement(" UPDATE NJCCCMS.CASE SET CASE_TYPE_ID = ?, CASE_PHASE_ID = ?, "
						+ " CASE_STATUS_ID = ?, CASE_WEIGHT = ?, CASE_CAPTION = ? WHERE CASE_ID = ? AND COURT_ID = ? ");

		stmt.setLong(1, this.caseType.getCaseTypeOID());
		stmt.setLong(2, this.casePhase.getCasePhaseOID());
		stmt.setLong(3, this.caseStatus.getStatusOID());
		stmt.setInt(4, this.caseWeight);
		stmt.setString(5, this.caseCaption);
		stmt.setLong(6, this.caseOID);
		stmt.setLong(7, this.court.getCourtOID());
		stmts.add(stmt);

		return stmts;
	}

	public String getCaseNumberBase()
	{
		int lastHyphenIndex = this.caseNumber.lastIndexOf("-");
		return this.caseNumber.substring(0, lastHyphenIndex);
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<CCMSPreparedStatementParameter> parms = new ArrayList<CCMSPreparedStatementParameter>();
		List<PreparedStatement> pStmts = new ArrayList<PreparedStatement>();

		String sqlString = "SELECT * FROM NJCCCMS.CASE WHERE COURT_ID = "
				+ this.court.getCourtOID();

		if (!this.caseNumber.isEmpty())
		{
			sqlString += " AND CASE_NUMBER like ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, "%"
							+ this.caseNumber + "%"));
		}

		if (this.caseOID != 0L)
		{
			sqlString += " AND CASE_ID = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.LONG_TYPE, this.caseOID));
		}

		if (this.searchCasePartyName != null
				&& !this.searchCasePartyName.isEmpty())
		{
			searchCasePartyName = searchCasePartyName.replace("'", "");
			sqlString += " AND CASE_ID IN ( "
					+ "SELECT CASE_PARTY_REL.CASE_ID FROM NJCCCMS.CASE_PARTY_REL, NJCCCMS.PARTY "
					+ " WHERE CASE_PARTY_REL.PARTY_ID = PARTY.ID "
					+ "   AND CASE_PARTY_REL.COURT_ID = "
					+ this.court.getCourtOID();

			if (this.searchCasePartyRoleOID != 0L)
			{
				sqlString += " AND CASE_PARTY_REL.ROLE_ID = ?";
				parms.add(new CCMSPreparedStatementParameter(
						CCMSPreparedStatementParameter.LONG_TYPE,
						this.searchCasePartyRoleOID));
			}

			sqlString += " AND ( FIRST_NAME LIKE ?  OR LAST_NAME LIKE ?  )) ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, "%"
							+ this.searchCasePartyName + "%"));
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, "%"
							+ this.searchCasePartyName + "%"));

		}

		if (this.caseType != null)
		{
			sqlString += " AND CASE_TYPE_ID = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.LONG_TYPE, caseType
							.getCaseTypeOID()));
		}

		if (this.casePhase != null)
		{
			sqlString += " AND CASE_PHASE_ID = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.LONG_TYPE, this.casePhase
							.getCasePhaseOID()));
		}

		if (this.caseStatus != null)
		{
			sqlString += " AND 	CASE_STATUS_ID = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.LONG_TYPE, caseStatus
							.getStatusOID()));
		}

		PreparedStatement stmt = conn.prepareStatement(sqlString);

		for (int i = 0; i < parms.size(); i++)
		{
			CCMSPreparedStatementParameter parm = parms.get(i);
			parm.replaceParameterValues(stmt, i + 1);
		}

		logger.debug(stmt.toString());

		pStmts.add(stmt);

		return pStmts;
	}

	public CCMSCourt getCourt()
	{
		return court;
	}

	public void setCourt(CCMSCourt court)
	{
		this.court = court;
	}

	public String getSearchCasePartyName()
	{
		return searchCasePartyName;
	}

	public void setSearchCasePartyName(String searchCasePartyName)
	{
		this.searchCasePartyName = searchCasePartyName;
	}

	public List<CCMSCaseCharge> getCaseCharges()
	{
		return caseCharges;
	}

	public List<CCMSCaseTask> getCaseTasks()
	{
		return caseTasks;
	}

	public List<CCMSCaseDocument> getCaseDocs()
	{
		return caseDocs;
	}

	public List<CCMSCaseHearing> getCaseHearings()
	{
		return caseHearings;
	}

	public List<CCMSCaseJudicialAssignment> getJudicialAssignments()
	{
		return judicialAssignments;
	}

	public void setJudicialAssignments(
			List<CCMSCaseJudicialAssignment> judicialAssignments)
	{
		this.judicialAssignments = judicialAssignments;
	}

	public List<CCMSCaseEvent> getCaseEvents()
	{
		return caseEvents;
	}

	public void setCaseEvents(List<CCMSCaseEvent> caseEvents)
	{
		this.caseEvents = caseEvents;
	}

	public Date getCaseFilingDate()
	{
		return caseFilingDate;
	}

	public void setCaseFilingDate(Date caseFilingDate)
	{
		this.caseFilingDate = caseFilingDate;
	}

	public void setCaseHearings(List<CCMSCaseHearing> caseHearings)
	{
		this.caseHearings = caseHearings;
	}

	public String getCaseChangeDescription(CCMSCase originalCase,
			Connection conn) throws Exception
	{
		String description = "";
		// Determine and log changes

		if (originalCase.getCaseCaption() != null
				&& !originalCase.getCaseCaption().equals(this.getCaseCaption()))
		{
			description += "Case Caption Changed from ["
					+ originalCase.getCaseCaption() + "] to ["
					+ this.getCaseCaption() + "]. ";
		}

		if (!originalCase.getCaseStatus().equals(this.getCaseStatus()))
		{
			description += "Case Status Changed from ["
					+ originalCase.getCaseStatus().getName() + "] to ["
					+ this.getCaseStatus().getName() + "]. ";
		}

		if (!originalCase.getCasePhase().equals(this.getCasePhase()))
		{
			description += "Case Phase Changed from ["
					+ originalCase.getCasePhase().getName() + "] to ["
					+ this.getCasePhase().getName() + "]. ";
		}

		if (originalCase.getCaseWeight() != this.getCaseWeight())
		{
			description += "Case Weight Changed from ["
					+ originalCase.getCaseWeight() + "] to ["
					+ this.getCaseWeight() + "]. ";
		}

		if (originalCase.getCaseType() != this.getCaseType())
		{
			description += "Case Type Changed From ["
					+ originalCase.getCaseType().getName() + "] to ["
					+ this.getCaseType().getName() + "]. ";
		}

		// Case parties should never be able to be removed, only an end date
		// added
		for (CCMSCaseParty uptdCaseParty : this.getCaseParties())
		{
			if (originalCase.getCaseParties().contains(uptdCaseParty))
			{
				// Determine changes in the role or start/end date
				CCMSCaseParty origParty = originalCase.getCaseParties().get(
						originalCase.getCaseParties().indexOf(uptdCaseParty));
				if (origParty.getRole() != null
						&& origParty.getRole().getCasePartyRoleOID() != uptdCaseParty
								.getRole().getCasePartyRoleOID())
				{
					description += "Case Party Role for "
							+ origParty.getCaseParty().getFullName()
							+ " Changed From " + origParty.getRole().getName()
							+ " to " + uptdCaseParty.getRole().getName() + ". ";
				}
				if (origParty.getStartDate() != null
						&& !origParty.getStartDate().equals(
								uptdCaseParty.getStartDate()))
				{
					description += "Case Party Start Date for "
							+ origParty.getCaseParty().getFullName()
							+ " Changed From " + origParty.getStartDate()
							+ " to " + uptdCaseParty.getStartDate() + ". ";
				}

				if (origParty.getEndDate() == null
						&& uptdCaseParty.getEndDate() != null)
				{
					description += " Case Party End Date for "
							+ origParty.getCaseParty().getFullName()
							+ " Added: " + uptdCaseParty.getEndDate() + ". ";
				}
				else if (origParty.getEndDate() != null
						&& uptdCaseParty.getEndDate() != null
						&& !origParty.getEndDate().equals(
								uptdCaseParty.getEndDate()))
				{
					description += " Case Party End Date for "
							+ origParty.getCaseParty().getFullName()
							+ " Changed from " + origParty.getEndDate()
							+ " to " + uptdCaseParty.getEndDate();
				}

			}
			else if (!originalCase.getCaseParties().contains(uptdCaseParty))
			{
				PreparedStatement stmt = uptdCaseParty.getCaseParty()
						.genSelectSQL(conn).get(0);
				ResultSet rs = stmt.executeQuery();
				while (rs.next())
				{
					uptdCaseParty.setCaseParty(new CCMSParty(rs, conn, true));
				}

				// Grab the full role
				CCMSCasePartyRole fullRole = (CCMSCasePartyRole) CCMSDomainUtilities
						.getInstance().retrieveFullObject(
								uptdCaseParty.getRole(), court);
				uptdCaseParty.setRole(fullRole);

				description += " Case Party ["
						+ uptdCaseParty.getParty().getFullName()
						+ "] added as [" + uptdCaseParty.getRole().getName()
						+ "] ";
			}
		}

		for (CCMSCaseCharge charge : this.getCaseCharges())
		{
			if (!originalCase.getCaseCharges().contains(charge))
			{
				description += " Added Charge: " + charge.toString();
			}
		}

		return description;
	}

	public boolean isAssociatedCase()
	{
		return associatedCase;
	}

	public void setAssociatedCase(boolean associatedCase)
	{
		this.associatedCase = associatedCase;
	}

	public String toString()
	{
		String objString = "Caption: " + this.caseCaption + " Case Number: "
				+ this.caseNumber + " CaseOID: " + this.caseOID
				+ " Case Weight: " + this.caseWeight + " Case Phase: "
				+ this.casePhase + " Case Status: " + this.caseStatus
				+ " Case Type: " + this.caseType;
		return objString;
	}

}
