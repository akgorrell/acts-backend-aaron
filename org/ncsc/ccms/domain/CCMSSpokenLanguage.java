package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CCMSSpokenLanguage extends CCMSDomain
{
	private long spokenLanguageOID;
	private long partyOID;
	private String languageName;
	
	public CCMSSpokenLanguage( long spokenLanguageOID )
	{
		this.spokenLanguageOID = spokenLanguageOID;
	}
	
	public CCMSSpokenLanguage( ResultSet rs, Connection conn ) throws Exception
	{
		this.spokenLanguageOID = rs.getLong("ID");
		this.partyOID = rs.getLong("PARTY_ID");
		this.languageName = rs.getString("LANGUAGE_NAME");
	}
	

	public long getSpokenLanguageOID()
	{
		return spokenLanguageOID;
	}

	public void setSpokenLanguageOID(long spokenLanguageOID)
	{
		this.spokenLanguageOID = spokenLanguageOID;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getLanguageName()
	{
		return languageName;
	}

	public void setLanguageName(String languageName)
	{
		this.languageName = languageName;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{

		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.PARTY_LANGUAGE " +
				" ( ID, PARTY_ID, LANGUAGE_NAME ) " + 
				" VALUES ( ?, ?, ? ) ");
		stmt.setLong(1, this.spokenLanguageOID);
		stmt.setLong(2, this.partyOID);
		stmt.setString(3, this.languageName);
		
		stmts.add(stmt);
		
		return stmts;
		
	}
}
