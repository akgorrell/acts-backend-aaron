package org.ncsc.ccms.domain;

import java.sql.PreparedStatement;
import java.util.Date;

public class CCMSPreparedStatementParameter
{
	private String parameterType;
	private Object parameterValue;

	public static String STRING_TYPE = "String";
	public static String DATE_TYPE = "Date";
	public static String LONG_TYPE = "Long";

	public CCMSPreparedStatementParameter(String parameterType,
			Object parameterValue)
	{
		this.parameterType = parameterType;
		this.parameterValue = parameterValue;
	}

	public String getParameterType()
	{
		return parameterType;
	}

	public void setParameterType(String parameterType)
	{
		this.parameterType = parameterType;
	}

	public Object getParameterValue()
	{
		return parameterValue;
	}

	public void setParameterValue(Object parameterValue)
	{
		this.parameterValue = parameterValue;
	}

	public void replaceParameterValues(PreparedStatement stmt,
			int parameterIndex) throws Exception
	{
		if (this.parameterType
				.equals(CCMSPreparedStatementParameter.STRING_TYPE))
		{
			stmt.setString(parameterIndex, (String) this.parameterValue);
		}
		else if (this.parameterType
				.equals(CCMSPreparedStatementParameter.LONG_TYPE))
		{
			stmt.setLong(parameterIndex, ((Long) parameterValue).longValue());
		}
		else if (this.parameterType
				.equals(CCMSPreparedStatementParameter.DATE_TYPE))
		{
			java.sql.Date sqlDate = new java.sql.Date(
					((Date) parameterValue).getTime());
			stmt.setDate(parameterIndex, sqlDate);
		}
	}
}
