package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CCMSPartyIdentifier extends CCMSDomain
{
	private long partyIdentifierOID;
	private long partyOID;
	private String identifierType;
	private String identifierValue;
	private String notes = "";
	private Date startDate = null;
	private Date endDate = null;
	private CCMSPersonIdentificationType identificationType;

	public CCMSPartyIdentifier(long partyIdentifierOID)
	{
		this.partyIdentifierOID = partyIdentifierOID;
	}

	public CCMSPartyIdentifier(ResultSet rs, Connection conn) throws Exception
	{
		this.partyIdentifierOID = rs.getLong("ID");
		this.partyOID = rs.getLong("PARTY_ID");
		this.identifierType = rs.getString("IDENTIFIER_TYPE");
		this.identifierValue = rs.getString("IDENTIFIER_VALUE");
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");
		this.notes = rs.getString("NOTES");
	}

	public long getPartyIdentifierOID()
	{
		return partyIdentifierOID;
	}

	public void setPartyIdentifierOID(long partyIdentifierOID)
	{
		this.partyIdentifierOID = partyIdentifierOID;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getIdentifierType()
	{
		return identifierType;
	}

	public void setIdentifierType(String identifierType)
	{
		this.identifierType = identifierType;
	}

	public String getIdentifierValue()
	{
		return identifierValue;
	}

	public void setIdentifierValue(String identifierValue)
	{
		this.identifierValue = identifierValue;
	}

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public String getNotes()
	{
		return notes;
	}

	// additional information about an identifier
	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public CCMSPersonIdentificationType getIdentificationType()
	{
		return identificationType;
	}

	public void setIdentificationType(
			CCMSPersonIdentificationType identificationType)
	{
		this.identificationType = identificationType;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.PARTY_IDENTIFIER "
						+ " ( ID, PARTY_ID, IDENTIFIER_TYPE, IDENTIFIER_VALUE, START_DATE, END_DATE, NOTES ) "
						+ " VALUES ( ?, ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.partyIdentifierOID);
		stmt.setLong(2, this.partyOID);
		stmt.setString(3, this.identifierType);
		stmt.setString(4, this.identifierValue);

		if (this.startDate != null)
		{
			stmt.setDate(5, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			stmt.setNull(5, java.sql.Types.DATE);
		}

		if (this.endDate != null)
		{
			stmt.setDate(6, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}

		stmt.setString(7, this.notes);

		stmts.add(stmt);

		return stmts;
	}

}
