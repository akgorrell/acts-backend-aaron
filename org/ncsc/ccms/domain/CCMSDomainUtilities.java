package org.ncsc.ccms.domain;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.fileupload.FileItem;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.openkm.sdk4j.OKMWebservices;
import com.openkm.sdk4j.OKMWebservicesFactory;
import com.openkm.sdk4j.bean.AppVersion;
import com.sendgrid.SendGrid;
import com.mchange.v2.c3p0.ComboPooledDataSource;

public class CCMSDomainUtilities
{
	// Max number for a JSON parser is 2^53
	public static long MAX_JSON_NBR = 9007199254740992L;
	private static String LOGIN_TOKEN_SALT = "KMEztKmGFFZB9rbP";
	private static String RESET_PWD_SALT = "5@vf64dN6XN7=BCC";
	private static String RESET_PWD_ISSUER = "CCMS_RESET";
	private Random gen = new Random(System.currentTimeMillis());
	private Connection conn = null;
	private OKMWebservices okm = null;
	private String openKM_VERSION = null;
	Properties properties = new Properties();
	public static final int MAX_LOG_FILE_SIZE = 1048576;
	// private String issuer = "NCSC";
	private String subject = "CCMS3";
	private static long SESSION_TIMEOUT_MIN = 1440;
	public static Logger logger = null;
	private static CCMSDomainUtilities _instance;
	private static long RESET_TIMEOUT_MIN = 5;

	private List<CourtLookupTable> lookupTables = new ArrayList<CourtLookupTable>();

	// Case Event Types
	public static String DOC_GEN_EVENT_NAME = "DOCG";
	public static String TASK_COMPLETED = "TASK";
	public static String CASE_CREATED_EVENT = "CRTD";

	public static void main(String[] args)
	{
		try
		{
			// CCMSDomainUtilities utils = CCMSDomainUtilities.getInstance();
			// System.out.println(CCMSDomainUtilities.generateHash("cherbert"));

			CCMSDomainUtilities utils = CCMSDomainUtilities.getInstance();

			for (int i = 0; i < 99; i++)
			{
				System.out.println(utils.genOID());
			}

		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static CCMSDomainUtilities getInstance()
	{
		if (_instance == null)
		{
			_instance = new CCMSDomainUtilities();
		}
		return _instance;
	}

	private CCMSDomainUtilities()
	{
		CCMSDomainUtilities.logger = LogManager
				.getLogger(CCMSDomainUtilities.class.getName());

		try
		{
			InputStream input = new FileInputStream(
					"/var/CCMS3/Config.properties");
			// Load properties
			properties.load(input);

			// Load lookup tables for all courts into memory
			Connection conn = this.connect();
			PreparedStatement pStmt = conn
					.prepareStatement("SELECT distinct COURT_ID FROM NJCCCMS.COURTS ORDER BY COURT_ID");
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				long courtOID = rs.getLong("COURT_ID");
				logger.info("Loading lookups for courtOID: " + courtOID);
				CourtLookupTable lookupTable = new CourtLookupTable(courtOID,
						conn);
				Thread t = new Thread(lookupTable);
				t.start();
				this.lookupTables.add(lookupTable);
			}

		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

	public CCMSCourt getSessionCourtObject(HttpSession session, String token)
			throws Exception
	{
		CCMSCourt court = (CCMSCourt) session.getAttribute("court");

		// Need to check the token to make sure it is valid.
		if (court != null && token != null && token.length() > 15)
		{
			CCMSParty tokenParty = CCMSDomainUtilities.getInstance()
					.parseLoginToken(token);
			logger.debug("Fetching court session object for user: "
					+ tokenParty.getUserName());
		}

		/**
		 * If there is no court object in the session, it is likely because we
		 * are not entering the service through the front-end authentication
		 * (i.e., through testing tool such as Postman). In this situation, a
		 * token is passed to authenticate the user and the court is hard coded
		 */
		/*
		 * if (court == null) {
		 * 
		 * court = new CCMSCourt(5);
		 * 
		 * }
		 */

		return court;
	}

	public static boolean isNumber(String nbr)
	{
		boolean flag = false;
		if (nbr != null && nbr.length() > 0)
		{
			try
			{
				Double.parseDouble(nbr);
				flag = true;
			}
			catch (NumberFormatException e)
			{
				flag = false;
			}
		}
		return flag;
	}

	public static boolean intToBool(int value)
	{
		boolean flag = false;
		if (value == 1)
		{
			flag = true;
		}

		return flag;
	}

	public static int boolToInt(boolean flag)
	{
		int value = 0;
		if (flag)
		{
			value = 1;
		}

		return value;
	}

	public long genOID()
	{
		long generatedOID = Math.abs(gen.nextLong()) / 10000;
		while (generatedOID > CCMSDomainUtilities.MAX_JSON_NBR)
		{
			System.out.println("regen");
			generatedOID = (long) Math.abs(gen.nextLong()) / 10000;
		}
		return generatedOID;
	}

	public long getGenOID()
	{
		return this.genOID();
	}

	public Connection connect() throws Exception
	{
		if (this.conn == null || this.conn.isClosed())
		{
			try
			{
				String dbDriver = properties.getProperty("DB_DRIVER");
				String dbName = properties.getProperty("DB_NAME");
				Class.forName(dbDriver).newInstance();
				this.conn = DriverManager.getConnection(dbName,
						properties.getProperty("DB_USER"),
						properties.getProperty("DB_PASS"));

				if (!this.conn.isClosed())
				{
					logger.info("Successfully connected to: " + dbName);
				}
				this.conn.setAutoCommit(false);

			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
		}

		return this.conn;
	}

	public OKMWebservices connOpenKM() throws Exception
	{
		if (this.okm == null)
		{
			try
			{
				String openkm_PASS = properties.getProperty("OPENKM_PASS");
				String openkm_USER = properties.getProperty("OPENKM_USER");
				String openkm_URL = properties.getProperty("OPENKM_URL");
				// open connections to openKM and make sure it really is there
				// (getAppVersion)
				this.okm = OKMWebservicesFactory.newInstance(openkm_URL,
						openkm_USER, openkm_PASS);
				this.openKM_VERSION = this.okm.getAppVersion().toString();
				if (this.openKM_VERSION != null)
				{
					// logger.info("Successfully connected to "
					// + "openKM, version:" + this.openKM_VERSION +
					// "using TCP/IP...");
				}
			}
			catch (Exception e)
			{
				e.printStackTrace();
				throw e;
			}
		}
		return this.okm;
	}

	public void disconnect() throws Exception
	{
		if (conn != null)
		{
			this.conn.close();
			conn = null;
		}
	}

	public static String getFileName(FileItem file)
	{
		String fileName = file.getName();
		fileName = fileName.replaceAll(" ", "_");

		return fileName;
	}

	public static File processUploadedFile(FileItem item, ServletContext context)
			throws Exception
	{
		String fieldName = item.getFieldName();
		String fileName = CCMSDomainUtilities.getInstance().genOID() + "_"
				+ getFileName(item);
		String contentType = item.getContentType();
		boolean isInMemory = item.isInMemory();
		long sizeInBytes = item.getSize();

		String fileDir = context.getRealPath("/");
		File uploadedFile = new File("", fileName);
		item.write(uploadedFile);
		return uploadedFile;
	}

	public Properties getProperties()
	{
		return properties;
	}

	public static final void copyInputStream(InputStream in, OutputStream out)
			throws IOException
	{
		byte[] buffer = new byte[1024];
		int len;

		while ((len = in.read(buffer)) >= 0)
			out.write(buffer, 0, len);

		in.close();
		out.close();
	}

	/**
	 * Recursively lists all files in directory and sub-directories
	 * 
	 * @param dir
	 * @return
	 */
	public static List<File> listFilesInDirectory(File dir)
	{
		File[] files = null;
		files = dir.listFiles();

		List<File> allFiles = new ArrayList<File>();
		if (files != null)
		{
			for (File f : files)
			{
				if (f.isDirectory())
				{
					allFiles.addAll(CCMSDomainUtilities.listFilesInDirectory(f));
				}
				else
				{
					allFiles.add(f);
				}
			}
		}
		return allFiles;
	}

	static public void unzipFile(String inputFile, String outputDirName)
			throws Exception
	{
		ZipFile zipFile = new ZipFile(inputFile);

		// Create the output directory
		File outputDir = new File(outputDirName);
		if (!outputDir.exists())
		{
			outputDir.mkdirs();
		}

		Enumeration entries = zipFile.entries();

		while (entries.hasMoreElements())
		{
			ZipEntry entry = (ZipEntry) entries.nextElement();

			if (entry.isDirectory())
			{
				// Assume directories are stored parents first then children.
				// DomainUtilities.getInstance().getLogger().info("Extracting directory: "
				// + entry.getName());
				// This is not robust, just for demonstration purposes.
				File directory = new File(outputDir, entry.getName());
				directory.mkdirs();
				continue;
			}
			else if (entry.getName().endsWith("xml"))
			{
				// DomainUtilities.getInstance().getLogger().info("Extracting file: "
				// + entry.getName());
				File unzippedFile = new File(outputDir, entry.getName());
				unzippedFile.getParentFile().mkdirs();
				FileOutputStream fileOutputStream = new FileOutputStream(
						unzippedFile);
				CCMSDomainUtilities.copyInputStream(
						zipFile.getInputStream(entry),
						new BufferedOutputStream(fileOutputStream));
				fileOutputStream.close();
			}
		}

		zipFile.close();

	}

	public List<CourtLookupTable> getLookupTables()
	{
		return lookupTables;
	}

	public void setLookupTables(List<CourtLookupTable> lookupTables)
	{
		this.lookupTables = lookupTables;
	}

	public static String removeSpecialCharacters(String originalString)
	{
		originalString = originalString.replace("\"", "'");
		return originalString;
	}

	public static String sendGetRequest(String url, String parameterString)
			throws IOException
	{

		String address;
		if (parameterString.length() != 0)
		{
			address = url + "?" + parameterString;
		}
		else
		{
			address = url;
		}

		return makeRequest(address, null);
	}

	public static String makeRequest(String address, String content)
			throws IOException
	{

		StringBuilder sb = new StringBuilder();
		URL url = new URL(address);

		HttpURLConnection conn;
		conn = (HttpURLConnection) url.openConnection();
		conn.setAllowUserInteraction(true);
		conn.setDoOutput(true);

		if (content != null)
		{
			OutputStream osw = conn.getOutputStream();
			osw.write(content.getBytes());
		}
		BufferedReader respContent = new BufferedReader(new InputStreamReader(
				conn.getInputStream()));
		String respLine;
		while ((respLine = respContent.readLine()) != null)
		{
			sb.append(respLine).append("\n");
		}
		respContent.close();
		conn.disconnect();
		return sb.toString();
	}

	public CCMSDomain retrieveFullObject(CCMSDomain dom, CCMSCourt qCourt)
			throws Exception
	{
		CCMSDomain fullObj = null;

		// Need to retrieve the applicable CourtLookup
		try
		{
			CourtLookupTable qLookup = new CourtLookupTable(
					qCourt.getCourtOID());

			if (this.lookupTables.contains(qLookup))
			{
				qLookup = this.lookupTables.get(this.lookupTables
						.indexOf(qLookup));
			}
			else
			{
				logger.error("ERROR ON LOAD OF LOOKUP TABLE");
			}

			if (dom instanceof CCMSCasePhase)
			{
				int index = qLookup.getCasePhases().indexOf(dom);
				fullObj = qLookup.getCasePhases().get(index);
			}
			else if (dom instanceof CCMSCaseStatus)
			{
				int index = qLookup.getCaseStatuses().indexOf(dom);
				fullObj = qLookup.getCaseStatuses().get(index);
			}
			else if (dom instanceof CCMSCaseType)
			{
				int index = qLookup.getCaseTypes().indexOf(dom);
				fullObj = qLookup.getCaseTypes().get(index);
			}
			else if (dom instanceof CCMSCasePartyRole)
			{
				int index = qLookup.getCasePartyRoles().indexOf(dom);
				fullObj = qLookup.getCasePartyRoles().get(index);
			}
			else if (dom instanceof CCMSICCSCode)
			{
				int index = qLookup.getIccsCodes().indexOf(dom);
				fullObj = qLookup.getIccsCodes().get(index);
			}
			else if (dom instanceof CCMSEventType)
			{
				int index = qLookup.getEventTypes().indexOf(dom);
				fullObj = qLookup.getEventTypes().get(index);
			}
			else if (dom instanceof CCMSHearingType)
			{
				int index = qLookup.getHearingTypes().indexOf(dom);
				fullObj = qLookup.getHearingTypes().get(index);
			}
			else if (dom instanceof CCMSChargeFactor)
			{
				int index = qLookup.getChargeFactors().indexOf(dom);
				fullObj = qLookup.getChargeFactors().get(index);
			}
			else if (dom instanceof CCMSCourtLocation)
			{
				int index = qLookup.getCourtLocations().indexOf(dom);
				fullObj = qLookup.getCourtLocations().get(index);
			}
			else if (dom instanceof CCMSStaffRole)
			{
				int index = qLookup.getChargeFactors().indexOf(dom);
				fullObj = qLookup.getChargeFactors().get(index);
			}
			else if (dom instanceof CCMSTaskType)
			{
				int index = qLookup.getCaseTaskTypes().indexOf(dom);
				fullObj = qLookup.getCaseTaskTypes().get(index);
			}
			else if (dom instanceof CCMSPersonIdentificationType)
			{
				int index = qLookup.getPersonIDTypes().indexOf(dom);
				fullObj = qLookup.getPersonIDTypes().get(index);
			}
			else if (dom instanceof CCMSPermission)
			{
				int index = qLookup.getPermissions().indexOf(dom);
				fullObj = qLookup.getPermissions().get(index);
			}
		}
		catch (Exception ex)
		{
			logger.error("Error retrieving lookup value for: "
					+ dom.getClass().getName() + "-" + dom.toString());
		}

		return fullObj;
	}

	public CourtLookupTable getLookupTableForCourt(CCMSCourt court)
			throws Exception
	{
		CourtLookupTable qLookup = new CourtLookupTable(court.getCourtOID());
		if (this.lookupTables.contains(qLookup))
		{
			qLookup = lookupTables.get(lookupTables.indexOf(qLookup));
		}
		return qLookup;
	}

	public List<CCMSCasePhase> getPhaseByType(long caseTypeOID, CCMSCourt court)
			throws Exception
	{
		List<CCMSCasePhase> casePhases = new ArrayList<CCMSCasePhase>();
		CourtLookupTable lookupTable = this.getLookupTableForCourt(court);
		for (CCMSCasePhase phase : lookupTable.getCasePhases())
		{
			if (phase.getCaseTypeOID() == caseTypeOID)
			{
				casePhases.add(phase);
			}
		}
		return casePhases;
	}

	public String generateHash(String passwd)
	{
		StringBuilder hash = new StringBuilder();
		passwd = LOGIN_TOKEN_SALT + passwd;
		try
		{
			MessageDigest sha = MessageDigest.getInstance("SHA-1");
			byte[] hashedBytes = sha.digest(passwd.getBytes());
			char[] digits =
			{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c',
					'd', 'e', 'f' };
			for (int idx = 0; idx < hashedBytes.length; ++idx)
			{
				byte b = hashedBytes[idx];
				hash.append(digits[(b & 0xf0) >> 4]);
				hash.append(digits[b & 0x0f]);
			}
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
		}

		return hash.toString();
	}

	// Sample method to construct a JWT
	// issuerOID is the OID of the court
	public String createLoginToken(String userName, String issuerOID)
	{
		long ttlMillis = SESSION_TIMEOUT_MIN * 1000 * 60;
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter
				.parseBase64Binary(LOGIN_TOKEN_SALT);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes,
				signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(userName).setIssuedAt(now)
				.setSubject(subject).setIssuer(issuerOID)
				.signWith(signatureAlgorithm, signingKey);

		// if it has been specified, let's add the expiration
		if (ttlMillis >= 0)
		{
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public CCMSParty parseLoginToken(String jwt) throws Exception
	{
		if (jwt.startsWith("Bearer"))
		{

			jwt = jwt.substring("Bearer".length());
			jwt.trim();
		}

		CCMSParty user = null;
		// This line will throw an exception if it is not a signed JWS (as
		// expected)
		Claims claims = Jwts
				.parser()
				.setSigningKey(
						DatatypeConverter.parseBase64Binary(LOGIN_TOKEN_SALT))
				.parseClaimsJws(jwt).getBody();

		user = new CCMSParty();
		user.setUserName(claims.getId());

		return user;
	}

	public String createResetToken(String userName)
	{
		long ttlMillis = RESET_TIMEOUT_MIN * 1000 * 60;
		// The JWT signature algorithm we will be using to sign the token
		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		// We will sign our JWT with our ApiKey secret
		byte[] apiKeySecretBytes = DatatypeConverter
				.parseBase64Binary(this.RESET_PWD_SALT);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes,
				signatureAlgorithm.getJcaName());

		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder().setId(userName).setIssuedAt(now)
				.setSubject(subject).setIssuer(this.RESET_PWD_ISSUER)
				.signWith(signatureAlgorithm, signingKey);

		// if it has been specified, let's add the expiration
		if (ttlMillis >= 0)
		{
			long expMillis = nowMillis + ttlMillis;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		// Builds the JWT and serializes it to a compact, URL-safe string
		return builder.compact();
	}

	public CCMSParty parseResetToken(String jwt) throws Exception
	{
		if (jwt.startsWith("Bearer"))
		{

			jwt = jwt.substring("Bearer".length());
			jwt.trim();
		}

		CCMSParty user = null;
		// This line will throw an exception if it is not a signed JWS (as
		// expected)
		Claims claims = Jwts
				.parser()
				.setSigningKey(
						DatatypeConverter
								.parseBase64Binary(this.RESET_PWD_SALT))
				.parseClaimsJws(jwt).getBody();

		if (claims != null)
		{
			user = new CCMSParty();
			user.setUserName(claims.getId());
		}

		return user;
	}

	/**
	 * This method is necessary because of the different ways ORACLE and MySQL
	 * implement like. ORACLE like Comparisons are NOT case sensitive unless you
	 * use the REGEXP_LIKE function
	 * 
	 * @param columnName
	 * @param likeValue
	 * @return
	 */
	public String genSQLLikeQuery(String columnName, String likeValue)
	{
		String isOracleDB = properties.getProperty("IS_ORACLE");

		String sqlString = "";

		if (isOracleDB.equals("Y"))
		{
			sqlString += " REGEXP_LIKE( " + columnName + ", '" + likeValue
					+ "', 'i') ";
		}
		else
		{
			sqlString += " " + columnName + " LIKE '%" + likeValue + "%' ";
		}

		return sqlString;
	}

	public String genSQLTimeQuery(String columnName, String timeValue)
	{
		String isOracleDB = properties.getProperty("IS_ORACLE");

		String sqlString = "";

		if (isOracleDB.equals("Y"))
		{
			sqlString += "TRUNC( " + columnName + ") = TO_DATE(" + timeValue
					+ ", 'YYYY-MM-DD')";
		}
		else
		{
			sqlString += " date( " + columnName + ") = '" + timeValue + "' ";
		}

		return sqlString;
	}

	public String fetchJSONString(BufferedReader reader)
	{
		// Code to reach the JSON message from the client
		StringBuffer clientJSON = new StringBuffer();
		String line = null;
		try
		{
			while ((line = reader.readLine()) != null)
			{
				clientJSON.append(line);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

		return clientJSON.toString();
	}

	public static String generateJSONNotifyMessage(String message)
	{
		return new String("[{notify:\"" + message + "\"}]");
	}

	/**
	 * 
	 * @param dateString
	 *            : Presumes format of YYYY-MM-DD
	 * @return
	 */
	public Date convertStringToDate(String dateString) throws Exception
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = formatter.parse(dateString);
		return date;
	}

	public static Timestamp convertStringToTimestamp(String dateString)
			throws Exception
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = formatter.parse(dateString);
		Timestamp tmsp = new Timestamp(date.getTime());
		return tmsp;
	}

	public String convertDateToString(Date myDate) throws Exception
	{
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.format(myDate);
	}

	public static String getDateStamp()
	{
		Date now = new Date(System.currentTimeMillis());
		String dateStamp = new SimpleDateFormat("yyyyMMdd").format(now);
		return dateStamp;
	}

	public static boolean hasColumn(ResultSet rs, String columnName)
			throws SQLException
	{
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		for (int x = 1; x <= columns; x++)
		{
			if (columnName.equals(rsmd.getColumnName(x))) { return true; }
		}
		return false;
	}

	/**
	 * Created a register of action entry
	 * 
	 * @param document
	 *            : Optional
	 * @param ccmsCase
	 *            : Required
	 * @param eventType
	 *            : Required
	 * @param eventParty
	 *            : Required
	 * @param court
	 *            : Required
	 * @param conn
	 *            : Required
	 * @param description
	 *            : Required
	 * @throws Exception
	 */
	public void triggerEvent(CCMSCaseDocument document, CCMSCase ccmsCase,
			CCMSEventType eventType, CCMSParty eventParty, CCMSCourt court,
			Connection conn, String description) throws Exception
	{

		// Trigger an event - in this case we are hard coding the OID for a File
		// Case Event (OID = 9093328897184976)
		CCMSCaseEvent caseEvent = new CCMSCaseEvent(CCMSDomainUtilities
				.getInstance().genOID(), court.getCourtOID());
		caseEvent.setCaseOID(ccmsCase.getCaseOID());
		caseEvent.setEventType(eventType);
		caseEvent.setEnteringParty(eventParty);
		caseEvent.setEventDateTime(new Timestamp(System.currentTimeMillis()));
		caseEvent.setDocument(document);
		caseEvent.setDescription(description);
		PreparedStatement stmt = caseEvent.genInsertSQL(conn).get(0);
		stmt.execute();
		stmt.close();

		CCMSDomainUtilities.getInstance().triggerWorkflow(caseEvent, court,
				conn);
	}

	/**
	 * Retrieve the workflow with a trigger of this event, create subsequent
	 * tasks
	 * 
	 * @param caseEvent
	 *            : The event that was entered by the user. Will be used as
	 *            search criteria to identify any associated workflow
	 * @param court
	 *            : Court the user is signed in as
	 * @param conn
	 *            : Database Connection
	 * @throws Exception
	 */
	public void triggerWorkflow(CCMSCaseEvent caseEvent, CCMSCourt court,
			Connection conn) throws Exception
	{
		CCMSEventWorkflow qWorkflow = new CCMSEventWorkflow(0L, court);
		qWorkflow.setTriggeringEvent(caseEvent.getEventType());
		PreparedStatement stmt = qWorkflow.genSelectSQL(conn).get(0);
		ResultSet rs = stmt.executeQuery();
		while (rs.next())
		{
			CCMSEventWorkflow workflow = new CCMSEventWorkflow(rs, conn, court);
			for (CCMSWorkflowStep step : workflow.getWorkflowSteps())
			{
				triggerWorkflowStep(court, caseEvent, step, conn);
			}
		}
	}

	/**
	 * Creates a new task based on the instructions provided by the workflow for
	 * the triggered event
	 * 
	 * @param court
	 *            : court user is signed in as
	 * @param triggeringCaseEvent
	 *            : The event that triggers the workflow step that was entered
	 *            by the user
	 * @param step
	 *            : Details about the workflow tasks that are triggered by the
	 *            event
	 * @param conn
	 *            : Database connection
	 * @throws Exception
	 */
	private void triggerWorkflowStep(CCMSCourt court,
			CCMSCaseEvent triggeringCaseEvent, CCMSWorkflowStep step,
			Connection conn) throws Exception
	{
		Date currentDate = new Date(System.currentTimeMillis());
		CCMSCaseTask newTask = new CCMSCaseTask(CCMSDomainUtilities
				.getInstance().genOID(), step.getCourtOID());
		java.util.Calendar cal = GregorianCalendar.getInstance();
		cal.setTime(currentDate);

		newTask.setAssignedDate(currentDate);
		if (step.getAssignedParty() != null
				&& step.getAssignedParty().getPartyOID() != 0L)
		{
			newTask.setAssignedParty(step.getAssignedParty());
		}

		if (step.getAssignedPool() != null
				&& step.getAssignedPool().getPoolOID() != 0L)
		{
			newTask.setAssignedPool(step.getAssignedPool());
		}

		if (step.getDocumentTemplateOID() != 0L)
		{
			CCMSDocumentTemplate template = new CCMSDocumentTemplate(
					step.getDocumentTemplateOID(), court.getCourtOID());
			newTask.setDocTemplate(template);
		}

		newTask.setAssociatedCase(new CCMSCase(
				triggeringCaseEvent.getCaseOID(), court));
		newTask.setCourtOID(court.getCourtOID());

		// Set the due date
		cal.add(GregorianCalendar.DAY_OF_YEAR, step.getDelayDays()); // date
																		// manipulation
		newTask.setDueDate(cal.getTime());

		newTask.setTaskType(step.getTaskType());

		PreparedStatement insertStmt = newTask.genInsertSQL(conn).get(0);
		insertStmt.execute();
	}

	public void setCORSHeader(HttpServletRequest request,
			HttpServletResponse response, HttpServlet servlet, Logger logger)
			throws Exception
	{
		// List of allowed origins
		List<String> incomingURLs = Arrays.asList(servlet.getServletContext()
				.getInitParameter("incomingURLs").trim().split(","));

		logger.trace("Allowed URLS: "
				+ servlet.getServletContext().getInitParameter("incomingURLs")
						.trim());
		// Get client's origin
		String clientOrigin = request.getHeader("origin");

		// Get client's IP address
		String ipAddress = request.getHeader("x-forwarded-for");
		if (ipAddress == null)
		{
			ipAddress = request.getRemoteAddr();
		}

		logger.trace("Client Origin: " + clientOrigin);

		response.setContentType("text/html");
		response.setHeader("Cache-control", "no-cache, no-store");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "-1");

		int myIndex = incomingURLs.indexOf(clientOrigin);
		// if the client origin is found in our list then give access
		// if you don't want to check for origin and want to allow access
		// to all incoming request then change the line to this
		// response.setHeader("Access-Control-Allow-Origin", "*");
		// if (myIndex != -1)
		if (1 == 1)
		{
			response.setHeader("Access-Control-Allow-Origin", clientOrigin);
			response.setHeader("Access-Control-Allow-Methods", "POST");
			response.setHeader("Access-Control-Allow-Headers", "Content-Type");
			response.setHeader("Access-Control-Max-Age", "86400");
		}
	}

	public CCMSParty getParty(CCMSParty authParty, Connection conn)
			throws Exception
	{
		PreparedStatement stmt3 = authParty.genSelectSQL(conn).get(0);
		ResultSet rs3 = stmt3.executeQuery();
		while (rs3.next())
		{
			authParty = new CCMSParty(rs3, conn, false);
		}
		stmt3.close();
		rs3.close();
		return authParty;
	}

	public SendGrid getSendGridFactory()
	{
		String sendGridKey = properties.getProperty("SENDGRID_MAIL_KEY");
		SendGrid sendGrid = new SendGrid(sendGridKey);
		return sendGrid;
	}

	public static Method getNoArgumentMethod(CCMSDomain myObj, String methodName)
	{
		Method matchingMethod = null;
		try
		{
			matchingMethod = myObj.getClass().getMethod(methodName, null);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return matchingMethod;
	}
}
