package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

public class CCMSParty extends CCMSDomain
{
	public static int CHILD_ROLE_ID = 1;

	// 18 years * 365.25
	public static long DAYS_TO_MAJORITY = 6574;

	private long partyOID, courtOID;
	private String lastName, firstName, type, sex, userName, password, phone;
	private Date dob;
	private boolean isCourtUser = false;
	private boolean anonymizationFlag = false;
	private ArrayList<CCMSStaffRole> roles = new ArrayList<CCMSStaffRole>();
	private String token = null;
	private String notes = "";
	private static Logger logger = null;
	private ArrayList<CCMSPartyAddress> addresses = new ArrayList<CCMSPartyAddress>();
	private ArrayList<CCMSPartyEmail> emails = new ArrayList<CCMSPartyEmail>();
	private ArrayList<CCMSPartyPhone> phoneNumbers = new ArrayList<CCMSPartyPhone>();
	private ArrayList<CCMSPartyIdentifier> identifiers = new ArrayList<CCMSPartyIdentifier>();
	private boolean interpreterRequiredIndicator = false;
	private ArrayList<CCMSSpokenLanguage> spokenLanguages = new ArrayList<CCMSSpokenLanguage>();
	private String alternativeName = "";

	private List<CCMSCourt> authorizedCourts = new ArrayList<CCMSCourt>();

	// TODO Determine how to include the court user role information - separate
	// field or use the role object?

	// This field will be set and used when querying by the party name so we can
	// look by first or last name using a wildcard (%)
	String queryName = "";

	public CCMSParty()
	{
		CCMSParty.logger = LogManager.getLogger(CCMSParty.class.getName());
	}

	public CCMSParty(long oid, long courtOID)
	{
		CCMSParty.logger = LogManager.getLogger(CCMSParty.class.getName());
		this.partyOID = oid;
		this.courtOID = courtOID;
	}

	public CCMSParty(long oid)
	{
		CCMSParty.logger = LogManager.getLogger(CCMSParty.class.getName());
		this.partyOID = oid;

	}

	// Summary result flag should only be set to false when retrieving only
	// basic information about the user
	public CCMSParty(ResultSet rs, Connection conn, boolean summaryResultFlag)
			throws Exception
	{
		CCMSParty.logger = LogManager.getLogger(CCMSParty.class.getName());
		this.partyOID = rs.getLong("ID");
		this.firstName = rs.getString("FIRST_NAME");
		this.lastName = rs.getString("LAST_NAME");
		this.dob = rs.getDate("DATE_OF_BIRTH");
		this.sex = rs.getString("SEX");
		// this.password = rs.getString("PASSWD");
		this.userName = rs.getString("USER_NAME");
		this.notes = rs.getString("NOTES");
		this.alternativeName = rs.getString("ALTERNATIVE_NAME");

		int courtUser = rs.getInt("COURT_USER_IND");
		this.isCourtUser = CCMSDomainUtilities.intToBool(courtUser);

		int interpreterReqInt = rs.getInt("INTERPRETER_REQ_IND");
		this.interpreterRequiredIndicator = CCMSDomainUtilities
				.intToBool(interpreterReqInt);

		// Only attempt to retrieve staff roles if this is a court user
		if (this.isCourtUser)
		{
			// Retrieve the roles that this user has been assigned for the court
			PreparedStatement roleStmt = conn
					.prepareStatement("SELECT * "
							+ "  FROM NJCCCMS.PARTY_STAFF_ROLE_REL, NJCCCMS.STAFF_ROLE "
							+ " WHERE PARTY_STAFF_ROLE_REL.ROLE_ID = STAFF_ROLE.ID "
							+ "   AND PARTY_STAFF_ROLE_REL.PARTY_ID = ? ");
			roleStmt.setLong(1, this.partyOID);
			ResultSet rs1 = roleStmt.executeQuery();
			while (rs1.next())
			{
				this.roles.add(new CCMSStaffRole(rs1, conn));
			}
			rs1.close();
			roleStmt.close();
		}

		if (!summaryResultFlag)
		{

			// Retrieve the Addresses associated with this party
			PreparedStatement addStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.PARTY_ADDRESS WHERE PARTY_ID = ? ");
			addStmt.setLong(1, this.partyOID);
			ResultSet rs2 = addStmt.executeQuery();
			while (rs2.next())
			{
				this.addresses.add(new CCMSPartyAddress(rs2, conn));
			}
			rs2.close();
			addStmt.close();

			// Retrieve the Emails associated with this party
			PreparedStatement emailStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.PARTY_EMAIL WHERE PARTY_ID = ? ");
			emailStmt.setLong(1, this.partyOID);
			ResultSet rs3 = emailStmt.executeQuery();
			while (rs3.next())
			{
				this.emails.add(new CCMSPartyEmail(rs3, conn));
			}
			rs3.close();
			emailStmt.close();

			// Retrieve the phone numbers associated with this party
			PreparedStatement phoneStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.PARTY_PHONE WHERE PARTY_ID = ? ");
			phoneStmt.setLong(1, this.partyOID);
			ResultSet rs4 = phoneStmt.executeQuery();
			while (rs4.next())
			{
				this.phoneNumbers.add(new CCMSPartyPhone(rs4, conn));
			}
			rs4.close();
			phoneStmt.close();

			// Retrieve the spoken languages associated with this party
			PreparedStatement langStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.PARTY_LANGUAGE WHERE PARTY_ID = ? ");
			langStmt.setLong(1, this.partyOID);
			ResultSet rs5 = langStmt.executeQuery();
			while (rs5.next())
			{
				this.spokenLanguages.add(new CCMSSpokenLanguage(rs5, conn));
			}
			rs5.close();
			langStmt.close();

			// Retrieve party identifiers
			PreparedStatement identifiersStmt = conn
					.prepareStatement("SELECT * FROM NJCCCMS.PARTY_IDENTIFIER WHERE PARTY_ID = ? ");
			identifiersStmt.setLong(1, this.partyOID);
			ResultSet rs6 = identifiersStmt.executeQuery();
			while (rs6.next())
			{
				this.identifiers.add(new CCMSPartyIdentifier(rs6, conn));
			}
			rs6.close();
			identifiersStmt.close();
		}
	}

	public static void main(String[] args)
	{
		CCMSParty party = new CCMSParty(0, 5);
		party.setPartyOID(0);

		try
		{
			Connection conn = CCMSDomainUtilities.getInstance().connect();
			PreparedStatement pStmt = party.genSelectSQL(conn).get(0);
			ResultSet rs = pStmt.executeQuery();
			while (rs.next())
			{
				CCMSParty dbParty = new CCMSParty(rs, conn, false);

				// Return all matching values to the AJAX calling screen
				System.out.println(dbParty);
			}
		}
		catch (SQLException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}
		catch (Exception e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e);
		}
	}

	/**
	 * Populates the authorized courts for this user
	 * 
	 * @param conn
	 * @return
	 * @throws Exception
	 */
	public List<CCMSCourt> getAuthorizedCourts(Connection conn)
			throws Exception
	{
		// Retrieve the authorized courts for this user
		PreparedStatement pStmt = conn
				.prepareStatement("SELECT * FROM NJCCCMS.PARTY, NJCCCMS.STAFF_COURT_REL, NJCCCMS.COURTS "
						+ " WHERE ID > 0 AND PARTY.ID = STAFF_COURT_REL.PARTY_ID "
						+ "   AND STAFF_COURT_REL.COURT_ID = COURTS.COURT_ID "
						+ "   AND PARTY.ID = ? " + " ORDER BY COURTS.COURT_ID ");
		pStmt.setLong(1, this.partyOID);
		ResultSet rs1 = pStmt.executeQuery();
		while (rs1.next())
		{
			CCMSCourt authCourt = new CCMSCourt(rs1, conn);

			// Iterate through the roles and add as an attribute of the
			// authCourt if
			// the court OID matches
			for (CCMSStaffRole role : this.roles)
			{
				if (role.getCourtOID() == authCourt.getCourtOID())
				{
					authCourt.getRoles().add(role);
				}
			}

			this.authorizedCourts.add(authCourt);
		}
		pStmt.close();
		rs1.close();

		return this.authorizedCourts;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("INSERT INTO NJCCCMS.PARTY "
						+ " ( ID, FIRST_NAME, LAST_NAME, DATE_OF_BIRTH, SEX, COURT_USER_IND, "
						+ "   PASSWD, USER_NAME, INTERPRETER_REQ_IND, NOTES, ALTERNATIVE_NAME ) "
						+ " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) ");
		stmt.setLong(1, this.partyOID);
		stmt.setString(2, this.firstName);
		stmt.setString(3, this.lastName);
		if (this.dob != null)
		{
			stmt.setDate(4, new java.sql.Date(this.dob.getTime()));
		}
		else
		{
			stmt.setNull(4, java.sql.Types.DATE);
		}
		stmt.setString(5, this.sex);
		stmt.setInt(6, CCMSDomainUtilities.boolToInt(this.isCourtUser));
		if (password != null)
		{
			stmt.setString(
					7,
					CCMSDomainUtilities.getInstance().generateHash(
							this.password));
		}
		else
		{
			stmt.setNull(7, java.sql.Types.VARCHAR);
		}
		stmt.setString(8, this.userName);

		stmt.setInt(9, CCMSDomainUtilities
				.boolToInt(this.interpreterRequiredIndicator));

		stmt.setString(10, this.notes);

		stmt.setString(11, this.alternativeName);

		stmts.add(stmt);

		// Add the addresses
		for (CCMSPartyAddress address : this.addresses)
		{
			stmts.addAll(address.genInsertSQL(conn));
		}

		// Add the emails
		for (CCMSPartyEmail email : this.emails)
		{
			stmts.addAll(email.genInsertSQL(conn));
		}

		// Add the phone numbers
		for (CCMSPartyPhone phone : this.phoneNumbers)
		{
			stmts.addAll(phone.genInsertSQL(conn));
		}

		// Add the languages
		for (CCMSSpokenLanguage language : this.spokenLanguages)
		{
			stmts.addAll(language.genInsertSQL(conn));
		}

		// Add the identifiers
		for (CCMSPartyIdentifier identifier : this.identifiers)
		{
			stmts.addAll(identifier.genInsertSQL(conn));
		}

		// Add the staff roles
		for (CCMSStaffRole role : this.roles)
		{
			PreparedStatement roleStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.PARTY_STAFF_ROLE_REL ( PARTY_ID, COURT_ID, ROLE_ID ) "
							+ " VALUES ( ?, ?, ? ) ");
			roleStmt.setLong(1, this.partyOID);
			roleStmt.setLong(2, role.getCourtOID());
			roleStmt.setLong(3, role.getStaffRoleOID());
			stmts.add(roleStmt);
		}

		// Authorized Courts - need to consider whether this is needed since the
		// authorized courts are effectively identified as
		// part of the staff role relationship table
		for (CCMSCourt authCourt : this.authorizedCourts)
		{
			PreparedStatement authCourtStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.STAFF_COURT_REL "
							+ " ( PARTY_ID, COURT_ID ) VALUES ( ?, ? )");
			authCourtStmt.setLong(1, this.partyOID);
			authCourtStmt.setLong(2, authCourt.getCourtOID());
			stmts.add(authCourtStmt);
		}

		return stmts;
	}

	public List<PreparedStatement> genDeleteSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		PreparedStatement stmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY WHERE  ID = ?");
		stmt.setLong(1, this.partyOID);
		stmts.add(stmt);

		return stmts;
	}

	public PreparedStatement genPasswordReset(Connection conn,
			String unhashedPassword) throws Exception
	{
		String hashedPassword = CCMSDomainUtilities.getInstance().generateHash(
				unhashedPassword);
		PreparedStatement stmt = conn.prepareStatement("UPDATE NJCCCMS.PARTY "
				+ " SET PASSWD = ? " + " WHERE USER_NAME = ?");

		stmt.setString(1, hashedPassword);
		stmt.setString(2, this.userName);

		return stmt;
	}

	/**
	 * TO DO - need to clear out addresses, phone and email before inserting
	 * back in
	 **/
	public List<PreparedStatement> genUpdateSQL(Connection conn)
			throws Exception
	{
		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();

		// Remove all associated addresses
		PreparedStatement delAddress = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_ADDRESS "
						+ " WHERE PARTY_ID = ?  ");
		delAddress.setLong(1, this.partyOID);
		stmts.add(delAddress);

		// Remove associated emails
		PreparedStatement emailDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_EMAIL "
						+ " WHERE PARTY_ID = ?");
		emailDelStmt.setLong(1, this.partyOID);
		stmts.add(emailDelStmt);

		// Remove associated phone numbers
		PreparedStatement phoneDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_PHONE "
						+ " WHERE PARTY_ID = ?");
		phoneDelStmt.setLong(1, this.partyOID);
		stmts.add(phoneDelStmt);

		// Remove associated languages
		PreparedStatement langDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_LANGUAGE "
						+ " WHERE PARTY_ID = ?");
		langDelStmt.setLong(1, this.partyOID);
		stmts.add(langDelStmt);

		// Remove associated identifiers
		PreparedStatement identifierDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_IDENTIFIER "
						+ " WHERE PARTY_ID = ?");
		identifierDelStmt.setLong(1, this.partyOID);
		stmts.add(identifierDelStmt);

		// Remove associated roles
		PreparedStatement rolesDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.PARTY_STAFF_ROLE_REL "
						+ " WHERE PARTY_ID = ?");
		rolesDelStmt.setLong(1, this.partyOID);
		stmts.add(rolesDelStmt);

		// Remove authorized courts
		PreparedStatement authCourtsDelStmt = conn
				.prepareStatement("DELETE FROM NJCCCMS.STAFF_COURT_REL "
						+ " WHERE PARTY_ID = ?");
		authCourtsDelStmt.setLong(1, this.partyOID);
		stmts.add(authCourtsDelStmt);

		// Update the Party
		if (this.password != null)
		{
			PreparedStatement stmt = conn
					.prepareStatement("UPDATE NJCCCMS.PARTY "
							+ " SET FIRST_NAME = ?, LAST_NAME = ?, DATE_OF_BIRTH = ?, SEX = ?, "
							+ " INTERPRETER_REQ_IND = ?, NOTES = ?, ALTERNATIVE_NAME = ?, PASSWD = ?, USER_NAME = ? "
							+ " WHERE ID = ?");

			stmt.setString(1, this.firstName);
			stmt.setString(2, this.lastName);
			if (this.dob != null)
			{
				stmt.setDate(3, new java.sql.Date(this.dob.getTime()));
			}
			else
			{
				stmt.setNull(3, java.sql.Types.DATE);
			}
			stmt.setString(4, this.sex);
			stmt.setInt(5, CCMSDomainUtilities
					.boolToInt(this.interpreterRequiredIndicator));
			stmt.setString(6, this.notes);
			stmt.setString(7, this.alternativeName);
			stmt.setString(
					8,
					CCMSDomainUtilities.getInstance().generateHash(
							this.password));
			stmt.setString(9, this.userName);
			stmt.setLong(10, this.partyOID);
			stmts.add(stmt);
		}
		else
		{
			// Password is null if the party non-password attributes (i.e.,
			// role) are being updated
			PreparedStatement stmt = conn
					.prepareStatement("UPDATE NJCCCMS.PARTY "
							+ " SET FIRST_NAME = ?, LAST_NAME = ?, DATE_OF_BIRTH = ?, SEX = ?, "
							+ " INTERPRETER_REQ_IND = ?, NOTES = ?, ALTERNATIVE_NAME = ?, USER_NAME = ? "
							+ " WHERE ID = ?");

			stmt.setString(1, this.firstName);
			stmt.setString(2, this.lastName);
			if (this.dob != null)
			{
				stmt.setDate(3, new java.sql.Date(this.dob.getTime()));
			}
			else
			{
				stmt.setNull(3, java.sql.Types.DATE);
			}
			stmt.setString(4, this.sex);
			stmt.setInt(5, CCMSDomainUtilities
					.boolToInt(this.interpreterRequiredIndicator));
			stmt.setString(6, this.notes);
			stmt.setString(7, this.alternativeName);
			stmt.setString(8, this.userName);
			stmt.setLong(9, this.partyOID);
			stmts.add(stmt);
		}

		// Re-add the addresses
		for (CCMSPartyAddress address : this.addresses)
		{
			stmts.addAll(address.genInsertSQL(conn));
		}

		// Re-add the emails
		for (CCMSPartyEmail email : this.emails)
		{
			stmts.addAll(email.genInsertSQL(conn));
		}

		// Re-add the phone
		for (CCMSPartyPhone phone : this.phoneNumbers)
		{
			stmts.addAll(phone.genInsertSQL(conn));
		}

		// Add the languages
		for (CCMSSpokenLanguage language : this.spokenLanguages)
		{
			stmts.addAll(language.genInsertSQL(conn));
		}

		// Add the identifiers
		for (CCMSPartyIdentifier identifier : this.identifiers)
		{
			stmts.addAll(identifier.genInsertSQL(conn));
		}

		// Add the staff roles
		for (CCMSStaffRole role : this.roles)
		{
			PreparedStatement roleStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.PARTY_STAFF_ROLE_REL ( PARTY_ID, COURT_ID, ROLE_ID ) "
							+ " VALUES ( ?, ?, ? ) ");
			roleStmt.setLong(1, this.partyOID);
			roleStmt.setLong(2, role.getCourtOID());
			roleStmt.setLong(3, role.getStaffRoleOID());
			stmts.add(roleStmt);
		}

		// Add authorized courts
		for (CCMSCourt authCourt : this.authorizedCourts)
		{
			PreparedStatement authCourtStmt = conn
					.prepareStatement("INSERT INTO NJCCCMS.STAFF_COURT_REL "
							+ " ( PARTY_ID, COURT_ID ) VALUES ( ?, ? )");
			authCourtStmt.setLong(1, this.partyOID);
			authCourtStmt.setLong(2, authCourt.getCourtOID());
			stmts.add(authCourtStmt);
		}

		return stmts;
	}

	public List<PreparedStatement> genSelectSQL(Connection conn)
			throws Exception
	{
		List<CCMSPreparedStatementParameter> parms = new ArrayList<CCMSPreparedStatementParameter>();

		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		String sqlString = " SELECT * FROM NJCCCMS.PARTY WHERE ID > 0 ";

		if (this.queryName != null && !queryName.isEmpty())
		{
			sqlString += " AND ( FIRST_NAME like ? OR LAST_NAME like ? ) ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, "%" + queryName
							+ "%"));
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, "%" + queryName
							+ "%"));
		}

		if (this.firstName != null && !firstName.isEmpty())
		{
			sqlString += " AND FIRST_NAME = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, this.firstName));
		}

		if (this.lastName != null && !lastName.isEmpty())
		{
			sqlString += " AND LAST_NAME = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, this.lastName));
		}

		if (this.dob != null)
		{
			sqlString += " AND DATE_OF_BIRTH = ?";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.DATE_TYPE, this.dob));
		}

		if (this.partyOID != 0L)
		{
			sqlString += " AND ID = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.LONG_TYPE, this.partyOID));
		}

		if (this.password != null && !password.isEmpty()
				&& this.userName != null && !userName.isEmpty())
		{
			// Encrypt the passed in password
			String hashPasswd = CCMSDomainUtilities.getInstance().generateHash(
					this.password);
			sqlString += " AND USER_NAME = ? AND PASSWD = ? ";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, this.userName));
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, hashPasswd));
		}

		// Used when querying by the login id from the token
		if (this.userName != null && !this.userName.isEmpty())
		{
			sqlString += " AND USER_NAME = ?";
			parms.add(new CCMSPreparedStatementParameter(
					CCMSPreparedStatementParameter.STRING_TYPE, this.userName));
		}

		if (this.isCourtUser)
		{
			sqlString += " AND COURT_USER_IND = 1 ";
		}

		sqlString += " ORDER BY LAST_NAME";
		PreparedStatement pStmt = conn.prepareStatement(sqlString);

		for (int i = 0; i < parms.size(); i++)
		{
			CCMSPreparedStatementParameter parm = parms.get(i);
			parm.replaceParameterValues(pStmt, i + 1);
		}

		stmts.add(pStmt);

		return stmts;
	}

	@Override
	public boolean hasChanged(CCMSDomain sub)
	{
		// TODO Auto-generated method stub
		return super.hasChanged(sub);
	}

	public long getOid()
	{
		return partyOID;
	}

	public void setOid(long oid)
	{
		this.partyOID = oid;
	}

	public String getLastName()
	{
		return lastName;
	}

	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getFirstName()
	{
		return firstName;
	}

	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public Date getDob()
	{
		return dob;
	}

	public void setDob(Date dob)
	{
		this.dob = dob;
	}

	public String getSex()
	{
		return sex;
	}

	public void setSex(String sex)
	{
		this.sex = sex;
	}

	public boolean isCourtUser()
	{
		return isCourtUser;
	}

	public void setCourtUser(boolean isCourtUser)
	{
		this.isCourtUser = isCourtUser;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getFullName()
	{
		return this.firstName + " " + this.lastName;
	}

	public boolean isAnonymizationFlag()
	{
		return anonymizationFlag;
	}

	public void setAnonymizationFlag(boolean anonymizationFlag)
	{
		this.anonymizationFlag = anonymizationFlag;
	}

	public String getQueryName()
	{
		return queryName;
	}

	public void setQueryName(String queryName)
	{
		this.queryName = queryName;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userID)
	{
		this.userName = userID;
	}

	public String getPassword()
	{
		return password;
	}

	public void setPassword(String password)
	{
		this.password = password;
	}

	public List<CCMSCourt> getAuthorizedCourts()
	{
		return authorizedCourts;
	}

	public String getToken()
	{
		return token;
	}

	public void setToken(String token)
	{
		this.token = token;
	}

	public ArrayList<CCMSPartyEmail> getEmails()
	{
		return emails;
	}

	public void setEmails(ArrayList<CCMSPartyEmail> emails)
	{
		this.emails = emails;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public ArrayList<CCMSPartyAddress> getAddresses()
	{
		return addresses;
	}

	public void setAddresses(ArrayList<CCMSPartyAddress> addresses)
	{
		this.addresses = addresses;
	}

	public ArrayList<CCMSPartyPhone> getPhoneNumbers()
	{
		return phoneNumbers;
	}

	public void setPhoneNumbers(ArrayList<CCMSPartyPhone> phoneNumbers)
	{
		this.phoneNumbers = phoneNumbers;
	}

	public long getCourtOID()
	{
		return courtOID;
	}

	public void setCourtOID(long courtOID)
	{
		this.courtOID = courtOID;
	}

	public ArrayList<CCMSPartyIdentifier> getIdentifiers()
	{
		return identifiers;
	}

	public void setIdentifiers(ArrayList<CCMSPartyIdentifier> identifiers)
	{
		this.identifiers = identifiers;
	}

	public boolean isInterpreterRequiredIndicator()
	{
		return interpreterRequiredIndicator;
	}

	public void setInterpreterRequiredIndicator(
			boolean interpreterRequiredIndicator)
	{
		this.interpreterRequiredIndicator = interpreterRequiredIndicator;
	}

	public ArrayList<CCMSSpokenLanguage> getSpokenLanguages()
	{
		return spokenLanguages;
	}

	public void setSpokenLanguages(ArrayList<CCMSSpokenLanguage> spokenLanguages)
	{
		this.spokenLanguages = spokenLanguages;
	}

	public ArrayList<CCMSStaffRole> getRoles()
	{
		return roles;
	}

	public String getNotes()
	{
		return notes;
	}

	public void setNotes(String notes)
	{
		this.notes = notes;
	}

	public String getAlternativeName()
	{
		return alternativeName;
	}

	public void setAlternativeName(String alternativeName)
	{
		this.alternativeName = alternativeName;
	}

	/**
	 * Returns a boolean indicating whether the person is authorized to make the
	 * indicated change
	 * 
	 * @param permissionRequired
	 *            : Based on static vars declared in CCMSPermission
	 * @return true: If the user is authorized for the permissionRequired false:
	 *         If the user is not authorized for the permissionRequired
	 */
	public boolean isAuthorized(int permissionRequired, CCMSCourt currentCourt)
	{
		// Need to get the applicable court
		for (CCMSCourt userCourt : this.authorizedCourts)
		{
			if (userCourt.equals(currentCourt))
			{
				for (CCMSStaffRole role : userCourt.getRoles())
				{
					for (CCMSPermission perm : role.getPermissions())
					{
						if (perm != null
								&& perm.getPermissionID() == permissionRequired) { return true; }
					}
				}
			}
		}

		return false;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj instanceof CCMSParty)
		{
			CCMSParty qObj = (CCMSParty) obj;
			if (qObj.getPartyOID() == this.getPartyOID())
			{
				return true;
			}
			else if (qObj.userName.equals(this.userName)) { return true; }
		}
		return false;
	}

}
