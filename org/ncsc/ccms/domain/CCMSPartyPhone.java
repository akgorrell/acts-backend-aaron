package org.ncsc.ccms.domain;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CCMSPartyPhone extends CCMSDomain
{

	private long partyPhoneOID;
	private long partyOID;
	private String phoneType;
	private String phoneNumber;
	private Date startDate;
	private Date endDate;
	
	/** 
	 * True indicates that the user has authorized transmission of information via text message
	 */
	private boolean textMessagesIndicator = false;
	
	public CCMSPartyPhone(long partyPhoneOID)
	{
		this.partyPhoneOID = partyPhoneOID;
	}

	public CCMSPartyPhone( ResultSet rs, Connection conn) throws Exception
	{
		this.partyPhoneOID = rs.getLong("ID");
		this.partyOID = rs.getLong("PARTY_ID");
		this.phoneType = rs.getString("PHONE_TYPE");
		this.phoneNumber = rs.getString("PHONE_NUMBER");
		
		int textMessageInt = rs.getInt("TEXT_MESSAGE_IND");
		this.textMessagesIndicator = CCMSDomainUtilities.intToBool(textMessageInt);
		
		this.startDate = rs.getDate("START_DATE");
		this.endDate = rs.getDate("END_DATE");
	}

	public long getPartyPhoneOID()
	{
		return partyPhoneOID;
	}

	public void setPartyPhoneOID(long partyPhoneOID)
	{
		this.partyPhoneOID = partyPhoneOID;
	}

	public long getPartyOID()
	{
		return partyOID;
	}

	public void setPartyOID(long partyOID)
	{
		this.partyOID = partyOID;
	}

	public String getPhoneType()
	{
		return phoneType;
	}

	public void setPhoneType(String phoneType)
	{
		this.phoneType = phoneType;
	}

	public String getPhoneNumber()
	{
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber)
	{
		this.phoneNumber = phoneNumber;
	}

	public boolean isTextMessagesIndicator()
	{
		return textMessagesIndicator;
	}

	public void setTextMessagesIndicator(boolean textMessagesIndicator)
	{
		this.textMessagesIndicator = textMessagesIndicator;
	}
	

	public Date getStartDate()
	{
		return startDate;
	}

	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}

	public Date getEndDate()
	{
		return endDate;
	}

	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}

	public List<PreparedStatement> genInsertSQL(Connection conn)
			throws Exception
	{

		List<PreparedStatement> stmts = new ArrayList<PreparedStatement>();
		
		PreparedStatement stmt = conn.prepareStatement("INSERT INTO NJCCCMS.PARTY_PHONE " +
				" ( ID, PARTY_ID, PHONE_TYPE, PHONE_NUMBER, TEXT_MESSAGE_IND, START_DATE, END_DATE ) " + 
				" VALUES ( ?, ?, ?, ?, ?, ?, ? ) ");
		stmt.setLong(1, this.partyPhoneOID);
		stmt.setLong(2, this.partyOID);
		stmt.setString(3, this.phoneType);
		stmt.setString(4, this.phoneNumber);
		stmt.setInt(5, CCMSDomainUtilities.boolToInt(this.textMessagesIndicator));
		
		if (this.startDate != null)
		{
			stmt.setDate(6, new java.sql.Date(startDate.getTime()));
		}
		else
		{
			stmt.setNull(6, java.sql.Types.DATE);
		}

		
		if (this.endDate != null)
		{
			stmt.setDate(7, new java.sql.Date(endDate.getTime()));
		}
		else
		{
			stmt.setNull(7, java.sql.Types.DATE);
		}
		
		stmts.add(stmt);
		
		return stmts;
		
	}
}
